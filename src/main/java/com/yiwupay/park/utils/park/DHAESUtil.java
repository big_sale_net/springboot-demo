
package com.yiwupay.park.utils.park;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

public class DHAESUtil {
	
	private static final String ENCODEING = "UTF-8";
	
	private static final String ALGORITHM = "AES";//加密算法

	private static final String CIPHER_ALGORITHM = "AES/ECB/PKCS5Padding";//算法/工作模式/填充方式 


    
  /** 
   * 将base 64 code AES解密 
   * @param encryptStr 待解密的base 64 code 
   * @param decryptKey 解密密钥 
   * @return 解密后的string 
   * @throws Exception 
   */  
  public static String aesDecrypt(String encryptStr, String decryptKey) throws Exception {  
      return DHStringUtils.isEmpty(encryptStr) ? null : aesDecryptByBytes(base64Decode(encryptStr), decryptKey);  
  }
  
  /** 
   * 将base 64 code AES解密 
   * @param encryptStr 待解密的base 64 code 
   * @param decryptKey 解密密钥 
   * @return 解密后的string 
   * @throws Exception 
   */  
  public static String aesDecryptHexString(String encryptStr, String decryptKey) throws Exception {
	  return DHStringUtils.isEmpty(encryptStr) ? null : aesDecryptByBytes(hexStringToByteArray(encryptStr), decryptKey);  
  }
  
    /** 
     * AES解密 
     * @param encryptBytes 待解密的byte[] 
     * @param decryptKey 解密密钥 
     * @return 解密后的String 
     * @throws Exception 
     */  
    public static String aesDecryptByBytes(byte[] encryptBytes, String decryptKey) throws Exception {  
    	SecretKeySpec skeySpec = getSecretKeySpec(decryptKey);
		
		Cipher cipher = Cipher.getInstance(CIPHER_ALGORITHM);// 创建密码器
        
        cipher.init(Cipher.DECRYPT_MODE, skeySpec);  
        byte[] decryptBytes = cipher.doFinal(encryptBytes);  
          
        return new String(decryptBytes, ENCODEING);  
    }  
    
    /** 
     * base 64 encode 
     * @param bytes 待编码的byte[] 
     * @return 编码后的base 64 code 
     */  
    public static String base64Encode(byte[] bytes){  
        return new String(Base64.encodeBase64(bytes)); 
    }  
      
    /** 
     * base 64 decode 
     * @param base64Code 待解码的base 64 code 
     * @return 解码后的byte[] 
     * @throws Exception 
     */  
    public static byte[] base64Decode(String base64Code) throws Exception{  
        return DHStringUtils.isEmpty(base64Code) ? null : Base64.decodeBase64(base64Code.getBytes());  
    }
    
	/**
	 * 
	 * @param secureKey
	 * @return
	 * @throws Exception
	 */
	private static SecretKeySpec getSecretKeySpec(String secureKey) throws Exception{
		if(secureKey == null || secureKey.trim().equals("") || secureKey.length() != 16){
			throw new Exception("密钥不能为空或密钥长度不对");
		}
		byte[] raw = secureKey.getBytes(ENCODEING);
		SecretKeySpec skeySpec = new SecretKeySpec(raw, ALGORITHM);
		return skeySpec;
	}
	
	
    
    /**
     * 十六进制字符串转换成字节数组
     * @param array
     * @return
     */
    private static byte[] hexStringToByteArray(String hexStr) {
    	if(DHStringUtils.isEmpty(hexStr)){
    		return null;
    	}
    	int length = hexStr.length()/2;
    	byte[] result = new byte[length];
    	for(int i=0; i<length; i++){
    		int high = Integer.parseInt(hexStr.substring(i*2, i*2+1), 16);
    		int low = Integer.parseInt(hexStr.substring(i*2+1, i*2+2), 16);
    		result[i] = (byte)(high*16 + low);
    	}
    	return result;
    }
}
