package com.yiwupay.park.utils.park;

import java.util.HashMap;
import java.util.Map;

public class JHTParkCodeMap {
    
	public static final Map<String, String> map = new HashMap<>();
	static{
		map.put("6楼76号入口", "0000007796");
		map.put("6楼87号入口", "0000007796");
		map.put("6楼87号出口", "0000007796");
		map.put("6楼75号出口", "0000007796");
		map.put("6楼88号出口", "0000007796");
		map.put("北门入口", "0000007797");
		map.put("北门出口", "0000007797");
		map.put("南门入口", "0000007798");
		map.put("南门出口", "0000007798");
		map.put("东侧五楼入口", "0000007799");
		map.put("东侧五楼出口", "0000007799");
	}
	
	/**
	 * 对应进出口车道
	 */
	public final static HashMap<String, String> JHT_OUTROADWAY = new HashMap<String, String>(){
		private static final long serialVersionUID = 1L;
		{
			put("6楼76号入口","1");
			put("6楼87号入口","4");
			put("北门入口","7");
			put("南门入口","10");
			put("6楼87号出口","5");
			put("6楼75号出口","3");
			put("6楼88号出口","6");
			put("北门出口","8");
			put("南门出口","9");
			put("东侧五楼入口","暂不知");
			put("东侧五楼出口","暂不知");
		}
	};
}
