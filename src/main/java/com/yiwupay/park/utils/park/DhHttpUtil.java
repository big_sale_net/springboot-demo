package com.yiwupay.park.utils.park;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;

import com.yiwupay.park.action.BaseAction;

import net.sf.json.JSONObject;

/**
 * 大华HTTP调用工具类   （主要提供GET请求与POST请求两种请求方法）
 * Title: DhHttpUtil
 * Description: 
 * Company:  
 * @author cailuxi
 * @date 2017年12月26日
 */
public class DhHttpUtil {
	
	static RestTemplate Template = new RestTemplate();
	
	static Logger logger=LoggerFactory.getLogger(BaseAction.class);

	/**
	 * get请求
	 * Description: 
	 * @param preUrl
	 * @param interfaceName
	 * @param paramMap
	 * @return
	 * @author cailuxi
	 * @date 2017年12月26日
	 */
	public static String doGet(String preUrl, String interfaceName, Map<String, Object> paramMap, String assessToken) {
		StringBuffer sb = new StringBuffer();
		
		try {
			// 遍历map 设置字符集为utf-8并转为string
			if (paramMap != null) {
				for (String key : paramMap.keySet()) {
					sb.append(key + "=" + URLEncoder.encode(paramMap.get(key).toString(), "utf-8") +"&");
				}
			}
		} catch (UnsupportedEncodingException e1) {
			logger.error("doGet error,请求参数"+paramMap);
			logger.error("doGet error "+e1);
			e1.printStackTrace();
		}
		
		String urlStr = preUrl + interfaceName + "?" + sb.toString();
		StringBuffer result = new StringBuffer();
		InputStream is = null;
		InputStreamReader inputStreamReader = null;
		BufferedReader reader = null;
		URL url = null;
		try {
			// 打开和URL之间的连接
			url = new URL(urlStr);
			HttpURLConnection conn = (HttpURLConnection)url.openConnection();
			conn.setRequestProperty("Accept-Charset", "utf-8");
			conn.setRequestProperty("accessToken", assessToken);
			// 定义 BufferedReader输入流来读取URL的响应
			is = conn.getInputStream();
			inputStreamReader = new InputStreamReader(is);
			reader = new BufferedReader(inputStreamReader);
			String line;
			while ((line = reader.readLine()) != null) {
				result.append(line);
			}
		} catch (Exception e) {
			logger.error("doGet error,请求参数"+paramMap);
			logger.error("doGet error "+e);
			e.printStackTrace();
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (inputStreamReader != null) {
				try {
					inputStreamReader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return result.toString();
	}
	
	
	public static String doPost(String preUrl, String interfaceName, JSONObject paramMap, String assessToken) {
		String urlStr = preUrl + interfaceName + "?" + paramMap.toString();
//		System.out.println(urlStr);
		StringBuffer result = new StringBuffer();
		InputStream is = null;
		InputStreamReader inputStreamReader = null;
		OutputStream os = null;
		OutputStreamWriter outputStreamWriter = null;
		BufferedReader reader = null;
		URL url = null;
		try {
			// 打开和URL之间的连接
			url = new URL(urlStr);
			HttpURLConnection conn = (HttpURLConnection)url.openConnection();
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("Accept-Charset", "UTF-8");
			conn.setRequestMethod("POST");
			conn.setRequestProperty("accessToken", assessToken);
			conn.setDoOutput(true);
			conn.setDoInput(true);
			conn.setUseCaches(false);
			
			// 写入url操作
			os = conn.getOutputStream();
			outputStreamWriter = new OutputStreamWriter(os, "UTF-8");
			outputStreamWriter.write(paramMap.toString());
			outputStreamWriter.flush();
			
			// 定义 BufferedReader输入流来读取URL的响应
			is = conn.getInputStream();
			inputStreamReader = new InputStreamReader(is, "UTF-8");
			reader = new BufferedReader(inputStreamReader);
			String line;
			while ((line = reader.readLine()) != null) {
				result.append(line);
			}
		} catch (Exception e) {
			logger.error("doPost error,请求参数"+paramMap);
			logger.error("doPost error "+e);
			e.printStackTrace();
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (inputStreamReader != null) {
				try {
					inputStreamReader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (outputStreamWriter != null) {
				try {
					outputStreamWriter.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (os != null) {
				try {
					os.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return result.toString();
	}
	
	 public static void main(String[] args) {
		 JSONObject paramMap = new JSONObject();
		 paramMap.put("carNum", "皖SE881Z");
		 paramMap.put("queryType", 1);
		 
		String result = doPost("http://10.100.10.202/ipms/", "integration/kingdo/payment/info", paramMap, "1gyYDH3IU9TVhE34fDxPPRSVUnNLDqH7bp+Yyo2yWzM0=");
		
		System.out.println("-----------"+result);
	}
}
