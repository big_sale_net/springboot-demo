package com.yiwupay.park.utils.park;

import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

import net.sf.json.JSONObject;

/**
 * 红门平台调用接口工具类
 * @author zls
 *
 */
public class HMHttpUtil {
	static Logger logger = LoggerFactory.getLogger(JHTHttpUtil.class);
	
	/**
	 * 发送请求
	 * @param template
	 * @param host
	 * @param json
	 * @return
	 */
	public static String doPost(String host, String appSecret, JSONObject json){
		String sign = DigestUtils.md5Hex(json.get("appId").toString() + json.get("requestId").toString() + json.get("type").toString() + appSecret +json.get("timestamp").toString()).toLowerCase();
		json.put("sign", sign);
		HttpHeaders headers = new HttpHeaders();
		MediaType mediaType = MediaType.parseMediaType("application/json; charset=UTF-8");
		headers.setContentType(mediaType);
		HttpEntity<JSONObject> formEntity = new HttpEntity<JSONObject>(json, headers);
		String result = null;
		try {
			RestTemplate template = new RestTemplate();
			result = template.postForObject(host, formEntity, String.class);
			System.out.println("红门物业停车系统请求成功，返回结果：{}"+result.toString());
			return result;
		} catch (Exception e) {
			logger.error("红门物业停车系统无响应 body={}", json.toString());
			logger.error("打印error", e);
		}
		return null;
	}
}
