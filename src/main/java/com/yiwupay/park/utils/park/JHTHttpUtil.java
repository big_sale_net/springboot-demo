package com.yiwupay.park.utils.park;

import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Date;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 捷慧通云平台Http调用接口
 * @author zls
 *
 */
@SuppressWarnings("deprecation")
public class JHTHttpUtil {
	static Logger logger = LoggerFactory.getLogger(JHTHttpUtil.class);
	/**
	 * 登录接口Http
	 * @param url
	 * @param cid
	 * @param usr
	 * @param psw
	 * @return
	 */
	public static String doGet(String url, String cid, String  usr, String psw) {
		try {
			// 构造参数
			HttpClient client = new DefaultHttpClient();
			HttpPost post = new HttpPost(url);
			ArrayList<NameValuePair> list = new ArrayList<NameValuePair>();
			list.add(new BasicNameValuePair("cid", cid));
			list.add(new BasicNameValuePair("usr", usr));
			list.add(new BasicNameValuePair("psw",psw));
			HttpEntity en = new UrlEncodedFormEntity(list, HTTP.UTF_8);
			post.setEntity(en);
			// 发送消息和处理结果
//			System.out.println(new Date()+"JHTHttpUtil.class获取token");
			HttpResponse response = client.execute(post);
			if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				String s = EntityUtils.toString(response.getEntity());
				return s;
			}else{
				logger.error("捷惠通系统发送get请求失败,返回失败状态码{}", response.getStatusLine().getStatusCode());
			}
		} catch (Exception e) {
			logger.error("捷惠通系统发送get请求失败",e);
		}
		

		return null;
		
	}
	
	/**
	 * 业务接口调用Http
	 * @param url
	 * @param cid
	 * @param v
	 * @param json
	 * @param signKey
	 * @return
	 */
	public static String doPost(String url, String cid, String v, String json, String signKey, String token){
		// 生成MD5签名
		try {
			MessageDigest md5Tool = MessageDigest.getInstance("MD5");
			byte[] md5Data = md5Tool.digest((json + signKey).getBytes("UTF-8"));
			String sn = toHexString(md5Data);
			// 构造参数
			HttpClient client = new DefaultHttpClient();
			HttpPost post = new HttpPost(url);
			ArrayList<NameValuePair> list = new ArrayList<NameValuePair>();
			list.add(new BasicNameValuePair("cid", cid));
			list.add(new BasicNameValuePair("v", v));
			list.add(new BasicNameValuePair("p",json));
			list.add(new BasicNameValuePair("sn",sn));
			list.add(new BasicNameValuePair("tn",token));
			HttpEntity en = new UrlEncodedFormEntity(list, HTTP.UTF_8);
			post.setEntity(en);
			// 发送消息和处理结果
			HttpResponse response = client.execute(post);
			if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				String s = EntityUtils.toString(response.getEntity());
				return s;
			}else{
				logger.error("捷惠通系统发送post请求失败,返回失败状态码{}", response.getStatusLine().getStatusCode());
			}
			
		}catch(Exception e) {
			logger.error("捷惠通系统发送post请求失败error",e);
			
		}
		return null;
	}
	
	private static String toHexString(byte[] bytes) {
		StringBuffer buffer = new StringBuffer();
		for (int i = 0; i < bytes.length; i++) {
			buffer.append(String.format("%02X", bytes[i]));
		}
		return buffer.toString();
}


/**
 * 推送系统调用登陆接口 Post请求
 * @param url
 * @param pno
 * @param secret
 * @return
 */
	public static String doPostLogin(String url, String pno, String secret){
		String response = null;
		try {
			CloseableHttpClient httpClient = null;
			CloseableHttpResponse httpResponse = null;
			httpClient = HttpClients.createDefault();
			HttpPost post = new HttpPost(url);
			StringEntity stringEntity = new StringEntity("{'pno':'"+pno+"','secret':'"+secret+"'}", ContentType.create("application/json", "UTF-8"));
			post.setEntity(stringEntity);
			httpResponse = httpClient.execute(post);
			String s = EntityUtils.toString(httpResponse.getEntity());
			return s;
		}catch(Exception e) {
			logger.error("推送系统调用登陆接口 Post请求发送post请求失败",e);
		}
		return null;
	}
}
