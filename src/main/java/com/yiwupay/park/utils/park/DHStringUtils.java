package com.yiwupay.park.utils.park;

public class DHStringUtils {
	
	public static boolean isEmpty(final CharSequence cs) {
        return cs == null || cs.length() == 0;
    }
	
	/**
	 * 得字符串最后一个数字（如果尾号为字母，则再向左取，直到取到数字为止）
	 * @param carPlate
	 * @return
	 */
	public static int getLastNum(String str){
		int lastNum = 0;
		if(str != null && !"".equals(str.trim())){
			StringBuilder sb = new StringBuilder(str).reverse();
			for (int i = 0; i < sb.length(); i++) {
				char lastChar = (char) sb.charAt(i);
				if(Character.isDigit(lastChar)){
					return Integer.valueOf(String.valueOf(lastChar));
				}
			}
		}
		return lastNum;
	}
	

}
