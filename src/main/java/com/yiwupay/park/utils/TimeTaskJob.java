package com.yiwupay.park.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.client.RestTemplate;

import com.yiwupay.park.service.ParkService;
import com.yiwupay.park.service.SynBaseService;
import com.yiwupay.park.service.SynDhService;
import com.yiwupay.park.service.SynHIKService;
import com.yiwupay.park.service.SynJHTService;

public class TimeTaskJob {

	@Resource(name = "hikSynService")
	SynHIKService hikSynService;

/*	@Resource(name = "JHT")
	TimeTaskService taskService;*/

	@Resource(name = "dh202SynService")
	SynDhService dh202SynService;

	@Resource(name = "dh203SynService")
	SynDhService dh203SynService;

	@Resource(name = "JHTSynService")
	SynJHTService synJHTService;

	@Resource(name = "synBase")
	SynBaseService synBase;

	@Resource(name = "parkServiceImpl")
	ParkService parkService;
	
/*	@Resource(name = "JHT")
	TimeTaskService taskService;*/

	public void hikJob1() {
		Date result = DateUtil.getCustomDayTime(0, 0, 0, 0);
		long today = result.getTime();
		long yesterday = today - DateUtil.ONE_DAY_TIME_MILLIS;
		hikSynService.saveVehicleRecord(yesterday, today);
	}
	
	public void hikJob2() {
		hikSynService.saveVehicleRecord2();
	}

	public void hikJob3() {
		Date result = DateUtil.getCustomDayTime(0, 0, 0, 0);
		long today = result.getTime();
		long yesterday = today - DateUtil.ONE_DAY_TIME_MILLIS;
		hikSynService.savePaymentRecord(yesterday, today);
	}

	public void dhJob1() {
		Date result = DateUtil.getCustomDayTime(0, 0, 0, 0);
		String today = DateUtil.formatDate(result, DateUtil.DB_YMDHMS_FORMAT);
		String yesterday = DateUtil.formatDate(new Date(result.getTime() - DateUtil.ONE_DAY_TIME_MILLIS),
				DateUtil.DB_YMDHMS_FORMAT);
		dh202SynService.saveVehicleRecord(yesterday, today);
		dh203SynService.saveVehicleRecord(yesterday, today);

	}

	public void dhJob2() {
		Date result = DateUtil.getCustomDayTime(0, 0, 0, 0);
		String today = DateUtil.formatDate(result, DateUtil.DB_YMDHMS_FORMAT);
		String yesterday = DateUtil.formatDate(new Date(result.getTime() - DateUtil.ONE_DAY_TIME_MILLIS),
				DateUtil.DB_YMDHMS_FORMAT);
		dh202SynService.savePaymentRecord(yesterday, today);
		dh203SynService.savePaymentRecord(yesterday, today);

	}
	
	public void jhtsynJob() {
		Date result = DateUtil.getCustomDayTime(0, 0, 0, 0);
		String today = DateUtil.formatDate(result, DateUtil.DB_YMDHMS_FORMAT);
		String yesterday = DateUtil.formatDate(new Date(result.getTime() - DateUtil.ONE_DAY_TIME_MILLIS),
				DateUtil.DB_YMDHMS_FORMAT);
		synJHTService.saveVehicleRecord(yesterday, today);
	}

	
	public void jhtsynpayJob(){
		Date result = DateUtil.getCustomDayTime(0, 0, 0, 0);
		String today = DateUtil.formatDate(result, DateUtil.DB_YMDHMS_FORMAT);
		String yesterday = DateUtil.formatDate(new Date(result.getTime() - DateUtil.ONE_DAY_TIME_MILLIS),
				DateUtil.DB_YMDHMS_FORMAT);
		synJHTService.savePaymentRecord(yesterday, today);
	}
	
	public void job1() {
		synBase.repPaymentRecord();
	}

	public void parkSystemJob() {
		List<String> list = new ArrayList<>();
		//list.add("1006");
		list.add("1007");
		for (String marketId : list) {
			String url = "http://127.0.0.1:8080/vehicle-park-service/park/getparkparking.htm?marketId=";
			String urlNameString = url + marketId;
			RestTemplate Template = new RestTemplate();
			Template.getForObject(urlNameString, String.class);
//			System.out.println(result);
		}
	}
}
