package com.yiwupay.park.utils;

public class Result {
	String error;
	String status;
	String data;
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	@Override
	public String toString() {
		return "Result [error=" + error + ", status=" + status + ", data=" + data + "]";
	}
	
	
}
