package com.yiwupay.park.utils;

import java.util.HashMap;
import java.util.Map;

import com.yiwupay.park.enums.OrderTypeEnum;
import com.yiwupay.park.enums.PayTypeEnum;

public class MapCacheUtil {
	
	public final static HashMap<Integer, String> HIK_CAR_COLOR = new HashMap<Integer, String>(){
		private static final long serialVersionUID = 1L;
		{
			put(0,"其它颜色");
			put(1,"白色");
			put(2,"银色");
			put(3,"灰色");
			put(4,"黑色");
			put(5,"红色");
			put(6,"深蓝");
			put(7,"蓝色");
			put(8,"黄色");
			put(9,"绿色");
			put(10,"棕色");
			put(11,"粉色");
			put(12,"紫色");
			put(255,"不明");
		}
	};
	
	public final static HashMap<Integer, String> DH_CAR_COLOR = new HashMap<Integer, String>(){
		private static final long serialVersionUID = 1L;
		{
			put(99,"未识别");
			put(100,"其他");
			put(0,"白色");
			put(1,"黑色");
			put(2,"红色");
			put(3,"黄色");
			put(4,"银灰色");
			put(5,"蓝色");
			put(6,"绿蓝");
			put(7,"橙色");
			put(8,"紫色");
			put(9,"青色");
			put(10,"粉色");
			put(11,"棕色");
		}
	};
	
	public final static HashMap<Integer, String> HIK_CAR_TYPE = new HashMap<Integer, String>(){
		private static final long serialVersionUID = 1L;
		{
			put(0,"其它车");
			put(1,"小型车");
			put(2,"大型车");
		}
	};
	
	public final static HashMap<Integer, String> DH_CAR_TYPE = new HashMap<Integer, String>(){
		private static final long serialVersionUID = 1L;
		{
			put(0,"未识别");
			put(1,"小型车");
			put(2,"大型车");
			put(3,"使馆车");
			put(4,"领馆车");
			put(5,"境外车");
			put(6,"外籍车");
			put(10,"教练车");
			put(11,"临时行驶车");
			put(12,"警用车");
		}
	};
	
	public final static HashMap<Integer, String> CROSS_TYPE = new HashMap<Integer, String>(){
		private static final long serialVersionUID = 1L;
		{
			put(0,"禁止放行");
			put(1,"固定车包期");
			put(2,"临时车入场");
			put(3,"预约车入场");
			put(10,"离线出场");
			put(11,"缴费出场");
			put(12,"预付费出场");
			put(13,"免费出场");
			put(30,"非法卡不放行");
			put(31,"手动开闸");
			put(32,"特殊车辆放行");
			put(33,"节假日放行");
		}
	};
	
	public final static HashMap<String, String> DH_CROSS_TYPE = new HashMap<String, String>(){
		private static final long serialVersionUID = 1L;
		{
			put("0","刷卡");
			put("1","自动识别");
		}
	};
	
	public final static HashMap<String, String> HIK_ORDER_TYPE = new HashMap<String, String>(){
		private static final long serialVersionUID = 1L;
		{
			put("0",OrderTypeEnum.NORMAL_ORDER.name());
			put("1",OrderTypeEnum.OVERTIME_ORDER.name());
		}
	};
	
	public final static HashMap<Integer, String> HIK_PAY_TYPE = new HashMap<Integer, String>(){
		private static final long serialVersionUID = 1L;
		{
			put(1,PayTypeEnum.SENTRY.name());
			put(2,PayTypeEnum.CENTRAL.name());
			put(3,PayTypeEnum.SELF.name());
			put(4,PayTypeEnum.ZFB.name());
			put(5,PayTypeEnum.WX.name());
			put(8,PayTypeEnum.MOBILE.name());
			put(10,PayTypeEnum.OTHER.name());
			put(11,PayTypeEnum.ACCOUNT.name());
		}
	};
	
	public final static HashMap<String, String> DH_PAY_TYPE = new HashMap<String, String>(){
		private static final long serialVersionUID = 1L;
		{
			put("0",PayTypeEnum.FREE.name());
			put("1",PayTypeEnum.AUTO.name());
			put("2",PayTypeEnum.ACCOUNT.name());
			put("3",PayTypeEnum.SENTRY.name());
			put("4",PayTypeEnum.ZFB.name());
			put("5",PayTypeEnum.WX.name());
			put("6",PayTypeEnum.SELF.name());
			put("7",PayTypeEnum.ZFB.name());
			put("8",PayTypeEnum.WX.name());
			put("9",PayTypeEnum.ZFB.name());
			put("10",PayTypeEnum.WX.name());
			put("11",PayTypeEnum.APP.name());
			put("12",PayTypeEnum.OTHER.name());
			put("13",PayTypeEnum.CENTRAL.name());
			put("14",PayTypeEnum.FATHER.name());
		}
	};
	
	public final static HashMap<String, String> BC_ROADWAY_ID = new HashMap<String, String>(){
		private static final long serialVersionUID = 1L;
		{
			put("10号门入口", "1");
			put("10号门出口", "2");
			put("5号门出口", "3");
			put("3号门出口", "4");
			put("20号门入口", "5");
			put("16号入口", "6");
			put("上行1", "7");
			put("上行2", "8");
			put("下行1", "9");
			put("下行2", "10");
			put("地下出口", "11");
			put("地下入口", "12");
		}
	};
	
	public final static HashMap<String, String> BC_PARKID_ID = new HashMap<String, String>(){
		private static final long serialVersionUID = 1L;
		{
			put("10号门入口", "1");
			put("10号门出口", "1");
			put("5号门出口", "1");
			put("3号门出口", "1");
			put("20号门入口", "1");
			put("16号入口", "1");
			put("上行1", "2");
			put("上行2", "2");
			put("下行1", "2");
			put("下行2", "2");
			put("地下出口", "3");
			put("地下入口", "3");
		}
	};
	
	public final static HashMap<String, String> HM_ROADWAY_ID = new HashMap<String, String>(){
		private static final long serialVersionUID = 1L;
		{
			put("东一进口", "0001");
			put("东一出口", "1001");
		}
	};
	
	public static void main(String[] args) {
		Map<Integer, String> a = CROSS_TYPE;
		System.out.println(a.get("1weqw"));
	}
}
