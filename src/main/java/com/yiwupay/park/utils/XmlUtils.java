/**
 * Project Name:pay-protocol
 * File Name:Xml.java
 * Package Name:cn.swiftpass.pay.protocol
 * Date:2014-8-10下午10:48:21
 *
*/

package com.yiwupay.park.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;

import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.springframework.util.StringUtils;

public class XmlUtils {

	public static String parseRequst(HttpServletRequest request) {
		String body = "";
		try {
			ServletInputStream inputStream = request.getInputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
			while (true) {
				String info = br.readLine();
				if (info == null) {
					break;
				}
				if (body == null || "".equals(body)) {
					body = info;
				} else {
					body += info;
				}
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return body;
	}

	public static String parseXML(SortedMap<String, String> parameters) {
		StringBuffer sb = new StringBuffer();
		sb.append("<xml>");
		Set es = parameters.entrySet();
		Iterator it = es.iterator();
		while (it.hasNext()) {
			Map.Entry entry = (Map.Entry) it.next();
			String k = (String) entry.getKey();
			String v = (String) entry.getValue();
			if (null != v && !"".equals(v) && !"appkey".equals(k)) {
				sb.append("<" + k + ">" + parameters.get(k) + "</" + k + ">\n");
			}
		}
		sb.append("</xml>");
		return sb.toString();
	}

	public static String getRequestXml(SortedMap<String, Object> parameters) {
		StringBuffer sb = new StringBuffer();
		sb.append("<xml>");
		Iterator<Entry<String, Object>> iterator = parameters.entrySet().iterator();
		while (iterator.hasNext()) {
			Map.Entry<String, Object> entry = (Map.Entry<String, Object>) iterator.next();
			String key = (String) entry.getKey();
			String value = (String) entry.getValue();
			sb.append("<" + key + ">" + "<![CDATA[" + value + "]]></" + key + ">");
		}
		sb.append("</xml>");
		return sb.toString();
	}

	public static SortedMap xmlBody2map(String xml, String rootElement) throws DocumentException {
		org.dom4j.Document doc = DocumentHelper.parseText(xml);
		Element body = (Element) doc.selectSingleNode("/" + rootElement);
		SortedMap vo = __buildXmlBody2map(body);
		return vo;
	}

	private static SortedMap __buildXmlBody2map(Element body) {
		SortedMap vo = new TreeMap();
		if (body != null) {
			List<Element> elements = body.elements();
			for (Element element : elements) {
				String key = element.getName();
				if (!StringUtils.isEmpty(key)) {
					String type = element.attributeValue("type", "java.lang.String");
					String text = element.getText().trim();
					Object value = null;
					if (java.lang.String.class.getCanonicalName().equals(type)) {
						value = text;
					} else if (java.lang.Character.class.getCanonicalName().equals(type)) {
						value = new java.lang.Character(text.charAt(0));
					} else if (java.lang.Boolean.class.getCanonicalName().equals(type)) {
						value = new java.lang.Boolean(text);
					} else if (java.lang.Short.class.getCanonicalName().equals(type)) {
						value = java.lang.Short.parseShort(text);
					} else if (java.lang.Integer.class.getCanonicalName().equals(type)) {
						value = java.lang.Integer.parseInt(text);
					} else if (java.lang.Long.class.getCanonicalName().equals(type)) {
						value = java.lang.Long.parseLong(text);
					} else if (java.lang.Float.class.getCanonicalName().equals(type)) {
						value = java.lang.Float.parseFloat(text);
					} else if (java.lang.Double.class.getCanonicalName().equals(type)) {
						value = java.lang.Double.parseDouble(text);
					} else if (java.math.BigInteger.class.getCanonicalName().equals(type)) {
						value = new java.math.BigInteger(text);
					} else if (java.math.BigDecimal.class.getCanonicalName().equals(type)) {
						value = new java.math.BigDecimal(text);
					} else if (java.util.Map.class.getCanonicalName().equals(type)) {
						value = __buildXmlBody2map(element);
					} else {
					}
					vo.put(key, value);
				}
			}
		}
		return vo;
	}

	@SuppressWarnings("rawtypes")
	public static String mapToXml(Map map) {
		StringBuffer sb = new StringBuffer();
		// sb.append("<?xml version='1.0' encoding='GBK' standalone='no'?>");
		// sb.append("<B2CReq>");
		Set set = map.keySet();
		for (Iterator it = set.iterator(); it.hasNext();) {
			String key = (String) it.next();
			Object value = map.get(key);
			if (null == value)
				value = "";
			if (value.getClass().getName().equals("java.util.TreeMap")) {
				// ArrayList list = (ArrayList) map.get(key);
				sb.append("<" + key + ">\r\n");
				// for (int i = 0; i < list.size(); i++) {
				// HashMap hm = (HashMap) list.get(i);
				// mapToXml(hm);
				// }
				TreeMap tMap = (TreeMap) value;
				Iterator titer = tMap.entrySet().iterator();
				while (titer.hasNext()) {
					Map.Entry ent = (Map.Entry) titer.next();
					String keyt = ent.getKey().toString();
					Object valuet = ent.getValue();
					if (valuet instanceof TreeMap) {
						sb.append("<" + keyt + ">");
						sb.append(mapToXml((TreeMap) valuet));
						sb.append("</" + keyt + ">\r\n");
					} else {
						sb.append("<" + keyt + ">" + valuet + "</" + keyt + ">\r\n");
					}
				}
				sb.append("</" + key + ">\r\n");
			} else {
				if (value instanceof TreeMap) {
					sb.append("<" + key + ">");
					sb.append(mapToXml((TreeMap) value));
					sb.append("</" + key + ">\r\n");
				} else {
					sb.append("<" + key + ">" + value + "</" + key + ">\r\n");
				}
			}
		}
		// sb.append("</B2CReq>");
		// sb.append("</xml>");
		return sb.toString();
	}

	public static String toXml(Map map) {
		StringBuffer sb = new StringBuffer();
		sb.append("<?xml version=\"1.0\" encoding=\"GBK\" standalone=\"no\"?>\r\n");
		sb.append("<B2CReq>\r\n");
		String XML = mapToXml(map);
		sb.append(XML);
		sb.append("</B2CReq>\r\n");
		sb.append("</xml>");

		return sb.toString();
	}

	public static void main(String[] args) {
		SortedMap<String, Object> order = new TreeMap<String, Object>();
		SortedMap<String, Object> orderInfo = new TreeMap<String, Object>();
		SortedMap<String, Object> subOrderInfoList = new TreeMap<String, Object>();
		SortedMap<String, String> subOrderInfo = new TreeMap<String, String>();
		SortedMap<String, String> custom = new TreeMap<String, String>();
		SortedMap<String, String> message = new TreeMap<String, String>();

		orderInfo.put("curType", "001");
		orderInfo.put("merID", "1208EC24395828");
		orderInfo.put("subOrderInfoList", subOrderInfoList);
		subOrderInfoList.put("subOrderInfo", subOrderInfo);

		subOrderInfo.put("orderid", "1");
		subOrderInfo.put("amount", "1");
		subOrderInfo.put("installmentTimes", "1");
		subOrderInfo.put("merAcct", "1");
		subOrderInfo.put("goodsID", "");
		subOrderInfo.put("goodsName", "1");
		subOrderInfo.put("goodsNum", "");
		subOrderInfo.put("carriageAmt", "");

		custom.put("verifyJoinFlag", "1");
		custom.put("Language", "ZH_CN");

		message.put("creditType", "2");
		message.put("notifyType", "1");
		message.put("resultType", "1");
		message.put("merReference", "");
		message.put("merCustomIp", "");
		message.put("goodsType", "");
		message.put("merCustomID", "");
		message.put("merCustomPhone", "");
		message.put("goodsAddress", "");
		message.put("merOrderRemark", "");
		message.put("merHint", "");
		message.put("remark1", "");
		message.put("remark2", "");
		message.put("merURL", "http://localhost:9080/EbizSimulate/emulator/Newb2c_Pay_Mer.jsp");
		message.put("merVAR", "");

		order.put("interfaceName", "1");
		order.put("interfaceVersion", "1");
		order.put("orderInfo", orderInfo);
		order.put("custom", custom);
		order.put("message", message);

		String a = XmlUtils.toXml(order);
		System.out.println(a);
	}
}
