package com.yiwupay.park.utils; 

import java.util.Map;

import org.springframework.util.StringUtils;

public class Utils {
	
	public static String buildMap(Map<String, String> map){
		StringBuffer sb = new StringBuffer();
		if(map.size()>0){
			for(String key:map.keySet()){
				sb.append(key+"=");
				if(StringUtils.isEmpty(map.get(key))){
					sb.append("&");
				}else{
					sb.append(map.get(key)+"&");
				}
			}
		}
		return sb.toString();
	}

}
