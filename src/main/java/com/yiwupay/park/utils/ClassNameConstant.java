package com.yiwupay.park.utils;

import java.util.HashMap;

public class ClassNameConstant {

	public static final String PAYMENTORDER = "com.yiwupay.pay.action.PaymenOrderAction";
	public static final String BANKPAY = "com.yiwupay.bank.action.BankAction";
	
	
	public static final HashMap<String , String> URL_MAP = new HashMap<String, String>(){
		{
			put(PAYMENTORDER , "/payment");
			put(BANKPAY , "/bankgw");
		}
	};

}
