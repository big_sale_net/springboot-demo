package com.yiwupay.park.utils;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import com.yiwupay.park.annotation.RelationField;
/**
 * 注解工具类
 * @author he
 *
 */
public class AnnotationUtil {
	public static Map<String, String> getFieldMapByRelationField(Class<?> clazz) {
		Field[] fields =clazz.getDeclaredFields();
		Map<String, String> fieldMap = new HashMap<>();
		for(Field field : fields){
			if(field.isAnnotationPresent(RelationField.class)){
				RelationField relationField = field.getAnnotation(RelationField.class);
				String relationValue = relationField.value();//对应的属性名
				if(!StringUtils.isEmpty(relationValue)){
					String fieldName = field.getName();
					fieldMap.put(relationValue, fieldName);
				}
			}
		}
		return fieldMap;
	}
	
	public static <T> T obj2Obj_T(Object obj, Class<T> clazz_T) throws Exception{
		Class<? extends Object> clazz = obj.getClass();
		Map<String, String> fieldMap = AnnotationUtil.getFieldMapByRelationField(clazz);
		T t = clazz_T.newInstance();
		Set<String> fields_T = fieldMap.keySet();
		for (String field_T : fields_T) {
			String getMethod = "get" + fieldMap.get(field_T).substring(0, 1).toUpperCase()
					+ fieldMap.get(field_T).substring(1);
			String setMethod_T = "set" + field_T.substring(0, 1).toUpperCase() + field_T.substring(1);
			Object val = clazz.getMethod(getMethod).invoke(obj);
			Class<?> argType = clazz_T.getDeclaredField(field_T).getType();
			clazz_T.getMethod(setMethod_T, argType).invoke(t, val);
		}
		return t;
	}
}
