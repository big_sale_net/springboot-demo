package com.yiwupay.park.utils;

public class Constant {
	public static String QRURL = "https://pay.swiftpass.cn/pay/qrcode?uuid=";

	public final static String ERRORCODE = "error";

	public final static String SUCCESSORCODE = "success";
	
	public final static String FAILCODE = "fail";
}
