package com.yiwupay.park.utils;

/**
 * 
 * @author ZhuGuoku
 * @version 1.0 2017-8-25
 *
 */
public class RedisUtil {

//	static Logger logger = LoggerFactory.getLogger(RedisUtil.class);
//
//	private static String ADDR = "ywphub-service.com";
//
//	private static int PORT = 6379;
//
//	private static int REDIS_DB = 11;
//
//	private static String AUTH = "123456";
//
//	private static int MAX_ACTIVE = 1024;
//
//	private static int MAX_IDLE = 200;
//
//	private static int MAX_WAIT = 10000;
//
//	private static int TIMEOUT = 10000;
//
//	private static boolean TEST_ON_BORROW = true;
//
//	private static JedisPool jedisPool = null;
//
//	static {
//		logger.info("now initialize jedisPool");
//		try {
//			JedisPoolConfig config = new JedisPoolConfig();
//			config.setMaxTotal(MAX_ACTIVE);
//			config.setMaxIdle(MAX_IDLE);
//			config.setMaxWaitMillis(MAX_WAIT);
//			config.setTestOnBorrow(TEST_ON_BORROW);
//			jedisPool = new JedisPool(config, ADDR, PORT, TIMEOUT, AUTH, REDIS_DB);
//
//		} catch (Exception e) {
//			logger.error("打印error", e);
//		}
//	}
//
//	public synchronized static Jedis getJedis() {
//		if (jedisPool == null) {
//			logger.error("jedisPool is not initialized");
//		}
//		Jedis resource = null;
//		try {
//			if (jedisPool != null) {
//				resource = jedisPool.getResource();
//			}
//		} catch (Exception e) {
//			logger.error("get resource error");
//			logger.error("打印error", e);
//		} finally {
//			monitorPool("getJedis");
//		}
//		return resource;
//	}
//
//	/**
//	 * 向缓存中设置对象
//	 * 
//	 * @param key
//	 * @param value
//	 * @return
//	 */
//	public static boolean set(String key, Object value) {
//		Jedis jedis = getJedis();
//		try {
//			JSONObject json = JSONObject.fromObject(value);
//			logger.info("jsonString:{}", json.toString());
//			jedis.set(key, json.toString());
//			return true;
//		} catch (Exception e) {
//			logger.error("打印error", e);
//			return false;
//		} finally {
//			returnResource(jedis);
//		}
//	}
//
//	/**
//	 * 根据key 获取对象
//	 * 
//	 * @param key
//	 * @param clazz
//	 * @return
//	 */
//	@SuppressWarnings("unchecked")
//	public static <T> T get(String key, Class<T> clazz) {
//		Jedis jedis = getJedis();
//		try {
//			String value = jedis.get(key);
//			JSONObject json = JSONObject.fromObject(value);
//			return (T) JSONObject.toBean(json, clazz);
//		} catch (Exception e) {
//			logger.error("打印error", e);
//			return null;
//		} finally {
//			returnResource(jedis);
//		}
//	}
//
//	/**
//	 * 根据key 删除对象，返回1 刪除成功
//	 * 
//	 * @param key
//	 * @return
//	 */
//	public static long del(String key) {
//		Jedis jedis = getJedis();
//		long s = -1;
//		try {
//			s = jedis.del(key);
//		} catch (Exception e) {
//			logger.error("打印error", e);
//		} finally {
//			returnResource(jedis);
//		}
//		return s;
//	}
//
//	/**
//	 * 在列表中的尾部添加一个值，返回列表的长度
//	 * 
//	 * @param key
//	 * @param value
//	 * @return Long
//	 */
//	public static Long rpush(String key, Object value) {
//		Jedis jedis = jedisPool.getResource();
//		long length = -1;
//		try {
//			JSONObject json = JSONObject.fromObject(value);
//			length = jedis.rpush(key, json.toString());
//			logger.info("jsonString:" + json.toString() + "rpush success!");
//		} catch (Exception e) {
//			logger.error("打印error", e);
//		} finally {
//			returnResource(jedis);
//		}
//		return length;
//	}
//
//	/**
//	 * 通过索引获取列表中的元素
//	 * 
//	 * @param key
//	 * @param index，索引，0表示最新的一个元素
//	 * @return String
//	 */
//	@SuppressWarnings("unchecked")
//	public static <T> T lindex(String key, long index, Class<T> clazz) {
//		Jedis jedis = jedisPool.getResource();
//		try {
//			String value = jedis.lindex(key, index);
//			JSONObject json = JSONObject.fromObject(value);
//			return (T) JSONObject.toBean(json, clazz);
//		} catch (Exception e) {
//			logger.error("打印error", e);
//			return null;
//		} finally {
//			returnResource(jedis);
//		}
//	}
//
//	/**
//	 * 移出并获取列表的第一个元素，当列表不存在或者为空时，返回Null
//	 * 
//	 * @param key
//	 * @return String
//	 */
//	@SuppressWarnings("unchecked")
//	public static <T> T lpop(String key, Class<T> clazz) {
//		Jedis jedis = jedisPool.getResource();
//		try {
//			String value = jedis.lpop(key);
//			JSONObject json = JSONObject.fromObject(value);
//			logger.info("jsonString:" + json.toString() + "\nlpop success!");
//			return (T) JSONObject.toBean(json, clazz);
//		} catch (Exception e) {
//			logger.error("打印error", e);
//			return null;
//		} finally {
//			returnResource(jedis);
//		}
//	}
//
//	/**
//	 * 移除并获取列表最后一个元素，当列表不存在或者为空时，返回Null
//	 * 
//	 * @param key
//	 * @return String
//	 */
//	@SuppressWarnings("unchecked")
//	public static <T> T rpop(String key, Class<T> clazz) {
//		Jedis jedis = jedisPool.getResource();
//		try {
//			String value = jedis.rpop(key);
//			JSONObject json = JSONObject.fromObject(value);
//			return (T) JSONObject.toBean(json, clazz);
//		} catch (Exception e) {
//			logger.error("打印error", e);
//			return null;
//		} finally {
//			returnResource(jedis);
//		}
//	}
//
//	/**
//	 * 获取列表长度，key为空时返回0
//	 * 
//	 * @param key
//	 * @return Long
//	 */
//	public static Long llen(String key) {
//		Jedis jedis = jedisPool.getResource();
//		long length = -1;
//		try {
//			length = jedis.llen(key);
//		} catch (Exception e) {
//			logger.error("打印error", e);
//			return null;
//		} finally {
//			returnResource(jedis);
//		}
//		return length;
//	}
//
//	/**
//	 * 刪除指定hashkey中的內容
//	 * 
//	 * @param key
//	 * @return
//	 */
//	public static Long hdel(String key, String field) {
//		Jedis jedis = jedisPool.getResource();
//		long result = -1;
//		try {
//			// 返回哈希表key中的所有域
//			Set<String> fieldsSet = jedis.hkeys(key);
//			logger.info("fieldsSet:{}", fieldsSet);
//
//			// 返回哈希表key中的所有值
//			List<String> valueList = jedis.hvals(key);
//			logger.info("valueList:{}", valueList);
//
//			boolean isExist = jedis.hexists(key, field);
//			logger.info("isExist:{}", isExist);
//
//			String key_field = jedis.hget(key, field);
//			logger.info("key_field:{}", key_field);
//
//			result = jedis.hdel(key, field);
//		} catch (Exception e) {
//			logger.error("打印error", e);
//		} finally {
//			returnResource(jedis);
//		}
//		return result;
//	}
//	
//	//****************Strings****************//	
//	
//	/**
//	 * 设置 String
//	 * 
//	 * @param key
//	 * @param value
//	 */
//	public static boolean setString(String key, String value) {
//		Jedis jedis = jedisPool.getResource();
//		try {
//			jedis.set(key, value);
//			return true;
//		} catch (Exception e) {
//			logger.error("Set key error");
//			logger.error("打印error", e);
//			return false;
//		} finally {
//			returnResource(jedis);
//		}
//	}
//
//	/**
//	 * 设置 过期时间
//	 * 
//	 * @param key
//	 * @param seconds
//	 * @param value
//	 */
//	public static boolean setString(String key, int seconds, String value) {
//		Jedis jedis = jedisPool.getResource();
//		try {
//			jedis.setex(key, seconds, value);
//			return true;
//		} catch (Exception e) {
//			logger.error("Set keyex error");
//			logger.error("打印error", e);
//			return false;
//		} finally {
//			returnResource(jedis);
//		}
//
//	}
//
//	/**
//	 * 获取String值
//	 * 
//	 * @param key
//	 * @return value
//	 */
//	public static String getString(String key) {
//		Jedis jedis = getJedis();
//		try {
//			return jedis.get(key);
//		} catch (Exception e) {
//			logger.error("打印error", e);
//			return null;
//		} finally {
//			returnResource(jedis);
//		}
//
//	}
//
//	/**
//	 * 资源回收
//	 * 
//	 * @param jedis
//	 */
//	public static void returnResource(final Jedis jedis) {
//		try {
//			if (jedis != null) {
//				jedis.close();
//			}
//		} catch (Exception e) {
//			logger.error("打印error", e);
//		} finally {
//			monitorPool("returnResource");
//		}
//	}
//
//	public static void monitorPool(String action) {
//		String redisPoolInfo = "redisPoolInfo<NumActive, NumIdle, NumWaiters>: " + jedisPool.getNumActive() + ","
//				+ jedisPool.getNumIdle() + "," + jedisPool.getNumWaiters();
//		//logger.info("{" + action + "}" + redisPoolInfo);
//	}

}