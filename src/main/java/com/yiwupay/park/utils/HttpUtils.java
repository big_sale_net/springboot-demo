/*
 *	Copyright © 2013 Changsha Shishuo Network Technology Co., Ltd. All rights reserved.
 *	长沙市师说网络科技有限公司 版权所有
 *	http://www.shishuo.com
 */

package com.yiwupay.park.utils;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.util.StringUtils;


public class HttpUtils {

    /**
     * 得到请求的IP地址
     *
     * @param request
     * @return
     */
    public static String getIp(HttpServletRequest request) {
        String ip = request.getHeader("X-Real-IP");
        if (StringUtils.isEmpty(ip)) {
            ip = request.getHeader("Host");
        }
        if (StringUtils.isEmpty(ip)) {
            ip = request.getHeader("X-Forwarded-For");
        }
        if (StringUtils.isEmpty(ip)) {
            ip = "0.0.0.0";
        }
        return ip;
    }

    /**
     * 得到请求的根目录
     *
     * @param request
     * @return
     */
    public static String getBasePath(HttpServletRequest request) {
        String path = request.getContextPath();
        String basePath = request.getScheme() + "://" + request.getServerName()
                + ":" + request.getServerPort() + path;
        return basePath;
    }

    /**
     * 得到结构目录
     *
     * @param request
     * @return
     */
    public static String getContextPath(HttpServletRequest request) {
        String path = request.getContextPath();
        return path;
    }
    
    public static Map<String,Object> convertRequestParam(HttpServletRequest request){
    	Map<String, String[]> map = request.getParameterMap();
    	Map<String, Object> result = new HashMap<String, Object>();
    	for(Iterator iter = map.entrySet().iterator();iter.hasNext();){ 
	        Map.Entry element = (Map.Entry)iter.next(); 
	        Object strKey = element.getKey(); 
	        String[] value=(String[])element.getValue();
	        
	        if (null != value && value.length>0 && !value[0].isEmpty()){
	        	result.put(strKey.toString(), value[0]);
//	        	result.append(strKey.toString() +"=");
//		        for(int i=0;i<value.length;i++){
//		        	result.append(value[i]+",");
//		        }
//		        result.deleteCharAt(result.length()-1);
//		        result.append(";");
	        }
	        
		}
    	return result;
    }

}
