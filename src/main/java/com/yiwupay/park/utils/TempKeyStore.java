package com.yiwupay.park.utils;

import java.util.HashMap;
import java.util.Map;

public class TempKeyStore {
	 public static Map<String,String> keystore = new HashMap<String,String>();
	 static{
		 
		 keystore.put("weiyipri", "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAIJN1tNjmG3.CplkspcS5oz92SWjahW9_stgT7UW5nBId3CJPib.4auCQpf0Cfc.2gDFNKAChWjeTCVuW0gM2pqFyDsmbPL_uCm3W9j2LGdrk0Gv3rIgNvgPUkRo7bVFffQGiXDzhxXX699kJHj9VgsLqCbvcib9L7ozK83z7C1VAgMBAAECgYA8quMXfqpso7fpnUGtJwaipFnitUfiIZistgXGaYgWxRafFbcTTt5hDP_WYa_2JpQ3gZmz2hilL8LCNrRXQmzQlBlqoHml8dQOSTp2EhyKikojkFu78Zhkl7q81tMrU6XtaOP8S7QKM9cpYMCPtpiAgvb3dTbCjJFBXtR5Fn8ebQJBAMj3IrZ5siBkpXdHjVFH1ffY_iqYwrBFnuRObr_lqQrAeQICTZ2cARBe.NtI.lp32zpj5t6YkAXP_XDPeAJVuV8CQQCl_O0pzNucbw9AUPoU15M_uLRX5fYJjvNMF.xvHbnVh_PkW5n4j7mGH4dEtP.A1BN7H7NQ3wdA54chZwRG6THLAkB64OGQwYhw1hl2fYGjGVFzYnLGiPFB5s_ouSxumXZq5JUY7V.X5zOfVvLC4jRx7KdXZI5dvNsTMsG8oJ64jsHxAkAYKOdQNTwl2dhrcR3lWLdbCARf4t3b6E2rXrQQPFBnLyGltFuF3U2QoxgqPPoPg07Olf.gnsXMgaaeiwMJWznfAkEAh0h5C3Gz.Sqt7XPZJJgxl7pYBsB7bana8UQ0VwH.AQJ11EBLDJCXglumUKEw._kxe1brwNyo.HxMaVQfySecdg==");
		 keystore.put("weiyipub", "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCCTdbTY5ht_gqZZLKXEuaM_dklo2oVvf7LYE.1FuZwSHdwiT4m_uGrgkKX9An3PtoAxTSgAoVo3kwlbltIDNqahcg7Jmzy_7gpt1vY9ixna5NBr96yIDb4D1JEaO21RX30Bolw84cV1.vfZCR4_VYLC6gm73Im_S.6MyvN8.wtVQIDAQAB");
	 }

	 public static void main(String[] args) throws Exception {
  
		 String test = "test";
		 byte [] b = RSAUtils.encryptByPrivateKey(test.getBytes(), keystore.get("weiyipri"));
		 String ben = Base64Utils.encode(b);
		 
		 
		 
		 byte [] d = RSAUtils.decryptByPublicKey(Base64Utils.decode(ben), keystore.get("weiyipub"));
		 System.out.println(ben);

		 System.out.println(new String(d));
		 
//		 // create the keys  
//		    KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA", "BC");  
//		    generator.initialize(512, new SecureRandom());  
//		    KeyPair pair = generator.generateKeyPair();  
//		    PublicKey pubKey = pair.getPublic();  
//		    PrivateKey privKey = pair.getPrivate();  
//		    byte[] pk = pubKey.getEncoded();  
//		    byte[] privk = privKey.getEncoded();  
//		    String strpk = new String(Base64Utils.decode(pk));  
//		    String strprivk = new String(Base64Utils.decode(privk));  
//		  
//		    System.out.println("公钥:" + Arrays.toString(pk));  
//		    System.out.println("私钥:" + Arrays.toString(privk));  
//		    System.out.println("公钥Base64编码:" + strpk);  
//		    System.out.println("私钥Base64编码:" + strprivk);  
//		  
//		    X509EncodedKeySpec pubX509 = new X509EncodedKeySpec(Base64Utils.decode(strpk.getBytes()));  
//		    PKCS8EncodedKeySpec priPKCS8 = new PKCS8EncodedKeySpec(Base64Utils.decode(strprivk.getBytes()));  
//		    KeyFactory keyf = KeyFactory.getInstance("RSA", "BC");  
//		    PublicKey pubkey2 = keyf.generatePublic(pubX509);  
//		    PrivateKey privkey2 = keyf.generatePrivate(priPKCS8);  
//		  
//		    System.out.println(pubKey.equals(pubkey2));  
//		    System.out.println(privKey.equals(privkey2));  
	}
}
