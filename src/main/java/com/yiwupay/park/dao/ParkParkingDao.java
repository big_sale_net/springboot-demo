package com.yiwupay.park.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.yiwupay.park.model.MarketParkingSelector;
import com.yiwupay.park.model.ParkParking;
import com.yiwupay.park.model.web.ParkSystemVo;
@Repository
public interface ParkParkingDao {
    int deleteByPrimaryKey(Long parkingId);

    int insert(ParkParking record);

    int insertSelective(ParkParking record);

    ParkParking selectByPrimaryKey(Long parkingId);
    
    List<ParkParking> selectAll();

    int updateByPrimaryKeySelective(ParkParking record);

    int updateByPrimaryKey(ParkParking record);
    
    List<ParkParking> getParkParkingByMarketId(String marketId);
    
    int updateleftPlaceByParkingId(Long parkingId,Integer leftPlace);
    
    ParkParking getParkParking(@Param("systemId")Long systemId, @Param("outParkingId")String outParkingId);
    
    List<ParkParking> getParkParkingBySystemId(Long systemId);
    
    int updateBySystemId(ParkParking record);
    
	List<ParkSystemVo> getParkSystemList(Map<String, Object> map);

	long getParkSystemListCount(Map<String, Object> map);
	
	List<MarketParkingSelector> marketParkingSelector(@Param("marketList")String marketList);
}