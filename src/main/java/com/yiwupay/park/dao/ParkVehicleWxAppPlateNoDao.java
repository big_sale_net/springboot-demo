package com.yiwupay.park.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.yiwupay.park.model.ParkVehicleWxAppPlateNo;
@Repository
public interface ParkVehicleWxAppPlateNoDao {
	ParkVehicleWxAppPlateNo selectByPrimaryKey(Long id);

    List<ParkVehicleWxAppPlateNo> selectByWxOpenid(String wxOpenid);
    
    int insertSelective(ParkVehicleWxAppPlateNo record);
}