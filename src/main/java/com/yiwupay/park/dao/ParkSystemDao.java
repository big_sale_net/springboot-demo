package com.yiwupay.park.dao;

import org.springframework.stereotype.Repository;

import com.yiwupay.park.model.ParkSystem;
@Repository
public interface ParkSystemDao {
    int deleteByPrimaryKey(Long systemId);

    int insert(ParkSystem record);

    int insertSelective(ParkSystem record);

    ParkSystem selectByPrimaryKey(Long systemId);

    int updateByPrimaryKeySelective(ParkSystem record);

    int updateByPrimaryKey(ParkSystem record);
}