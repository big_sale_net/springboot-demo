package com.yiwupay.park.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.yiwupay.park.model.ParkVehicleRecord;
import com.yiwupay.park.model.web.ParkSystemVo;
import com.yiwupay.park.model.web.VehicleSearchVo;
import com.yiwupay.park.model.web.VehicleStaticSearchVo;
@Repository
public interface ParkVehicleRecordDao {
    int deleteByPrimaryKey(Long recordId);

    int insert(ParkVehicleRecord record);

    int insertSelective(ParkVehicleRecord record);

    ParkVehicleRecord selectByPrimaryKey(Long recordId);

    int updateByPrimaryKeySelective(ParkVehicleRecord record);

    int updateByPrimaryKey(ParkVehicleRecord record);
    
    List<ParkVehicleRecord> selectByParams(ParkVehicleRecord record);
    //若isOut为null 则查此车所有过车记录 "0"为未出场，"1"为已出场
    List<ParkVehicleRecord> selectByPlateNo(@Param("plateNo")String plateNo, @Param("isOut")String isOut);
    //大华更新出场记录
    int updateDHVehicleExit(ParkVehicleRecord record);
    //捷惠通更新出场记录
    int updateJHTVehicleExit(ParkVehicleRecord record);
    //捷惠通云支付情况第二条更新出场记录
    int updateJHT2VehicleExit(ParkVehicleRecord record);
    
    int updateVehicleByParam(ParkVehicleRecord record);

	/**
	 * 流动车辆列表获取
	 * @param map
	 * @return
	 */
	List<VehicleSearchVo> getVehicleList(Map<String, Object> map);

	/**
	 * 流动车辆总数获取
	 * @param map
	 * @return
	 */
	long getVehicleListCount(Map<String, Object> map);
	
	/**
	 * 车辆统计列表获取
	 * @param map
	 * @return
	 */
    List<VehicleStaticSearchVo> getVehicleStatisList(Map<String, Object> map);
	/**
	 * 车辆统计总数获取
	 * @param map
	 * @return
	 */
	long getVehicleStatisListCount(Map<String, Object> map);
	
	Long getJHTMaxUpdateTime();
	
	/**
	 * 根据接入系统过车记录id更新过车记录
	 */
	int updateRecordByOutRecordId(ParkVehicleRecord record);
	
}