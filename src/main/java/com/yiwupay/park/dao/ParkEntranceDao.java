package com.yiwupay.park.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.yiwupay.park.model.ParkEntrance;
@Repository
public interface ParkEntranceDao {
    int deleteByPrimaryKey(Long entranceId);

    int insert(ParkEntrance record);

    int insertSelective(ParkEntrance record);

    ParkEntrance selectByPrimaryKey(Long entranceId);

    int updateByPrimaryKeySelective(ParkEntrance record);

    int updateByPrimaryKey(ParkEntrance record);
    
    List<ParkEntrance> selectAll();
}