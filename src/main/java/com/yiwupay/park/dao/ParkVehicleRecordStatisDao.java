package com.yiwupay.park.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.yiwupay.park.model.ParkVehicleRecordStatis;
import com.yiwupay.park.model.ParkVehicleRecordTemp;
import com.yiwupay.park.model.web.VehicleSearchVo;
import com.yiwupay.park.model.web.VehicleStaticSearchVo;
@Repository
public interface ParkVehicleRecordStatisDao {
    int deleteByPrimaryKey(Long recordStId);

    int insertSelective(ParkVehicleRecordStatis record);
    
    int insertEnterRecordByTemp(ParkVehicleRecordTemp recordTemp);

    ParkVehicleRecordStatis selectByPrimaryKey(Long recordStId);

    int updateByPrimaryKeySelective(ParkVehicleRecordStatis record);
    
    ParkVehicleRecordStatis getModelBySystemAndOutRecordId(@Param("systemId")Long systemId,
    		@Param("outRecordId")String outRecordId);
    
    List<ParkVehicleRecordStatis> getParkVehicleRecordStatisByParams(ParkVehicleRecordStatis record);
    
    int updateByParams(ParkVehicleRecordStatis record);
    
    List<ParkVehicleRecordStatis> getRecoedsOfIsOut(String isOut);

    ParkVehicleRecordStatis getModelByParams(@Param("systemId")Long systemId, 
    		@Param("plateNo") String plateNo, @Param("enterTime")Long enterTime);
    
	List<VehicleSearchVo> getVehicleSearchVoList(Map<String, Object> map);
	
	Long getVehicleSearchVoListCount(Map<String, Object> map);
	
	
	
	List<VehicleStaticSearchVo> getVehicleStatisHisList(Map<String, Object> map);
	
	long getVehicleStatisHisListCount(Map<String, Object> map);
	
	int updateJhtOutByParams(ParkVehicleRecordStatis recor);
}