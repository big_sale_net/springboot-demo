package com.yiwupay.park.dao;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.yiwupay.park.model.ParkVehiclePaymentOrder;
import com.yiwupay.park.model.vo.PayPage;
import com.yiwupay.park.model.web.OrderSearchVo;
@Repository
public interface ParkVehiclePaymentOrderDao {
    int deleteByPrimaryKey(Long orderId);

    int insert(ParkVehiclePaymentOrder order);

    int insertSelective(ParkVehiclePaymentOrder order);

    ParkVehiclePaymentOrder selectByPrimaryKey(Long orderId);

    int updateByPrimaryKeySelective(ParkVehiclePaymentOrder order);

    int updateByPrimaryKey(ParkVehiclePaymentOrder order);
    
    List<OrderSearchVo> getOrderSearchList(Map<String, Object> map);
    
    long getOrderSearchListCount(Map<String, Object> map);
    
    BigDecimal getOrderSumCost(Map<String, Object> map);
    
    List<PayPage> queryPaylist(Map<String, Object> map);
    
    long queryPaylistCount(Map<String, Object> map);
    
    ParkVehiclePaymentOrder selectByPaymentId(@Param("paymentId") Long paymentId);
}