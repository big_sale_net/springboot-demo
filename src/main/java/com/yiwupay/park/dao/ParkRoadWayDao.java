package com.yiwupay.park.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.yiwupay.park.model.ParkRoadWay;
@Repository
public interface ParkRoadWayDao {
    int deleteByPrimaryKey(Long roadwayId);

    int insert(ParkRoadWay record);

    int insertSelective(ParkRoadWay record);

    ParkRoadWay selectByPrimaryKey(Long roadwayId);

    int updateByPrimaryKeySelective(ParkRoadWay record);

    int updateByPrimaryKey(ParkRoadWay record);

	List<String> getInOutNameByParkingId(Long parkingId);
	
	List<ParkRoadWay> selectAll();
}