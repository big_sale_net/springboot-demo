package com.yiwupay.park.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.yiwupay.park.model.ParkVehiclePaymentStatis;
import com.yiwupay.park.model.web.CaiwuStatisWeb;
import com.yiwupay.park.model.web.VehicleCostVo;
@Repository
public interface ParkVehiclePaymentStatisDao {
    int deleteByPrimaryKey(Long paymentStId);

    int insert(ParkVehiclePaymentStatis record);

    int insertSelective(ParkVehiclePaymentStatis record);

    ParkVehiclePaymentStatis selectByPrimaryKey(Long paymentStId);

    int updateByPrimaryKeySelective(ParkVehiclePaymentStatis record);

    int updateByPrimaryKey(ParkVehiclePaymentStatis record);
    
    ParkVehiclePaymentStatis getModelBySystemAndOutPaymentId(@Param("systemId")Long systemId,
    		@Param("outPaymentId")String outPaymentId);
    
    List<ParkVehiclePaymentStatis> getModelByParams(@Param("systemId")Long systemId,
    		@Param("plateNo")String plateNo, @Param("enterTime")Long enterTime);
    
    List<ParkVehiclePaymentStatis> getModelsOfNullRecord();
    
    
    List<CaiwuStatisWeb> caiwuStatisList(Map map);
    
    long caiwuStatisCount(Map map);
    
    List<VehicleCostVo> getVehicleCostVoByRecordStId(Long recordStId);
}