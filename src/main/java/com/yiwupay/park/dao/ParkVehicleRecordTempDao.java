package com.yiwupay.park.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.yiwupay.park.model.ParkVehicleRecordTemp;
@Repository
public interface ParkVehicleRecordTempDao {
    int deleteByPrimaryKey(Long recordStId);

    int insertSelective(ParkVehicleRecordTemp record);

    ParkVehicleRecordTemp selectByPrimaryKey(Long recordStId);

    int updateByPrimaryKeySelective(ParkVehicleRecordTemp record);

    ParkVehicleRecordTemp getModelBySystemAndOutRecordId(@Param("systemId")Long systemId,
    		@Param("outRecordId")String outRecordId);
    
    ParkVehicleRecordTemp getModelByPlateNoAndEnterTime(@Param("systemId")Long systemId,
    		@Param("plateNo")String plateNo, @Param("enterTime") Long enterTime);
    
    List<ParkVehicleRecordTemp> getModelRecord(@Param("systemId")Long systemId, 
    		@Param("enterTime") Long enterTime, @Param("outTime") Long outTime);
}