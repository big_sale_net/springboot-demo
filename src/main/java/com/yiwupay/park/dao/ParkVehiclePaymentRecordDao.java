package com.yiwupay.park.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.yiwupay.park.model.ParkVehiclePaymentRecord;
import com.yiwupay.park.model.vo.JHTVehiclePaymentVO;
import com.yiwupay.park.model.vo.PaymentPage;
import com.yiwupay.park.model.web.VehicleSearchVo;
@Repository
public interface ParkVehiclePaymentRecordDao {
    int deleteByPrimaryKey(Long paymentId);

    int insert(ParkVehiclePaymentRecord record);

    //时间取插入数据库时间
    int insertSelective(ParkVehiclePaymentRecord record);

    ParkVehiclePaymentRecord selectByPrimaryKey(Long paymentId);

    int updateByPrimaryKeySelective(ParkVehiclePaymentRecord record);

    int updateByPrimaryKey(ParkVehiclePaymentRecord record);
    
    ParkVehiclePaymentRecord getModelByOutRecordIdAndStatus(@Param("outRecordId")String outRecordId,
    		@Param("systemId")Long systemId, @Param("orderStatus")String orderStatus);
    
    ParkVehiclePaymentRecord getModelByOutPaymentId(@Param("outPaymentId")String outPaymentId,
    		@Param("systemId")Long systemId);
    
    List<PaymentPage> getPaymentPage(Map<String, Object> map);
    
    long getPaymentPageCount(Map<String, Object> map);
    
    List<ParkVehiclePaymentRecord> getModelByRecordId(Long recordId);
    
    //获取捷惠通微信支付缴费记录
    List<JHTVehiclePaymentVO> getJHTWXPaymentRecord(@Param("beginTime")Long beginTime, @Param("endTime")Long endTime);

	VehicleSearchVo getCostAndType(Long recordStId);
}