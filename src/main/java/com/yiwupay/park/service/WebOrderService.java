package com.yiwupay.park.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import com.yiwupay.park.model.web.OrderSearchVo;

/**
 * 停车场管理平台	订单Service
 * Title: WebOrderService
 * Description: 
 * Company:  
 * @author cailuxi
 * @date 2018年1月18日
 */
public interface WebOrderService {

	List<OrderSearchVo> getOrderSearchList(Map<String, Object> map);
	
	long getOrderSearchListCount(Map<String, Object> map);
	
	BigDecimal getOrderSumCost(Map<String, Object> map);
}
