package com.yiwupay.park.service;

import java.util.List;
import java.util.Map;

import com.yiwupay.park.model.web.CaiwuStatisWeb;

public interface CaiwuService {

	//获取财务统计列表
	List<CaiwuStatisWeb> getCaiwuStatisList(Map<String, Object> map);

	//获取财务统计数据量
	long getCaiwuStatisCount(Map<String, Object> map);
 
}
