package com.yiwupay.park.service;

import java.util.Map;

import com.yiwupay.park.model.result.ParkResult;
import com.yiwupay.park.model.result.ParkResultList;
import com.yiwupay.park.model.vo.ChargeBillVO;
import com.yiwupay.park.model.vo.ChargeRuleVO;
import com.yiwupay.park.model.vo.PayBackVO;

/**
 * 停车订单接口
 */
public interface ParkChargeService {
	/**
	 * 获取实时订单
	 * @param map
	 * @return
	 */
	ParkResult<ChargeBillVO> getChargeBill(Map<String, Object> map);
	/**
	 * 获取收费规则
	 * @param map
	 * @return
	 */
	ParkResultList<ChargeRuleVO> getChargeRule(Map<String, Object> map);
	/**
	 * 通知支付
	 * @param map
	 * @return
	 */
	ParkResult<PayBackVO> payChargeBill(Map<String, Object> map);

	/**
	 * 获取历史支付订单
	 * @param map
	 * @return
	 */
	ParkResult getpayChargeBills(Map<String, Object> map);
}
