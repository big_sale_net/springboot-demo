package com.yiwupay.park.service;

import com.yiwupay.park.model.NotifyParam;

public interface NotifyExecutorService {

//	boolean notifyOutterSyetem(ParkVehiclePaymentRecord porder, String key);

	boolean notifyOutterSyetem(NotifyParam notifyParam, String key);

}
