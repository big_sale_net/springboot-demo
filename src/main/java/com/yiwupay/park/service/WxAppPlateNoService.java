package com.yiwupay.park.service;

import java.util.List;

import com.yiwupay.park.model.ParkVehicleWxAppPlateNo;

public interface WxAppPlateNoService {

	List<ParkVehicleWxAppPlateNo> getWxAppPlateNoList(String wxOpenid);

	void addWxAppPlateNo(ParkVehicleWxAppPlateNo wxAppPlateNo);

}
