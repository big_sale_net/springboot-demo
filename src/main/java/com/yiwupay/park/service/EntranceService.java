package com.yiwupay.park.service;

import java.util.List;

import com.yiwupay.park.model.ParkEntrance;

public interface EntranceService {

	List<ParkEntrance> getEntranceAll();

}
