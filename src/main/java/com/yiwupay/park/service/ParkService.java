package com.yiwupay.park.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.yiwupay.park.model.ParkParking;
import com.yiwupay.park.model.ParkVehiclePaymentOrder;
import com.yiwupay.park.model.ParkVehiclePaymentRecord;
import com.yiwupay.park.model.ParkVehicleRecord;
import com.yiwupay.park.model.result.ParkResult;
import com.yiwupay.park.model.result.ParkResultList;
import com.yiwupay.park.model.vo.ChargeBillVO;
import com.yiwupay.park.model.vo.ChargeRuleVO;
import com.yiwupay.park.model.vo.ParkParkingVO;
import com.yiwupay.park.model.vo.PayPage;
import com.yiwupay.park.model.vo.PaymentPage;
import com.yiwupay.park.model.web.ParkSystemVo;
import com.yiwupay.park.model.web.VehicleCostVo;
import com.yiwupay.park.model.web.VehicleSearchVo;
import com.yiwupay.park.model.web.VehicleStaticSearchVo;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public interface ParkService {

	/**
	 * 根据marketId获取PS_PARK_PARKING表的SYSTEM_ID,OUT_PARKING_ID,PARKING_CODE字段
	 * Description:
	 * 
	 * @param marketId
	 * @return
	 * @author cailuxi
	 * @date 2017年11月1日
	 */
	List<ParkParking> getParkParkingByMarketId(String marketId, HttpServletRequest request);

	/**
	 * 实时获取车辆信息 Description:
	 * 
	 * @param pp
	 * @param request
	 * @return
	 * @author cailuxi
	 * @date 2017年11月9日
	 */
	public ParkResultList<ParkParkingVO> getParkInfo(ParkParking pp, HttpServletRequest request);

	ParkResultList<ChargeRuleVO> getChargeRule(ParkVehicleRecord record, HttpServletRequest request);

	/** 根据车牌获取车辆订单信息 */
	ParkResult<ChargeBillVO> getPreChargeBill(ParkVehicleRecord record, HttpServletRequest request);

	/** 根据车牌号获取在场的车辆记录 */
	ParkVehicleRecord getNotOutVehicleRecordByPlateNo(String plateNo);

	// void saveVehicleRecord(ParkVehicleRecord inputRecord);

	boolean saveDHVehicleRecordExit(ParkVehicleRecord inputRecord);

	Boolean saveJHTVehicleRecordEnter(ParkVehicleRecord inputRecord);
	Boolean saveJHTVehicleRecordOut(ParkVehicleRecord inputRecord, String flag);

	List<ParkParking> getParkParkingAll();

	/** 初始化订单 */
	ParkVehiclePaymentOrder initOrder(ParkVehicleRecord record, ChargeBillVO bill);

	ParkVehiclePaymentOrder getPaymentOrder(Long order);

	void payRecev(ParkVehiclePaymentOrder order, ParkVehiclePaymentRecord record);

	ParkParking getParkParkingById(Long parkingId);

	JSONObject toPay(ParkVehiclePaymentOrder order, String ip, String wxAppId, String wxOpenId,
			ParkVehicleRecord parkVehicleRecord) throws Exception;

	List<PaymentPage> getPaymentPage(Map<String, Object> map);

	long getPaymentPageCount(Map<String, Object> map);

	int updatePaymentByPrimaryKey(ParkVehiclePaymentRecord recor);

	/**
	 * 获取停车场列表
	 * 
	 * @param map
	 * @return
	 */
	List<ParkSystemVo> getParkSystemList(Map<String, Object> map);

	/**
	 * 获取停车场总个数
	 * 
	 * @param map
	 * @return
	 */
	long getParkSystemListCount(Map<String, Object> map);

	/**
	 * 获取车辆查询list Description:
	 * 
	 * @param map
	 * @return
	 * @author cailuxi
	 * @date 2018年1月18日
	 */
	List<VehicleSearchVo> getVehicleSearchVoList(Map<String, Object> map);

	/**
	 * 获取车辆查询 count Description:
	 * 
	 * @param map
	 * @return
	 * @author cailuxi
	 * @date 2018年1月18日
	 */
	Long getVehicleSearchVoListCount(Map<String, Object> map);

	/**
	 * 流动车辆列表获取
	 * 
	 * @param map
	 * @return
	 */
	List<VehicleSearchVo> getVehicleList(Map<String, Object> map);

	/**
	 * 流动车辆总数获取
	 * 
	 * @param map
	 * @return
	 */
	long getVehicleListCount(Map<String, Object> map);

	/**
	 * 车辆统计列表获取（历史数据）
	 * 
	 * @param map
	 * @return
	 */
	List<VehicleStaticSearchVo> getVehicleStatisHisList(Map<String, Object> map);

	/**
	 * 车辆统计总数获取（历史数据）
	 * 
	 * @param map
	 * @return
	 */
	long getVehicleStatisHisListCount(Map<String, Object> map);

	/**
	 * 车辆统计列表获取（实时数据）
	 * 
	 * @param map
	 * @return
	 */
	List<VehicleStaticSearchVo> getVehicleStatisList(Map<String, Object> map);

	/**
	 * 车辆统计总数获取（实时数据）
	 * 
	 * @param map
	 * @return
	 */
	long getVehicleStatisListCount(Map<String, Object> map);

	JSONArray marketParkingSelector(String markets);

	/**
	 * 获取出入口名称
	 * 
	 * @param parkingId
	 * @return
	 */
	List<String> getInOutNameByParkingId(Long parkingId);

	List<VehicleCostVo> getVehicleCostVoByRecordStId(Long recordStId);

	/**
	 * @param recordStId
	 * @return
	 */
	VehicleSearchVo getCostAndType(Long recordStId);

	Long getJHTMaxUpdateTime();

	boolean saveDHVehicleRecordEnter(ParkVehicleRecord inputRecord);

	void saveHIKVehicleRecordEnter(ParkVehicleRecord inputRecord);

	void saveHIKVehicleRecordExit(ParkVehicleRecord inputRecord);

	List<PayPage> queryPaylist(Map<String, Object> map);

	long queryPaylistCount(Map<String, Object> map);

	int updateParkParing(ParkParking parkParking);

	int updateBySystemId(ParkParking pp);

}
