package com.yiwupay.park.service;


import java.util.Map;

import com.yiwupay.park.model.result.ParkResult;
import com.yiwupay.park.model.result.ParkResultList;
import com.yiwupay.park.model.vo.ChargeBillVO;
import com.yiwupay.park.model.vo.ChargeRuleVO;
import com.yiwupay.park.model.vo.ParkParkingVO;
import com.yiwupay.park.model.vo.PayBackVO;
/**
 * 停车管理中转接口Service
 */
public interface ParkSystemParkService {

	/**
	 * 获取停车场信息
	 * @param parkingIndexCodes
	 * @return
	 */
	ParkResultList<ParkParkingVO> getSysParkInfo(String systemCode, Map<String, Object> map);
	
	ParkResult<ChargeBillVO> getSysChargeBill(String systemCode, Map<String, Object> map);
	
	ParkResultList<ChargeRuleVO> getSysChargeRule(String systemCode, Map<String, Object> map);

	ParkResult getSysVehicleRecord(String systemCode, Map<String, Object> map);

	ParkResult<PayBackVO> paySysChargeBill(String systemCode, Map<String, Object> map);
}
