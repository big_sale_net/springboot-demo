package com.yiwupay.park.service;

import java.util.Map;

import com.yiwupay.park.model.result.ParkResultList;
import com.yiwupay.park.model.vo.ParkParkingVO;

/**
 * 停车场信息接口
 */
public interface ParkParkingService {

	/**
	 * 获取停车场信息
	 * @param parkingIndexCodes
	 * @return
	 */
	ParkResultList<ParkParkingVO> getParkInfo(Map<String, Object> paramMap);

}
