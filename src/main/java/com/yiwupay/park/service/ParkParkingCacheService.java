package com.yiwupay.park.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yiwupay.park.model.ParkParking;

@Service
public class ParkParkingCacheService {
	/*
	 * 缓存ParkParking数据
	 * key：systemId + "#" + outParkId
	 * value： parkingId
	 */
	private static Map<String, Long> parkIdMap;
	
	private static ParkService parkService;
	
	@Autowired
	public void setParkService(ParkService parkService) {
		ParkParkingCacheService.parkService = parkService;
	}
	
	public  ParkService getParkService() {
		return parkService;
	}
	
	public static Map<String, Long> getParkIdMap(){
		init();
		return parkIdMap;
	}
	
	public static void init() {
		if(parkIdMap==null || parkIdMap.size()<=0){
			parkIdMap = new HashMap<>();
			List<ParkParking> ppList = parkService.getParkParkingAll();
			for(ParkParking pp : ppList){
				String key = creatKeyOfParkIdMap(pp.getSystemId(),pp.getOutParkingId());
				Long value = pp.getParkingId();
				parkIdMap.put(key, value);
			}
		}
	}
	public static void clearCache(){
		parkIdMap = null;
	}
	
	public static String creatKeyOfParkIdMap(Long systemId, String outParkId){
		return systemId + "#" + outParkId;
	}
	
}
