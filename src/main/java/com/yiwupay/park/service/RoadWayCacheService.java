package com.yiwupay.park.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yiwupay.park.model.ParkRoadWay;

@Service
public class RoadWayCacheService {
	/*
	 * 缓存ParkParking数据
	 * key：systemId + "#" + outParkId
	 * value： parkingId
	 */
	private static Map<String, Long> roadWayIdMap;
	
	private static RoadWayService roadWayService;
	
	@Autowired
	public void setRoadWayService(RoadWayService roadWayService) {
		RoadWayCacheService.roadWayService = roadWayService;
	}
	
	public  RoadWayService getRoadWayService() {
		return roadWayService;
	}
	
	public static Map<String, Long> getRoadWayIdMap(){
		init();
		return roadWayIdMap;
	}
	
	public static void init() {
		if(roadWayIdMap==null || roadWayIdMap.size()<=0){
			roadWayIdMap = new HashMap<>();
			List<ParkRoadWay> rwList = roadWayService.getRoadWayAll();
			for(ParkRoadWay rw : rwList){
				String key = creatKey(rw.getSystemId(),rw.getOutRoadwayId());
				Long value = rw.getRoadwayId();
				roadWayIdMap.put(key, value);
			}
		}
	}
	public static void clearCache(){
		roadWayIdMap = null;
	}
	
	public static String creatKey(Long systemId, String outRoadwayId){
		return systemId + "#" + outRoadwayId;
	}
	
}
