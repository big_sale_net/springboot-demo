package com.yiwupay.park.service.impl.park;

import java.util.Map;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.listener.DefaultMessageListenerContainer;
import org.springframework.stereotype.Service;

import com.yiwupay.park.enums.BCFieldEnum;
import com.yiwupay.park.enums.OrderStatusEnum;
import com.yiwupay.park.enums.ResultEnum;
import com.yiwupay.park.model.bc.BCJsonToBean;
import com.yiwupay.park.model.result.ParkResult;
import com.yiwupay.park.model.result.ParkResultList;
import com.yiwupay.park.model.vo.ChargeBillVO;
import com.yiwupay.park.model.vo.ChargeRuleVO;
import com.yiwupay.park.model.vo.PayBackVO;
import com.yiwupay.park.service.ParkChargeService;
import com.yiwupay.park.utils.Constant;
import com.yiwupay.park.utils.DateUtil;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Service("BC")
@PropertySource("classpath:/parkconf/bc/bc.properties")
public class BCServiceImpl implements ParkChargeService {
	private Logger log = LoggerFactory.getLogger(BCServiceImpl.class);
	@Value("${bc.parkId}")
	String  parkId;
	@Autowired
	JmsTemplate jmsTemplate;
//	@Autowired
//	DefaultMessageListenerContainer bcContainerGetcharge;
//	@Autowired
//	DefaultMessageListenerContainer bcContainerPayonline;
	
	@Override
	public ParkResult<ChargeBillVO> getChargeBill(Map<String, Object> map) {
//		final JSONObject finalJson = new JSONObject();
//		final String timeStamp = System.currentTimeMillis()+"";
//		String destinationName = parkId + ".getcharge";
//		// 异步接收消息
//		bcContainerGetcharge.setMessageListener(new MessageListener() {
//			public void onMessage(Message msg) {
//				try {
//					TextMessage message = (TextMessage) msg;
//					String commandBack = message.getText();
//					//System.out.println("测试获取订单----------蓝卡mq返回"+commandBack);
//					JSONObject backJson = JSONObject.fromObject(commandBack);
//					if(backJson.containsKey(BCFieldEnum.timeStamp.name())){						
//						String timeStampback = backJson.getString(BCFieldEnum.timeStamp.name());
//						if(timeStampback.equals(timeStamp)){
//							finalJson.put(timeStamp, backJson);
//						}
//					}
//				} catch (Exception e) {
//					log.error("蓝卡系统-------获取订单mq消费异常 error={}",  e);
//				}
//
//			}
//		});
//		// 发送消息
//		JSONObject sendMsg = new JSONObject();
//		sendMsg.put(BCFieldEnum.plateId.name(), map.get("plateNo").toString().toUpperCase());
//		sendMsg.put(BCFieldEnum.recordId.name(), map.get("outRecordId"));
//		sendMsg.put(BCFieldEnum.timeStamp.name(), timeStamp);
//		jmsTemplate.convertAndSend(destinationName, sendMsg.toString());
//		long getUserTime = System.currentTimeMillis();
//		ParkResult<ChargeBillVO> result = new ParkResult<ChargeBillVO>();
//		while (true) {
//			if (System.currentTimeMillis() - getUserTime > 10000) {
//				result.setCode(ResultEnum.ERROR_SYS.getCode());
//				result.setMessage("蓝卡系统获取订单超时");
//				log.error("蓝卡系统-------------获取订单超时  map={} ", map);
//				return result;
//			}
//			if (finalJson.containsKey(timeStamp)) {
//				JSONObject backJson = finalJson.getJSONObject(timeStamp);
//				if(Constant.FAILCODE.equals(backJson.optString("status"))) {
//					result.setCode(ResultEnum.ERROR_SYS.getCode());
//					result.setMessage("蓝卡系统获取订单失败");
//					log.error("蓝卡系统------------获取订单失败  map={} backJson={}", map, backJson);
//					return result;
//				}
//				ChargeBillVO bill = BCJsonToBean.toChargeBillVO(backJson);
//				result.setCode(ResultEnum.SUCCESS.getCode());
//				result.setData(bill);
//				return result;
//			}
//		}
		return null;
	}

	@Override
	public ParkResultList<ChargeRuleVO> getChargeRule(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ParkResult<PayBackVO> payChargeBill(Map<String, Object> map) {
		final String timeStamp = System.currentTimeMillis()+"";
		final JSONObject finalJson = new JSONObject();
		/*String destinationName = parkId + ".payonline";
		bcContainerPayonline.setMessageListener(new MessageListener() {
			public void onMessage(Message msg) {
				try {
					TextMessage message = (TextMessage) msg;
					String commandBack = message.getText();
					//System.out.println("测试(通知支付)----------蓝卡mq返回"+commandBack);
					JSONObject backJson = JSONObject.fromObject(commandBack);
					if(backJson.containsKey(BCFieldEnum.timeStamp.name())){						
						String timeStampback = backJson.getString(BCFieldEnum.timeStamp.name());
						if(timeStampback.equals(timeStamp)){
							finalJson.put(timeStamp, backJson);
						}
					}
				} catch (Exception e) {
					log.error("蓝卡系统-------通知已支付（开闸）mq消费异常 error={}",  e);
				}

			}
		});
		// 发送消息
		JSONObject sendMsg = createMsg(map, timeStamp);
		//System.out.println("封装完成参数-----------"+sendMsg);
		jmsTemplate.convertAndSend(destinationName, sendMsg.toString());
		String plateNo = map.get("plateNo").toString();
		log.info("蓝卡系统——————["+plateNo+"] 已支付（开闸）通知已发送到mq");
		long getUserTime = System.currentTimeMillis();
		ParkResult<PayBackVO> result = new ParkResult<PayBackVO>();
		while (true) {
			if (System.currentTimeMillis() - getUserTime > 10000) {
				result.setCode(ResultEnum.ERROR_SYS.getCode());
				log.error("蓝卡系统-------通知已支付（开闸）无响应  map={} ", map);
				return result;
			}
			if (finalJson.containsKey(timeStamp)) {
				JSONObject backJson = finalJson.getJSONObject(timeStamp);
				if(Constant.SUCCESSORCODE.equals(backJson.optString(BCFieldEnum.status.name()))) {
					result.setCode(ResultEnum.SUCCESS.getCode());
					PayBackVO pb= new PayBackVO();
					pb.setStatus(OrderStatusEnum.pay_yes.name());
					//返回交易流水是车牌
					pb.setPlateNo(plateNo);
					result.setData(pb);
					return result;
				}else {
					result.setCode(ResultEnum.ERROR_SYS.getCode());
					log.error("蓝卡系统-------通知已支付（开闸）失败  map={} ", map);
				}
				return result;
			}
		}*/
		return null;
	}
	
	@Override
	public ParkResult getpayChargeBills(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return null;
	}

	private JSONObject createMsg(Map<String, Object> map, final String timeStamp) {
		JSONObject sendMsg = new JSONObject();
		String plateNo = map.get("plateNo").toString().toUpperCase();
		String payTime = DateUtil.stringFormatString(map.get("payTime").toString(), DateUtil.DB_YMDHMS_FORMAT);
		sendMsg.put(BCFieldEnum.plateId.name(), plateNo);
		sendMsg.put(BCFieldEnum.recordId.name(), map.get("outRecordId"));
		sendMsg.put(BCFieldEnum.payCharge.name(), map.get("realCost"));
		sendMsg.put(BCFieldEnum.realCharge.name(), map.get("cost"));
		sendMsg.put(BCFieldEnum.payTime.name(), payTime);
		sendMsg.put(BCFieldEnum.payType.name(), map.get("payType"));
		/*
		 * geiTime说明：
		 * 1.影响收费金额：此处发送getTime后，收费金额为getTime-inTime的时间计算停车费用，而停车时长却不变，只改变了费用
		 * 2.若getTime大于正常值异常发送导致费用改变后，用获取订单接口获取费用的getTime即使正常，费用也变大。 
		 * 3.若第一次发送的getTime小于第一次获取的getTime值，或后一次发送的getTime小于或等于前一次发送的getTime，
		 * 已支付费用并不会改变，即此次发送的已支付费用无效，返回仍是success
		 * 4.于是我取payTime做getTime, 如此可避免用户早获取订单迟支付而造成费用上的bug，也可避免上述几点的影响。
		 */
		sendMsg.put(BCFieldEnum.getTimes.name(), payTime);
		sendMsg.put(BCFieldEnum.profitTime.name(), 0);
		sendMsg.put(BCFieldEnum.profitCharge.name(), 0);
		sendMsg.put(BCFieldEnum.profitCode.name(), new JSONArray());
		//车牌作交易流水上传
		sendMsg.put(BCFieldEnum.orderNo.name(), plateNo); 
		sendMsg.put(BCFieldEnum.parkId.name(), parkId);
		sendMsg.put(BCFieldEnum.timeStamp.name(), timeStamp);
		return sendMsg;
	}

}
