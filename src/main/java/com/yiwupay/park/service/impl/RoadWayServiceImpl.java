package com.yiwupay.park.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yiwupay.park.dao.ParkRoadWayDao;
import com.yiwupay.park.model.ParkRoadWay;
import com.yiwupay.park.service.RoadWayService;
@Service
public class RoadWayServiceImpl implements RoadWayService{
	
	@Autowired
	ParkRoadWayDao parkRoadWayDao;
	
	@Override
	public List<ParkRoadWay> getRoadWayAll() {
		return parkRoadWayDao.selectAll();
	}
}
