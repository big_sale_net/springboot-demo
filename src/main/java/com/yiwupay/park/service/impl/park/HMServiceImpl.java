package com.yiwupay.park.service.impl.park;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import com.aliyun.mns.client.CloudAccount;
import com.aliyun.mns.client.CloudQueue;
import com.aliyun.mns.client.MNSClient;
import com.aliyun.mns.common.http.ClientConfiguration;
import com.aliyun.mns.model.Message;
import com.yiwupay.park.dao.ParkVehiclePaymentOrderDao;
import com.yiwupay.park.dao.ParkVehiclePaymentRecordDao;
import com.yiwupay.park.dao.ParkVehicleRecordDao;
import com.yiwupay.park.enums.HMErrorCodeEnum;
import com.yiwupay.park.enums.OrderStatusEnum;
import com.yiwupay.park.enums.ResultEnum;
import com.yiwupay.park.model.ParkVehicleRecord;
import com.yiwupay.park.model.hm.HmCommParam;
import com.yiwupay.park.model.hm.HmRecevEnter;
import com.yiwupay.park.model.hm.HmRecevOut;
import com.yiwupay.park.model.result.HMResult;
import com.yiwupay.park.model.result.ParkResult;
import com.yiwupay.park.model.result.ParkResultList;
import com.yiwupay.park.model.vo.ChargeBillVO;
import com.yiwupay.park.model.vo.ChargeRuleVO;
import com.yiwupay.park.model.vo.HMChargeBillVO;
import com.yiwupay.park.model.vo.HMParkParkingVO;
import com.yiwupay.park.model.vo.ParkParkingVO;
import com.yiwupay.park.model.vo.PayBackVO;
import com.yiwupay.park.service.ParkChargeService;
import com.yiwupay.park.service.ParkParkingService;
import com.yiwupay.park.service.ParkVehicleRecordService;
import com.yiwupay.park.utils.DateUtil;
import com.yiwupay.park.utils.JsonUtils;
import com.yiwupay.park.utils.park.HMHttpUtil;

import net.sf.json.JSONObject;

/**
 * 红门物业停车系统接入
 */
@Service("HM")
@PropertySource("classpath:/parkconf/hm/hm.properties")
public class HMServiceImpl implements ParkParkingService, ParkChargeService {
	
	private static Logger logger = LoggerFactory.getLogger(HMServiceImpl.class);
	
	/**
	 * 队列配置
	 */
	public static final String ACCESSKEY_ID = "LTAIjGopTDnBGrVx";
	public static final String ACCESSKEY_SECRET = "UMa8I98EL1krHwJ6P130nqxreJRJDk";
	public static final String ACCOUNTENDPOINT = "https://1550904678292021.mns.cn-shenzhen.aliyuncs.com";
	public static final String QUEUE = "parking-app-mns-bolanggu";
	
	/**
	 * 不同请求的Type
	 */
	public static final String ORDER_QUERY_TYPE = "TEMP_PARKING_ORDER_QUERY";
	public static final String PAY_NOTIFY_TYPE = "TEMP_PARKING_PAY_NOTIFY";
	public static final String  SPACE_QUERY_TYPE = "PARKING_SPACE_QUERY";
	
	/**
	 * 请求地址
	 */
	@Value("${hm.host}")
	private String host;
	@Value("${hm.appId}")
	private String appId;
	@Value("${hm.appSecret}")
	private String appSecret;
	@Value("${hm.systemid}")
	private long systemId;
	@Value("${hm.parkId}")
	private String parkId;
	
	
	@Autowired
	ParkVehicleRecordDao recordDao;
	@Autowired
	ParkVehiclePaymentRecordDao payRecordDao;
	@Autowired
	ParkVehiclePaymentOrderDao payOrderDao;
	@Autowired
    ParkVehicleRecordService parkVehicleRecordService;
	
	private static MNSClient client;
	static{
		ClientConfiguration clientConf = new ClientConfiguration();
		clientConf.setMaxConnections(50);
		clientConf.setIoReactorThreadCount(10);
		CloudAccount account = new CloudAccount(ACCESSKEY_ID, ACCESSKEY_SECRET, ACCOUNTENDPOINT, clientConf);
		client = account.getMNSClient();
	}
	
	
	
	
	
	/**
	 * 获取预付账单
	 */
	@Override
	public ParkResult<ChargeBillVO> getChargeBill(Map<String, Object> map) {
		try {
			JSONObject body = getPreBody();
			body.put("carPlateNo", map.get("plateNo"));
//			paramMap.put("cardNo", "");
			//获取前置参数
			JSONObject params = getPreParams();
			//请求类型
			params.put("type", ORDER_QUERY_TYPE);
			params.put("body", body);
			String json = HMHttpUtil.doPost(host, appSecret, params);
			JSONObject jsonObj = JSONObject.fromObject(json);
			if(json != null && HMErrorCodeEnum.SUCCESS.name().equals(jsonObj.get("code").toString())){
				System.out.println("红门物业停车系统——————获取预付订单成功：json{}"+json);
				HMResult result = (HMResult) JSONObject.toBean(jsonObj, HMResult.class);
				return ParkResult.createParkResult(result, HMChargeBillVO.class, ChargeBillVO.class);
			}else{
				logger.error("红门物业停车系统——————获取预付订单失败：json{},map{}", json.toString(), map.toString());
				HMResult result = (HMResult) JSONObject.toBean(jsonObj, HMResult.class);
				return ParkResult.createParkResult(result, null, null);
			}
		} catch (Exception e) {
			logger.error("红门物业停车系统——————获取预付订单失败 map{}", map.toString());
			logger.error("打印error", e);
			throw new RuntimeException("红门物业停车系统——————获取预付订单   ERR!!!");
		}
	}

	/**
	 * 临停支付通知
	 * */
	@Override
	public ParkResult<PayBackVO> payChargeBill(Map<String, Object> map) {
		try {
			JSONObject body = getPreBody();
			String plateNo = map.get("plateNo").toString().toUpperCase();
			body.put("carPlateNo", plateNo);
//			body.put("cardNo", "");
			body.put("parkingOrder",  map.get("outBillId"));
			body.put("parkingSerial", map.get("parkingSerial"));
			body.put("payTime", map.get("payTime"));
			body.put("payValue", map.get("cost"));
			body.put("payType", "3");
			body.put("payOriginDesc", "[vehicle-park-service]平台");
			
			//获取前置参数
			JSONObject params = getPreParams();
			//请求类型
			params.put("type", PAY_NOTIFY_TYPE);
			params.put("body", body);
			String json=HMHttpUtil.doPost(host, appSecret, params);
			logger.info("红门物业停车系统——————["+plateNo+"] 通知开闸成功响应 ");
			ParkResult<PayBackVO> result = new ParkResult<PayBackVO>();
			JSONObject jsonObj = JSONObject.fromObject(json);
			if(json != null && HMErrorCodeEnum.SUCCESS.name().equals(jsonObj.get("code").toString())){
				result.setCode(ResultEnum.SUCCESS.getCode());
				PayBackVO pb= new PayBackVO();
				pb.setStatus(OrderStatusEnum.pay_yes.name());
				pb.setPlateNo(plateNo);
				result.setData(pb);
				return result;
			}else{
				result.setCode(ResultEnum.ERROR_SYS.getCode());
				return result;
			}
		} catch (Exception e) {
			logger.error("红门物业停车系统——————临停支付通知失败 map{}", map.toString());
			logger.error("打印error", e);
			throw new RuntimeException("红门物业停车系统——————临停支付通知   ERR!!!");
		}
	}

	@Override
	public ParkResult getpayChargeBills(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public ParkResultList<ChargeRuleVO> getChargeRule(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return null;
	}
	/**
	 * 查询剩余车位数
	 */
	@Override
	public ParkResultList<ParkParkingVO> getParkInfo(Map<String, Object> map) {
		try {
			JSONObject body = getPreBody();
			//获取前置参数
			JSONObject params = getPreParams();
			//请求类型
			params.put("type", SPACE_QUERY_TYPE);
			params.put("body", body);
			String json = HMHttpUtil.doPost(host, appSecret, params);
			System.out.println("红门系统——————停车场信息："+json);
			HMResult result = JsonUtils.json2Object(json, HMResult.class);
			return ParkResultList.createParkResultList(result, HMParkParkingVO.class, ParkParkingVO.class);
		
		} catch (Exception e) {
			logger.error("打印error", e);
			logger.error("红门系统——————获取停车场信息失败 map{}", map.toString());
			throw new RuntimeException("红门系统——————获取停车场信息   ERR!!!");
		}
		
	}
	
	/**
	 * 保存过车记录
	 * @return
	 */
	
	public void saveVehiceRecord(){
		try {
			CloudQueue queue  = client.getQueueRef(QUEUE);
			long nanoTime = System.nanoTime();
			while(true) {
                List<Message> batchPopMessage = queue.batchPopMessage(10, 1);//每次批量取出10条，长轮询等待时间1秒，默认30秒
                if(batchPopMessage == null || batchPopMessage.size() == 0 ) {
                 	return;
                 }
                for (Message popMsg : batchPopMessage) {
                    String msg = popMsg.getMessageBodyAsString();
                    HmCommParam commParam = (HmCommParam) JSONObject.toBean(JSONObject.fromObject(msg), HmCommParam.class);
                    if("VEHICLE_REGISTER".equals(commParam.getType())) {
                    	queue.deleteMessage(popMsg.getReceiptHandle());
                    	continue;
                    }
                    System.out.println("红门消息队列获取消息数据  msg:{}"+msg);
                    if("VEHICLE_ENTER".equals(commParam.getType())){//进场
                    	HmRecevEnter result = (HmRecevEnter) JSONObject.toBean(JSONObject.fromObject(commParam.getBody()), HmRecevEnter.class);
                        boolean enterFlag = saveVehicleEnter(result);
                        if(enterFlag){
                        	//删除已经取出消费的消息
                            queue.deleteMessage(popMsg.getReceiptHandle());
                            logger.info(result.getVehiclePlate()+" [VEHICLE_ENTER] msg save suc, delete msg!!");
                       }
                     }else if("VEHICLE_EXIT".equals(commParam.getType())){//出场
                    	HmRecevOut result=(HmRecevOut) JSONObject.toBean(JSONObject.fromObject(commParam.getBody()),HmRecevOut.class);
                        boolean exitflag= saveVehicleExit(result);
                        if(exitflag){
                        	//删除已经取出消费的消息
                            queue.deleteMessage(popMsg.getReceiptHandle());
                            logger.info(result.getVehiclePlate()+" [VEHICLE_EXIT] msg save suc, delete msg!!");
                        }
                     }  
				} 
                if(System.nanoTime()-nanoTime>9999999000L) return; //循环运行9秒停止，外部定时器10秒运行一次方法
            }
		} catch (Exception e) {
			logger.error("红门系统-------推送数据拉取异常error！",e);
		}
	}
	
	/**
	 * 保存进场记录
	 * @param enterParam
	 */
	public Boolean saveVehicleEnter(HmRecevEnter enter){
		try {
			if(enter != null){
				if(StringUtils.isBlank(enter.getVehiclePlate())|| enter.getVehiclePlate().length()<7){
					logger.info("无车牌, 忽略这条推送");
					return true;
				}
				String enterTime = enter.getEnterTime();
				if(StringUtils.isNotBlank(enterTime)) {
					enterTime = DateUtil.timeMillisToLong(Long.parseLong(enterTime)).toString();
					enter.setEnterTime(enterTime);
				}
				logger.info(enter.getVehiclePlate()+" ["+enterTime+"]·····红门系统#车场编号"+enter.getParkingId()+"·····进场");
				System.out.println("红门系统入场纪录hmRecevEnter{}"+enter.toString());
				ParkVehicleRecord parkVehicleRecord = HmRecevEnter.toParkVehicleRecord(enter, systemId);
				Boolean booleanFlag = parkVehicleRecordService.saveHMVehicleRecordEnter(parkVehicleRecord);
				return booleanFlag;
		   }
			return true;
		} catch (Exception e) {
			logger.error("红门系统入场纪录保存失败hmRecevEnter{}", enter.toString());
			return false;
		}
		
	}
	
	/**
	 * 保存出场记录
	 * @param enterParam
	 */
	public Boolean saveVehicleExit(HmRecevOut exit){
	  try{
		if(exit!=null){
			if(StringUtils.isBlank(exit.getVehiclePlate()) || exit.getVehiclePlate().length()<7){
				logger.info("无车牌,忽略这条推送");
				return true;
			}
			String exitTime = exit.getExitTime();
			if(StringUtils.isNotBlank(exitTime)) {
				exitTime = DateUtil.timeMillisToLong(Long.parseLong(exitTime)).toString();
				exit.setExitTime(exitTime);
			}
			String enterTime = exit.getEnterTime();
			if(StringUtils.isNotBlank(enterTime)) {
				enterTime = DateUtil.timeMillisToLong(Long.parseLong(enterTime)).toString();
				exit.setEnterTime(enterTime);
			}
			logger.info(exit.getVehiclePlate()+" ["+exitTime+"]·····红门系统#车场编号"+exit.getParkingId()+"·····出场");
			System.out.println("红门系统出场纪录hmRecevOut{}"+ exit.toString());
			//将红门出场记录转化为系统过车记录
			ParkVehicleRecord parkVehicleRecord = HmRecevOut.toParkVehicleRecord(exit, systemId);
			Boolean booleanFlag = parkVehicleRecordService.saveHMVehicleRecevOut(parkVehicleRecord);
			return booleanFlag;
		}
		return true;
	} catch (Exception e) {
		logger.error("红门系统出场数据保存error！",e);
		return false;
	}
	
}
	
	/**
	 * 组装前置必须请求 params
	 * @return
	 */
	public JSONObject getPreParams(){
		JSONObject json = new JSONObject();
		//请求id
		String requestId = UUID.randomUUID().toString().replace("-", "");
		//时间戳
		Long timestamp = DateUtil.timeMillisToLong(System.currentTimeMillis()); 
		json.put("appId", appId);
		json.put("requestId", requestId);
		json.put("timestamp", timestamp);
		return json;
	}
	
	/**
	 * 组装前置必须请求 body
	 * @return
	 */
	public JSONObject getPreBody(){
		JSONObject json = new JSONObject();
		json.put("parkingId", parkId);
		return json;
	}

}
