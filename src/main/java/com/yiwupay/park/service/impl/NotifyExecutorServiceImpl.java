package com.yiwupay.park.service.impl;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.TimerTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import com.yiwupay.park.dao.ParkSystemDao;
import com.yiwupay.park.enums.OrderStatusEnum;
import com.yiwupay.park.enums.ResultEnum;
import com.yiwupay.park.model.NotifyParam;
import com.yiwupay.park.model.ParkSystem;
import com.yiwupay.park.model.result.ParkResult;
import com.yiwupay.park.model.vo.ChargeBillVO;
import com.yiwupay.park.model.vo.PayBackVO;
import com.yiwupay.park.service.NotifyExecutorService;
import com.yiwupay.park.service.ParkSystemParkService;
import com.yiwupay.park.utils.RedisUtil;

import net.sf.json.JSONObject;

/**
 * 
 * @author ZhuGuoku
 * @version 1.0 2017-8-25
 * 
 */
@Service("notifyExecutor")
public class NotifyExecutorServiceImpl extends TimerTask implements NotifyExecutorService{

	@Override
	public boolean notifyOutterSyetem(NotifyParam notifyParam, String key) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		
	}
//	Logger logger = LoggerFactory.getLogger(NotifyExecutorServiceImpl.class);
//	
//	@Autowired
//	private ParkSystemParkService parkSystemParkService;
//	@Autowired
//	private ParkSystemDao parkSystemDao;
//	
//	private ThreadPoolTaskExecutor threadPool;
//
//	public ThreadPoolTaskExecutor getThreadPool() {
//		return threadPool;
//	}
//
//	public void setThreadPool(ThreadPoolTaskExecutor threadPool) {
//		this.threadPool = threadPool;
//	}
//
//	public void resourceManage(String key) {
//		int keyLen = RedisUtil.llen(key).intValue();
//		
//		for (int i = 0; i<keyLen; i++) {
//			int isExecute = -1;
//			NotifyParam notifyParam = RedisUtil.lindex(key, 0, NotifyParam.class);
//			if (notifyParam == null) {
//				return;
//			} else {
//				logger.info("lindex:" + notifyParam.toString());
//			}
//
//			int second = 0;
//			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
//			try {
//				long from = sdf.parse(String.valueOf(notifyParam.getPayTime())).getTime();
//				long to = System.currentTimeMillis();
//				second = (int) ((to - from) / 1000);
//			} catch (Exception e) {
//				logger.error("日期转化失败",e);
//				return;
//			}
//			//10秒，30秒，1分，10分，30分
//			if ("list1".equals(key) && (10 <= second && second <= 30)) {
//				isExecute = 1;
//			} else if ("list2".equals(key) && (30 <= second && second <= 60)) {
//				isExecute = 1;
//			} else if ("list3".equals(key) && (60 <= second && second <= 600)) {
//				isExecute = 1;
//			} else if ("list4".equals(key) && (600 <= second && second <= 1800)) {
//				isExecute = 1;
//			} else {
//				if (second > 1800) {
//					isExecute = 0;
//					logger.info("通知接入系统开闸 out of time!" + "[" + "key:" + key + ", minutes:" + second/60 + " minutes]"
//							+ " drop from the queue");
//				}
//			}
//
//			if (isExecute > -1) {
//				notifyParam = RedisUtil.lpop(key, NotifyParam.class);
//				logger.info("通知接入系统开闸成功! notifyParam={} " + notifyParam.toString());
//				if (notifyParam != null && isExecute == 1) {
//					threadPool.execute(new NotifyTask(notifyParam, key));
//				}
//			}
//			
//		}
//
//			
//		
//	}
//
//	private class NotifyTask implements Runnable {
//		private NotifyParam notifyParam;
//		private String key;
//
//		public NotifyTask(NotifyParam notifyParam, String key) {
//			this.notifyParam = notifyParam;
//			this.key = key;
//		}
//
//		public void run() {
//			notifyOutterSyetem(notifyParam, key);
//		}
//	}
//
//	@Override
//	public void run() {
//		//System.out.println("支付通知任务启动...");
//		/**
//		 * list1- 10 分钟队列 list2- 30 分钟队列 lsit3- 60 分钟队列 list4- 120 分钟队列
//		 */
//		resourceManage("list1");
//		//System.out.println("start list1!");
//		resourceManage("list2");
//		//System.out.println("start list2!");
//		resourceManage("list3");
//		//System.out.println("start list3!");
//		resourceManage("list4");
//		//System.out.println("start list4!");
////		while (threadPool.getActiveCount() > 0) {
////			try {
////				Thread.sleep(1000);
////			} catch (Exception e) {
////				logger.error("打印error",e);
////			}
////		}
//		//System.out.println("支付通知任务结束!");
//	}
//
//	// 通知接入系统修改订单(开闸)
//	@Override
//	public boolean notifyOutterSyetem(NotifyParam notifyParam, String key) {
//		boolean nofityBack = false;
//		try {
//			JSONObject json = JSONObject.fromObject(notifyParam);
//			ParkSystem parkSystem = parkSystemDao.selectByPrimaryKey(notifyParam.getSystemId());
//			ParkResult<PayBackVO> parkResult = parkSystemParkService.paySysChargeBill(parkSystem.getSystemCode(), json);
//			logger.info("已发送通知! parkResult{}" + parkResult.toString());
//			if(ResultEnum.SUCCESS.getCode() == parkResult.getCode() && parkResult.getData()!=null){
//				//支付成功
//				if(OrderStatusEnum.pay_yes.name().equals(parkResult.getData().getStatus())){
//					nofityBack = true;
//					logger.info(parkResult.getData().getPlateNo()+"  通知接入系统车辆已支付(开闸)成功！");
//				}
//			}else{
//				ParkResult<ChargeBillVO> parkResult2 = parkSystemParkService.getSysChargeBill(parkSystem.getSystemCode(), json);
//				if(ResultEnum.SUCCESS.getCode()==parkResult.getCode()){
//					ChargeBillVO bill = parkResult2.getData();
//					if(new BigDecimal(bill.getRealCost()).compareTo(BigDecimal.ZERO)==0){
//						nofityBack = true;
//						logger.info(parkResult2.getData().getPlateNo()+"  通知接入系统车辆已支付(开闸)成功！");
//					}
//				}
//			}
//			
//		} catch (Exception e) {
//			logger.error("通知接入系统修改订单(开闸)error",e);
//		} finally {
//			// 通知失败或者响应失败
//			if (!nofityBack) {
//				long length = -1;
//				if ("list0".equals(key)) {
//					length = RedisUtil.rpush("list1", notifyParam);
//				} else if ("list1".equals(key)) {
//					length = RedisUtil.rpush("list2", notifyParam);
//				} else if ("list2".equals(key)) {
//					length = RedisUtil.rpush("list3", notifyParam);
//				} else if ("list3".equals(key)) {
//					length = RedisUtil.rpush("list4", notifyParam);
//				} else {
//					logger.error("通知接入系统失败5次 notifyParam={}", notifyParam.toString());
//				}
//				logger.info("length:" + length);
//			}
//		}
//
//		return nofityBack;
//	}

}
