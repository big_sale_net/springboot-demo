package com.yiwupay.park.service.impl.park;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import com.yiwupay.park.enums.OrderStatusEnum;
import com.yiwupay.park.enums.ResultEnum;
import com.yiwupay.park.enums.VehicleStatusEnum;
import com.yiwupay.park.model.ParkParking;
import com.yiwupay.park.model.ParkVehiclePaymentStatis;
import com.yiwupay.park.model.ParkVehicleRecordStatis;
import com.yiwupay.park.model.ParkVehicleRecordTemp;
import com.yiwupay.park.model.result.ParkResult;
import com.yiwupay.park.model.vo.HIKChargeBillData;
import com.yiwupay.park.model.vo.HIKVehicleRecordData;
import com.yiwupay.park.service.ParkChargeService;
import com.yiwupay.park.service.SynHIKService;
import com.yiwupay.park.service.VehicleRecordService;
import com.yiwupay.park.utils.DateUtil;
import com.yiwupay.park.utils.JsonUtils;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Service("hikSynService")
@PropertySource("classpath:/parkconf/hik/hik.properties")
public class HikSynServiceImpl extends SynBase implements SynHIKService {

	@Value("${hik.systemid}")
	long systemId;

	@Resource(name = "HIK")
	private VehicleRecordService vehicleRecordService;
	@Resource(name = "HIK")
	private ParkChargeService parkChargeService;

	/**
	 * 同步过车记录
	 * 
	 * @param startTime,endTime
	 *            时间戳
	 */
	@Override
	public boolean saveVehicleRecord(long startTime, long endTime) {
		int errorCount = 0; // 过车记录插入失败条数
		int countTemp = 0; // 出场记录插入临时表数
		int countStatis = 0; // 进场记录插入统计表数
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			// 获取海康系统所有停车场
			List<ParkParking> ppList = parkParkingDao.getParkParkingBySystemId(systemId);
			for (ParkParking pp : ppList) {
				int getVehicleRecordErrorCount = 0; // 记录获取海康系统过车记录数据失败次数
				Long parkId = pp.getParkingId();
				int pageNo = 0; // 记录页码
				do {
					map.put("outParkId", pp.getOutParkingId());
					map.put("pageNo", ++pageNo);
					map.put("pageSize", pageSize);
					map.put("startTime", startTime);
					map.put("endTime", endTime);
					ParkResult pr = null;
					try {
						pr = vehicleRecordService.getVehicleRecords(map);
					} catch (Exception e) {
						logger.error("海康系统————过车记录获取异常");
						logger.error("打印error",e);
					}
					if (pr != null && pr.getCode() == ResultEnum.SUCCESS.getCode()) {
						getVehicleRecordErrorCount = 0; // 获取成功，失败清零
						JSONObject json = JSONObject.fromObject(pr.getData());
						JSONArray recordList = JSONArray.fromObject(json.get("list"));
						for (Object obj : recordList) {
							JSONObject jsonsss = JSONObject.fromObject(obj);
							HIKVehicleRecordData record = JsonUtils.json2Object(jsonsss.toString(),
									HIKVehicleRecordData.class);

							if ("无车牌".equals(record.getPlateNo())) {
								logger.info("无车牌, 忽略这条数据");
								continue;
							}
							// 假如这是一条入场记录，则插入过车记录表
							if (VehicleStatusEnum.ENTER.getStatus().equals(record.getCarOut())) {
								ParkVehicleRecordStatis pvr_statis = parkVehicleRecordStatisDao
										.getModelBySystemAndOutRecordId(systemId, record.getRecordUuid());
								if (pvr_statis != null) {
									logger.info("数据已存在");
									continue;
								}
								ParkVehicleRecordTemp pvr_temp = HIKVehicleRecordData.toParkVehicleRecordTemp(record);
								pvr_temp.setSystemId(systemId);
								pvr_temp.setParkingId(parkId);
								try {
									parkVehicleRecordStatisDao.insertEnterRecordByTemp(pvr_temp);
									countStatis++;
								} catch (Exception e) {
									logger.error("海康系统————过车记录[" + record.getRecordUuid() + "]插入失败");
									logger.error("打印error",e);
									errorCount++;
								}
							}

							// 假如这是一条入场记录，则插入过车记录表临时表
							if (VehicleStatusEnum.OUT.getStatus().equals(record.getCarOut())) {
								ParkVehicleRecordTemp pvr_temp = parkVehicleRecordTempDao
										.getModelBySystemAndOutRecordId(systemId, record.getRecordUuid());
								if (pvr_temp != null) {
									logger.info("数据已存在");
									continue;
								}
								pvr_temp = HIKVehicleRecordData.toParkVehicleRecordTemp(record);
								pvr_temp.setSystemId(systemId);
								pvr_temp.setParkingId(parkId);
								try {
									parkVehicleRecordTempDao.insertSelective(pvr_temp);
									countTemp++;
								} catch (Exception e) {
									logger.error("海康系统————过车记录[" + record.getRecordUuid() + "]插入失败");
									logger.error("打印error",e);
									errorCount++;
								}
							}
						}
						int pageTotal = (int) Math.ceil(json.getInt("total") / pageSize); // 总页数
						logger.info("!!!!!!!!!!!!!!!总页数=" + pageTotal + "当前页数=" + pageNo);
						if (pageTotal <= pageNo)
							break; // 已经到最后一页
					} else {
						if (pr != null) {
							logger.error("海康系统————过车记录获取无异常，但获取失败:   errorCode=" + pr.getCode() + ", errorMessage="
									+ pr.getMessage());
						}
						if (++getVehicleRecordErrorCount == 5) {
							throw new RuntimeException("海康系统过车记录同步异常、失败累计五次!!");
						}
						pageNo--; // 回到当前页码
					}
				} while (true);
			}
			logger.info("海康系统————获取过车记录:" + "入场[" + countStatis + "]条，" + "出场[" + countTemp + "]条" + "失败[" + errorCount
					+ "]条");
			return true;
		} catch (Exception e) {
			logger.error("海康系统————" + DateUtil.dateToString(new Date()) + "同步数据出现异常");
			logger.error("打印error",e);
			return false;
		}
	}

	/**
	 * 同步出场记录到statis表
	 * 
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	@Override
	public boolean saveVehicleRecord2() {
		try {

			int count = 0;
			int errorCount = 0;
			List<ParkVehicleRecordStatis> records = parkVehicleRecordStatisDao
					.getRecoedsOfIsOut(VehicleStatusEnum.ENTER.getStatus());
			for (ParkVehicleRecordStatis record : records) {
				try {
					// 根据进场时间从临时表获取出场记录
					ParkVehicleRecordTemp record_temp = parkVehicleRecordTempDao.getModelByPlateNoAndEnterTime(
							record.getSystemId(), record.getPlateNo(), record.getEnterTime());
					if (record_temp != null) {
						record.setExitTime(record_temp.getCrosstime());
						record.setExitType(record_temp.getCrossType());
						record.setOutExitId(record_temp.getOutEntranceId());
						record.setOutExitRoadwayId(record_temp.getOutRoadwayId());
						record.setIsOut(VehicleStatusEnum.OUT.getStatus());
						parkVehicleRecordStatisDao.updateByPrimaryKeySelective(record);
						count++;
					}
				} catch (Exception e) {
					logger.error("recordStId=[" + record.getRecordStId() + "] 更新出场数据异常");
					logger.error("打印error",e);
					errorCount++;
				}
			}
			logger.info("海康系统———— 更新出场记录成功"+count+"条，失败"+errorCount+"条");
			return true;
		} catch (Exception e) {
			logger.error("系统异常  " + DateUtil.dateToString(new Date()) + " 更新出场记录失败");
			logger.error("打印error",e);
			return false;
		}

	}

	/**
	 * 同步缴费记录
	 * 
	 * @param startTime,endTime
	 *            时间戳
	 */
	@Override
	public boolean savePaymentRecord(long startTime, long endTime) {
		int errorCount = 0; // 记录错误条数
		int insertCount = 0; // 记录插入条数
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			// 获取海康系统所有停车场
			List<ParkParking> ppList = parkParkingDao.getParkParkingBySystemId(systemId);
			for (ParkParking pp : ppList) {
				Long parkId = pp.getParkingId();
				int getPaymentsErrorCount = 0; // 记录获取海康系统缴费记录数据失败次数
				int pageNo = 0; // 记录页码
				do {
					map.put("outParkId", pp.getOutParkingId());
					map.put("pageNo", ++pageNo);
					map.put("pageSize", pageSize);
					map.put("startTime", startTime);
					map.put("endTime", endTime);
					ParkResult pr = null;
					try {
						pr = parkChargeService.getpayChargeBills(map);
					} catch (Exception e) {
						logger.error("海康系统————缴费记录获取异常");
						logger.error("打印error",e);
					}
					if (pr != null && pr.getCode() == ResultEnum.SUCCESS.getCode()) {
						getPaymentsErrorCount = 0; // 获取成功，失败清零
						JSONObject json = JSONObject.fromObject(pr.getData());
						JSONArray billList = JSONArray.fromObject(json.get("list"));
						for (Object obj : billList) {
							JSONObject jsonsss = JSONObject.fromObject(obj);
							HIKChargeBillData bill = JsonUtils.json2Object(jsonsss.toString(), HIKChargeBillData.class);
							if(bill.getRealCost().longValue()==0L){
								logger.info("此订单收费为0，不保存");
								continue;
							}
							ParkVehiclePaymentStatis payment = parkVehiclePaymentStatisDao
									.getModelBySystemAndOutPaymentId(systemId, bill.getBillUuid());
							if (payment != null) {
								logger.info("数据已存在");
								continue;
							}
							try {
								payment = HIKChargeBillData.toParkVehiclePaymentStatis(bill);
								payment.setSystemId(systemId);
								payment.setOrderStatus(OrderStatusEnum.pay_yes.name());
								payment.setParkingId(parkId);
								// 利用入场时间和车牌获取过车记录
								ParkVehicleRecordStatis pvr_statis = parkVehicleRecordStatisDao
										.getModelByParams(systemId, payment.getPlateNo(), payment.getEnterTime());
								if(pvr_statis!=null){
									payment.setRecordId(pvr_statis.getRecordStId());
									payment.setOutRecordId(pvr_statis.getOutRecordId());
									//如果该记录为出场记录，则计算停车时长
									payment.setParkTime(getParkTime(pvr_statis));
								}
								parkVehiclePaymentStatisDao.insertSelective(payment);
								insertCount++;
							} catch (Exception e) {
								logger.error("海康系统————缴费记录[" + bill.getBillUuid() + "]插入/更新 失败");
								logger.error("打印error",e);
								errorCount++;
							}
						}

						int pageTotal = (int) Math.ceil(json.getInt("total") / pageSize); // 总页数
						logger.info("!!!!!!!!!!!!!!!总页数=" + pageTotal + "当前页数=" + pageNo);
						if (pageTotal <= pageNo)
							break; // 已经到最后一页
					} else {
						if (pr != null) {
							logger.error("海康系统————缴费记录获取无异常，但获取失败: errorCode=" + pr.getCode() + ", errorMessage="
									+ pr.getMessage());
						}
						if (++getPaymentsErrorCount == 5) {
							throw new RuntimeException("海康系统缴费记录同步异常、失败累计五次!!");
						}
						pageNo--; // 回到当前页码
					}
				} while (true);
			}
			logger.info("海康系统————缴费记录：" + "新增[" + insertCount + "]条，失败[" + errorCount + "]条");
			return true;
		} catch (Exception e) {
			logger.error("海康系统————" + DateUtil.dateToString(new Date()) + "同步数据出现异常");
			logger.error("打印error",e);
			return false;
		}
	}

//	public static void main(String[] args) {
//		double c = 3 / 2;
//		int a = (int) Math.ceil(c);
//		System.out.println("c===>" + a); // 2.0
//	}
}
