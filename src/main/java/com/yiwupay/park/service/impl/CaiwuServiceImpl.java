package com.yiwupay.park.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yiwupay.park.dao.ParkVehiclePaymentStatisDao;
import com.yiwupay.park.model.web.CaiwuStatisWeb;
import com.yiwupay.park.service.CaiwuService;

@Service
public class CaiwuServiceImpl implements CaiwuService {

	@Autowired
	private ParkVehiclePaymentStatisDao parkVehiclePaymentStatisDao;

	/**
	 * 获取财务统计列表
	 */
	@Override
	public List<CaiwuStatisWeb> getCaiwuStatisList(Map<String, Object> map) {
		return parkVehiclePaymentStatisDao.caiwuStatisList(map);
	}

	/**
	 * 获取财务统计数据量
	 */
	@Override
	public long getCaiwuStatisCount(Map<String, Object> map) {
		return parkVehiclePaymentStatisDao.caiwuStatisCount(map);
	}
}
