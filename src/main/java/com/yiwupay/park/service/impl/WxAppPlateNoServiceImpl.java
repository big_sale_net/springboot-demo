package com.yiwupay.park.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yiwupay.park.dao.ParkVehicleWxAppPlateNoDao;
import com.yiwupay.park.model.ParkVehicleWxAppPlateNo;
import com.yiwupay.park.service.WxAppPlateNoService;

@Service
public class WxAppPlateNoServiceImpl implements WxAppPlateNoService {

	Logger logger = LoggerFactory.getLogger(WxAppPlateNoServiceImpl.class);
	@Autowired
	private ParkVehicleWxAppPlateNoDao parkVehicleWxAppPlateNoDao;

	@Override
	public List<ParkVehicleWxAppPlateNo> getWxAppPlateNoList(String wxOpenid) {
		return parkVehicleWxAppPlateNoDao.selectByWxOpenid(wxOpenid);
	}

	@Override
	public void addWxAppPlateNo(ParkVehicleWxAppPlateNo wxAppPlateNo) {
		parkVehicleWxAppPlateNoDao.insertSelective(wxAppPlateNo);
	}
}
