package com.yiwupay.park.service.impl;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yiwupay.park.dao.ParkVehiclePaymentOrderDao;
import com.yiwupay.park.model.web.OrderSearchVo;
import com.yiwupay.park.service.WebOrderService;

@Service
public class WebOrderServiceImpl implements WebOrderService{

	@Autowired
	private ParkVehiclePaymentOrderDao parkVehiclePaymentOrderDao;
	
	@Override
	public List<OrderSearchVo> getOrderSearchList(Map<String, Object> map) {
		return parkVehiclePaymentOrderDao.getOrderSearchList(map);
	}

	@Override
	public long getOrderSearchListCount(Map<String, Object> map) {
		return parkVehiclePaymentOrderDao.getOrderSearchListCount(map);
	}

	@Override
	public BigDecimal getOrderSumCost(Map<String, Object> map) {
		return parkVehiclePaymentOrderDao.getOrderSumCost(map);
	}

}
