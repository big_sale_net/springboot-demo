package com.yiwupay.park.service.impl.park;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.jms.BytesMessage;
import javax.jms.Message;
import javax.jms.MessageListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import com.google.protobuf.ByteString;
import com.yiwupay.park.enums.ResultEnum;
import com.yiwupay.park.enums.VehicleStatusEnum;
import com.yiwupay.park.model.ParkVehicleRecord;
import com.yiwupay.park.model.result.HikResult;
import com.yiwupay.park.model.result.ParkResult;
import com.yiwupay.park.model.result.ParkResultList;
import com.yiwupay.park.model.vo.ChargeBillVO;
import com.yiwupay.park.model.vo.ChargeRuleVO;
import com.yiwupay.park.model.vo.HIKChargeBillVO;
import com.yiwupay.park.model.vo.HIKChargeRuleVO;
import com.yiwupay.park.model.vo.HIKParkParkingVO;
import com.yiwupay.park.model.vo.HIKPayBackVO;
import com.yiwupay.park.model.vo.ParkParkingVO;
import com.yiwupay.park.model.vo.PayBackVO;
import com.yiwupay.park.service.ParkChargeService;
import com.yiwupay.park.service.ParkParkingCacheService;
import com.yiwupay.park.service.ParkParkingService;
import com.yiwupay.park.service.ParkService;
import com.yiwupay.park.service.VehicleRecordService;
import com.yiwupay.park.utils.DateUtil;
import com.yiwupay.park.utils.JsonUtils;
import com.yiwupay.park.utils.MapCacheUtil;
import com.yiwupay.park.utils.park.HikEmuServerDevEvent.CEmuEvent;
import com.yiwupay.park.utils.park.HikEmuServerDevEvent.CEmuInResult;
import com.yiwupay.park.utils.park.HikEmuServerDevEvent.CEmuOutResult;
import com.yiwupay.park.utils.park.HikEmuServerDevEvent.CEmuRlsResult;
import com.yiwupay.park.utils.park.HikEventDis;
import com.yiwupay.park.utils.park.HikHttpUtil;
import com.yiwupay.park.utils.park.HikPmsEvent;

/**
 * 海康威视接口服务
 * 
 * @author he
 */
@Service("HIK")
@PropertySource("classpath:/parkconf/hik/hik.properties")
public class HikServiceImpl implements MessageListener, ParkParkingService, ParkChargeService, VehicleRecordService {

	private Logger logger = LoggerFactory.getLogger(HikServiceImpl.class);
	/**
	 * 网关地址
	 */
	@Value("${hik.host}")
	private String host;
	/**
	 * ： iVMS-8700 平台 SDK HTTP-OpenAPI 提供给第三方接入的唯一标识符参数
	 */
	@Value("${hik.appkey}")
	private String appkey;
	/**
	 * 密钥，用于生成平台 SDK HTTP-OpenAPI 校验的 token 值；
	 */
	@Value("${hik.secret}")
	private String secret;

	@Value("${hik.opuseruuid}")
	private String opUserUuid;

	@Value("${hik.systemid}")
	private long systemId;
	/**
	 * 进场事件类型编码
	 */
	@Value("${hik.eventtype.enter}")
	private int enter;
	/**
	 * 出场事件类型编码
	 */
	@Value("${hik.eventtype.exit}")
	private int exit;

	@Autowired
	private ParkService parkService;

	private Map<String, Object> getParmMap() {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("opUserUuid", opUserUuid);
		return paramMap;
	}

	/**
	 * 获取停车场信息
	 */
	@Override
	public ParkResultList<ParkParkingVO> getParkInfo(Map<String, Object> map) {
		try {
			Map<String, Object> paramMap = getParmMap();
			paramMap.put("parkUuids", map.get("outParkId"));
			String method = "pms/res/getParkingInfosByParkUuids";
			String json = HikHttpUtil.doPost(host, method, paramMap, appkey, secret);
			// System.out.println("海康系统——————停车场信息：" + json);
			HikResult result = JsonUtils.json2Object(json, HikResult.class);
			// 成功返回
			return ParkResultList.createParkResultList(result, HIKParkParkingVO.class, ParkParkingVO.class);
		} catch (Exception e) {
			logger.error("海康系统——————获取停车场信息失败   map{}", map.toString());
			logger.error("打印error", e);
			throw new RuntimeException("海康系统——————获取停车场信息   ERR!!!");
		}
	}

	/**
	 * 获取预付订单
	 */
	@Override
	public ParkResult<ChargeBillVO> getChargeBill(Map<String, Object> map) {
		try {
			Map<String, Object> paramMap = getParmMap();
			paramMap.put("parkUuid", map.get("outParkId"));
			paramMap.put("plateNo", map.get("plateNo").toString().toUpperCase());
			String method = "pms/charge/getChargeBill";
			String json = HikHttpUtil.doPost(host, method, paramMap, appkey, secret);
			// System.out.println("海康系统——————订单场信息：" + json);
			HikResult result = JsonUtils.json2Object(json, HikResult.class);
			// 成功返回
			return ParkResult.createParkResult(result, HIKChargeBillVO.class, ChargeBillVO.class);
		} catch (Exception e) {
			logger.error("海康系统——————获取预付订单失败  map{}", map.toString());
			logger.error("打印error", e);
			throw new RuntimeException("海康系统——————获取预付订单   ERR!!!");
		}
	}

	/**
	 * 通知接入系统车辆已支付-----车辆可以通过
	 */
	@Override
	public ParkResult<PayBackVO> payChargeBill(Map<String, Object> map) {
		try {
			Map<String, Object> paramMap = getParmMap();
			paramMap.put("preBillUuid", map.get("outBillId"));
			paramMap.put("totalCost", String.format("%.2f", Integer.parseInt(map.get("totalCost").toString()) / 100.0));// 总金额
			paramMap.put("realCost", String.format("%.2f", Integer.parseInt(map.get("realCost").toString()) / 100.0));// 应收金额
			paramMap.put("cost", String.format("%.2f", Integer.parseInt(map.get("cost").toString()) / 100.0));// 支付金额
			// System.out.println("通知海康系统支付成功——————"+paramMap.toString());
			String method = "pms/charge/payVehilceBill";
			String json = HikHttpUtil.doPost(host, method, paramMap, appkey, secret);
			logger.info("海康系统——————" + map.get("plateNo") + "  通知车辆已支付返回信息  SUC!!!");
			HikResult result = JsonUtils.json2Object(json, HikResult.class);
			ParkResult<PayBackVO> parkResult = new ParkResult<>(result.getErrorCode(), result.getErrorMessage());
			Object data = result.getData();
			if (result.getErrorCode() == ResultEnum.SUCCESS.getCode() && data != null) {
				HIKPayBackVO hikpb = JsonUtils.conveterObject(data, HIKPayBackVO.class);
				parkResult.setData(HIKPayBackVO.toPayBackVO(hikpb));
			}
			// 成功返回
			return parkResult;
		} catch (Exception e) {
			logger.error("海康系统——————通知接入系统车辆已支付(开闸)失败 map{}", map.toString());
			logger.error("打印error", e);
			throw new RuntimeException("海康系统——————通知车辆已支付   ERR!!!");
		}
	}

	/**
	 * 获取收费规则
	 */
	@Override
	public ParkResultList<ChargeRuleVO> getChargeRule(Map<String, Object> map) {
		try {
			Map<String, Object> paramMap = getParmMap();
			paramMap.put("parkUuid", map.get("outParkId"));
			String method = "pms/res/getChargeRule";
			String json = HikHttpUtil.doPost(host, method, paramMap, appkey, secret);
			// System.out.println("海康系统——————停车场收费规则：" + json);
			HikResult result = JsonUtils.json2Object(json, HikResult.class);
			// 成功返回
			return ParkResultList.createParkResultList(result, HIKChargeRuleVO.class, ChargeRuleVO.class);
		} catch (Exception e) {
			logger.error("海康系统——————获取停车场规则失败  map{}", map.toString());
			logger.error("打印error", e);
			throw new RuntimeException("海康系统——————获取停车场规则   ERR!!!");
		}
	}

	/******************************************
	 * mq事件订阅 start
	 *******************************/

	public String getEventsFromMQEx() {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("opUserUuid", opUserUuid);
		map.put("eventTypes", "524545,524546"); // 入口,出口

		String method = "eps/subscribeEventsFromMQEx"; // 获取事件mq
		String json = HikHttpUtil.doPost(host, method, map, appkey, secret);
		// System.out.println(json);
		return json;
	}

	// public static void main(String[] args) {
	// new HikServiceImpl().getEventsFromMQEx(); //事件订阅 执行
	// }

	@Override
	public ParkResult getVehicleRecords(Map<String, Object> map) {
		try {

			Map<String, Object> paramMap = getParmMap();
			paramMap.put("parkUuid", map.get("outParkId"));
			if (map.containsKey("pageNo")) {
				paramMap.put("pageNo", map.get("pageNo")); // 当前页码
			} else {
				paramMap.put("pageNo", 1);
			}
			paramMap.put("pageSize", map.get("pageSize")); // 每页记录数
			paramMap.put("startTime", map.get("startTime"));// 开始的过车时间
			paramMap.put("endTime", map.get("endTime"));// 结束的过车时间
			String method = "pms/record/getVehicleRecords";
			String json = HikHttpUtil.doPost(host, method, paramMap, appkey, secret);
			// System.out.println("海康系统——————过车记录：" + json);
			HikResult result = JsonUtils.json2Object(json, HikResult.class);
			// 成功返回
			ParkResult parkResult = ParkResult.createParkResult(result);
			parkResult.setData(result.getData());
			return parkResult;
		} catch (Exception e) {
			logger.error("海康系统——————获取过车记录失败  map{}", map.toString());
			logger.error("打印error", e);
			throw new RuntimeException("海康系统——————获取过车记录   ERR!!!");
		}
	}

	@Override
	public ParkResult getpayChargeBills(Map<String, Object> map) {
		try {

			Map<String, Object> paramMap = getParmMap();
			paramMap.put("parkUuid", map.get("outParkId"));
			if (map.containsKey("pageNo")) {
				paramMap.put("pageNo", map.get("pageNo")); // 当前页码
			} else {
				paramMap.put("pageNo", 1);
			}
			paramMap.put("pageSize", map.get("pageSize")); // 每页记录数
			paramMap.put("startTime", map.get("startTime"));// 开始的过车时间
			paramMap.put("endTime", map.get("endTime"));// 结束的过车时间
			String method = "pms/record/getTempCarChargeRecords";
			String json = HikHttpUtil.doPost(host, method, paramMap, appkey, secret);
			// System.out.println("海康系统——————过车记录：" + json);
			HikResult result = JsonUtils.json2Object(json, HikResult.class);
			// 成功返回
			ParkResult parkResult = ParkResult.createParkResult(result);
			parkResult.setData(result.getData());
			return parkResult;
		} catch (Exception e) {
			logger.error("海康系统——————获取缴费记录失败  map{}", map.toString());
			logger.error("打印error", e);
			throw new RuntimeException("海康系统——————获取缴费记录   ERR!!!");
		}
	}

	@Override
	public void onMessage(Message msg) {
		String plateNo = null;
		try {
			// cms里发送的消息为BytesMessage，此处不做判断亦可
			// if (msg instanceof BytesMessage) {
			BytesMessage bytesMessage = (BytesMessage) msg;
			long length = bytesMessage.getBodyLength();
			// System.out.println(length);
			byte[] bt = new byte[(int) length];
			// 将BytesMessage转换为byte类型
			bytesMessage.readBytes(bt);
			// 壳文件字段，EventDis类为event_dis.proto文件解析而来，CommEventLog类为事件壳文件类
			HikEventDis.CommEventLog parseFrom = HikEventDis.CommEventLog.parseFrom(bt);
			// 事件类型
			int eventType = parseFrom.getEventType();
			// System.out.println(eventType);
			// 扩展字段，此字段为设备上报事件内容，部分事件需要使用pb文件再次解析
			ByteString extInfo = parseFrom.getExtInfo();
			byte[] b = extInfo.toByteArray();
			HikPmsEvent.MsgPmsEvent msgPmsEvent = HikPmsEvent.MsgPmsEvent.parseFrom(b);
			CEmuEvent event = msgPmsEvent.getPmsEvent();
			plateNo = event.getPlateNo();
			if ("无车牌".equals(plateNo)) {
				logger.info("无车牌, 忽略这条推送");
				return;
			}
			if (enter == eventType && event.getEventCmd() == 3) {
				logger.info(plateNo + " [" + new Date() + "] ·········五区西侧内环道(海康)停车场车辆数 ········· 进场");
				vehicleEnter(event);
			} else if (exit == eventType && event.getEventCmd() == 4) {
				logger.info(plateNo + " [" + new Date() + "] ·········五区西侧内环道(海康)停车场车辆数 ········· 出场");
				vehicleExit(event);
			}
			// msg.acknowledge();
			// }
		} catch (Exception e) {
			logger.error("过车记录保存失败！！！ plateNo={} error={} ", plateNo, e);
			throw new RuntimeException(plateNo + "保存失败");
		}
	}

	public void vehicleExit(CEmuEvent event) {
		CEmuOutResult outResult = event.getOutResult();
		if (outResult != null) {
			CEmuRlsResult rls = outResult.getRlsResult();
			if (isPass(rls)) {
				ParkVehicleRecord inputRecord = new ParkVehicleRecord();
				inputRecord.setSystemId(systemId);
				inputRecord.setPlateNo(event.getPlateNo());// 获取车牌号
				inputRecord.setIsOut(VehicleStatusEnum.OUT.getStatus());
				inputRecord.setOutTime(DateUtil.dateToLong_YMDHMS(new Date()));
				inputRecord.setOutType(MapCacheUtil.CROSS_TYPE.get(rls.getReleaseResult()));
				inputRecord.setOutRoadwayIdExit(event.getRoadwayIndex());
				parkService.saveHIKVehicleRecordExit(inputRecord);
			}
		}

	}

	public void vehicleEnter(CEmuEvent event) {
		CEmuInResult inResult = event.getInResult();
		if (inResult != null) {
			CEmuRlsResult rls = inResult.getRlsResult();
			if (isPass(rls)) {
				ParkVehicleRecord inputRecord = new ParkVehicleRecord();
				inputRecord.setPlateNo(event.getPlateNo());// 获取车牌号
				inputRecord.setSystemId(systemId);
				inputRecord.setIsOut(VehicleStatusEnum.ENTER.getStatus());
				inputRecord.setOutParkingId(event.getParkIndex());
				inputRecord.setEnterType(MapCacheUtil.CROSS_TYPE.get(rls.getReleaseResult()));
				inputRecord.setEnterTime(DateUtil.dateToLong_YMDHMS(new Date()));
				inputRecord.setOutEntranceId(event.getGateIndex());// 出入口id
				inputRecord.setOutRoadwayId(event.getRoadwayIndex());// 车道id
				inputRecord.setCarColor(MapCacheUtil.HIK_CAR_COLOR.get(event.getVehicleColor()));
				inputRecord.setCarType(MapCacheUtil.HIK_CAR_TYPE.get(event.getVehicleType()));
				String key_parkingIdMap = ParkParkingCacheService.creatKeyOfParkIdMap(systemId, event.getParkIndex());
				inputRecord.setParkingId(ParkParkingCacheService.getParkIdMap().get(key_parkingIdMap));
				parkService.saveHIKVehicleRecordEnter(inputRecord);
			}
		}
	}

	/** 判断车辆是否真实通过 */
	private boolean isPass(CEmuRlsResult rlsResult) {
		try {
			int release = rlsResult.getReleaseResult();
			if (release != 0 && release != 30) {
				return true;
			}
		} catch (Exception e) {
			logger.error("解析海康系统推送数据----判断车辆是否真实通过异常", e);
		}
		return false;
	}

	/******************************************
	 * mq事件订阅 end
	 *******************************/

	@Override
	public ParkResult getVehicleAssessRecords(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return null;
	}

}
