package com.yiwupay.park.service.impl; 

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import com.yiwupay.park.dao.ParkParkingDao;
import com.yiwupay.park.dao.ParkRoadWayDao;
import com.yiwupay.park.dao.ParkSystemDao;
import com.yiwupay.park.dao.ParkVehiclePaymentOrderDao;
import com.yiwupay.park.dao.ParkVehiclePaymentRecordDao;
import com.yiwupay.park.dao.ParkVehiclePaymentStatisDao;
import com.yiwupay.park.dao.ParkVehicleRecordDao;
import com.yiwupay.park.dao.ParkVehicleRecordStatisDao;
import com.yiwupay.park.enums.OrderStatusEnum;
import com.yiwupay.park.enums.ResultEnum;
import com.yiwupay.park.enums.VehicleStatusEnum;
import com.yiwupay.park.model.MarketParkingSelector;
import com.yiwupay.park.model.ParkParking;
import com.yiwupay.park.model.ParkSystem;
import com.yiwupay.park.model.ParkVehiclePaymentOrder;
import com.yiwupay.park.model.ParkVehiclePaymentRecord;
import com.yiwupay.park.model.ParkVehicleRecord;
import com.yiwupay.park.model.result.ParkResult;
import com.yiwupay.park.model.result.ParkResultList;
import com.yiwupay.park.model.vo.ChargeBillVO;
import com.yiwupay.park.model.vo.ChargeRuleVO;
import com.yiwupay.park.model.vo.ParkParkingVO;
import com.yiwupay.park.model.vo.PayPage;
import com.yiwupay.park.model.vo.PaymentPage;
import com.yiwupay.park.model.web.ParkSystemVo;
import com.yiwupay.park.model.web.VehicleCostVo;
import com.yiwupay.park.model.web.VehicleSearchVo;
import com.yiwupay.park.model.web.VehicleStaticSearchVo;
import com.yiwupay.park.service.ParkService;
import com.yiwupay.park.service.ParkSystemParkService;
import com.yiwupay.park.utils.Base64Utils;
import com.yiwupay.park.utils.DateUtil;
import com.yiwupay.park.utils.HttpUtils;
import com.yiwupay.park.utils.RSAUtils;
import com.yiwupay.park.utils.TempKeyStore;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Service
@PropertySource("classpath:/parkconf/payment_order.properties")
public class ParkServiceImpl implements ParkService {
	Logger logger = LoggerFactory.getLogger(ParkServiceImpl.class);

	@Autowired
	private ParkRoadWayDao parkRoadWayDao;

	@Autowired
	private ParkParkingDao parkParkingDao;

	@Autowired
	private ParkSystemDao parkSystemDao;

	@Autowired
	private ParkVehicleRecordDao parkVehicleRecordDao;

	@Autowired
	private ParkVehiclePaymentRecordDao parkVehiclePaymentRecordDao;

	@Autowired
	private ParkVehiclePaymentOrderDao parkVehiclePaymentOrderDao;

	@Autowired
	private ParkSystemParkService parkSystemParkService;

	@Autowired
	private ParkVehicleRecordStatisDao parkVehicleRecordStatisDao;

	@Autowired
	private ParkVehiclePaymentStatisDao parkVehiclePaymentStatisDao;

	@Value("${belongApp}")
	String belongApp;
	@Value("${bankName}")
	String bankName;
	@Value("${bankInterface}")
	String bankInterface;
	@Value("${interfaceId}")
	String interfaceId;
	@Value("${getOrderUrl}")
	String getOrderUrl;
	@Value("${innerNotifyUrl}")
	String innerNotifyUrl;
	@Value("${version}")
	String version;
	@Value("${servicename}")
	String servicename;

	@Override
	public List<ParkParking> getParkParkingAll() {
		return parkParkingDao.selectAll();
	}

	@Override
	public ParkParking getParkParkingById(Long parkingId) {
		return parkParkingDao.selectByPrimaryKey(parkingId);
	}
	
	@Override
	public int updateBySystemId(ParkParking pp) {
		return parkParkingDao.updateBySystemId(pp);
	}

	@Transactional
	public List<ParkParking> getParkParkingByMarketId(String marketId, HttpServletRequest request) {
		List<ParkParking> pplist = parkParkingDao.getParkParkingByMarketId(marketId);
		//篁园更新车位机制不同，可直接返回
		if("10008".equals(marketId)) {
			return pplist;
		}
		// 遍历
		for (int i = 0; i < pplist.size(); i++) {
			// 调用开发公司接口，获取停车场数据
			ParkParking pp = pplist.get(i);
			ParkResultList<ParkParkingVO> parkResult = getParkInfo(pp, request);
			// 判断请求是否成功
			if (parkResult != null && parkResult.getCode() == ResultEnum.SUCCESS.getCode()) {
				ParkParkingVO ppVo = parkResult.getData().get(0);
				pp.setLeftPlace(ppVo.getLeftPlace());// 剩余车位数
				pp.setTotalPlace(ppVo.getTotalPlace());// 总车位数
				pp.setUpdateTime(DateUtil.dateToLong_YMDHMS(new Date()));
				pp.setRemark("0"); // 接口正常
			} else {
				pp.setRemark("1"); // 接口异常

			}
			parkParkingDao.updateByPrimaryKeySelective(pp);
		}

		return pplist;

	}

	public ParkResultList<ParkParkingVO> getParkInfo(ParkParking pp, HttpServletRequest request) {
		try {
			Long systemId = pp.getSystemId();
			ParkSystem parkSystem = parkSystemDao.selectByPrimaryKey(systemId); // 获取接入系统信息
			Map<String, Object> map = HttpUtils.convertRequestParam(request);
			map.put("outParkId", pp.getOutParkingId());
			return parkSystemParkService.getSysParkInfo(parkSystem.getSystemCode(), map);
		} catch (Exception e) {
			logger.error("获取停车场信息error  parkingName{}",e, pp.getParkingName());
			return null;
		}
	}

	/**
	 * 获取停车场临时车收费规则
	 */
	@Override
	public ParkResultList<ChargeRuleVO> getChargeRule(ParkVehicleRecord record, HttpServletRequest request) {
		Long systemId = record.getSystemId();
		ParkSystem parkSystem = parkSystemDao.selectByPrimaryKey(systemId); // 获取接入系统信息
		// 封装请求接入系统参数
		Map<String, Object> map = HttpUtils.convertRequestParam(request);
		map.put("outParkId", record.getOutParkingId());
		return parkSystemParkService.getSysChargeRule(parkSystem.getSystemCode(), map);

	}

	/************************** 过车记录相关start *********************************/

	/** 根据车牌号获取在场的车辆记录 */
	@Override
	public ParkVehicleRecord getNotOutVehicleRecordByPlateNo(String plateNo) {
		List<ParkVehicleRecord> vehicleRecordList = parkVehicleRecordDao.selectByPlateNo(plateNo.toUpperCase(),
				VehicleStatusEnum.ENTER.getStatus());// 未出场
		return vehicleRecordList != null && vehicleRecordList.size() > 0 ? vehicleRecordList.get(0) : null;
	}

	/**
	 * 海康进场记录保存
	 */
	@Override
	@Transactional
	public void saveHIKVehicleRecordEnter(ParkVehicleRecord inputRecord) {
		try {
			ParkVehicleRecord vehicleRecord = getNotOutVehicleRecordByPlateNo(inputRecord.getPlateNo());
			if (vehicleRecord == null) {
				// 无在场车辆记录，则将新的进场记录插入
				parkVehicleRecordDao.insertSelective(inputRecord);
			} else {
				/* 已存在未出场的记录 */
				// 1.假如保存的记录停车系统与推送的记录停车系统不同，则更新其为出场，再新增一条过车记录。
				if (vehicleRecord.getSystemId().longValue() != inputRecord.getSystemId().longValue()) {
					vehicleRecord.setIsOut(VehicleStatusEnum.OUT.getStatus());
					parkVehicleRecordDao.updateByPrimaryKeySelective(vehicleRecord);
					parkVehicleRecordDao.insertSelective(inputRecord);
					return;
				}
				List<ParkVehiclePaymentRecord> list = parkVehiclePaymentRecordDao
						.getModelByRecordId(vehicleRecord.getRecordId());
				// 2.假如保存的记录已缴费，则更新其为出场，再新增一条过车记录.
				if (list != null && list.size() > 0) {
					vehicleRecord.setIsOut(VehicleStatusEnum.OUT.getStatus());
					parkVehicleRecordDao.updateByPrimaryKeySelective(vehicleRecord);
					parkVehicleRecordDao.insertSelective(inputRecord);
					return;
				}
				// 3.假如推送的记录入场时间大于保存记录的入场时间，则更新改记录为新的记录
				if (inputRecord.getEnterTime().longValue() > vehicleRecord.getEnterTime().longValue()) {
					inputRecord.setRecordId(vehicleRecord.getRecordId());
					parkVehicleRecordDao.updateByPrimaryKeySelective(inputRecord);
				}
			}
		} catch (Exception e) {
		
			logger.error(
					"海康进场记录保存失败 plateNo{}, enterTime{}" ,inputRecord.getPlateNo() ,inputRecord.getEnterTime());
			logger.error("海康进场记录保存error",e);

		}
	}

	/**
	 * 海康出场记录保存
	 */
	@Override
	@Transactional
	public void saveHIKVehicleRecordExit(ParkVehicleRecord inputRecord) {
		try {
			ParkVehicleRecord vehicleRecord = getNotOutVehicleRecordByPlateNo(inputRecord.getPlateNo());
			if (vehicleRecord != null
					// 保存的记录停车系统与推送的记录停车系统是否一样
					&& vehicleRecord.getSystemId().longValue() == inputRecord.getSystemId().longValue()
					// 出场时间大于进场时间
					&& inputRecord.getOutTime().longValue() > vehicleRecord.getEnterTime().longValue()) {

				inputRecord.setRecordId(vehicleRecord.getRecordId());
				parkVehicleRecordDao.updateByPrimaryKeySelective(inputRecord);
			}
		} catch (Exception e) {
			logger.error(
					"海康出场记录保存失败 plateNo{}, enterTime{}" ,inputRecord.getPlateNo() ,inputRecord.getEnterTime());
			logger.error("海康出场记录保存error",e);

		}
	}

	/**
	 * 捷惠通进场
	 *
	 * */
	@Override
	public Boolean saveJHTVehicleRecordEnter(ParkVehicleRecord inputRecord) {
		try {
			ParkVehicleRecord vehicleRecord = getNotOutVehicleRecordByPlateNo(inputRecord.getPlateNo());
			if (vehicleRecord == null) {
				// 无在场车辆记录，则将新的进场记录插入
				parkVehicleRecordDao.insertSelective(inputRecord);
				return true;
			} else {
				/* 已存在未出场的记录 */
				// 1.假如保存的记录停车系统与推送的记录停车系统不同，则更新其为出场，再新增一条过车记录。
				if (vehicleRecord.getSystemId().longValue() != inputRecord.getSystemId().longValue()) {
					vehicleRecord.setIsOut(VehicleStatusEnum.OUT.getStatus());
					parkVehicleRecordDao.updateByPrimaryKeySelective(vehicleRecord);
					parkVehicleRecordDao.insertSelective(inputRecord);
					return true;
				}
				// 2.假如推送的记录入场时间不等于保存记录的入场时间，则更新其为出场，再新增一条过车记录。
				if (inputRecord.getEnterTime().longValue() != vehicleRecord.getEnterTime().longValue()) {
					vehicleRecord.setIsOut(VehicleStatusEnum.OUT.getStatus());
					parkVehicleRecordDao.updateByPrimaryKeySelective(vehicleRecord);
					parkVehicleRecordDao.insertSelective(inputRecord);
					return true;
				}
			}
		} catch (Exception e) {
			logger.error("捷惠通进场记录保存失败 plateNo{}, enterTime{}" ,inputRecord.getPlateNo() ,inputRecord.getEnterTime());
			logger.error("捷惠通进场记录error",e);
			
		}
		return false;
	}
	
	/**
	 * 捷惠通出场
	 */
	@Override
	public Boolean saveJHTVehicleRecordOut(ParkVehicleRecord inputRecord, String flag) {
		Boolean flagB = false;//默认失败
		try {
			// 更新捷惠通停车场车辆出场记录
			if(flag == null){
				//非云支付的情况
				parkVehicleRecordDao.updateJHTVehicleExit(inputRecord);
				flagB = true;
			}else{
				//云支付第二条出场的记录更新（拿第一条出场时间与第二条入场时间匹配）
				parkVehicleRecordDao.updateJHT2VehicleExit(inputRecord);
				flagB = true;
			}
			
		} catch (Exception e) {
			logger.error(
					"捷惠通出场记录保存失败 plateNo{}, enterTime{}" ,inputRecord.getPlateNo() ,inputRecord.getEnterTime());
			logger.error("捷惠通出场记录保存error",e);
		}
		return flagB;
	}
	/**
	 * 大华记录过车记录
	 */
	@Override
	public boolean saveDHVehicleRecordEnter(ParkVehicleRecord inputRecord) {
		try {
			ParkVehicleRecord vehicleRecord = getNotOutVehicleRecordByPlateNo(inputRecord.getPlateNo());
			if (vehicleRecord == null) {
				// 无在场车辆记录，则将新的进场记录插入
				parkVehicleRecordDao.insertSelective(inputRecord);
				return true;
			} else {
				/* 已存在未出场的记录 */
				// 1.假如保存的记录停车系统与推送的记录停车系统不同，则更新其为出场，再新增一条过车记录。
				if (vehicleRecord.getSystemId().longValue() != inputRecord.getSystemId().longValue()) {
					vehicleRecord.setIsOut(VehicleStatusEnum.OUT.getStatus());
					parkVehicleRecordDao.updateByPrimaryKeySelective(vehicleRecord);
					parkVehicleRecordDao.insertSelective(inputRecord);
					return true;
				}
				// 2.假如推送的记录入场时间大于保存记录的入场时间，则更新其为出场，再新增一条过车记录。
				if (vehicleRecord.getEnterTime().longValue() < inputRecord.getEnterTime().longValue()) {
					vehicleRecord.setIsOut(VehicleStatusEnum.OUT.getStatus());
					parkVehicleRecordDao.updateByPrimaryKeySelective(vehicleRecord);
					parkVehicleRecordDao.insertSelective(inputRecord);
					return true;
				}else if(vehicleRecord.getEnterTime().longValue() == inputRecord.getEnterTime().longValue()){
					//3.假如保存记录的入场时间等于推送的记录入场时间，则记录已存在无需保存。
					return true;
				}else {
					//4.假如保存记录的入场时间大于于推送的记录入场时间，则记录为延迟推送，并此记录对应的车已出场。
					inputRecord.setIsOut(VehicleStatusEnum.OUT.getStatus());
					List<ParkVehicleRecord> rList = parkVehicleRecordDao.selectByParams(inputRecord);
					if(rList==null || rList.size()==0) {
						//若延迟推送的记录未保存，则将其保存
						parkVehicleRecordDao.insertSelective(inputRecord);
					}
					return true;
				}
			}
		} catch (Exception e) {
			logger.error(
					"大华进场记录保存失败 plateNo{}, enterTime{}" ,inputRecord.getPlateNo() ,inputRecord.getEnterTime());
			logger.error("大华进场记录保存error",e);
			return false;
		}
		
	}

	/**
	 * 大华记录过车记录
	 */
	@Override
	public boolean saveDHVehicleRecordExit(ParkVehicleRecord inputRecord) {
		try {
			// 更新大华停车场车辆出场记录
			parkVehicleRecordDao.updateDHVehicleExit(inputRecord);
			return true;
		} catch (Exception e) {
			logger.error(
					"大华出场记录保存失败 plateNo{}, enterTime{}" ,inputRecord.getPlateNo() ,inputRecord.getEnterTime());
			logger.error("大华出场记录保存error",e);
		}
		return false;
	}

	/************************** 过车记录相关end *********************************/

	/************************** 支付相关start *********************************/
	/**
	 * 根据停车场UUID和车牌号获取预账单（未支付）
	 */
	@Override
	public ParkResult<ChargeBillVO> getPreChargeBill(ParkVehicleRecord record, HttpServletRequest request) {
		Long systemId = record.getSystemId();
		String outParkingId = record.getOutParkingId();
		ParkSystem parkSystem = parkSystemDao.selectByPrimaryKey(systemId); // 获取接入系统信息
		// 封装请求接入系统参数
		Map<String, Object> map = HttpUtils.convertRequestParam(request);
		map.put("outParkId", outParkingId);
		map.put("plateNo", record.getPlateNo());
		map.put("outRecordId", record.getOutRecordId());
		ParkResult<ChargeBillVO> parkResult = parkSystemParkService.getSysChargeBill(parkSystem.getSystemCode(), map);
		if (ResultEnum.SUCCESS.getCode() == parkResult.getCode()) {
			ChargeBillVO bill = parkResult.getData();
			if (bill == null) {
				logger.info("获取预付订单失败  plateNo", record.getPlateNo());
				return new ParkResult(ResultEnum.ERROR_1.getCode(), "查无此车辆或此车辆已出场");
			}
			ParkParking pp = parkParkingDao.selectByPrimaryKey(record.getParkingId());
			bill.setParkingId(pp.getParkingId());
			bill.setParkName(pp.getParkingName());
			bill.setFeeRule(pp.getFeeRule());
		}
		return parkResult;
	}

	/**
	 * 初始化订单
	 */
	@Override
	@Transactional
	public ParkVehiclePaymentOrder initOrder(ParkVehicleRecord record, ChargeBillVO bill) {
		ParkVehiclePaymentRecord paymentRecord = ChargeBillVO.toParkVehiclePaymentRecord(bill);
		paymentRecord.setRecordId(record.getRecordId());
		paymentRecord.setSystemId(record.getSystemId());
		parkVehiclePaymentRecordDao.insertSelective(paymentRecord);
		ParkVehiclePaymentOrder order = createOrder(paymentRecord);
		parkVehiclePaymentOrderDao.insertSelective(order);

		return order;
	}
	
	public ParkVehiclePaymentOrder createOrder(ParkVehiclePaymentRecord paymentRecord) {
		ParkVehiclePaymentOrder order = new ParkVehiclePaymentOrder();
		order.setOrderId(System.currentTimeMillis());
		order.setPaymentId(paymentRecord.getPaymentId());
		order.setTotalFee(0L);
		order.setOrderStatus(OrderStatusEnum.pay_not.name());
		order.setTotalCost(paymentRecord.getRealCost());// 应付金额
		order.setCreateTime(DateUtil.dateToLong_YMDHMS(new Date()));
		return order;
	}

	/**
	 * 获取订单
	 */
	@Override
	public ParkVehiclePaymentOrder getPaymentOrder(Long orderId) {
		ParkVehiclePaymentOrder order = parkVehiclePaymentOrderDao.selectByPrimaryKey(orderId);
		return order;
	}

	@Override
	public List<PaymentPage> getPaymentPage(Map<String, Object> map) {
		List<PaymentPage> reList = parkVehiclePaymentRecordDao.getPaymentPage(map);
		return reList;
	}

	@Override
	public long getPaymentPageCount(Map<String, Object> map) {
		long reCount = parkVehiclePaymentRecordDao.getPaymentPageCount(map);
		return reCount;
	}
	
	@Override
	public List<PayPage> queryPaylist(Map<String, Object> map) {
		List<PayPage> reList = parkVehiclePaymentOrderDao.queryPaylist(map);
		return reList;
	}

	@Override
	public long queryPaylistCount(Map<String, Object> map) {
		long reCount = parkVehiclePaymentOrderDao.queryPaylistCount(map);
		return reCount;
	}

	/************************** 订单相关end *********************************/

	/************************** 支付相关end *********************************/
	/**
	 * 发起支付
	 * 
	 * @throws Exception
	 */
	@Override
	public JSONObject toPay(ParkVehiclePaymentOrder order, String ip,
			String appid, String wxOpenId, ParkVehicleRecord parkVehicleRecord) throws Exception {
		saveWxOpenidAndAppid(order, wxOpenId, appid);
		JSONObject json = new JSONObject();
		String body = "[vehicle-park-service]临时停车费_parkingId="+parkVehicleRecord.getParkingId()+
				"_"+parkVehicleRecord.getPlateNo()+
				"_"+parkVehicleRecord.getEnterTime();
		json.put("innerOrderNo", order.getOrderId().toString());
		json.put("name", "临时停车费");
		json.put("bankName", bankName);
		json.put("bankInterface", bankInterface);
		json.put("interfaceId", interfaceId);
		json.put("body", body);
		json.put("returnUrl", "aaaa");// 支付成功返回的页面,可以不填
		// json.put("tradeAmount", "1");
		json.put("tradeAmount", order.getTotalCost().toString());
		json.put("wxAppId", appid);
		json.put("wxOpenId", wxOpenId);
		json.put("belongApp", belongApp);
		json.put("ip", ip);
		json.put("innerNotifyUrl", innerNotifyUrl);// 支付成功回调函数接口

		byte[] bytes = RSAUtils.encryptByPrivateKey(json.toString().getBytes(), TempKeyStore.keystore.get("weiyipri"));
		String base64 = Base64Utils.encode(bytes);
		String ts = String.valueOf(System.currentTimeMillis());

		// 组装参数
		Map<String, String> params = new HashMap<String, String>();
		params.put("param", base64);
		params.put("version", version);
		params.put("ts", ts);
		params.put("appno", belongApp);
		params.put("servicename", servicename);
		String url = getOrderUrl + "?" + buildMap(params);
		try {
			// System.out.println("发起支付=========="+url);
			// 发起支付
			RestTemplate template = new RestTemplate();
			JSONObject result = template.postForObject(url, null, JSONObject.class);
			logger.info("发起支付==========支付系统响应    SUC!!!");
			return result;
		} catch (Exception e) {
			logger.error("支付系统响应error",e);
			throw new RuntimeException("发起支付==========支付系统响应   ERR!!!");
		}
	}
	
	/**
	 * 订单数据插入wxOpenid
	 */
	private void saveWxOpenidAndAppid(ParkVehiclePaymentOrder order,  String wxOpenid, String appid) {
		try {
			order.setWxOpenid(wxOpenid);
			order.setAppid(appid);
			parkVehiclePaymentOrderDao.updateByPrimaryKeySelective(order);
		} catch (Exception e) {
			logger.error("订单数据插入wxOpenid异常 orderId={}", order.getOrderId());
			logger.error("wxOpenid={}, appid={}", wxOpenid, appid);
			logger.error("打印error", e);
		}
	}

	/**
	 * 支付结果回调函数
	 */
	@Override
	@Transactional
	public void payRecev(ParkVehiclePaymentOrder order, ParkVehiclePaymentRecord record) {
		parkVehiclePaymentOrderDao.updateByPrimaryKeySelective(order);
		parkVehiclePaymentRecordDao.updateByPrimaryKeySelective(record);
	}

	/************************** 支付相关end *********************************/

	public String buildMap(Map<String, String> map) {
		StringBuffer sb = new StringBuffer();
		if (map.size() > 0) {
			for (String key : map.keySet()) {
				sb.append(key + "=");
				if (StringUtils.isEmpty(map.get(key))) {
					sb.append("&");
				} else {
					sb.append(map.get(key) + "&");
				}
			}
		}
		return sb.toString();
	}

	@Override
	public List<ParkSystemVo> getParkSystemList(Map<String, Object> map) {
		return parkParkingDao.getParkSystemList(map);
	}

	@Override
	public long getParkSystemListCount(Map<String, Object> map) {
		return parkParkingDao.getParkSystemListCount(map);
	}

	/**
	 * 更新订单
	 */
	@Override
	public int updatePaymentByPrimaryKey(ParkVehiclePaymentRecord record) {

		return parkVehiclePaymentRecordDao.updateByPrimaryKeySelective(record);
	}

	@Override
	public List<VehicleSearchVo> getVehicleList(Map<String, Object> map) {
		return parkVehicleRecordDao.getVehicleList(map);
	}

	@Override
	public long getVehicleListCount(Map<String, Object> map) {
		return parkVehicleRecordDao.getVehicleListCount(map);
	}

	@Override
	public List<VehicleSearchVo> getVehicleSearchVoList(Map<String, Object> map) {
		return parkVehicleRecordStatisDao.getVehicleSearchVoList(map);
	}

	@Override
	public Long getVehicleSearchVoListCount(Map<String, Object> map) {
		return parkVehicleRecordStatisDao.getVehicleSearchVoListCount(map);
	}

	@Override
	public List<VehicleStaticSearchVo> getVehicleStatisHisList(Map<String, Object> map) {
		return parkVehicleRecordStatisDao.getVehicleStatisHisList(map);
	}

	@Override
	public long getVehicleStatisHisListCount(Map<String, Object> map) {
		return parkVehicleRecordStatisDao.getVehicleStatisHisListCount(map);
	}

	@Override
	public JSONArray marketParkingSelector(String marketList) {
		List<MarketParkingSelector> list = parkParkingDao.marketParkingSelector(marketList);
		JSONArray selector = new JSONArray();
		for (MarketParkingSelector mp : list) {
			JSONObject json = new JSONObject();
			json.put("key", mp.getMarketName());
			json.put("code", mp.getMarketId());
			String[] parkingNames = mp.getParkingNames().split(",");
			String[] getParkingIds = mp.getParkingIds().split(",");
			JSONArray parkingNameJSONArr = new JSONArray();
			for (int i = 0; i < parkingNames.length; i++) {
				JSONObject parkingNameJSON = new JSONObject();
				parkingNameJSON.put("key", parkingNames[i]);
				parkingNameJSON.put("code", getParkingIds[i]);
				parkingNameJSONArr.add(parkingNameJSON);
			}
			json.put("value", parkingNameJSONArr);
			selector.add(json);
		}
		return selector;
	}

	@Override
	public List<VehicleStaticSearchVo> getVehicleStatisList(Map<String, Object> map) {

		return parkVehicleRecordDao.getVehicleStatisList(map);
	}

	@Override
	public long getVehicleStatisListCount(Map<String, Object> map) {

		return parkVehicleRecordDao.getVehicleStatisListCount(map);
	}

	@Override
	public List<String> getInOutNameByParkingId(Long parkingId) {
		return parkRoadWayDao.getInOutNameByParkingId(parkingId);
	}

	@Override
	public VehicleSearchVo getCostAndType(Long recordStId) {
		return parkVehiclePaymentRecordDao.getCostAndType(recordStId);
	}

	@Override
	public List<VehicleCostVo> getVehicleCostVoByRecordStId(Long recordStId) {
		return parkVehiclePaymentStatisDao.getVehicleCostVoByRecordStId(recordStId);
	}

	@Override
	public Long getJHTMaxUpdateTime() {
		return parkVehicleRecordDao.getJHTMaxUpdateTime();
	}
	
	@Override
	public int updateParkParing(ParkParking parkParking) {
		return parkParkingDao.updateByPrimaryKeySelective(parkParking);
	}
	
	

}
