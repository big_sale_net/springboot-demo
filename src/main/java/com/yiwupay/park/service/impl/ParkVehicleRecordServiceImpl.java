package com.yiwupay.park.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yiwupay.park.dao.ParkVehicleRecordDao;
import com.yiwupay.park.enums.VehicleStatusEnum;
import com.yiwupay.park.model.ParkVehicleRecord;
import com.yiwupay.park.service.ParkService;
import com.yiwupay.park.service.ParkVehicleRecordService;

@Service
public class ParkVehicleRecordServiceImpl implements ParkVehicleRecordService{
	private Logger logger = LoggerFactory.getLogger(ParkVehicleRecordServiceImpl.class);
	
	@Autowired
	private ParkVehicleRecordDao parkVehicleRecordDao;
	
	@Autowired
	private ParkService parkService;
	/**
	 * 保存蓝卡进场过车记录
	 */
	@Override
	public boolean saveBCVehicleRecordEnter(ParkVehicleRecord inputRecord) {
		try {
			ParkVehicleRecord vehicleRecord = parkService.getNotOutVehicleRecordByPlateNo(inputRecord.getPlateNo());
			if (vehicleRecord == null) {
				// 无在场车辆记录，则将新的进场记录插入
				parkVehicleRecordDao.insertSelective(inputRecord);
				return true;
			} else {
				/* 已存在未出场的记录 */
				// 1.假如保存的记录停车系统与推送的记录停车系统不同，则更新其为出场，再新增一条过车记录。
				if (vehicleRecord.getSystemId().longValue() != inputRecord.getSystemId().longValue()) {
					vehicleRecord.setIsOut(VehicleStatusEnum.OUT.getStatus());
					parkVehicleRecordDao.updateByPrimaryKeySelective(vehicleRecord);
					parkVehicleRecordDao.insertSelective(inputRecord);
					return true;
				}
				// 2.假如保存记录的入场时间小于推送的记录入场时间，则更新其为出场，再新增一条过车记录。
				if (vehicleRecord.getEnterTime().longValue() < inputRecord.getEnterTime().longValue()) {
					vehicleRecord.setIsOut(VehicleStatusEnum.OUT.getStatus());
					parkVehicleRecordDao.updateByPrimaryKeySelective(vehicleRecord);
					parkVehicleRecordDao.insertSelective(inputRecord);
					return true;
				}else if(vehicleRecord.getEnterTime().longValue() == inputRecord.getEnterTime().longValue()){
					//3.假如保存记录的入场时间等于推送的记录入场时间，则记录已存在无需保存。
					return true;
				}else {
					//4.假如保存记录的入场时间大于于推送的记录入场时间，则记录为延迟推送，并此记录对应的车已出场。
					inputRecord.setIsOut(VehicleStatusEnum.OUT.getStatus());
					List<ParkVehicleRecord> rList = parkVehicleRecordDao.selectByParams(inputRecord);
					if(rList==null || rList.size()==0) {
						//若延迟推送的记录未保存，则将其保存
						parkVehicleRecordDao.insertSelective(inputRecord);
					}
					return true;
				}
				
			}
		} catch (Exception e) {
			logger.error(
					"蓝卡进场记录保存失败 plateNo{}, enterTime{}" ,inputRecord.getPlateNo() ,inputRecord.getEnterTime());
			logger.error("蓝卡进场记录保存error",e);
			return false;
		}
		
	}

	/**
	 * 保存蓝卡出场过车记录
	 */
	@Override
	public boolean saveBCVehicleRecordExit(ParkVehicleRecord inputRecord) {
		try {
			parkVehicleRecordDao.updateRecordByOutRecordId(inputRecord);
			return true;
		} catch (Exception e) {
			logger.error(
					"蓝卡出场记录保存失败 plateNo{}, enterTime{}" ,inputRecord.getPlateNo() ,inputRecord.getEnterTime());
			logger.error("蓝卡出场记录保存error",e);
		}
		return false;
	}

	/**
	 * 红门系统保存进场数据
	 */
	@Override
	public boolean saveHMVehicleRecordEnter(ParkVehicleRecord inputRecord) {
		try {
			ParkVehicleRecord vehicleRecord = parkService.getNotOutVehicleRecordByPlateNo(inputRecord.getPlateNo());
			if (vehicleRecord == null) {
				// 无在场车辆记录，则将新的进场记录插入
				parkVehicleRecordDao.insertSelective(inputRecord);
			    return true;
			} else {
				/* 已存在未出场的记录 */
				// 1.假如保存的记录停车系统与推送的记录停车系统不同，则更新其为出场，再新增一条过车记录。
				if (vehicleRecord.getSystemId().longValue() != inputRecord.getSystemId().longValue()) {
					vehicleRecord.setIsOut(VehicleStatusEnum.OUT.getStatus());
					parkVehicleRecordDao.updateByPrimaryKeySelective(vehicleRecord);
					parkVehicleRecordDao.insertSelective(inputRecord);
					return true;
				}
				// 2.假如推送的记录入场时间不等于保存记录的入场时间，则更新其为出场，再新增一条过车记录。
				if (inputRecord.getEnterTime().longValue() != vehicleRecord.getEnterTime().longValue()) {
					vehicleRecord.setIsOut(VehicleStatusEnum.OUT.getStatus());
					parkVehicleRecordDao.updateByPrimaryKeySelective(vehicleRecord);
					parkVehicleRecordDao.insertSelective(inputRecord);
					return true;
				}
			}
			return true;
		} catch (Exception e) {
			logger.error("红门进场记录保存失败 plateNo{}, enterTime{}" ,inputRecord.getPlateNo() ,inputRecord.getEnterTime());
			logger.error("红门进场记录error",e);
			return false;
		}
		
	}
	
	/**
	 * 红门系统保存出场数据
	 * */
	@Override
	public boolean saveHMVehicleRecevOut(ParkVehicleRecord inputRecord) {
		try {
			parkVehicleRecordDao.updateDHVehicleExit(inputRecord);
			return true;
		} catch (Exception e) {
			logger.error("红门出场记录保存失败 plateNo{}, enterTime{}" ,inputRecord.getPlateNo() ,inputRecord.getEnterTime());
			logger.error("红门出场记录error",e);
			return false;
		}
	}
}
