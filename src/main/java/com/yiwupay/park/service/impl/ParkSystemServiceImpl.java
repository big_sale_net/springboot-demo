package com.yiwupay.park.service.impl;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yiwupay.park.model.result.ParkResult;
import com.yiwupay.park.model.result.ParkResultList;
import com.yiwupay.park.model.vo.ChargeBillVO;
import com.yiwupay.park.model.vo.ChargeRuleVO;
import com.yiwupay.park.model.vo.ParkParkingVO;
import com.yiwupay.park.model.vo.PayBackVO;
import com.yiwupay.park.service.ParkChargeService;
import com.yiwupay.park.service.ParkParkingService;
import com.yiwupay.park.service.ParkSystemParkService;
import com.yiwupay.park.service.VehicleRecordService;

@Service
public class ParkSystemServiceImpl implements ParkSystemParkService {
	Logger logger = LoggerFactory.getLogger(ParkSystemServiceImpl.class);
	
    @Autowired
    private BeanFactory beanFactory;
	
    
	/**
	 * 停车场信息查询
	 */
	@Override
	public ParkResultList<ParkParkingVO> getSysParkInfo(String systemCode, Map<String, Object> map) {
		ParkParkingService parkParkingService = (ParkParkingService) beanFactory.getBean(systemCode);
		//成功返回
		return parkParkingService.getParkInfo(map);
	}

	@Override
	public ParkResult<ChargeBillVO> getSysChargeBill(String systemCode, Map<String, Object> map) {
		ParkChargeService parkChargeService = (ParkChargeService) beanFactory.getBean(systemCode);
		//成功返回
		return parkChargeService.getChargeBill(map);
	}

	@Override
	public ParkResultList<ChargeRuleVO> getSysChargeRule(String systemCode, Map<String, Object> map) {
		ParkChargeService parkChargeService = (ParkChargeService) beanFactory.getBean(systemCode);
		return parkChargeService.getChargeRule(map);
	}
	
	@Override
	public ParkResult getSysVehicleRecord(String systemCode, Map<String, Object> map) {
		VehicleRecordService vehicleRecordService = (VehicleRecordService) beanFactory.getBean(systemCode);
		return vehicleRecordService.getVehicleRecords(map);
	}
	
	@Override
	public ParkResult<PayBackVO> paySysChargeBill(String systemCode, Map<String, Object> map) {
		ParkChargeService parkChargeService = (ParkChargeService) beanFactory.getBean(systemCode);
		return parkChargeService.payChargeBill(map);
	}
}
