package com.yiwupay.park.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yiwupay.park.dao.ParkEntranceDao;
import com.yiwupay.park.model.ParkEntrance;
import com.yiwupay.park.service.EntranceService;
@Service
public class EntranceServiceImpl implements EntranceService{
	
	@Autowired
	ParkEntranceDao parkEntranceDao;
	
	@Override
	public List<ParkEntrance> getEntranceAll() {
		return parkEntranceDao.selectAll();
	}
}
