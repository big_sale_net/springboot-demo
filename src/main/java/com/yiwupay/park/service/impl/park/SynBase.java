package com.yiwupay.park.service.impl.park;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yiwupay.park.dao.ParkParkingDao;
import com.yiwupay.park.dao.ParkVehiclePaymentStatisDao;
import com.yiwupay.park.dao.ParkVehicleRecordStatisDao;
import com.yiwupay.park.dao.ParkVehicleRecordTempDao;
import com.yiwupay.park.enums.VehicleStatusEnum;
import com.yiwupay.park.model.ParkVehiclePaymentStatis;
import com.yiwupay.park.model.ParkVehicleRecordStatis;
import com.yiwupay.park.service.SynBaseService;
import com.yiwupay.park.utils.DateUtil;

@Service("synBase")
public class SynBase implements SynBaseService{
	Logger logger = LoggerFactory.getLogger(SynBase.class);

	@Autowired
	ParkParkingDao parkParkingDao;
	@Autowired
	protected ParkVehicleRecordTempDao parkVehicleRecordTempDao;
	@Autowired
	protected ParkVehiclePaymentStatisDao parkVehiclePaymentStatisDao;
	@Autowired
	protected ParkVehicleRecordStatisDao parkVehicleRecordStatisDao;

	static int pageSize = 200;

	@Override
	public boolean repPaymentRecord() {
		try {
			int count = 0;
			int repCount = 0;
			List<ParkVehiclePaymentStatis> payments = parkVehiclePaymentStatisDao.getModelsOfNullRecord();
			if(payments!=null){
				count = payments.size();
				for (ParkVehiclePaymentStatis payment : payments) {
					// 利用入场时间和车牌获取过车记录
					ParkVehicleRecordStatis pvr_statis = parkVehicleRecordStatisDao.getModelByParams(payment.getSystemId(),
							payment.getPlateNo(), payment.getEnterTime());
					if (pvr_statis != null) {
						payment.setRecordId(pvr_statis.getRecordStId());
						payment.setOutRecordId(pvr_statis.getOutRecordId());
						payment.setParkingId(pvr_statis.getParkingId());
						payment.setOutParkingId(pvr_statis.getOutParkingId());
						payment.setRecordId(pvr_statis.getRecordStId());
						payment.setParkTime(getParkTime(pvr_statis));
						parkVehiclePaymentStatisDao.updateByPrimaryKeySelective(payment);
						repCount++;
					}
				}
			}
			logger.info("没有过车记录id的缴费记录有 "+count+"条， 填补了过车记录id "+repCount+"条");
			return true;
		} catch (Exception e) {
			logger.error("填补没有过车记录id的缴费记录error",e);
			return false;
		}
	}

	public Integer getParkTime(ParkVehicleRecordStatis pvr_statis) {
		if(VehicleStatusEnum.OUT.getStatus().equals(pvr_statis.getIsOut())){
			long exitMillis = DateUtil.longToTimeMillis(pvr_statis.getExitTime());
			long enterMillis = DateUtil.longToTimeMillis(pvr_statis.getEnterTime());
			long delayTimeL = (exitMillis - enterMillis)/1000; //秒 
			int delayTime = (int)Math.ceil(delayTimeL/60.0);//分
			return delayTime;
		}
		return null;
	}

}
