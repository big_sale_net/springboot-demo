package com.yiwupay.park.service.impl;

import java.util.TimerTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import com.yiwupay.park.service.impl.park.HMServiceImpl;
/**
 *红门消费端需要向队列周期性拉取消息
 */
@Service("hmPullExecutor")
public class HmPullVehiRecExecutorServiceImpl extends TimerTask{
	Logger logger = LoggerFactory.getLogger(HmPullVehiRecExecutorServiceImpl.class);
	
	@Autowired
	HMServiceImpl serviceImpl;
	
	private ThreadPoolTaskExecutor threadPool;

	public ThreadPoolTaskExecutor getThreadPool() {
		return threadPool;
	}

	public void setThreadPool(ThreadPoolTaskExecutor threadPool) {
		this.threadPool = threadPool;
	}

	

	@Override
	public void run() {
		threadPool.execute(() -> {serviceImpl.saveVehiceRecord();});
//		while (threadPool.getActiveCount() > 0) {
//			try {
//				Thread.sleep(1000);
//			} catch (Exception e) {
//				logger.error("打印error",e);
//			}
//		}
		
	}
	
	
	
}

