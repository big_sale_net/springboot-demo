package com.yiwupay.park.service.impl.park;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import com.yiwupay.park.service.SynHMService;
/**
 * 3区红门同步数据接口
 * @author zls
 *
 */
@Service("hmSynService")
@PropertySource("classpath:/parkconf/hm/hm.properties")
public class HMSynServiceImpl  extends SynBase implements SynHMService{
	
	@Value("${hm.systemid}")
	long systemId;


	
	
	
	@Override
	public boolean savePaymentRecord(String startTime, String endTime) {
		
		return false;
	}

	@Override
	public boolean saveVehicleRecord(String startTime, String endTime) {
		
		return false;
	}

}
