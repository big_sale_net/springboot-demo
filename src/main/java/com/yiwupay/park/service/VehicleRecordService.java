package com.yiwupay.park.service;

import java.util.Map;

import com.yiwupay.park.model.result.ParkResult;

public interface VehicleRecordService {
	ParkResult getVehicleRecords(Map<String, Object> map);
	
	ParkResult getVehicleAssessRecords(Map<String, Object> map);
}
