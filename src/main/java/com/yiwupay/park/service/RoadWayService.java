package com.yiwupay.park.service;

import java.util.List;

import com.yiwupay.park.model.ParkRoadWay;

public interface RoadWayService {

	List<ParkRoadWay> getRoadWayAll();

}
