package com.yiwupay.park.service;

public interface SynHMService {

	boolean savePaymentRecord(String startTime, String endTime);

	boolean saveVehicleRecord(String startTime, String endTime);
}
