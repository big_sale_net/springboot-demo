package com.yiwupay.park.service;

public interface SynHIKService {

	boolean savePaymentRecord(long startTime, long endTime);

	boolean saveVehicleRecord(long startTime, long endTime);

	boolean saveVehicleRecord2();

}
