package com.yiwupay.park.service;

import com.yiwupay.park.model.ParkVehicleRecord;

public interface ParkVehicleRecordService {

	boolean saveBCVehicleRecordExit(ParkVehicleRecord inputRecord);

	boolean saveBCVehicleRecordEnter(ParkVehicleRecord inputRecord);
	
	boolean saveHMVehicleRecordEnter(ParkVehicleRecord inputRecord);
	
	boolean saveHMVehicleRecevOut(ParkVehicleRecord inputRecord);

}
