package com.yiwupay.park.action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yiwupay.park.enums.ResultEnum;
import com.yiwupay.park.enums.VehicleOrderStatusEnum;
import com.yiwupay.park.model.ParkVehicleRecord;
import com.yiwupay.park.model.ParkVehicleWxAppPlateNo;
import com.yiwupay.park.model.result.ParkResult;
import com.yiwupay.park.model.result.ParkResultList;
import com.yiwupay.park.model.vo.ChargeBillVO;
import com.yiwupay.park.model.vo.VehicleOrderVO;
@Controller
@RequestMapping("/parkwx")
@Service
public class ParkWxAction extends BaseAction {
	
	/**
	 * 查询缴费列表
	 * @param request
	 * @param wxOpenid
	 * @return
	 */
	@RequestMapping("/queryPlatelist")
	@ResponseBody
	public ParkResultList<VehicleOrderVO> queryPlatelist(HttpServletRequest request,
			@RequestParam("wxOpenid") String wxOpenid){
		
		ParkResultList<VehicleOrderVO> prList = new ParkResultList<VehicleOrderVO>();
		try {
			List<VehicleOrderVO> list = new ArrayList<VehicleOrderVO>();
			
			//根据wxOpenid查询所有车牌
			List<ParkVehicleWxAppPlateNo> wpList = wxAppPlateNoService.getWxAppPlateNoList(wxOpenid);
			for(ParkVehicleWxAppPlateNo wp : wpList){
				VehicleOrderVO vo = new VehicleOrderVO();
				String plateNo = wp.getPlateNo();
				
				//保存车牌
				vo.setPlateNo(plateNo);
				//查询判断车辆是否在场
				ParkVehicleRecord parkVehicleRecord = parkService.getNotOutVehicleRecordByPlateNo(plateNo);
				if (null != parkVehicleRecord) {
					ParkResult<ChargeBillVO> parkResult = parkService.getPreChargeBill(parkVehicleRecord, request);
					if(ResultEnum.SUCCESS.getCode() == parkResult.getCode()){
						ChargeBillVO bill = parkResult.getData();
						vo.setEnterTime(Long.parseLong(bill.getEnterTime()));
						vo.setParkName(bill.getParkName());
						vo.setParkTime(bill.getParkingTime());
						Long fee = Long.parseLong(bill.getRealCost());
						if(fee.longValue() > 0L){
							vo.setOrderStatus(VehicleOrderStatusEnum.WAIT_PAY.name());
						}else{
							vo.setOrderStatus(VehicleOrderStatusEnum.NO_FEE.name());
						}
						vo.setTotalFee(fee);
					}else{
						vo.setOrderStatus(VehicleOrderStatusEnum.NOT_IN.name());
					}
				} else {
					vo.setOrderStatus(VehicleOrderStatusEnum.NOT_IN.name());
				}
				list.add(vo);
			}
			prList.setCode(ResultEnum.SUCCESS.getCode());
			prList.setData(list);
		} catch (Exception e) {
			logger.error("查询缴费列表失败  wxOpenid{}", wxOpenid);
			logger.error("查询缴费列表error", e);
			prList.setCode(ResultEnum.ERROR_SYS.getCode());
		}
		return prList;
		
	}
	

}
