package com.yiwupay.park.action;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yiwupay.park.dao.ParkVehiclePaymentOrderDao;
import com.yiwupay.park.dao.ParkVehiclePaymentRecordDao;
import com.yiwupay.park.dao.ParkVehicleRecordDao;
import com.yiwupay.park.enums.OrderStatusEnum;
import com.yiwupay.park.enums.PayTypeEnum;
import com.yiwupay.park.model.NotifyParam;
import com.yiwupay.park.model.ParkVehiclePaymentOrder;
import com.yiwupay.park.model.ParkVehiclePaymentRecord;
import com.yiwupay.park.model.ParkVehicleRecord;
import com.yiwupay.park.model.ParkVehicleWxAppPlateNo;
import com.yiwupay.park.model.PayNotifyParam;
import com.yiwupay.park.service.NotifyExecutorService;
import com.yiwupay.park.service.ParkService;
import com.yiwupay.park.service.WxAppPlateNoService;
import com.yiwupay.park.utils.DateUtil;

@Controller
@RequestMapping("/parkrecev")
public class ParkRecevAction {
	private	Logger logger = LoggerFactory.getLogger(ParkRecevAction.class);
	@Autowired
	private	ParkService parkService;

	@Autowired
	private	NotifyExecutorService notifyExecutorService;

	@Autowired
	private	ParkVehiclePaymentRecordDao parkVehiclePaymentRecordDao;

	@Autowired
	private	ParkVehiclePaymentOrderDao parkVehiclePaymentOrderDao;
	
	@Autowired
	private	WxAppPlateNoService wxAppPlateNoService;
	
	@Autowired
	private	ParkVehicleRecordDao parkVehicleRecordDao;

	@RequestMapping(value = "/pay_recev_notify")
	@ResponseBody
	public boolean payRecev(PayNotifyParam payNotifyParam) {
		logger.info("成功接收到支付结果通知 ");
		boolean result = false;
		Long orderId = payNotifyParam.getOuterOrderNo();
		Long cost = payNotifyParam.getTradeAmount(); // 实付金额
		Long outTradeNo = payNotifyParam.getPaymentOrderId();// 支付网关id
		String status = payNotifyParam.getPaymentStatus();// 订单状态
		String payWay = payNotifyParam.getBankInterface();// 支付渠道
		String payType = payNotifyParam.getBank();// 支付方式
		Long payTime = DateUtil.dateToLong_YMDHMS(new Date());
		try {
			if ("PAYED".equals(status)) {
				//获取订单信息
				ParkVehiclePaymentOrder order = parkVehiclePaymentOrderDao.selectByPrimaryKey(orderId);
				if(OrderStatusEnum.pay_yes.name().equals(order.getOrderStatus())){
					logger.info("已支付, 支付结果重复通知/异常通知 orderId{}", orderId);
					return true;
				}
				if(order.getTotalCost().longValue()!=cost.longValue()){
					logger.error("支付金额异常 orderId={}, cost{}<>totalCost{}", orderId, cost+"<>"+order.getTotalCost());
				}
				//获取缴费信息
				ParkVehiclePaymentRecord record = parkVehiclePaymentRecordDao.selectByPrimaryKey(order.getPaymentId());
				order.setOrderStatus(OrderStatusEnum.pay_yes.name());
				order.setPayWay(payWay);
				order.setPayType(payType);
				order.setOutTradeNo(outTradeNo);
				order.setPayTime(payTime);
				order.setTotalFee(cost);// 实付金额
				record.setCost(cost);
				record.setPayType(PayTypeEnum.OTHER.name());
				record.setOrderStatus(OrderStatusEnum.pay_yes.name());
				record.setPayTime(payTime);
				//获取过车信息
				ParkVehicleRecord pvr = parkVehicleRecordDao.selectByPrimaryKey(record.getRecordId());
				//封装通知开闸参数类
				NotifyParam notifyParam = NotifyParam.getNotifyParam(order, record, pvr);
				//通知接入系统支付结果
				boolean r = notifyExecutorService.notifyOutterSyetem(notifyParam, "list0");
				logger.info("通知接入系统支付结果  r={},plateNo={}",r, pvr.getPlateNo());
				result = true;
				// 更新自身表格
				parkService.payRecev(order, record);
				logger.info("支付回调成功 orderId{}", orderId);
				addWxAppPlateNo(order, pvr.getPlateNo());
			}
		} catch (Exception e) {
			logger.error("支付回调错误",e);
			logger.error("支付回调错误 payNotifyParam{}",payNotifyParam.toString());
			e.printStackTrace();
		}
		if (!result) {
			logger.error("支付回调失败  orderId{}，cost{} ", orderId, cost);
		}
		return result;
	}

	private void addWxAppPlateNo(ParkVehiclePaymentOrder order, String plateNo) {
		try {
			
			ParkVehicleWxAppPlateNo wxAppPlateNo = new ParkVehicleWxAppPlateNo();
			wxAppPlateNo.setPlateNo(plateNo);
			wxAppPlateNo.setAppid(order.getAppid());
			wxAppPlateNo.setWxOpenid(order.getWxOpenid());
			wxAppPlateNoService.addWxAppPlateNo(wxAppPlateNo);
		} catch (Exception e) {
			logger.error("新增微信和车牌关系数据失败  wxOpenid={},plateNo={}", order.getWxOpenid(), plateNo);
			logger.error("打印error", e);
		}
	}
	
//	public static void main(String[] args) {
//		Long a = 1L;
//		Long b = new Long(1);
//		long c = 1L;
//		System.out.println(a.longValue()==b.longValue());
//	}
}
