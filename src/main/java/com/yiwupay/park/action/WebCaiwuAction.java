package com.yiwupay.park.action;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yiwupay.park.model.web.CaiwuStatisWeb;
import com.yiwupay.park.utils.PageView;

@Controller
@RequestMapping("/webcaiwus")
public class WebCaiwuAction extends BaseAction {
	/**
	 * 获取停车场列表
	 */
	@RequestMapping("/getCaiwuStatisList")
	@ResponseBody
	public PageView<CaiwuStatisWeb> getCaiwuStatisList(HttpServletRequest request,
			@RequestParam(value = "currentPage", defaultValue = "1") int currentPage) {

		// 封装请求参数
		Map<String, Object> map = getParamMap(request);
		// 页数、行数
		int rows = 10;

		// 参数装载
		if (currentPage != 0) {
			map.put("start", (currentPage - 1) * rows + 1);
			map.put("end", currentPage * rows);
		}else{
			map.remove("start");
			map.remove("end");
		}


		List<CaiwuStatisWeb> list = caiwuService.getCaiwuStatisList(map);
		long count = caiwuService.getCaiwuStatisCount(map);
		
		// 结果返回
		PageView<CaiwuStatisWeb> page = new PageView<CaiwuStatisWeb>(rows, currentPage);
		page.setRecords(list);
		page.setTotalrecord(count);

		return page;
	}
}
