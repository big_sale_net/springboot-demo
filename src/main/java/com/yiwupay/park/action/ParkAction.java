package com.yiwupay.park.action;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yiwupay.park.enums.OrderStatusEnum;
import com.yiwupay.park.enums.ResultEnum;
import com.yiwupay.park.model.ParkParking;
import com.yiwupay.park.model.ParkVehiclePaymentOrder;
import com.yiwupay.park.model.ParkVehiclePaymentRecord;
import com.yiwupay.park.model.ParkVehicleRecord;
import com.yiwupay.park.model.result.ParkResult;
import com.yiwupay.park.model.result.ParkResultList;
import com.yiwupay.park.model.vo.ChargeBillVO;
import com.yiwupay.park.utils.Base64Utils;
import com.yiwupay.park.utils.JsonUtils;
import com.yiwupay.park.utils.RSAUtils;
import com.yiwupay.park.utils.TempKeyStore;

import net.sf.json.JSONObject;

@Controller
@RequestMapping("/park")
public class ParkAction extends BaseAction {

	/**
	 * 获取停车场信息，目前展示：总车位数/剩余车位数
	 */
	@RequestMapping("/getparkparking")
	@ResponseBody
	public ParkResultList<ParkParking> getParkParkingByMarketId(HttpServletRequest request,
			@RequestParam("marketId") String marketId) {

		ParkResultList<ParkParking> parkResult = new ParkResultList<ParkParking>();
		try {
			// 根据marketId获取PS_PARK_PARKING表的SYSTEM_ID,OUT_PARKING_ID,PARKING_CODE字段
			List<ParkParking> ppList = parkService.getParkParkingByMarketId(marketId, request);
			if (ppList != null && ppList.size() > 0) {
				parkResult.setCode(ResultEnum.SUCCESS.getCode());
				parkResult.setData(ppList);
				return parkResult;
			}
		} catch (Exception e) {
			logger.error("获取停车场数据error marketId{}", marketId);
			logger.error("获取停车场数据error", e);
			parkResult.setCode(ResultEnum.ERROR_SYS.getCode());
		}
		return parkResult;
	}

	/**
	 * 用户输入车牌号获取停车费用
	 * 
	 * @param plateNo
	 * @param request
	 * @return
	 */
	@RequestMapping("/getprechargebill")
	@ResponseBody
	public ParkResult<ChargeBillVO> getPreChargeBill(HttpServletRequest request,
			@RequestParam("plateNo") String plateNo) {

		plateNo = plateNo.toUpperCase();
		ParkResult<ChargeBillVO> parkResult = null;
		try {
			ParkVehicleRecord parkVehicleRecord = parkService.getNotOutVehicleRecordByPlateNo(plateNo);
			if (null != parkVehicleRecord) {
				parkResult = parkService.getPreChargeBill(parkVehicleRecord, request);
			} else {
				parkResult = new ParkResult(ResultEnum.ERROR_1.getCode(), "查无此车辆或此车辆已出场");
				logger.info("查无此车辆或此车辆已出场   plateNo{}", plateNo);
			}
			System.out.println("用户输入车牌号plateNo{}"+plateNo+"获取停车费用 parkResult{}"+parkResult.toString());
		} catch (Exception e) {
			logger.error("获取停车预付订单error plateNo{}", plateNo);
			logger.error("获取停车预付订单error", e);
			parkResult = new ParkResult(ResultEnum.ERROR_SYS.getCode(), "系统错误！");
		}
		return parkResult;

	}

	/**
	 * 用户点击支付费用，初始化【缴费订单】，并返回订单信息
	 * 
	 * @param plateNo
	 * @param realCost
	 */
	@RequestMapping("/savechargebill")
	@ResponseBody
	public ParkResult initOrder(HttpServletRequest request, @RequestParam("plateNo") String plateNo,
			@RequestParam("realCost") BigDecimal realCost) {

		plateNo = plateNo.toUpperCase();
		ParkResult parkResult = null;
		try {
			ParkVehicleRecord parkVehicleRecord = parkService.getNotOutVehicleRecordByPlateNo(plateNo);
			if(parkVehicleRecord == null) {
				logger.info("初始化订单失败，查无此车辆或此车辆已出场   plateNo{}", plateNo);
				return new ParkResult(ResultEnum.ERROR_1.getCode(), "初始化失败，查无此车辆或此车辆已出场");
			}
			ParkResult<ChargeBillVO> preChargeBill = parkService.getPreChargeBill(parkVehicleRecord, request);
			if(ResultEnum.SUCCESS.getCode() != preChargeBill.getCode()){
				logger.error("初始化订单时查询订单失败   plateNo{},realCost{}", plateNo,realCost);
				return new ParkResult(ResultEnum.ERROR_SYS.getCode(), "获取订单有误!");
			}
			ChargeBillVO bill = preChargeBill.getData();
			BigDecimal outRealCost = new BigDecimal(bill.getRealCost());
			if (outRealCost.compareTo(BigDecimal.ZERO) == 0) {
				// 应付金额为0，无需支付
				return parkResult = new ParkResult(ResultEnum.ERROR_1.getCode(), "应付金额为0，无需支付");

			} else if (outRealCost.compareTo(realCost) == 0) {
				ParkVehiclePaymentOrder order = parkService.initOrder(parkVehicleRecord,bill);
				Map<String, String> orderMap = JsonUtils.object2Map(order);
				orderMap.put("orderName", "临时停车费用");
				ParkParking pp = parkService.getParkParkingById(parkVehicleRecord.getParkingId());
				orderMap.put("parkingName", pp.getParkingName());
				parkResult = new ParkResult(ResultEnum.SUCCESS.getCode(), "初始化订单成功！");
				parkResult.setData(orderMap);
				System.out.println("用户点击支付费用，初始化【缴费订单】，并返回订单信息 parkResult{}"+ parkResult.toString());
			} else {
				parkResult = new ParkResult(ResultEnum.ERROR_5_ORDER.getCode(), "订单已过期！");
				logger.info("用户停车费用变化，初始化失败 plateNo{}", plateNo);
			}
		} catch (Exception e) {
			parkResult = new ParkResult(ResultEnum.ERROR_SYS.getCode(), "系统错误！");
			logger.error("初始化订单失败   plateNo{},realCost{}", plateNo,realCost);
			logger.error("打印error", e);
		}
		return parkResult;
	}

	/**
	 * 用户点击确认支付，系统向支付系统发起支付请求，创建【支付订单】并返回前端
	 */
	@RequestMapping("/topay")
	@ResponseBody
	public ParkResult toPay(HttpServletRequest request, @RequestParam("orderId") Long orderId,
			@RequestParam("ip") String ip, String wxAppId, @RequestParam("wxOpenId") String wxOpenId,
			@RequestParam("cost") BigDecimal cost, @RequestParam("plateNo") String plateNo) {

		plateNo = plateNo.toUpperCase();
		try {
			// 判断订单是否已支付
			ParkVehiclePaymentOrder order = parkService.getPaymentOrder(orderId);
			if (OrderStatusEnum.pay_yes.equals(order.getOrderStatus())) {
				logger.info("发起支付失败----订单已支付  orderId{}", orderId);
				return new ParkResult(ResultEnum.ERROR_5_ORDER.getCode(), "发起支付失败，此订单已支付！");
			}

			// 判断缴费金额是否发生变化
			ParkVehicleRecord parkVehicleRecord = parkService.getNotOutVehicleRecordByPlateNo(plateNo);
			if(parkVehicleRecord == null) {
				logger.info("发起支付失败，查无此车辆或此车辆已出场   plateNo{}", plateNo);
				return new ParkResult(ResultEnum.ERROR_1.getCode(), "发起支付失败，查无此车辆或此车辆已出场");
			}
			ParkResult<ChargeBillVO> preChargeBill = parkService.getPreChargeBill(parkVehicleRecord, request);
			if(ResultEnum.SUCCESS.getCode() != preChargeBill.getCode()){
				logger.error("发起支付时查询订单失败error plateNo{},orderId{}", plateNo,orderId);
				return new ParkResult(ResultEnum.ERROR_SYS.getCode(), "获取订单有误!");
			}
			ChargeBillVO bill = preChargeBill.getData();
			BigDecimal realcost = new BigDecimal(bill.getRealCost());// 真实应付金额
			BigDecimal ordercost = new BigDecimal(order.getTotalCost());// 订单应付金额
			if (realcost.compareTo(ordercost) != 0) {
				logger.info("发起支付失败----用户停车费用变化  plateNo{}", plateNo);
				return new ParkResult(ResultEnum.ERROR_5_ORDER.getCode(), "发起支付失败，订单已过期！");
			}
			ParkVehiclePaymentRecord payRecord = new ParkVehiclePaymentRecord();
			payRecord.setPaymentId(order.getPaymentId());
			payRecord.setOutPaymentId(bill.getOutPreBillId());
			parkService.updatePaymentByPrimaryKey(payRecord);
			logger.info("wxAppId:{}", wxAppId);
			JSONObject result = parkService.toPay(order, ip, wxAppId, wxOpenId, parkVehicleRecord);
			// System.out.println(result);
			if (result != null && "Y".equals(result.get("status"))) {
				String data = result.getString("data");
				byte[] b = RSAUtils.decryptByPrivateKey(Base64Utils.decode(data),
						TempKeyStore.keystore.get("weiyipri"));
				String resultObj = new String(b);
				// System.out.println(resultObj);
				ParkResult parkResult = new ParkResult(ResultEnum.SUCCESS.getCode(), "发起支付成功");
				logger.info("发起支付成功  orderId={}", orderId);
				parkResult.setData(JsonUtils.jsonToMap(resultObj));
				return parkResult;
			} else {
				logger.error("发起支付失败  orderId{} plateNo{}", orderId,plateNo);
				return new ParkResult(ResultEnum.ERROR_6_PAY.getCode(), "发起支付失败");
			}

		} catch (Exception e) {
			logger.error("发起支付失败error plateNo{},orderId{}", plateNo,orderId);
			logger.error("发起支付失败error", e);
			return new ParkResult(ResultEnum.ERROR_SYS.getCode(), "系统错误！");
		}

	}

////	/**
////	 * 获取缴费记录
////	 * 
////	 * @param plateNo
////	 * @param currentPage
////	 * @param recordPerPage
////	 * @return
////	 */
////	@RequestMapping("/getpaymentlist")
////	@ResponseBody
////	public ParkResult getPaymentList(HttpServletRequest request, @RequestParam("plateNo") String plateNo,
////			@RequestParam(value = "currentPage", defaultValue = "1") int currentPage,
////			@RequestParam(value = "recordPerPage", defaultValue = "10") int recordPerPage) {
////
////		plateNo = plateNo.toUpperCase();
////		try {
////			PageView<PaymentPage> page = new PageView<PaymentPage>(recordPerPage, currentPage);
////			Map<String, Object> map = getParamMap(request);
////			if (currentPage != 0) {
////				map.put("start", (currentPage - 1) * recordPerPage + 1);
////				map.put("end", currentPage * recordPerPage);
////			} else {
////				map.remove("start");
////				map.remove("end");
////			}
////			long date = DateUtil.dateToLong_YMD(new Date());
////			long payTime1 = DateUtil.addDay(date, -1) * 1000000 + 594500;// 昨天59:45:00
////			long payTime2 = date * 1000000 + 595959; // 今天结束 59:59:59
////			map.put("plateNo", plateNo);
////			map.put("payTime1", payTime1);
////			map.put("payTime2", payTime2);
////			map.put("orderStatus", OrderStatusEnum.pay_yes.name());
////			map.put("orderBy", "payTime");
////			List<PaymentPage> paymentList = parkService.getPaymentPage(map);
////			long count = parkService.getPaymentPageCount(map);
////			page.setRecords(paymentList);
////			page.setTotalrecord(count);
////
////			ParkResult<PageView<PaymentPage>> pr = new ParkResult<PageView<PaymentPage>>();
////			pr.setCode(ResultEnum.SUCCESS.getCode());
////			pr.setData(page);
////			return pr;
////		} catch (Exception e) {
////			logger.error("获取缴费记录失败 plateNo{}", plateNo);
////			logger.error("获取缴费记录error", e);
////			return new ParkResult(ResultEnum.ERROR_SYS.getCode(), "系统错误！");
////		}
//	}

}
