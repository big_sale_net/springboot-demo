package com.yiwupay.park.action; 

import java.io.BufferedReader;
import java.lang.reflect.Field;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import com.yiwupay.park.service.CaiwuService;
import com.yiwupay.park.service.ParkService;
import com.yiwupay.park.service.ParkVehicleRecordService;
import com.yiwupay.park.service.WebOrderService;
import com.yiwupay.park.service.WxAppPlateNoService;


/**
 * 
 * Title: BaseAction
 * Description: 
 * Company:  
 * @author cailuxi
 * @date 2017年11月9日
 */
public class BaseAction {

	protected Logger logger = LoggerFactory.getLogger(this.getClass());
    
     @Resource 
     ParkService parkService;
     @Autowired
     CaiwuService caiwuService;
     
     @Autowired
     ParkVehicleRecordService parkVehicleRecordService;
     
     @Autowired 
     WebOrderService webOrderService;
     
     @Autowired
     WxAppPlateNoService wxAppPlateNoService;
     
     RestTemplate Template = new RestTemplate();
     
	/**
	 * 将请求参数包装为一个实体
	 * @param request
	 * @param clazz
	 * @return
	 */
	public Object getRequestEntity(HttpServletRequest request, Class clazz) throws Exception{
		Object obj=clazz.newInstance();
		Enumeration<String> params=request.getParameterNames();
        while(params.hasMoreElements()){
        	String name=params.nextElement();
        	try{
        		Field field=clazz.getDeclaredField(name);
        		field.setAccessible(true);
            	field.set(obj, request.getParameter(name));
        	}catch(NoSuchFieldException e){
        		logger.error("将请求参数包装为一个实体",e);
        		logger.error("未找到'"+name+"'的setter方法");
        	}
        }
		return obj;
	}
	
	/**
	 * 将请求参数存入map
	 * @param request
	 * @return
	 */
	public Map<String, Object> getParamMap(HttpServletRequest request){
		Enumeration<String> params=request.getParameterNames();
		Map<String, Object> map=new HashMap<String, Object>();
        while(params.hasMoreElements()){
        	String name=params.nextElement();
        	String value=request.getParameter(name);
        	map.put(name, value);
//        	System.out.println(name+":"+value);
        }
		return map;
	}
	
	public String buildMap(Map<String, String> map) {
		StringBuffer sb = new StringBuffer();
		if (map.size() > 0) {
			for (String key : map.keySet()) {
				sb.append(key + "=");
				if (StringUtils.isEmpty(map.get(key))) {
					sb.append("&");
				} else {
					sb.append(map.get(key) + "&");
				}
			}
		}
		return sb.toString();
	}
	
	/**
	 * 获取请求参数
	 * @param request
	 * @return
	 */
	protected String getRequsetBody(HttpServletRequest request){
		try{
			BufferedReader br = request.getReader();
			String buffer = null;
			StringBuffer buff = new StringBuffer();
			while ((buffer = br.readLine()) != null) {
				buff.append(buffer + "\n");
			}
			br.close();
			String param = buff.toString();
			return param;
		}catch (Exception e) {
			logger.error("参数处理异常", e);
			throw new RuntimeException("参数处理异常");
		}
	}
}
