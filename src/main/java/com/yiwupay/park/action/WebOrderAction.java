package com.yiwupay.park.action;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yiwupay.park.model.web.OrderSearchVo;
import com.yiwupay.park.utils.PageView;

/**
 * 停车场管理平台	订单action
 * Title: WebOrderAction
 * Description: 
 * Company:  
 * @author cailuxi
 * @date 2018年1月18日
 */
@Controller
@RequestMapping("/weborder")
@Service
public class WebOrderAction extends BaseAction {

	
	/**
	 * 获取订单查询list
	 * Description: 
	 * @param request
	 * @param currentPage
	 * @param recordPerPage
	 * @return
	 * @author cailuxi
	 * @date 2018年1月18日
	 */
	@RequestMapping("/getordersearchlist")
	@ResponseBody
	public PageView<OrderSearchVo> getOrderSearchList(HttpServletRequest request,
			@RequestParam(value = "currentPage", defaultValue = "1") int currentPage,
			@RequestParam(value = "recordPerPage", defaultValue = "10") int recordPerPage) {
		
		PageView<OrderSearchVo> page = new PageView<OrderSearchVo>(recordPerPage, currentPage);
		// 封装请求参数
		Map<String, Object> map = getParamMap(request);
		// 参数装载
		if(currentPage != 0){
			map.put("start", (currentPage - 1) * recordPerPage + 1);
			map.put("end", currentPage * recordPerPage);
		}else{
			map.remove("start");
			map.remove("end");
		}

		if (null != map.get("marketList") && !"".equals(map.get("marketList"))) {
			map.put("marketList", map.get("marketList").toString().split(","));
		}

		try {
			//获取订单结果集
			List<OrderSearchVo>  list = webOrderService.getOrderSearchList(map);
			//获取订单count
			Long count = webOrderService.getOrderSearchListCount(map);
			
			page.setRecords(list);
			page.setTotalrecord(count);
			
			//返回分页对象给前端
			return page;
		} catch (Exception e) {
			logger.error("获取订单列表error",e);
		}
		return null;
	}
	
	
	/**
	 * 返回所有支付金额
	 * Description: 
	 * @param request
	 * @param currentPage
	 * @param recordPerPage
	 * @return
	 * @author cailuxi
	 * @date 2018年1月31日
	 */
	@RequestMapping("/getordersumcost")
	@ResponseBody
	public BigDecimal getOrderSumCost(HttpServletRequest request) {
		
		// 封装请求参数
		Map<String, Object> map = getParamMap(request);

		if (null != map.get("marketList") && !"".equals(map.get("marketList"))) {
			map.put("marketList", map.get("marketList").toString().split(","));
		}

		try {
			//获取订单count
			BigDecimal sumCost = webOrderService.getOrderSumCost(map);
			
			//返回给前端
			return sumCost;
		} catch (Exception e) {
			logger.error("返回所有支付金额error",e);
		}
		return new BigDecimal(0);
	}
}
