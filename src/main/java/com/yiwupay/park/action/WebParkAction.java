/**
 * 
 */
package com.yiwupay.park.action;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yiwupay.park.enums.PayTypeEnum;
import com.yiwupay.park.model.web.ParkSystemVo;
import com.yiwupay.park.model.web.VehicleCostVo;
import com.yiwupay.park.model.web.VehicleSearchVo;
import com.yiwupay.park.model.web.VehicleStaticSearchVo;
import com.yiwupay.park.utils.PageView;

import net.sf.json.JSONArray;

/**
 * 
 * Title: VehicleAction 
 * Description: 停车系统action
 * 
 * @author wangpeng
 * @date 2018年01月16日
 */
@Controller
@RequestMapping("/webpark")
public class WebParkAction extends BaseAction {

	/**
	 * 获取停车场列表
	 */
	@RequestMapping(value = "/parkSystemList", produces = "text/plain;charset=UTF-8")
	@ResponseBody
	public PageView<ParkSystemVo> carstoplist(HttpServletRequest request,
			@RequestParam(value = "currentPage", defaultValue = "1") int currentPage) {

		// 封装请求参数
		Map<String, Object> map = getParamMap(request);

		// 页数、行数
		int rows = 10;

		// 参数装载
		if (currentPage != 0) {
			map.put("start", (currentPage - 1) * rows + 1);
			map.put("end", currentPage * rows);
		}else{
			map.remove("start");
			map.remove("end");
		}
		
		if (null != map.get("marketList") && !"".equals(map.get("marketList"))) {
			map.put("marketList", map.get("marketList").toString().split(","));
		}

		List<ParkSystemVo> list = null;
		long count = 0;

		list = parkService.getParkSystemList(map);
		count = parkService.getParkSystemListCount(map);
		
		for (ParkSystemVo parkSystemVo : list) {
			Long parkingId = parkSystemVo.getParkingId();
			List<String> roadWayNameList = parkService.getInOutNameByParkingId(parkingId);
			parkSystemVo.setRoadWayName(roadWayNameList);
			parkSystemVo.setDrivewayCount(roadWayNameList.size());
		}
		
		// 结果返回
		PageView<ParkSystemVo> page = new PageView<ParkSystemVo>(rows, currentPage);
		page.setRecords(list);
		page.setTotalrecord(count);

		return page;
	}
	
	/**
	 * 获取流动车辆查询list(平台)
	 */
	@RequestMapping(value = "/vehicleList", produces = "text/plain;charset=UTF-8")
	@ResponseBody
	public PageView<VehicleSearchVo> vehicleList(HttpServletRequest request,
			@RequestParam(value = "currentPage", defaultValue = "1") int currentPage) {

		// 封装请求参数
		Map<String, Object> map = getParamMap(request);

		// 页数、行数
		int rows = 10;

		// 参数装载
		if (currentPage != 0) {
			map.put("start", (currentPage - 1) * rows + 1);
			map.put("end", currentPage * rows);
		}else{
			map.remove("start");
			map.remove("end");
		}
		
		if (null != map.get("marketList") && !"".equals(map.get("marketList"))) {
			map.put("marketList", map.get("marketList").toString().split(","));
		}

		List<VehicleSearchVo> list = null;
		long count = 0;

		list = parkService.getVehicleList(map);
		count = parkService.getVehicleListCount(map);
		
		for (VehicleSearchVo vehicleSearchVo : list) {
			Long recordStId = vehicleSearchVo.getRecordStId();
			VehicleSearchVo vo = parkService.getCostAndType(recordStId);
			if(vo!= null){
				vehicleSearchVo.setCost(vo.getCost());
				//vehicleSearchVo.setPayType(vo.getPayType().replace(PayTypeEnum.OTHER.name(), PayTypeEnum.OTHER.getText()));
			}else{
				if("1".equals(vehicleSearchVo.getIsOut())){
					vehicleSearchVo.setCost(0L);
				}
			}
		}
		
		// 结果返回
		PageView<VehicleSearchVo> page = new PageView<VehicleSearchVo>(rows, currentPage);
		page.setRecords(list);
		page.setTotalrecord(count);

		return page;
	}
	
	
	
	/**
	 * 获取流动车辆查询list(历史)
	 * Description: 
	 * @param request
	 * @param currentPage
	 * @param recordPerPage
	 * @return
	 * @author cailuxi
	 * @date 2018年1月18日
	 */
	@RequestMapping("/getvehiclesearchvolist")
	@ResponseBody
	public PageView<VehicleSearchVo> getVehicleSearchVoList(HttpServletRequest request,
			@RequestParam(value = "currentPage", defaultValue = "1") int currentPage,
			@RequestParam(value = "recordPerPage", defaultValue = "10") int recordPerPage) {
		
		PageView<VehicleSearchVo> page = new PageView<VehicleSearchVo>(recordPerPage, currentPage);
		// 封装请求参数
		Map<String, Object> map = getParamMap(request);
		// 参数装载
		if(currentPage != 0){
			map.put("start", (currentPage - 1) * recordPerPage + 1);
			map.put("end", currentPage * recordPerPage);
		}else{
			map.remove("start");
			map.remove("end");
		}

		if (null != map.get("marketList") && !"".equals(map.get("marketList"))) {
			map.put("marketList", map.get("marketList").toString().split(","));
		}

		try {
			//获取订单结果集
			List<VehicleSearchVo>  list = parkService.getVehicleSearchVoList(map);
			if(list != null && list.size() > 0){
				for(int i = 0;i < list.size();i++){
					//通过过车id查找缴费同步表的应付金额和支付类型
					List<VehicleCostVo> listCost = parkService.getVehicleCostVoByRecordStId(list.get(i).getRecordStId());
					if(listCost != null && listCost.size() > 0){
						Long cost = 0l;
						String payType = "";
						
						for(int j = 0;j < listCost.size();j++){
							cost = cost + listCost.get(j).getCost();
							payType = payType.concat(PayTypeEnum.valueOf(listCost.get(j).getPayType()).getText());
							if(j < listCost.size()-1){
								payType = payType.concat(",");
							}
						}
						list.get(i).setCost(cost);
						list.get(i).setPayType(payType);
					}else{
						if("1".equals(list.get(i).getIsOut())){
							list.get(i).setCost(0l);
							list.get(i).setPayType("免费");
						}
					}
				}
			}
			//获取订单count
			Long count = parkService.getVehicleSearchVoListCount(map);
			
			page.setRecords(list);
			page.setTotalrecord(count);
			
			//返回分页对象给前端
			return page;
		} catch (Exception e) {
			logger.error("获取流动车辆列表error",e);
		}
		return null;
	}
	
	
	
	/**
	 * 获取车辆统计列表(历史数据)
	 * @param request
	 * @param currentPage
	 * @return
	 */
	@RequestMapping(value = "/carStatishisList",produces = "text/plain;charset=UTF-8")
	@ResponseBody
	public PageView<VehicleStaticSearchVo> carStaisHisList(HttpServletRequest request,
			@RequestParam(value = "currentPage", defaultValue = "1") int currentPage) {

		// 封装请求参数
		Map<String, Object> map = getParamMap(request);

		// 页数、行数
		int rows = 10;

		// 参数装载
		if (currentPage != 0) {
			map.put("start", (currentPage - 1) * rows + 1);
			map.put("end", currentPage * rows);
		}else{
			map.remove("start");
			map.remove("end");
		}
		String ss = map.get("marketList").toString();
//		System.out.println(ss);
		if (null != map.get("marketList") && !"".equals(map.get("marketList"))) {
			map.put("marketList", map.get("marketList").toString().split(","));
		}

		List<VehicleStaticSearchVo> list = null;
		long count = 0;

		list = parkService.getVehicleStatisHisList(map);
		count = parkService.getVehicleStatisHisListCount(map);
		// 结果返回
		PageView<VehicleStaticSearchVo> page = new PageView<VehicleStaticSearchVo>(rows, currentPage);
		page.setRecords(list);
		page.setTotalrecord(count);

		return page;
	}
	
	/**
	 * 获取车辆统计列表(实时数据)
	 * @param request
	 * @param currentPage
	 * @return
	 */
	@RequestMapping(value = "/carStatisList",produces = "text/plain;charset=UTF-8")
	@ResponseBody
	public PageView<VehicleStaticSearchVo> carStaisList(HttpServletRequest request,
			@RequestParam(value = "currentPage", defaultValue = "1") int currentPage) {

		// 封装请求参数
		Map<String, Object> map = getParamMap(request);

		// 页数、行数
		int rows = 10;

		// 参数装载
		if (currentPage != 0) {
			map.put("start", (currentPage - 1) * rows + 1);
			map.put("end", currentPage * rows);
		}else{
			map.remove("start");
			map.remove("end");
		}
		String ss = map.get("marketList").toString();
//		System.out.println(ss);
		if (null != map.get("marketList") && !"".equals(map.get("marketList"))) {
			map.put("marketList", map.get("marketList").toString().split(","));
		}

		List<VehicleStaticSearchVo> list = null;
		long count = 0;

		list = parkService.getVehicleStatisList(map);
		count = parkService.getVehicleStatisListCount(map);
		// 结果返回
		PageView<VehicleStaticSearchVo> page = new PageView<VehicleStaticSearchVo>(rows, currentPage);
		page.setRecords(list);
		page.setTotalrecord(count);

		return page;
	}
	
	
	/**
	 * 联动
	 */
	@RequestMapping(value = "/marketParkingSelector", produces = "text/plain;charset=UTF-8")
	@ResponseBody
	public JSONArray marketParkingSelector(String marketList) {
		return parkService.marketParkingSelector(marketList);
	}
}
