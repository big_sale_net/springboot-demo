package com.yiwupay.park.config;

import javax.sql.DataSource;

import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;

@Configuration
public class ApplicationMybatisConfig {
	
	//这里在测试的时候使用Autowired的方式无法注入
	//@Autowired
	//DataSource dataSource;
	//以参数传进的方式，springboot会根据引用自动地注入dataSource
	@Bean("sqlSessionFactory")
	public SqlSessionFactoryBean sqlSessionFactory(DataSource dataSource) throws Exception {
		
		SqlSessionFactoryBean sqlSessionFactory = new SqlSessionFactoryBean();
		sqlSessionFactory.setDataSource(dataSource);
		
		ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        Resource mybatisConfigXml = resolver.getResource("classpath:mybatis/mybatis-config.xml");
		sqlSessionFactory.setConfigLocation(mybatisConfigXml);
//		sqlSessionFactory.setTypeAliasesPackage("com.yiwupay.park.model");
		return sqlSessionFactory;
	}
	
	
	
	
}