package com.yiwupay.park.config;

import org.mybatis.spring.mapper.MapperScannerConfigurer;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@AutoConfigureAfter(ApplicationMybatisConfig.class) //保证在MyBatisConfig实例化之后再实例化该类
public class MapperScannerConfig {
    
    // mapper接口的扫描器
	@Bean
	public MapperScannerConfigurer mapperScannerConfigurer() {
		
		MapperScannerConfigurer msc = new MapperScannerConfigurer();
		
		msc.setBasePackage("com.yiwupay.park.dao");
		msc.setSqlSessionFactoryBeanName("sqlSessionFactory");
		
		return msc;
		
	}
}
