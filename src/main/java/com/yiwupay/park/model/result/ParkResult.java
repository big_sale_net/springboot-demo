package com.yiwupay.park.model.result;

import java.util.List;

import com.yiwupay.park.enums.HMErrorCodeEnum;
import com.yiwupay.park.utils.JsonUtils;

import net.sf.json.JSONObject;

public class ParkResult<T> {

	private int code;

	private String message;

	private T data;

	public ParkResult() {

	}

	public ParkResult(int code, String message) {
		super();
		this.code = code;
		this.message = message;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public static void main(String[] args) {
		HikResult result = new HikResult();
	}

	/**
	 * 海康结果集转化通用结果集，data为非集合obj
	 */
	public static <T> ParkResult<T> createParkResult(HikResult result, Class<?> clazz, Class<T> clazz_T) {
		ParkResult<T> p = new ParkResult<T>(result.getErrorCode(), result.getErrorMessage());
		Object data = result.getData();
		if (data != null && clazz != null) {
			try {
				//讲data转化为clazz类型的对象
				Object obj = JSONObject.toBean(JSONObject.fromObject(data), clazz);
				//将clazz类型的对象转化为clazz_T的对象
				T t = JsonUtils.obj2Obj_T(obj, clazz, clazz_T);
				p.setData(t);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return p;
	}
	
	
	
	/**
	 * 捷惠通结果集转化通用结果集，data为非集合obj
	 * @throws Exception 
	 */
	public static <T> ParkResult<T> createParkResult(JHTResult result, Class<?> clazz, Class<T> clazz_T) throws Exception {
		ParkResult<T> p = new ParkResult<T>(result.getResultCode(), result.getMessage());
		 List<JHTDataItemsResult> dataItems = result.getDataItems();
		if (dataItems != null && clazz != null) {
			JHTDataItemsResult data = dataItems.get(0);
			//讲data转化为clazz类型的对象
			Object obj = JSONObject.toBean(JSONObject.fromObject(data.getAttributes()), clazz);
			//将clazz类型的对象转化为clazz_T的对象
			T t = JsonUtils.obj2Obj_T(obj, clazz, clazz_T);
			p.setData(t);
		}
		return p;
	}
	
	/**
	 * 红门停车系统结果集转化通用结果集，data为非集合obj
	 * @throws Exception 
	 */
	public static <T> ParkResult<T> createParkResult(HMResult result, Class<?> clazz, Class<T> clazz_T) throws Exception {
		ParkResult<T> p = new ParkResult<T>(HMErrorCodeEnum.getEnumValue(result.getCode()), HMErrorCodeEnum.getEnumMsg(result.getCode()));
		 Object obj = result.getBody();
		if (obj != null && clazz != null) {
			//讲data转化为clazz类型的对象
			Object objs = JSONObject.toBean(JSONObject.fromObject(obj), clazz);
			//将clazz类型的对象转化为clazz_T的对象
			T t = JsonUtils.obj2Obj_T(objs, clazz, clazz_T);
			p.setData(t);
		}
		return p;
	}
	
	public static  ParkResult<?> createParkResult(HikResult result){
		return createParkResult(result, null, null);
	}
	
	
	public static  ParkResult<?> createParkResult(JHTResult result) throws Exception{
		return createParkResult(result, null, null);
	}

	@Override
	public String toString() {
		return "ParkResult [code=" + code + ", message=" + message + ", data=" + data + "]";
	}
	
}
