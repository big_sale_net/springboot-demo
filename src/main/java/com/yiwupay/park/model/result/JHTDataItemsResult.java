package com.yiwupay.park.model.result;

import java.util.List;

/**
 * 捷惠通返回结果子对象结果
 * @author zls
 * @param <E>
 *
 */
public class JHTDataItemsResult<E> {
	//子对象ID
	private String objectId;
	//子对象操作类型,固定值传入：READ
	private String operateType;
	//返回结果值对象
	private E attributes;
	
	private List failItems;
	
	private List subItems;

	public String getObjectId() {
		return objectId;
	}

	public void setObjectId(String objectId) {
		this.objectId = objectId;
	}

	public String getOperateType() {
		return operateType;
	}

	public void setOperateType(String operateType) {
		this.operateType = operateType;
	}

	


	public E getAttributes() {
		return attributes;
	}

	public void setAttributes(E attributes) {
		this.attributes = attributes;
	}

	public List getFailItems() {
		return failItems;
	}

	public void setFailItems(List failItems) {
		this.failItems = failItems;
	}

	public List getSubItems() {
		return subItems;
	}

	public void setSubItems(List subItems) {
		this.subItems = subItems;
	}

	@Override
	public String toString() {
		return "JHTDataItemsResult [objectId=" + objectId + ", operateType=" + operateType + ", attributes="
				+ attributes + ", failItems=" + failItems + ", subItems=" + subItems + "]";
	}
	
	
}
