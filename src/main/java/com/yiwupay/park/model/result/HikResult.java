package com.yiwupay.park.model.result;

import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 停车场管理自定义响应结果（对接第三方响应结果）
 * @author zls
 *
 */
public class HikResult {
	// 定义jackson对象
    private static final ObjectMapper MAPPER = new ObjectMapper();

    // 响应业务状态(0:代表成功返回，非0代表非成功返回)
    private Integer errorCode;

    // 错误消息
    private String errorMessage;

    // 节点列表
    private Object data;
    
    public static HikResult build(Integer errorCode, String errorMessage, Object data) {
        return new HikResult(errorCode, errorMessage, data);
    }
    public static HikResult ok(Object data) {
        return new HikResult(data);
    }
    public static HikResult ok() {
        return new HikResult(null);
    }

    public HikResult() {

    }
    public static HikResult build(Integer errorCode, String errorMessage) {
        return new HikResult(errorCode, errorMessage, null);
    }

    public HikResult(Integer errorCode, String errorMessage, Object data) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
        this.data = data;
    }

    public HikResult(Object data) {
        this.errorCode = 0;
        this.errorMessage = "OK";
        this.data = data;
    }
	public Integer getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	 /**
     * 将json结果集转化为ParkResult对象
     * 
     * @param jsonData json数据
     * @param clazz ParkResult中的object类型
     * @return
     */
    public static HikResult formatToPojo(String jsonData, Class<?> clazz) {
        try {
            if (clazz == null) {
                return MAPPER.readValue(jsonData, HikResult.class);
            }
            JsonNode jsonNode = MAPPER.readTree(jsonData);
            JsonNode data = jsonNode.get("data");
            Object obj = null;
            if (clazz != null) {
                if (data.isObject()) {
                    obj = MAPPER.readValue(data.traverse(), clazz);
                } else if (data.isTextual()) {
                    obj = MAPPER.readValue(data.asText(), clazz);
                }
            }
            return build(jsonNode.get("errorCode").intValue(), jsonNode.get("errorMessage").asText(), obj);
        } catch (Exception e) {
            return null;
        }
    }
    
    /**
     * 没有object对象的转化
     * 
     * @param json
     * @return
     */
    public static HikResult format(String json) {
        try {
            return MAPPER.readValue(json, HikResult.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    
    /**
     * Object是集合转化
     * 
     * @param jsonData json数据
     * @param clazz 集合中的类型
     * @return
     */
    public static HikResult formatToList(String jsonData, Class<?> clazz) {
        try {
            JsonNode jsonNode = MAPPER.readTree(jsonData);
            JsonNode data = jsonNode.get("data");
            Object obj = null;
            if (data.isArray() && data.size() > 0) {
                obj = MAPPER.readValue(data.traverse(),
                        MAPPER.getTypeFactory().constructCollectionType(List.class, clazz));
            }
            return build(jsonNode.get("errorCode").intValue(), jsonNode.get("errorMessage").asText(), obj);
        } catch (Exception e) {
            return null;
        }
    }
	@Override
	public String toString() {
		return "HikResult [errorCode=" + errorCode + ", errorMessage=" + errorMessage + ", data=" + data + "]";
	}
    
}

