package com.yiwupay.park.model.result;

import com.fasterxml.jackson.databind.ObjectMapper;
/**
 * 红门返回结果封装
 * author:tongsonghao
 * */
public class HMResult {
	// 定义jackson对象
    private static final ObjectMapper MAPPER = new ObjectMapper();
    
    //返回状态 success表示成功
    private String code;
    //请求Id
    private String requestId;
    //返回内容的body
    private Object body;
    
    public HMResult(){}
    public HMResult(String requesrId,String code,Object body){
    	this.requestId=requesrId;
    	this.code=code;
    	this.body=body;
    }
    public static HMResult bulid(String requesrId,String code,Object body){
    	return new HMResult(requesrId, code, body);
    }
	public String getRequestId() {
		return requestId;
	}
	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}
	public String getCode() {
		return code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	public Object getBody() {
		return body;
	}
	public void setBody(Object body) {
		this.body = body;
	}
	@Override
	public String toString() {
		return "HMResult [requestId=" + requestId + ", resultCode=" + code + ", body=" + body + "]";
	}
    
}
