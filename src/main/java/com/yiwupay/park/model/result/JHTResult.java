package com.yiwupay.park.model.result;

import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 捷慧通返回结果封装
 * @author zls
 *
 */
public class JHTResult {
	// 定义jackson对象
    private static final ObjectMapper MAPPER = new ObjectMapper();
    //服务标识
	private String serviceId;
	//返回码0：成功,1：未知错误,2：参数不正确
	private Integer resultCode;
	private String seqId;
    private Object attributes;
    private String message;

	private List<JHTDataItemsResult> dataItems;
	
	public JHTResult(){}
	public JHTResult(String serviceId, Integer resultCode, String attributes, List<JHTDataItemsResult> dataItems, String seqId,String message){
		this.serviceId = serviceId;
		this.resultCode = resultCode;
		this.attributes = attributes;
		this.dataItems = dataItems;
		this.seqId = seqId;
		this.message = message;
	}
	public static JHTResult build(String serviceId, Integer resultCode, String attributes, List<JHTDataItemsResult> dataItems, String seqId, String message){
		return new JHTResult(serviceId,resultCode,attributes,dataItems,seqId,message);
	}
	public String getServiceId() {
		return serviceId;
	}
	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}
	public Integer getResultCode() {
		return resultCode;
	}
	public void setResultCode(Integer resultCode) {
		this.resultCode = resultCode;
	}
	public String getSeqId() {
		return seqId;
	}
	public void setSeqId(String seqId) {
		this.seqId = seqId;
	}
	public Object getAttributes() {
		return attributes;
	}
	public void setAttributes(Object attributes) {
		this.attributes = attributes;
	}
	public List<JHTDataItemsResult> getDataItems() {
		return dataItems;
	}
	public void setDataItems(List<JHTDataItemsResult> dataItems) {
		this.dataItems = dataItems;
	}
	@Override
	public String toString() {
		return "JHTResult [serviceId=" + serviceId + ", resultCode=" + resultCode + ", seqId=" + seqId + ", attributes="
				+ attributes + ", dataItems=" + dataItems + "]";
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	

}
