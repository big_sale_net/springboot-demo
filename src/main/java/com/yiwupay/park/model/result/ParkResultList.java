package com.yiwupay.park.model.result;

import java.util.ArrayList;
import java.util.List;

import com.yiwupay.park.enums.HMErrorCodeEnum;
import com.yiwupay.park.utils.JsonUtils;

import net.sf.json.JSONArray;

public class ParkResultList<T> {

	private int code;

	private String message;

	private List<T> data;
	
	public ParkResultList() {

	}

	public ParkResultList(int code, String message) {
		super();
		this.code = code;
		this.message = message;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<T> getData() {
		return data;
	}

	public void setData(List<T> data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "ParkResultList [code=" + code + ", message=" + message + ", data=" + data + "]";
	}

	/**
	 * 海康结果集转化通用结果集，data为list类型
	 */
	public static <T> ParkResultList<T> createParkResultList(HikResult result, Class<?> clazz, Class<T> clazz_T) {
		ParkResultList<T> p = new ParkResultList<T>(result.getErrorCode(), result.getErrorMessage());
		Object data = result.getData();
		if (data != null && clazz != null) {
			List<T> list = new ArrayList<T>();
			try {
				//将data转化为clazz类型的list 
				List hikpplist = (List) JSONArray.toCollection(JSONArray.fromObject(data), clazz);
				for (int i = 0; i < hikpplist.size(); i++) {
					Object obj = hikpplist.get(i);
					//讲clazz类型的对象转化为clazz_T的对象
					T t = JsonUtils.obj2Obj_T(obj, clazz, clazz_T);
					list.add(t);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			p.setData(list);
		}
		return p;
	}
	
	/**
	 * 捷惠通结果集转化通用结果集，data为list类型
	 */
	public static <T> ParkResultList<T> createParkResultList(JHTResult result, Class<?> clazz, Class<T> clazz_T) {
		ParkResultList<T> p = new ParkResultList<T>(result.getResultCode(), null);
		List<JHTDataItemsResult> data = (List)result.getDataItems();
		List<Object> listObject = new ArrayList<>();
		for (JHTDataItemsResult object : data) {
			Object attributes = object.getAttributes();
			listObject.add(attributes);
		}
		if (listObject != null && clazz != null) {
			List<T> list = new ArrayList<T>();
			try {
				//将data转化为clazz类型的list 
				List hikpplist = (List) JSONArray.toCollection(JSONArray.fromObject(listObject), clazz);
				for (int i = 0; i < hikpplist.size(); i++) {
					Object obj = hikpplist.get(i);
					//讲clazz类型的对象转化为clazz_T的对象
					T t = JsonUtils.obj2Obj_T(obj, clazz, clazz_T);
					list.add(t);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			p.setData(list);
		}
		return p;
	}
	
	
	/**
	 * 红门结果集转化通用结果集，data为list类型
	 */
	public static <T> ParkResultList<T> createParkResultList(HMResult result, Class<?> clazz, Class<T> clazz_T) {
		ParkResultList<T> p = new ParkResultList<T>(HMErrorCodeEnum.getEnumValue(result.getCode()), HMErrorCodeEnum.getEnumMsg(result.getCode()));
		Object data = result.getBody();
		if (data != null && clazz != null) {
			List<T> list = new ArrayList<T>();
			try {
				//将data转化为clazz类型的list 
				List dhpplist = (List) JSONArray.toCollection(JSONArray.fromObject(data), clazz);
				for (int i = 0; i < dhpplist.size(); i++) {
					Object obj = dhpplist.get(i);
					//讲clazz类型的对象转化为clazz_T的对象
					T t = JsonUtils.obj2Obj_T(obj, clazz, clazz_T);
					list.add(t);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			p.setData(list);
		}
		return p;
	}

}
