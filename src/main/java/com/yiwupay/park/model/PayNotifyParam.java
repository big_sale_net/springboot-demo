package com.yiwupay.park.model;

//通知对象
public class PayNotifyParam {
	// 内部订单号
	private Long outerOrderNo;
	// 支付网关订单号
	private Long paymentOrderId;
	// 所属应用
	private String belongApp;
	// 支付金额
	private Long tradeAmount;
	// 服务费用
	private Long feeAmount;
	// 支付状态
	private String paymentStatus;
	// 银行
	private String bank;
	// 支付通道
	private String bankInterface;
	// 支付时间
	private String payTime;
	// 支付方式
	private String interfaceType;

	public Long getOuterOrderNo() {
		return outerOrderNo;
	}

	public void setOuterOrderNo(Long outerOrderNo) {
		this.outerOrderNo = outerOrderNo;
	}

	public Long getPaymentOrderId() {
		return paymentOrderId;
	}

	public void setPaymentOrderId(Long paymentOrderId) {
		this.paymentOrderId = paymentOrderId;
	}

	public String getBelongApp() {
		return belongApp;
	}

	public void setBelongApp(String belongApp) {
		this.belongApp = belongApp;
	}

	public Long getTradeAmount() {
		return tradeAmount;
	}

	public void setTradeAmount(Long tradeAmount) {
		this.tradeAmount = tradeAmount;
	}

	public Long getFeeAmount() {
		return feeAmount;
	}

	public void setFeeAmount(Long feeAmount) {
		this.feeAmount = feeAmount;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public String getBank() {
		return bank;
	}

	public void setBank(String bank) {
		this.bank = bank;
	}

	public String getBankInterface() {
		return bankInterface;
	}

	public void setBankInterface(String bankInterface) {
		this.bankInterface = bankInterface;
	}


	@Override
	public String toString() {
		return "PayNotifyParam [outerOrderNo=" + outerOrderNo + ", paymentOrderId=" + paymentOrderId + ", belongApp="
				+ belongApp + ", tradeAmount=" + tradeAmount + ", feeAmount=" + feeAmount + ", paymentStatus="
				+ paymentStatus + ", bank=" + bank + ", bankInterface=" + bankInterface + ", payTime=" + payTime
				+ ", interfaceType=" + interfaceType + "]";
	}

	public String getInterfaceType() {
		return interfaceType;
	}

	public void setInterfaceType(String interfaceType) {
		this.interfaceType = interfaceType;
	}

	public String getPayTime() {
		return payTime;
	}

	public void setPayTime(String payTime) {
		this.payTime = payTime;
	}
	

}
