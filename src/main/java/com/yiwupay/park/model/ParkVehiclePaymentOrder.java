package com.yiwupay.park.model;

/**
 * 过车缴费订单(对应PS_PARK_VEHICLE_PAYMENT_ORDER表)
 * @author zls
 *
 */
public class ParkVehiclePaymentOrder {
	/**
	 * 订单号
	 */
    private Long orderId;
    
    /**
     * 过车缴费记录单号
     */
    private Long paymentId;
    
    /**
     * 支付系统订单号
     */
    private Long outTradeNo;
    
    /**
     * 缴费金额
     */
    private Long totalCost;
    
    /**
     * 手续费金额
     */
    private Long totalFee;
    
    /**
     * 订单创建时间
     */
    private Long createTime;
    
    /**
     * 订单支付时间
     */
    private Long payTime;
    
    /**
     * 订单状态
     */
    private String orderStatus;
     
    /**
     * 支付通道
     */
    private String payWay;
    
    /**
     * 支付类型
     */
    private String payType;
    
    /**
     * 微信Openid
     */
    private String wxOpenid;
    /**
     * 应用appid
     */
    private String appid;
    
    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(Long paymentId) {
        this.paymentId = paymentId;
    }

    public Long getOutTradeNo() {
        return outTradeNo;
    }

    public void setOutTradeNo(Long outTradeNo) {
        this.outTradeNo = outTradeNo;
    }

    public Long getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(Long totalCost) {
        this.totalCost = totalCost;
    }

    public Long getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(Long totalFee) {
        this.totalFee = totalFee;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Long getPayTime() {
        return payTime;
    }

    public void setPayTime(Long payTime) {
        this.payTime = payTime;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus == null ? null : orderStatus.trim();
    }

    public String getPayWay() {
        return payWay;
    }

    public void setPayWay(String payWay) {
        this.payWay = payWay == null ? null : payWay.trim();
    }

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	public String getWxOpenid() {
		return wxOpenid;
	}

	public void setWxOpenid(String wxOpenid) {
		this.wxOpenid = wxOpenid;
	}

	public String getAppid() {
		return appid;
	}

	public void setAppid(String appid) {
		this.appid = appid;
	}

}