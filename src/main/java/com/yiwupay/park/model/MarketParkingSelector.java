package com.yiwupay.park.model;

/**
 * 下拉选联动返回实体类
 * @author he
 *
 */
public class MarketParkingSelector {
	private String parkingIds;
	private String parkingNames;
	private String marketName;
	private Long marketId;
	public String getParkingIds() {
		return parkingIds;
	}
	public void setParkingIds(String parkingIds) {
		this.parkingIds = parkingIds;
	}
	public String getParkingNames() {
		return parkingNames;
	}
	public void setParkingNames(String parkingNames) {
		this.parkingNames = parkingNames;
	}
	public String getMarketName() {
		return marketName;
	}
	public void setMarketName(String marketName) {
		this.marketName = marketName;
	}
	public Long getMarketId() {
		return marketId;
	}
	public void setMarketId(Long marketId) {
		this.marketId = marketId;
	}
	
}
