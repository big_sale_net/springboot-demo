package com.yiwupay.park.model;
/**
 * 停车场车道实体类（对应PS_PARK_ROADWAY表）
 * @author zls
 *
 */
public class ParkRoadWay {
	/**
	 * ID
	 */
    private Long roadwayId;
    
    /**
     * 系统id
     */
    private Long systemId;
    
    /**
     * 市场id
     */
    private String marektId;
    
    /**
     * 出入口id
     */
    private Long entranceId;
    
    /**
     * 接入系统出口id
     */
    private String outEntranceId;
    
    /**
     * 车道id
     */
    private String outRoadwayId;
    
    /**
     * 车道code
     */
    private String roadwayCode;
    
    /**
     * 车道名称
     */
    private String roadwayName;

    public Long getRoadwayId() {
        return roadwayId;
    }

    public void setRoadwayId(Long roadwayId) {
        this.roadwayId = roadwayId;
    }

    public Long getSystemId() {
        return systemId;
    }

    public void setSystemId(Long systemId) {
        this.systemId = systemId;
    }

    public String getMarektId() {
        return marektId;
    }

    public void setMarektId(String marektId) {
        this.marektId = marektId == null ? null : marektId.trim();
    }

    public Long getEntranceId() {
        return entranceId;
    }

    public void setEntranceId(Long entranceId) {
        this.entranceId = entranceId;
    }

    public String getOutEntranceId() {
        return outEntranceId;
    }

    public void setOutEntranceId(String outEntranceId) {
        this.outEntranceId = outEntranceId == null ? null : outEntranceId.trim();
    }

    public String getOutRoadwayId() {
        return outRoadwayId;
    }

    public void setOutRoadwayId(String outRoadwayId) {
        this.outRoadwayId = outRoadwayId == null ? null : outRoadwayId.trim();
    }

    public String getRoadwayCode() {
        return roadwayCode;
    }

    public void setRoadwayCode(String roadwayCode) {
        this.roadwayCode = roadwayCode == null ? null : roadwayCode.trim();
    }

    public String getRoadwayName() {
        return roadwayName;
    }

    public void setRoadwayName(String roadwayName) {
        this.roadwayName = roadwayName == null ? null : roadwayName.trim();
    }
}