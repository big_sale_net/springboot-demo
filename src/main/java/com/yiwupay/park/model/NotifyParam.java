package com.yiwupay.park.model;

public class NotifyParam {
	//订单号
	private Long orderNo;
	private Long systemId;
	//订单创建时间
	private Long orderCreateTime;
	private String outParkId;
	//接入系统车道（匝道）id
	private String outRoadwayId;
	//车牌号
	private String plateNo;
	//接入系统账单id	
	private String outBillId;
	//接入系统过车记录
	private String outRecordId;
	//总金额（分）
	private Long totalCost;
	//应收金额（分）
	private Long realCost;
	//支付金额（分）
	private Long cost;
	
	private Long payTime;
	/**
	 * 停车流水号(3区)
	 */
	private String parkingSerial;
	
	/**
	 * 支付方式
	 */
	private String payType;
	
	
	public String getParkingSerial() {
		return parkingSerial;
	}
	public void setParkingSerial(String parkingSerial) {
		this.parkingSerial = parkingSerial;
	}
	public String getPayType() {
		return payType;
	}
	public void setPayType(String payType) {
		this.payType = payType;
	}
	public String getOutParkId() {
		return outParkId;
	}
	public void setOutParkId(String outParkId) {
		this.outParkId = outParkId;
	}
	public String getOutRoadwayId() {
		return outRoadwayId;
	}
	public void setOutRoadwayId(String outRoadwayId) {
		this.outRoadwayId = outRoadwayId;
	}
	public String getPlateNo() {
		return plateNo;
	}
	public void setPlateNo(String plateNo) {
		this.plateNo = plateNo;
	}
	
	public String getOutRecordId() {
		return outRecordId;
	}
	public void setOutRecordId(String outRecordId) {
		this.outRecordId = outRecordId;
	}
	public String getOutBillId() {
		return outBillId;
	}
	public void setOutBillId(String outBillId) {
		this.outBillId = outBillId;
	}
	public Long getTotalCost() {
		return totalCost;
	}
	public void setTotalCost(Long totalCost) {
		this.totalCost = totalCost;
	}
	public Long getRealCost() {
		return realCost;
	}
	public void setRealCost(Long realCost) {
		this.realCost = realCost;
	}
	public Long getCost() {
		return cost;
	}
	public void setCost(Long cost) {
		this.cost = cost;
	}
	public Long getSystemId() {
		return systemId;
	}
	public void setSystemId(Long systemId) {
		this.systemId = systemId;
	}
	public Long getPayTime() {
		return payTime;
	}
	public void setPayTime(Long payTime) {
		this.payTime = payTime;
	}

	public Long getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(Long orderNo) {
		this.orderNo = orderNo;
	}
	public Long getOrderCreateTime() {
		return orderCreateTime;
	}
	public void setOrderCreateTime(Long orderCreateTime) {
		this.orderCreateTime = orderCreateTime;
	}
	
	
	@Override
	public String toString() {
		return "NotifyParam [orderNo=" + orderNo + ", systemId=" + systemId + ", orderCreateTime=" + orderCreateTime
				+ ", outParkId=" + outParkId + ", outRoadwayId=" + outRoadwayId + ", plateNo=" + plateNo
				+ ", outBillId=" + outBillId + ", outRecordId=" + outRecordId + ", totalCost=" + totalCost
				+ ", realCost=" + realCost + ", cost=" + cost + ", payTime=" + payTime + ", parkingSerial="
				+ parkingSerial + ", payType=" + payType + "]";
	}
	public static NotifyParam getNotifyParam(ParkVehiclePaymentOrder order, ParkVehiclePaymentRecord record, ParkVehicleRecord pvr) {
		NotifyParam n = new NotifyParam();
		n.setCost(record.getCost());
		n.setOutRecordId(record.getOutRecordId());
		n.setOutBillId(record.getOutPaymentId());
		n.setOutParkId(pvr.getOutParkingId());
		n.setOutRoadwayId(pvr.getOutRoadwayId());
		n.setPayTime(record.getPayTime());
		n.setPlateNo(pvr.getPlateNo());
		n.setRealCost(record.getRealCost());
		n.setSystemId(record.getSystemId());
		n.setTotalCost(record.getTotalCost());
		n.setPayType(order.getPayType());
		n.setParkingSerial(record.getParkingSerial());
		n.setOrderCreateTime(order.getCreateTime());
		n.setOrderNo(order.getOrderId());
		return n;
	}
	
}
