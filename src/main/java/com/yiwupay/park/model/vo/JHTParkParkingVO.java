package com.yiwupay.park.model.vo;

import com.yiwupay.park.annotation.RelationField;

/**
 * 捷惠通停车场信息查询接口返回结果接收实体
 * @author he
 *
 */
public class JHTParkParkingVO {
	/**
	 * 停车场ID
	 */
	@RelationField("outParkId")
	private String parkCode;

	/**
	 * 名称
	 */
	@RelationField("parkName")
	private String parkName;

	/**
	 * 总车位
	 */
	@RelationField("totalPlace")
	private Integer totalSpace;

	/**
	 * 剩余车位
	 */
	@RelationField("leftPlace")
	private Integer restSpace;


	public String getParkCode() {
		return parkCode;
	}

	public void setParkCode(String parkCode) {
		this.parkCode = parkCode;
	}

	public String getParkName() {
		return parkName;
	}

	public void setParkName(String parkName) {
		this.parkName = parkName;
	}

	public Integer getTotalSpace() {
		return totalSpace;
	}

	public void setTotalSpace(Integer totalSpace) {
		this.totalSpace = totalSpace;
	}

	public Integer getRestSpace() {
		return restSpace;
	}

	public void setRestSpace(Integer restSpace) {
		this.restSpace = restSpace;
	}

}