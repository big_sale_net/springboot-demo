package com.yiwupay.park.model.vo;

import com.yiwupay.park.model.ParkVehicleRecordTemp;
import com.yiwupay.park.utils.DateUtil;
import com.yiwupay.park.utils.MapCacheUtil;

/**
 * 海康过车记录实体类
 * @author he
 *
 */
public class HIKVehicleRecordData {
	
	private	String	recordUuid; 	//	过车记录UUID
	
	private	String	plateNo; 	//	车牌号码
	
	private	Long	crossTime; 	//	通过时间
	
	private	String	releaseMode;//	放行方式
	
	private	String	carOut;	//	是否出场 
	
	private	String	parkUuid;	//	停车场UUID 
	
	private	String	parkName; 	//	停车场名称
	
	private	String	entranceUuid;	//	出入口UUID
	
	private	String	entranceName; 	//	出入口名称
	
	private	String	roadwayUuid; 	//	车道UUID 
	
	private	String	roadwayName; 	//	车道名称 
	
	private	String	vehicleType;	//	车辆类型 
	
	private	String	vehicleColor;	//	车辆颜色类型码 
	
	private	String	cardNo; 	//	卡片号码 
	
	private	String	personName; 	//	车主姓名 
	
	private	String	phoneNo; 	//	联系电话 
	
	private	String	platePicUrl; 	//	车牌照片URL
	
	private	String	vehiclePicUrl; 	//	车辆照片URL
	
	private	String	facePicUrl; 	//	人脸照片URL
	

	public String getRecordUuid() {
		return recordUuid;
	}


	public void setRecordUuid(String recordUuid) {
		this.recordUuid = recordUuid;
	}


	public String getPlateNo() {
		return plateNo;
	}


	public void setPlateNo(String plateNo) {
		this.plateNo = plateNo;
	}


	public Long getCrossTime() {
		return crossTime;
	}


	public void setCrossTime(String crossTime) {
		this.crossTime = DateUtil.stringToLong(crossTime);
	}


	public String getReleaseMode() {
		return releaseMode;
	}


	public void setReleaseMode(Integer releaseMode) {
		this.releaseMode = MapCacheUtil.CROSS_TYPE.get(releaseMode);
	}


	public String getCarOut() {
		return carOut;
	}


	public void setCarOut(Integer carOut) {
		this.carOut = carOut+"";
	}


	public String getParkUuid() {
		return parkUuid;
	}


	public void setParkUuid(String parkUuid) {
		this.parkUuid = parkUuid;
	}


	public String getParkName() {
		return parkName;
	}


	public void setParkName(String parkName) {
		this.parkName = parkName;
	}


	public String getEntranceUuid() {
		return entranceUuid;
	}


	public void setEntranceUuid(String entranceUuid) {
		this.entranceUuid = entranceUuid;
	}


	public String getEntranceName() {
		return entranceName;
	}


	public void setEntranceName(String entranceName) {
		this.entranceName = entranceName;
	}


	public String getRoadwayUuid() {
		return roadwayUuid;
	}


	public void setRoadwayUuid(String roadwayUuid) {
		this.roadwayUuid = roadwayUuid;
	}


	public String getRoadwayName() {
		return roadwayName;
	}


	public void setRoadwayName(String roadwayName) {
		this.roadwayName = roadwayName;
	}


	public String getVehicleType() {
		return vehicleType;
	}


	public void setVehicleType(Integer vehicleType) {
		this.vehicleType = MapCacheUtil.HIK_CAR_TYPE.get(vehicleType);
	}


	public String getVehicleColor() {
		return vehicleColor;
	}


	public void setVehicleColor(Integer vehicleColor) {
		this.vehicleColor = MapCacheUtil.HIK_CAR_COLOR.get(vehicleColor);
	}


	public String getCardNo() {
		return cardNo;
	}


	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}


	public String getPersonName() {
		return personName;
	}


	public void setPersonName(String personName) {
		this.personName = personName;
	}


	public String getPhoneNo() {
		return phoneNo;
	}


	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}


	public String getPlatePicUrl() {
		return platePicUrl;
	}


	public void setPlatePicUrl(String platePicUrl) {
		this.platePicUrl = platePicUrl;
	}


	public String getVehiclePicUrl() {
		return vehiclePicUrl;
	}


	public void setVehiclePicUrl(String vehiclePicUrl) {
		this.vehiclePicUrl = vehiclePicUrl;
	}


	public String getFacePicUrl() {
		return facePicUrl;
	}


	public void setFacePicUrl(String facePicUrl) {
		this.facePicUrl = facePicUrl;
	}


	@Override
	public String toString() {
		return "HIKVehicleRecord [recordUuid=" + recordUuid + ", plateNo=" + plateNo + ", crossTime=" + crossTime
				+ ", releaseMode=" + releaseMode + ", carOut=" + carOut + ", parkUuid=" + parkUuid + ", parkName="
				+ parkName + ", entranceUuid=" + entranceUuid + ", entranceName=" + entranceName + ", roadwayUuid="
				+ roadwayUuid + ", roadwayName=" + roadwayName + ", vehicleType=" + vehicleType + ", vehicleColor="
				+ vehicleColor + ", cardNo=" + cardNo + ", personName=" + personName + ", phoneNo=" + phoneNo
				+ ", platePicUrl=" + platePicUrl + ", vehiclePicUrl=" + vehiclePicUrl + ", facePicUrl=" + facePicUrl
				+ "]";
	} 
	
	/**过车记录转化*/
	public static ParkVehicleRecordTemp toParkVehicleRecordTemp(HIKVehicleRecordData data){
		ParkVehicleRecordTemp re = new ParkVehicleRecordTemp();
		re.setOutRecordId(data.recordUuid);
		re.setPlateNo(data.plateNo);
		re.setCrosstime(data.crossTime);
		re.setCrossType(data.releaseMode);
		re.setIsOut(data.carOut);
		re.setOutParkingId(data.parkUuid);
		re.setOutEntranceId(data.entranceUuid);
		re.setOutRoadwayId(data.roadwayUuid);
		re.setCarType(data.vehicleType);
		re.setCarColor(data.vehicleColor);
		re.setPlateImgUrl(data.platePicUrl);
		re.setCarImgUrl(data.vehiclePicUrl);
		re.setFaceImgUrl(data.facePicUrl);
		return re;
	}
//	public static void main(String[] args) {
//		String n = null;
//		System.out.println(n+"");
//	}
}
