package com.yiwupay.park.model.vo;
/**
 * 接入系统通知已支付返回通用实体类
 * @author he
 *
 */
public class PayBackVO {
	private String	plateNo; 	//	车牌号码	
	private String	totalCost; 	//	总收费金额	
	private String	realCost; 	//	应收金额	
	private String	cost; 	//	实收金额	
	private String	status; 	//	支付状态	
	private String	reductCost; 	//	减免金额	
	
	public String getPlateNo() {
		return plateNo;
	}
	public void setPlateNo(String plateNo) {
		this.plateNo = plateNo;
	}
	public String getTotalCost() {
		return totalCost;
	}
	public void setTotalCost(String totalCost) {
		this.totalCost = totalCost;
	}
	public String getRealCost() {
		return realCost;
	}
	public void setRealCost(String realCost) {
		this.realCost = realCost;
	}
	public String getCost() {
		return cost;
	}
	public void setCost(String cost) {
		this.cost = cost;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getReductCost() {
		return reductCost;
	}
	public void setReductCost(String reductCost) {
		this.reductCost = reductCost;
	}
	@Override
	public String toString() {
		return "PayBackVO [plateNo=" + plateNo + ", totalCost=" + totalCost + ", realCost=" + realCost + ", cost="
				+ cost + ", status=" + status + ", reductCost=" + reductCost + "]";
	}
	
}
