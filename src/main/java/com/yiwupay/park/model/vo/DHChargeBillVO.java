/**
 * 
 */
package com.yiwupay.park.model.vo;

import org.apache.commons.lang.StringUtils;

import com.yiwupay.park.annotation.RelationField;
import com.yiwupay.park.utils.DateUtil;

/**
 * @author wangpeng
 * 大华预付订单实体类
 */
public class DHChargeBillVO {
	
	/**
	 * 车牌图片
	 */
	private String carInNumPicUrl;
	
	/**
	 * 车辆图片
	 */
	private Integer carInPicUrl;
	
	/**
	 * 车牌
	 */
	@RelationField("plateNo")
	private String carNum;
	
	/**
	 * 车辆类型
	 * 
	 * 0:未识别
	 * 1:小型汽车
	 * 2:大型汽车
	 * 3:使馆汽车
	 * 4:领馆汽车
	 * 5:境外汽车
	 * 6:外籍汽车
	 * 10:教练车
	 * 11:临时行驶车
	 * 12:警用汽车
	 * 
	 */
	@RelationField("vehicleType")
	private String carType;
	
	/**
	 * 进场时间
	 */
	@RelationField("enterTime")
	private String inParkTime;
	
	/**
	 * 用户类别
	 * 
	 * 0:临时用户
	 * 1:储蓄用户
	 * 2:长期用户
	 * 
	 */
	private Integer ownerType;
	
	/**
	 * 停车场编号（扩展）
	 */
	@RelationField("outParkId")
	private String parkCode;
	
	/**
	 * 停车场名称
	 */
	@RelationField("parkName")
	private String parkName;
	
	/**
	 * 停车时长，单位秒
	 */
	@RelationField("parkingTime")
	private Integer parkTime;
	
	/**
	 * 默认传1
	 */
	private String queryType;
	
	/**
	 * 收费金额，单位元
	 */
	@RelationField("realCost")
	private String receivableMoney;

	public String getCarInNumPicUrl() {
		return carInNumPicUrl;
	}

	public void setCarInNumPicUrl(String carInNumPicUrl) {
		this.carInNumPicUrl = carInNumPicUrl;
	}

	public Integer getCarInPicUrl() {
		return carInPicUrl;
	}

	public void setCarInPicUrl(Integer carInPicUrl) {
		this.carInPicUrl = carInPicUrl;
	}

	public String getCarNum() {
		return carNum;
	}

	public void setCarNum(String carNum) {
		this.carNum = carNum;
	}

	public String getCarType() {
		return carType;
	}

	public void setCarType(String carType) {
		this.carType = carType;
	}

	public String getInParkTime() {
		return inParkTime;
	}

	public void setInParkTime(String inParkTime) {
		if(StringUtils.isNotBlank(inParkTime)){
			this.inParkTime = DateUtil.timeMillisToLong(Long.parseLong(inParkTime)).toString();
		}else{
			this.inParkTime = "0";
		}
	}

	public Integer getOwnerType() {
		return ownerType;
	}

	public void setOwnerType(Integer ownerType) {
		this.ownerType = ownerType;
	}

	public String getParkCode() {
		return parkCode;
	}

	public void setParkCode(String parkCode) {
		this.parkCode = parkCode;
	}

	public String getParkName() {
		return parkName;
	}

	public void setParkName(String parkName) {
		this.parkName = parkName;
	}

	public Integer getParkTime() {
		return parkTime;
	}

	public void setParkTime(Integer parkTime) {
		if(parkTime != null){
			double parseDouble = Double.parseDouble(parkTime.toString());
			double ceil = Math.ceil(parseDouble/60.0);
			this.parkTime = ((int)ceil);
		}else{
			this.parkTime = 0;
		}
	}

	public String getQueryType() {
		return queryType;
	}

	public void setQueryType(String queryType) {
		this.queryType = queryType;
	}

	public String getReceivableMoney() {
		return receivableMoney;
	}

	public void setReceivableMoney(String receivableMoney) {
		if(receivableMoney!=null){
			Double costD = Double.parseDouble(receivableMoney)*100;
			this.receivableMoney = String.valueOf(costD.longValue());
		}else{
			this.receivableMoney = "0";
		}
	}
}
