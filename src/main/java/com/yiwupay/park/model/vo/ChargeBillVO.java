package com.yiwupay.park.model.vo;

import org.apache.commons.lang.StringUtils;

import com.yiwupay.park.enums.OrderStatusEnum;
import com.yiwupay.park.model.ParkVehiclePaymentRecord;

public class ChargeBillVO {
	/**
     * 停车场id
     */
	private Long parkingId;
	/**
	 * 预账单UUID
	 */
    private String outPreBillId;
    
    /**
     * 车牌号码
     */
    private String plateNo;
    
    /**
     * 停车场UUID
     */
    private String outParkId;
    
    /**
     * 停车场名称
     */
    private String parkName;
    
    /**
     * 入场时间
     */
    private String enterTime;
    
    /**
     * 停车时长(分钟)
     */
    private Integer parkingTime;
    
    /**
     * 账单缴费类型
     */
    private String type;
    
    /**
     * 应付金额（分）
     */
    private String realCost;
    
    /**
     * 总收费金额（分）
     */
    private String totalCost;
    
    /**
     * 已支付金额（分）
     */
    private String cost;
    
    /**
     * 过车记录UUID
     */
    private String outRecordId;
    
    /**
     * 缴费后允许延时出场时间(分钟)

     */
    private Integer delayTime;
    
    /**
     * 车辆类型
     */
    private String vehicleType;

    /**
     * 收费规则
     */
    private String feeRule;
    
    /**
     * 停车流水号（3区红门系统特有）
     */
    private String parkingSerial;
    
	public Long getParkingId() {
		return parkingId;
	}

	public void setParkingId(Long parkingId) {
		this.parkingId = parkingId;
	}

	public String getOutPreBillId() {
		return outPreBillId;
	}

	public void setOutPreBillId(String outPreBillId) {
		this.outPreBillId = outPreBillId;
	}

	public String getPlateNo() {
		return plateNo;
	}

	public void setPlateNo(String plateNo) {
		this.plateNo = plateNo;
	}

	public String getOutParkId() {
		return outParkId;
	}

	public void setOutParkId(String outParkId) {
		this.outParkId = outParkId;
	}

	public String getParkName() {
		return parkName;
	}

	public void setParkName(String parkName) {
		this.parkName = parkName;
	}

	public String getEnterTime() {
		return enterTime;
	}

	public void setEnterTime(String enterTime) {
		this.enterTime = enterTime;
	}

	public Integer getParkingTime() {
		return parkingTime;
	}

	public void setParkingTime(Integer parkingTime) {
		this.parkingTime = parkingTime;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getRealCost() {
		return realCost;
	}

	public void setRealCost(String realCost) {
		this.realCost = realCost;
	}

	public String getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(String totalCost) {
		this.totalCost = totalCost;
	}

	public String getCost() {
		return cost;
	}

	public void setCost(String cost) {
		this.cost = cost;
	}

	public String getOutRecordId() {
		return outRecordId;
	}

	public void setOutRecordId(String outRecordId) {
		this.outRecordId = outRecordId;
	}

	public Integer getDelayTime() {
		return delayTime;
	}

	public void setDelayTime(Integer delayTime) {
		this.delayTime = delayTime;
	}

	public String getVehicleType() {
		return vehicleType;
	}

	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}

	public String getFeeRule() {
		return feeRule;
	}

	public void setFeeRule(String feeRule) {
		this.feeRule = feeRule;
	}

	
	public String getParkingSerial() {
		return parkingSerial;
	}

	public void setParkingSerial(String parkingSerial) {
		this.parkingSerial = parkingSerial;
	}

	
	@Override
	public String toString() {
		return "ChargeBillVO [parkingId=" + parkingId + ", outPreBillId=" + outPreBillId + ", plateNo=" + plateNo
				+ ", outParkId=" + outParkId + ", parkName=" + parkName + ", enterTime=" + enterTime + ", parkingTime="
				+ parkingTime + ", type=" + type + ", realCost=" + realCost + ", totalCost=" + totalCost + ", cost="
				+ cost + ", outRecordId=" + outRecordId + ", delayTime=" + delayTime + ", vehicleType=" + vehicleType
				+ ", feeRule=" + feeRule + ", parkingSerial=" + parkingSerial + "]";
	}

	/**
	 * 获取的预交费订单初始转化
	 * @param chargeBillVO
	 * @return
	 */
	public static ParkVehiclePaymentRecord toParkVehiclePaymentRecord(ChargeBillVO chargeBillVO) {
		ParkVehiclePaymentRecord pvpr = new ParkVehiclePaymentRecord();
		pvpr.setOutPaymentId(chargeBillVO.outPreBillId);
		pvpr.setOutRecordId(chargeBillVO.outRecordId);
		pvpr.setParkTime(chargeBillVO.parkingTime);
		pvpr.setDelayTime(chargeBillVO.delayTime);
		pvpr.setOrderType(chargeBillVO.type);
		if(!StringUtils.isEmpty(chargeBillVO.realCost)){
			Long realCost = new Long(chargeBillVO.realCost);//应付金额
			pvpr.setRealCost(realCost);
		}else{
			pvpr.setRealCost(0L);
		}
		if(!StringUtils.isEmpty(chargeBillVO.totalCost)){ 
			Long totalCost = new Long(chargeBillVO.totalCost);//总金额
			pvpr.setTotalCost(totalCost);
		}else{
			pvpr.setTotalCost(pvpr.getRealCost()); //若没有总金额，则按应收费用算
		}
		if(!StringUtils.isEmpty(chargeBillVO.getParkingSerial())){
			pvpr.setParkingSerial(chargeBillVO.getParkingSerial());//停车流水号(3区)
		}
		pvpr.setCost(0L);//已付金额，初始为0
		pvpr.setOrderStatus(OrderStatusEnum.pay_not.name());
		return pvpr;
	}

}
