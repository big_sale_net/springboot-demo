package com.yiwupay.park.model.vo;

/**
 * 捷惠通过车记录实体类
 * @author zls
 *
 */
public class JHTVehicleRecordData {
	/**
	 * 停车场编号
	 */
	private String parkCode;
	
	/**
	 * 停车场名称
	 */
	private String parkName;
	
	/**
	 * 车牌号
	 */
	private String carNo;
	
	/**
	 * 入场时间
	 */
	private String inTime;
	
	/**
	 * 入场事件名
	 */
	private String inEventType;
	
	/**
	 * 入场设备名
	 */
	private String inEquip;
	
	/**
	 * 入场操作员
	 */
	private String inOperator;
	
	/**
	 * 卡号
	 */
	private String cardNo;
	
	/**
	 * 卡类型
	 */
	private String cardType;
	
	/**
	 * 是否入场
	 */
	private String isOut;
	
    /**
     * 出场时间
     */
	private String outTime;
	
	/**
	 * 出场事件名
	 */
	private String outEventType;
	
	/**
	 * 出场设备名
	 */
	private String outEquip;
	
	/**
	 * 出场操作员
	 */
	private String outOperator;
	
	/**
	 * 支付方式
	 */
	private String payTypeName;
	
	/**
	 * 应交金额
	 */
	private String ysMoney;
	
	/**
	 * 优惠打折金额
	 */
	private String yhMoney;
	
	/**
	 * 最高收费回滚减免金额
	 */
	private String hgMoney;
	
	/**
	 * 实缴金额
	 */
	private String ssMoney;
	
	/**
	 * 停车时长
	 */
	private String parkingTime;
	
	/**
	 * 车辆出场图片url的id集合
	 */
	private String outPhotoUrlIds;

	public String getParkCode() {
		return parkCode;
	}

	public void setParkCode(String parkCode) {
		this.parkCode = parkCode;
	}

	public String getParkName() {
		return parkName;
	}

	public void setParkName(String parkName) {
		this.parkName = parkName;
	}

	public String getCarNo() {
		return carNo;
	}

	public void setCarNo(String carNo) {
		this.carNo = carNo;
	}

	public String getInTime() {
		return inTime;
	}

	public void setInTime(String inTime) {
		this.inTime = inTime;
	}

	public String getInEventType() {
		return inEventType;
	}

	public void setInEventType(String inEventType) {
		this.inEventType = inEventType;
	}

	public String getInEquip() {
		return inEquip;
	}

	public void setInEquip(String inEquip) {
		this.inEquip = inEquip;
	}

	public String getInOperator() {
		return inOperator;
	}

	public void setInOperator(String inOperator) {
		this.inOperator = inOperator;
	}

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public String getIsOut() {
		return isOut;
	}

	public void setIsOut(String isOut) {
		this.isOut = isOut;
	}

	public String getOutTime() {
		return outTime;
	}

	public void setOutTime(String outTime) {
		this.outTime = outTime;
	}

	public String getOutEventType() {
		return outEventType;
	}

	public void setOutEventType(String outEventType) {
		this.outEventType = outEventType;
	}

	public String getOutEquip() {
		return outEquip;
	}

	public void setOutEquip(String outEquip) {
		this.outEquip = outEquip;
	}

	public String getOutOperator() {
		return outOperator;
	}

	public void setOutOperator(String outOperator) {
		this.outOperator = outOperator;
	}

	public String getPayTypeName() {
		return payTypeName;
	}

	public void setPayTypeName(String payTypeName) {
		this.payTypeName = payTypeName;
	}

	public String getYsMoney() {
		return ysMoney;
	}

	public void setYsMoney(String ysMoney) {
		if(ysMoney != null){
			Double costD = new Double(ysMoney)*100;
			this.ysMoney = String.valueOf(costD.longValue());
		}else{
			this.ysMoney = "0";
		}
	}

	public String getYhMoney() {
		return yhMoney;
	}

	public void setYhMoney(String yhMoney) {
		this.yhMoney = yhMoney;
	}

	public String getHgMoney() {
		return hgMoney;
	}

	public void setHgMoney(String hgMoney) {
		this.hgMoney = hgMoney;
	}

	public String getSsMoney() {
		return ssMoney;
	}

	public void setSsMoney(String ssMoney) {
		if(ssMoney != null){
			Double costD = new Double(ssMoney)*100;
			this.ssMoney = String.valueOf(costD.longValue());
		}else{
			this.ssMoney = "0";
		}
	}

	public String getParkingTime() {
		return parkingTime;
	}

	public void setParkingTime(String parkingTime) {
		this.parkingTime = parkingTime;
	}

	public String getOutPhotoUrlIds() {
		return outPhotoUrlIds;
	}

	public void setOutPhotoUrlIds(String outPhotoUrlIds) {
		this.outPhotoUrlIds = outPhotoUrlIds;
	}

	@Override
	public String toString() {
		return "JHTVehicleRecordData [parkCode=" + parkCode + ", parkName=" + parkName + ", carNo=" + carNo
				+ ", inTime=" + inTime + ", inEventType=" + inEventType + ", inEquip=" + inEquip + ", inOperator="
				+ inOperator + ", cardNo=" + cardNo + ", cardType=" + cardType + ", isOut=" + isOut + ", outTime="
				+ outTime + ", outEventType=" + outEventType + ", outEquip=" + outEquip + ", outOperator=" + outOperator
				+ ", payTypeName=" + payTypeName + ", ysMoney=" + ysMoney + ", yhMoney=" + yhMoney + ", hgMoney="
				+ hgMoney + ", ssMoney=" + ssMoney + ", parkingTime=" + parkingTime + ", outPhotoUrlIds="
				+ outPhotoUrlIds + "]";
	}
	
    
}
