package com.yiwupay.park.model.vo;

import com.yiwupay.park.model.ParkVehicleRecordStatis;
import com.yiwupay.park.utils.DateUtil;
import com.yiwupay.park.utils.MapCacheUtil;

/**
 * 车辆进出数据 实体类 List pageData中的类
 * Title: DHVehicleAssessRecordPageData
 * Description: 
 * Company:  
 * @author cailuxi
 * @date 2018年1月16日
 */
public class DHVehicleAssessRecordPageData {
	
	private String carNum; //车牌号码
	
	private String carOwnerId; //人员ID
	
	private String carStatus; //车辆停车状态(0-入场，1-已出场，2-只有出场信息) 
	
	private String carTypeStr; //内部车/外部车
	
	private String cardNumber; //卡号
	
	private String draw; //
	
	private String enterItcDevChnid; //入场卡口相机通道编码
	
	private String enterItcDevChnname; //入场卡口相机通道名称
	
	private String enterMode; //入场方式:0-刷卡,1-自动识别
	
	private String enterSluiceDevChnid; //入场道闸通道编码
	
	private String enterSluiceDevChnname; //入场道闸通道名称
	
	private String enterTime; //入场时间
	
	private String enterTimeStr; //入场时间YYYY-MM-DD HH24:MI:SS
	
	private String enterWayCode;//入场车道号
	
	private String exitCarNum; //出场车牌号码
	
	private String exitItcDevChnid; //出场卡口相机通道编码
	
	private String exitItcDevChnname; //出场卡口相机通道名称
	
	private String exitSluiceDevChnid; //出场道闸通道编码
	
	private String exitSluiceDevChnname; //出场道闸通道名称
	
	private String exitTime;//出场时间
	
	private String exitTimeStr;//出场时间YYYY-MM-DD HH24:MI:SS
	
	private String exitWayCode;//出场车道号
	
	private String id;//DB id
	
	private String originalPicPathEnter;//入场抓拍图片，大图，前面加http://IP:端口 可打开
	
	private String originalPicPathExit;//出场抓拍图片，大图，前面加http://IP:端口 可打开
	
	private String ownerType;//用户类别：0-临时用户；1-储值用户；2-月卡用户；3-长期用户；
	
	private String pageNum;//第几页
	
	private String pageSize;//每页显示条数
	
	private String parkingLot;//停车场名称
	
	private String parkingLotCode;//停车场编码
	
	private String payedMoney;//停车收费金额
	
	private String realCapturePicPathEnter;//入场抓拍图片，小图，前面加http://IP:端口 可打开
	
	private String realCapturePicPathExit;//出场抓拍图片，小图，前面加http://IP:端口 可打开

	private String carType;
	
	private String enterImg;
	
	private String enterNumImg;
	
	private String exitImg;
	
	private String exitNumImg;
	
	private String ownerCode;
	
	private String ownerName;
	
	private String operatorName;
	
	private String forceOperatorName;
	
	public String getCarNum() {
		return carNum;
	}

	public void setCarNum(String carNum) {
		this.carNum = carNum;
	}

	public String getCarOwnerId() {
		return carOwnerId;
	}

	public void setCarOwnerId(String carOwnerId) {
		this.carOwnerId = carOwnerId;
	}

	public String getCarStatus() {
		return carStatus;
	}

	public void setCarStatus(String carStatus) {
		this.carStatus = carStatus;
	}

	public String getCarTypeStr() {
		return carTypeStr;
	}

	public void setCarTypeStr(String carTypeStr) {
		this.carTypeStr = carTypeStr;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getDraw() {
		return draw;
	}

	public void setDraw(String draw) {
		this.draw = draw;
	}

	public String getEnterItcDevChnid() {
		return enterItcDevChnid;
	}

	public void setEnterItcDevChnid(String enterItcDevChnid) {
		this.enterItcDevChnid = enterItcDevChnid;
	}

	public String getEnterItcDevChnname() {
		return enterItcDevChnname;
	}

	public void setEnterItcDevChnname(String enterItcDevChnname) {
		this.enterItcDevChnname = enterItcDevChnname;
	}

	public String getEnterMode() {
		return enterMode;
	}

	public void setEnterMode(String enterMode) {
		this.enterMode = enterMode;
	}

	public String getEnterSluiceDevChnid() {
		return enterSluiceDevChnid;
	}

	public void setEnterSluiceDevChnid(String enterSluiceDevChnid) {
		this.enterSluiceDevChnid = enterSluiceDevChnid;
	}

	public String getEnterSluiceDevChnname() {
		return enterSluiceDevChnname;
	}

	public void setEnterSluiceDevChnname(String enterSluiceDevChnname) {
		this.enterSluiceDevChnname = enterSluiceDevChnname;
	}

	public String getEnterTime() {
		return enterTime;
	}

	public void setEnterTime(String enterTime) {
		this.enterTime = enterTime;
	}

	public String getEnterTimeStr() {
		return enterTimeStr;
	}

	public void setEnterTimeStr(String enterTimeStr) {
		this.enterTimeStr = enterTimeStr;
	}

	public String getExitCarNum() {
		return exitCarNum;
	}

	public void setExitCarNum(String exitCarNum) {
		this.exitCarNum = exitCarNum;
	}

	public String getExitItcDevChnid() {
		return exitItcDevChnid;
	}

	public void setExitItcDevChnid(String exitItcDevChnid) {
		this.exitItcDevChnid = exitItcDevChnid;
	}

	public String getExitItcDevChnname() {
		return exitItcDevChnname;
	}

	public void setExitItcDevChnname(String exitItcDevChnname) {
		this.exitItcDevChnname = exitItcDevChnname;
	}

	public String getExitSluiceDevChnid() {
		return exitSluiceDevChnid;
	}

	public void setExitSluiceDevChnid(String exitSluiceDevChnid) {
		this.exitSluiceDevChnid = exitSluiceDevChnid;
	}

	public String getExitSluiceDevChnname() {
		return exitSluiceDevChnname;
	}

	public void setExitSluiceDevChnname(String exitSluiceDevChnname) {
		this.exitSluiceDevChnname = exitSluiceDevChnname;
	}

	public String getExitTime() {
		return exitTime;
	}

	public void setExitTime(String exitTime) {
		this.exitTime = exitTime;
	}

	public String getExitTimeStr() {
		return exitTimeStr;
	}

	public void setExitTimeStr(String exitTimeStr) {
		this.exitTimeStr = exitTimeStr;
	}

	public String getExitWayCode() {
		return exitWayCode;
	}

	public void setExitWayCode(String exitWayCode) {
		this.exitWayCode = exitWayCode;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOriginalPicPathEnter() {
		return originalPicPathEnter;
	}

	public void setOriginalPicPathEnter(String originalPicPathEnter) {
		this.originalPicPathEnter = originalPicPathEnter;
	}

	public String getOriginalPicPathExit() {
		return originalPicPathExit;
	}

	public void setOriginalPicPathExit(String originalPicPathExit) {
		this.originalPicPathExit = originalPicPathExit;
	}

	public String getOwnerType() {
		return ownerType;
	}

	public void setOwnerType(String ownerType) {
		this.ownerType = ownerType;
	}

	public String getPageNum() {
		return pageNum;
	}

	public void setPageNum(String pageNum) {
		this.pageNum = pageNum;
	}

	public String getPageSize() {
		return pageSize;
	}

	public void setPageSize(String pageSize) {
		this.pageSize = pageSize;
	}

	public String getParkingLot() {
		return parkingLot;
	}

	public void setParkingLot(String parkingLot) {
		this.parkingLot = parkingLot;
	}

	public String getParkingLotCode() {
		return parkingLotCode;
	}

	public void setParkingLotCode(String parkingLotCode) {
		this.parkingLotCode = parkingLotCode;
	}

	public String getPayedMoney() {
		return payedMoney;
	}

	public void setPayedMoney(String payedMoney) {
		this.payedMoney = payedMoney;
	}

	public String getRealCapturePicPathEnter() {
		return realCapturePicPathEnter;
	}

	public void setRealCapturePicPathEnter(String realCapturePicPathEnter) {
		this.realCapturePicPathEnter = realCapturePicPathEnter;
	}

	public String getRealCapturePicPathExit() {
		return realCapturePicPathExit;
	}

	public void setRealCapturePicPathExit(String realCapturePicPathExit) {
		this.realCapturePicPathExit = realCapturePicPathExit;
	}
	
	public String getEnterWayCode() {
		return enterWayCode;
	}

	public void setEnterWayCode(String enterWayCode) {
		this.enterWayCode = enterWayCode;
	}

	public String getCarType() {
		return carType;
	}

	public void setCarType(String carType) {
		this.carType = carType;
	}

	public String getEnterImg() {
		return enterImg;
	}

	public void setEnterImg(String enterImg) {
		this.enterImg = enterImg;
	}

	public String getEnterNumImg() {
		return enterNumImg;
	}

	public void setEnterNumImg(String enterNumImg) {
		this.enterNumImg = enterNumImg;
	}

	public String getExitImg() {
		return exitImg;
	}

	public void setExitImg(String exitImg) {
		this.exitImg = exitImg;
	}

	public String getExitNumImg() {
		return exitNumImg;
	}

	public void setExitNumImg(String exitNumImg) {
		this.exitNumImg = exitNumImg;
	}

	public String getOwnerCode() {
		return ownerCode;
	}

	public void setOwnerCode(String ownerCode) {
		this.ownerCode = ownerCode;
	}

	public String getOwnerName() {
		return ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	public String getOperatorName() {
		return operatorName;
	}

	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}

	public String getForceOperatorName() {
		return forceOperatorName;
	}

	public void setForceOperatorName(String forceOperatorName) {
		this.forceOperatorName = forceOperatorName;
	}

	/**过车记录转化*/
	public static ParkVehicleRecordStatis toParkVehicleRecordStatis(DHVehicleAssessRecordPageData data){
		ParkVehicleRecordStatis re = new ParkVehicleRecordStatis();
		re.setOutRecordId(data.getId());
		re.setPlateNo(data.getCarNum());
		if(data.getEnterTime() != null){
			re.setEnterTime(DateUtil.timeMillisToLong(new Long(data.getEnterTime())));
		}
		re.setEnterType(MapCacheUtil.DH_CROSS_TYPE.get(data.getEnterMode()));
		re.setOutParkingId(data.getParkingLotCode());
		re.setOutEnterRoadwayId(data.getEnterSluiceDevChnid());
		if(data.getExitTime() != null){
			re.setExitTime(DateUtil.timeMillisToLong(new Long(data.getExitTime())));
		}
		re.setOutExitRoadwayId(data.getExitSluiceDevChnid());
		re.setIsOut(data.getCarStatus());
		re.setCarImgUrl(data.getEnterImg());
		re.setPlateImgUrl(data.getEnterNumImg());
		re.setCarType(data.getCarTypeStr());
		return re;
	}


}
