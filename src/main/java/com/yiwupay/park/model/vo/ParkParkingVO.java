package com.yiwupay.park.model.vo;

import java.util.Date;

import com.yiwupay.park.utils.DateUtil;

/**
 * 停车场信息查询接口返回结果接收实体
 * 
 * @author zls
 *
 */
public class ParkParkingVO {
	/**
	 * 停车场ID
	 */
	private String outParkId;

	/**
	 * 停车场Code
	 */
	private String parkCode;

	/**
	 * 名称
	 */
	private String parkName;

	/**
	 * 总车位
	 */
	private Integer totalPlace;

	/**
	 * 剩余车位
	 */
	private Integer leftPlace;

	/**
	 * 描述
	 */
	private String description;

	/**
	 * 获取时间
	 */
	private Long time;

	public String getOutParkId() {
		return outParkId;
	}

	public void setOutParkId(String outParkId) {
		this.outParkId = outParkId;
	}

	public String getParkCode() {
		return parkCode;
	}

	public void setParkCode(String parkCode) {
		this.parkCode = parkCode;
	}

	public String getParkName() {
		return parkName;
	}

	public void setParkName(String parkName) {
		this.parkName = parkName;
	}

	public Integer getTotalPlace() {
		return totalPlace;
	}

	public void setTotalPlace(Integer totalPlace) {
		this.totalPlace = totalPlace;
	}

	public Integer getLeftPlace() {
		return leftPlace;
	}

	public void setLeftPlace(Integer leftPlace) {
		this.leftPlace = leftPlace;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getTime() {
		return time;
	}

	public void setTime(Long time) {
		this.time = time;
	}

	@Override
	public String toString() {
		return "ParkParkingVO [outParkId=" + outParkId + ", parkCode=" + parkCode + ", parkName=" + parkName
				+ ", totalPlace=" + totalPlace + ", leftPlace=" + leftPlace + ", description=" + description + ", time="
				+ time + "]";
	}

	public static ParkParkingVO build(JHTParkParkingVO jht) {
		ParkParkingVO pp = new ParkParkingVO();
		pp.setLeftPlace(jht.getRestSpace());
		pp.setParkName(jht.getParkName());
		pp.setOutParkId(jht.getParkCode());
		pp.setTotalPlace(jht.getTotalSpace());
		pp.setTime(DateUtil.dateToLong_YM(new Date()));
		return pp;
	}
	
}
