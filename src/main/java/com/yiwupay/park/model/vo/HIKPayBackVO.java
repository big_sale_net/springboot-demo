package com.yiwupay.park.model.vo;

import com.yiwupay.park.enums.OrderStatusEnum;

/**
 * 海康系统通知已支付返回实体类
 * @author he
 *
 */
public class HIKPayBackVO {
	private String	plateNo; 	//	车牌号码	
	
	private Long enterTime; 	//	入场时间	
	
	private Integer	parkingTime; 	//	停车时长(单位：分钟)	
	
	private String	totalCost; 	//	总收费金额	
	
	private String	realCost; 	//	应收金额	

	private String	cost; 	//	实收金额	
	
	private Integer	state; 	//	支付状态	
	
	private Long costTime; 	//	缴费时间	
	
	private Integer	chargeType; 	//	收费类型	
	
	private String	parkUuid; 	//	停车场UUID	
	
	private String	parkName; 	//	停车场名称	
	
	private String	ruleUuid; 	//	收费规则UUID	
	
	private String	ruleName; 	//	收费规则名称	
	
	private String	reductCost; 	//	减免金额	
	
	private Integer	reductType; 	//	减免类型	
	
	private String	reductTypeName; 	//	减免类型名称	
	
	private String	reductCode; 	//	优惠码	
	
	private String	operator; 	//	操作员	
	
	
	public String getPlateNo() {
		return plateNo;
	}
	public void setPlateNo(String plateNo) {
		this.plateNo = plateNo;
	}
	public Long getEnterTime() {
		return enterTime;
	}
	public void setEnterTime(Long enterTime) {
		this.enterTime = enterTime;
	}
	public Integer getParkingTime() {
		return parkingTime;
	}
	public void setParkingTime(Integer parkingTime) {
		this.parkingTime = parkingTime;
	}
	public String getTotalCost() {
		return totalCost;
	}
	public void setTotalCost(String totalCost) {
		this.totalCost = totalCost;
	}
	public String getRealCost() {
		return realCost;
	}
	public void setRealCost(String realCost) {
		this.realCost = realCost;
	}
	public String getCost() {
		return cost;
	}
	public void setCost(String cost) {
		this.cost = cost;
	}
	public Integer getState() {
		return state;
	}
	public void setState(Integer state) {
		this.state = state;
	}
	public Long getCostTime() {
		return costTime;
	}
	public void setCostTime(Long costTime) {
		this.costTime = costTime;
	}
	public Integer getChargeType() {
		return chargeType;
	}
	public void setChargeType(Integer chargeType) {
		this.chargeType = chargeType;
	}
	public String getParkUuid() {
		return parkUuid;
	}
	public void setParkUuid(String parkUuid) {
		this.parkUuid = parkUuid;
	}
	public String getParkName() {
		return parkName;
	}
	public void setParkName(String parkName) {
		this.parkName = parkName;
	}
	public String getRuleUuid() {
		return ruleUuid;
	}
	public void setRuleUuid(String ruleUuid) {
		this.ruleUuid = ruleUuid;
	}
	public String getRuleName() {
		return ruleName;
	}
	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}
	public String getReductCost() {
		return reductCost;
	}
	public void setReductCost(String reductCost) {
		this.reductCost = reductCost;
	}
	public Integer getReductType() {
		return reductType;
	}
	public void setReductType(Integer reductType) {
		this.reductType = reductType;
	}
	public String getReductTypeName() {
		return reductTypeName;
	}
	public void setReductTypeName(String reductTypeName) {
		this.reductTypeName = reductTypeName;
	}
	public String getReductCode() {
		return reductCode;
	}
	public void setReductCode(String reductCode) {
		this.reductCode = reductCode;
	}
	public String getOperator() {
		return operator;
	}
	public void setOperator(String operator) {
		this.operator = operator;
	}
	@Override
	public String toString() {
		return "HIKPayBackVO [plateNo=" + plateNo + ", enterTime=" + enterTime + ", parkingTime=" + parkingTime
				+ ", totalCost=" + totalCost + ", realCost=" + realCost + ", cost=" + cost + ", state=" + state
				+ ", costTime=" + costTime + ", chargeType=" + chargeType + ", parkUuid=" + parkUuid + ", parkName="
				+ parkName + ", ruleUuid=" + ruleUuid + ", ruleName=" + ruleName + ", reductCost=" + reductCost
				+ ", reductType=" + reductType + ", reductTypeName=" + reductTypeName + ", reductCode=" + reductCode
				+ ", operator=" + operator + "]";
	}
	
	public static PayBackVO toPayBackVO(HIKPayBackVO hikpb){
		PayBackVO pb = new PayBackVO();
		pb.setCost(hikpb.cost);
		pb.setPlateNo(hikpb.plateNo);
		pb.setRealCost(hikpb.realCost);
		pb.setReductCost(hikpb.reductCost);
		pb.setStatus(hikpb.state==1 ? OrderStatusEnum.pay_yes.name() : OrderStatusEnum.pay_not.name());
		pb.setTotalCost(hikpb.totalCost);
		return pb;
	}

}
