package com.yiwupay.park.model.vo;

import com.yiwupay.park.annotation.RelationField;

/**
 * 海康停车场信息查询接口返回结果接收实体
 * @author he
 *
 */
public class HIKParkParkingVO {
	/**
	 * 停车场ID
	 */
	@RelationField("outParkId")
	private String parkUuid;

	/**
	 * 名称
	 */
	@RelationField("parkName")
	private String parkName;

	/**
	 * 总车位
	 */
	@RelationField("totalPlace")
	private Integer totalPlot;

	/**
	 * 剩余车位
	 */
	@RelationField("leftPlace")
	private Integer leftPlot;

	/**
	 * 固定车总车位数
	 */
	private Integer totalFiexdPlot;

	/**
	 * 固定车剩余车位数
	 */
	private Integer leftFiexdPlot;

	/**
	 * 描述
	 */
	@RelationField("description")
	private String description;

	public String getParkUuid() {
		return parkUuid;
	}

	public void setParkUuid(String parkUuid) {
		this.parkUuid = parkUuid;
	}

	public String getParkName() {
		return parkName;
	}

	public void setParkName(String parkName) {
		this.parkName = parkName;
	}

	public Integer getTotalPlot() {
		return totalPlot;
	}

	public void setTotalPlot(Integer totalPlot) {
		this.totalPlot = totalPlot;
	}

	public Integer getLeftPlot() {
		return leftPlot;
	}

	public void setLeftPlot(Integer leftPlot) {
		this.leftPlot = leftPlot;
	}

	public Integer getTotalFiexdPlot() {
		return totalFiexdPlot;
	}

	public void setTotalFiexdPlot(Integer totalFiexdPlot) {
		this.totalFiexdPlot = totalFiexdPlot;
	}

	public Integer getLeftFiexdPlot() {
		return leftFiexdPlot;
	}

	public void setLeftFiexdPlot(Integer leftFiexdPlot) {
		this.leftFiexdPlot = leftFiexdPlot;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	

}