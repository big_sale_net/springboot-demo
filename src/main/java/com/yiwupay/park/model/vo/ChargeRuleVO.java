package com.yiwupay.park.model.vo;

public class ChargeRuleVO {
	/**
	 * 收费规则UUID
	 */
	private String outRuleId;

	/**
	 * 收费规则名称
	 */
	private String ruleName;

	/**
	 * 收费规则类型
	 */
	private String ruleType;

	/**
	 * 收费规则详细内容
	 */
	private String ruleDetail;

	/**
	 * 被收费的车辆类型
	 */
	private String vehicleType;

	/**
	 * 停车场UUID
	 */
	private String outParkId;

	/**
	 * 停车场名称
	 */
	private String parkName;

	public String getOutRuleId() {
		return outRuleId;
	}

	public void setOutRuleId(String outRuleId) {
		this.outRuleId = outRuleId;
	}

	public String getRuleName() {
		return ruleName;
	}

	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}

	public String getRuleType() {
		return ruleType;
	}

	public void setRuleType(String ruleType) {
		this.ruleType = ruleType;
	}

	public String getRuleDetail() {
		return ruleDetail;
	}

	public void setRuleDetail(String ruleDetail) {
		this.ruleDetail = ruleDetail;
	}

	public String getVehicleType() {
		return vehicleType;
	}

	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}

	public String getOutParkId() {
		return outParkId;
	}

	public void setOutParkId(String outParkId) {
		this.outParkId = outParkId;
	}

	public String getParkName() {
		return parkName;
	}

	public void setParkName(String parkName) {
		this.parkName = parkName;
	}

	@Override
	public String toString() {
		return "ChargeRuleVO [outRuleId=" + outRuleId + ", ruleName=" + ruleName + ", ruleType=" + ruleType
				+ ", ruleDetail=" + ruleDetail + ", vehicleType=" + vehicleType + ", outParkId=" + outParkId
				+ ", parkName=" + parkName + "]";
	}

}
