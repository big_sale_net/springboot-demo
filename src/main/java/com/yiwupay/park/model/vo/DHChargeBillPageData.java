/**
 * 
 */
package com.yiwupay.park.model.vo;

import com.yiwupay.park.model.ParkVehiclePaymentStatis;
import com.yiwupay.park.utils.DateUtil;
import com.yiwupay.park.utils.MapCacheUtil;

/**
 * @author wangp
 * 大华查询收费信息实体类List pageData中的类
 */
public class DHChargeBillPageData {

	private String accessId; //车辆进出记录ID
	
	private String carInPic; //入场拍照
	
	private String carInTime; //入场时间戳
	
	private String carNum; //车牌
	
	private String carOutPic; //出场拍照
	
	private String carOutTime; //出场时间戳
	
	private String carType; //车辆类型编码
	
	private String carTypeStr; //车辆类型
	
	private String cardNumber; //卡号
	
	private String chargeDetail; //收费描述
	
	private String chargeRuleId; //收费规则
	
	private String consumeMoney; //消费金额/应收金额
	
	private String consumeTerminal; //收费终端
	
	private String consumeTime; //收费时间
	
	private String dealType; //收费类型
	
	private String entranceDevChnid; //入场卡口通道ID
	
	private String entranceDevChnname; //入场卡口通道名称
	
	private String exitDevChnid; //出场卡口通道ID
	
	private String exitDevChnname; //出场卡口通道名称
	
	private String favorableAmount; //优惠金额
	
	private String favorableTicketNumber; //优惠券编号
	
	private String favorableType; //优惠类型
	
	private String feeAmount; //实收金额（消费金额-优惠金额）
	
	private String feeType; //收费类型编号
	
	private String feeTypeStr; //收费类型
	
	private String id; //主键ID
	
	private String memo; //备注
	
	private String operatorId; //收费员id
	
	private String operatorName; //收费员名称
	
	private String originalPicPathIn; //入场抓拍图片，大图，前面加http://IP:端口 可打开
	
	private String originalPicPathOut; //出场抓拍图片，小图，前面加http://IP:端口 可打开
	
	private String ownerId;
	
	private String ownerName; //车主姓名
	
	private String ownerType; //车主用户类型编号
	
	private String ownerTypeStr; //车主用户类型
	
	private String realCapturePicPathIn; //入场抓拍图片，小图，前面加http://IP:端口 可打开
	
	private String realCapturePicPathOut; //出场抓拍图片，小图，前面加http://IP:端口 可打开
	
	private String serialNumber; //订单号
	
	private String timeLength; //停车时长

	public String getAccessId() {
		return accessId;
	}

	public String getCarInPic() {
		return carInPic;
	}

	public String getCarInTime() {
		return carInTime;
	}

	public String getCarNum() {
		return carNum;
	}

	public String getCarOutPic() {
		return carOutPic;
	}

	public String getCarOutTime() {
		return carOutTime;
	}

	public String getCarType() {
		return carType;
	}

	public String getCarTypeStr() {
		return carTypeStr;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public String getChargeDetail() {
		return chargeDetail;
	}

	public String getChargeRuleId() {
		return chargeRuleId;
	}

	public String getConsumeMoney() {
		return consumeMoney;
	}

	public String getConsumeTerminal() {
		return consumeTerminal;
	}

	public String getConsumeTime() {
		return consumeTime;
	}

	public String getDealType() {
		return dealType;
	}

	public String getEntranceDevChnid() {
		return entranceDevChnid;
	}

	public String getEntranceDevChnname() {
		return entranceDevChnname;
	}

	public String getExitDevChnid() {
		return exitDevChnid;
	}

	public String getExitDevChnname() {
		return exitDevChnname;
	}

	public String getFavorableAmount() {
		return favorableAmount;
	}

	public String getFavorableTicketNumber() {
		return favorableTicketNumber;
	}

	public String getFavorableType() {
		return favorableType;
	}

	public String getFeeAmount() {
		return feeAmount;
	}

	public String getFeeType() {
		return feeType;
	}

	public String getFeeTypeStr() {
		return feeTypeStr;
	}

	public String getId() {
		return id;
	}

	public String getMemo() {
		return memo;
	}

	public String getOperatorId() {
		return operatorId;
	}

	public String getOperatorName() {
		return operatorName;
	}

	public String getOriginalPicPathIn() {
		return originalPicPathIn;
	}

	public String getOriginalPicPathOut() {
		return originalPicPathOut;
	}

	public String getOwnerName() {
		return ownerName;
	}

	public String getOwnerType() {
		return ownerType;
	}

	public String getOwnerTypeStr() {
		return ownerTypeStr;
	}

	public String getRealCapturePicPathIn() {
		return realCapturePicPathIn;
	}

	public String getRealCapturePicPathOut() {
		return realCapturePicPathOut;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public String getTimeLength() {
		return timeLength;
	}

	public void setAccessId(String accessId) {
		this.accessId = accessId;
	}

	public void setCarInPic(String carInPic) {
		this.carInPic = carInPic;
	}

	public void setCarInTime(String carInTime) {
		this.carInTime = carInTime;
	}

	public void setCarNum(String carNum) {
		this.carNum = carNum;
	}

	public void setCarOutPic(String carOutPic) {
		this.carOutPic = carOutPic;
	}

	public void setCarOutTime(String carOutTime) {
		this.carOutTime = carOutTime;
	}

	public void setCarType(String carType) {
		this.carType = carType;
	}

	public void setCarTypeStr(String carTypeStr) {
		this.carTypeStr = carTypeStr;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public void setChargeDetail(String chargeDetail) {
		this.chargeDetail = chargeDetail;
	}

	public void setChargeRuleId(String chargeRuleId) {
		this.chargeRuleId = chargeRuleId;
	}

	public void setConsumeMoney(String consumeMoney) {
		this.consumeMoney = consumeMoney;
	}

	public void setConsumeTerminal(String consumeTerminal) {
		this.consumeTerminal = consumeTerminal;
	}

	public void setConsumeTime(String consumeTime) {
		this.consumeTime = consumeTime;
	}

	public void setDealType(String dealType) {
		this.dealType = dealType;
	}

	public void setEntranceDevChnid(String entranceDevChnid) {
		this.entranceDevChnid = entranceDevChnid;
	}

	public void setEntranceDevChnname(String entranceDevChnname) {
		this.entranceDevChnname = entranceDevChnname;
	}

	public void setExitDevChnid(String exitDevChnid) {
		this.exitDevChnid = exitDevChnid;
	}

	public void setExitDevChnname(String exitDevChnname) {
		this.exitDevChnname = exitDevChnname;
	}

	public void setFavorableAmount(String favorableAmount) {
		this.favorableAmount = favorableAmount;
	}

	public void setFavorableTicketNumber(String favorableTicketNumber) {
		this.favorableTicketNumber = favorableTicketNumber;
	}

	public void setFavorableType(String favorableType) {
		this.favorableType = favorableType;
	}

	public void setFeeAmount(String feeAmount) {
		this.feeAmount = feeAmount;
	}

	public void setFeeType(String feeType) {
		this.feeType = feeType;
	}

	public void setFeeTypeStr(String feeTypeStr) {
		this.feeTypeStr = feeTypeStr;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public void setOperatorId(String operatorId) {
		this.operatorId = operatorId;
	}

	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}

	public void setOriginalPicPathIn(String originalPicPathIn) {
		this.originalPicPathIn = originalPicPathIn;
	}

	public void setOriginalPicPathOut(String originalPicPathOut) {
		this.originalPicPathOut = originalPicPathOut;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	public void setOwnerType(String ownerType) {
		this.ownerType = ownerType;
	}

	public void setOwnerTypeStr(String ownerTypeStr) {
		this.ownerTypeStr = ownerTypeStr;
	}

	public void setRealCapturePicPathIn(String realCapturePicPathIn) {
		this.realCapturePicPathIn = realCapturePicPathIn;
	}

	public void setRealCapturePicPathOut(String realCapturePicPathOut) {
		this.realCapturePicPathOut = realCapturePicPathOut;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public void setTimeLength(String timeLength) {
		this.timeLength = timeLength;
	}

	public String getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	@Override
	public String toString() {
		return "DHChargeBillPageData [accessId=" + accessId + ", carInPic=" + carInPic + ", carInTime=" + carInTime
				+ ", carNum=" + carNum + ", carOutPic=" + carOutPic + ", carOutTime=" + carOutTime + ", carType="
				+ carType + ", carTypeStr=" + carTypeStr + ", cardNumber=" + cardNumber + ", chargeDetail="
				+ chargeDetail + ", chargeRuleId=" + chargeRuleId + ", consumeMoney=" + consumeMoney
				+ ", consumeTerminal=" + consumeTerminal + ", consumeTime=" + consumeTime + ", dealType=" + dealType
				+ ", entranceDevChnid=" + entranceDevChnid + ", entranceDevChnname=" + entranceDevChnname
				+ ", exitDevChnid=" + exitDevChnid + ", exitDevChnname=" + exitDevChnname + ", favorableAmount="
				+ favorableAmount + ", favorableTicketNumber=" + favorableTicketNumber + ", favorableType="
				+ favorableType + ", feeAmount=" + feeAmount + ", feeType=" + feeType + ", feeTypeStr=" + feeTypeStr
				+ ", id=" + id + ", memo=" + memo + ", operatorId=" + operatorId + ", operatorName=" + operatorName
				+ ", originalPicPathIn=" + originalPicPathIn + ", originalPicPathOut=" + originalPicPathOut
				+ ", ownerName=" + ownerName + ", ownerType=" + ownerType + ", ownerTypeStr=" + ownerTypeStr
				+ ", realCapturePicPathIn=" + realCapturePicPathIn + ", realCapturePicPathOut=" + realCapturePicPathOut
				+ ", serialNumber=" + serialNumber + ", timeLength=" + timeLength + "]";
	}
	
	//转换
	public static ParkVehiclePaymentStatis toParkVehiclePaymentStatis(DHChargeBillPageData data){
		ParkVehiclePaymentStatis payStatis = new ParkVehiclePaymentStatis();
		payStatis.setOutPaymentId(data.getSerialNumber());
		payStatis.setOutRecordId(data.getAccessId());
		//payStatis.setOutParkingId(data.parkUuid);
		payStatis.setPlateNo(data.getCarNum());
		payStatis.setTotalCost(new Long(data.getFeeAmount()) * 100);
		payStatis.setRealCost((new Long(data.getFeeAmount()) + new Long(data.getFavorableAmount())) * 100);
		payStatis.setCost(new Long(data.getFeeAmount()) * 100);
		payStatis.setPaymentRuleName(data.getChargeDetail());
		//payStatis.setExceptionRuleName(data.exceptionRuleName);		
		payStatis.setPayType(MapCacheUtil.DH_PAY_TYPE.get(data.getFeeType()));
		if(data.getConsumeTime() != null){
			String time = data.getConsumeTime();
			Long timeMillisToLong = DateUtil.timeMillisToLong(Long.parseLong(time));
			payStatis.setPayTime(timeMillisToLong);
		}
		//payStatis.setCreateTime(data.createTime);
		if(data.getCarInTime() != null){
			String time = data.getCarInTime();
			Long timeMillisToLong = DateUtil.timeMillisToLong(Long.parseLong(time));
			payStatis.setEnterTime(timeMillisToLong);
		}
		double parseDouble = Double.parseDouble(data.getTimeLength());
		double ceil = Math.ceil(parseDouble/60.0);
		payStatis.setParkTime((int)ceil);
		return payStatis;
	}
	
}
