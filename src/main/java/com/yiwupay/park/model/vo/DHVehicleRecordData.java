package com.yiwupay.park.model.vo;

import java.util.List;

/**
 * @author wangp
 * 大华过车记录接收实体类
 */
public class DHVehicleRecordData {

	private Long currentPage;
	
	private List<DHVehicleRecordPageData> pageData;
	
	private Long pageSize;
	
	private Long totalPage;
	
	private Long totalRows;

	public Long getCurrentPage() {
		return currentPage;
	}

	public List<DHVehicleRecordPageData> getPageData() {
		return pageData;
	}

	public Long getPageSize() {
		return pageSize;
	}

	public Long getTotalPage() {
		return totalPage;
	}

	public Long getTotalRows() {
		return totalRows;
	}

	public void setCurrentPage(Long currentPage) {
		this.currentPage = currentPage;
	}

	public void setPageData(List<DHVehicleRecordPageData> pageData) {
		this.pageData = pageData;
	}

	public void setPageSize(Long pageSize) {
		this.pageSize = pageSize;
	}

	public void setTotalPage(Long totalPage) {
		this.totalPage = totalPage;
	}

	public void setTotalRows(Long totalRows) {
		this.totalRows = totalRows;
	}

	@Override
	public String toString() {
		return "DHVehicleRecordData [currentPage=" + currentPage + ", pageData=" + pageData + ", pageSize=" + pageSize
				+ ", totalPage=" + totalPage + ", totalRows=" + totalRows + "]";
	}
	
}
