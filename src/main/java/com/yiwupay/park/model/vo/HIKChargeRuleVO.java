package com.yiwupay.park.model.vo;

import com.yiwupay.park.annotation.RelationField;

public class HIKChargeRuleVO {
	/**
	 * 收费规则UUID
	 */
	@RelationField("outRuleId")
	private String ruleUuid;

	/**
	 * 收费规则名称
	 */
	@RelationField("ruleName")
	private String ruleName;

	/**
	 * 收费规则类型
	 */
	@RelationField("ruleType")
	private String ruleType;

	/**
	 * 收费规则详细内容
	 */
	@RelationField("ruleDetail")
	private String ruleDetail;

	/**
	 * 被收费的车辆类型
	 */
	@RelationField("vehicleType")
	private String vehicleType;

	/**
	 * 停车场UUID
	 */
	@RelationField("outParkId")
	private String parkUuid;

	/**
	 * 停车场名称
	 */
	@RelationField("parkName")
	private String parkName;

	public String getRuleUuid() {
		return ruleUuid;
	}

	public void setRuleUuid(String ruleUuid) {
		this.ruleUuid = ruleUuid;
	}

	public String getRuleName() {
		return ruleName;
	}

	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}

	public String getRuleType() {
		return ruleType;
	}

	public void setRuleType(String ruleType) {
		this.ruleType = ruleType;
	}

	public String getRuleDetail() {
		return ruleDetail;
	}

	public void setRuleDetail(String ruleDetail) {
		this.ruleDetail = ruleDetail;
	}

	public String getVehicleType() {
		return vehicleType;
	}

	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}

	public String getParkUuid() {
		return parkUuid;
	}

	public void setParkUuid(String parkUuid) {
		this.parkUuid = parkUuid;
	}

	public String getParkName() {
		return parkName;
	}

	public void setParkName(String parkName) {
		this.parkName = parkName;
	}

}
