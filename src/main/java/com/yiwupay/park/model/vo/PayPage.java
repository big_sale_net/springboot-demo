package com.yiwupay.park.model.vo;

/**
 * 缴费记录
 * @author he
 *
 */
public class PayPage {
	private Long orderId;//订单编号
	private String plateNo;//车牌号
	private Long totalFee;//支付费用
	private String parkName;//停车场名称
	private String orderStatus;//支付状态
	private Long enterTime;//入场时间
	private Long leaveTime;//离场时间
	private Long payTime;//支付时间
	private Long parkTime;//停车时长（分）
	public Long getOrderId() {
		return orderId;
	}
	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}
	public String getPlateNo() {
		return plateNo;
	}
	public void setPlateNo(String plateNo) {
		this.plateNo = plateNo;
	}
	public Long getTotalFee() {
		return totalFee;
	}
	public void setTotalFee(Long totalFee) {
		this.totalFee = totalFee;
	}
	public String getParkName() {
		return parkName;
	}
	public void setParkName(String parkName) {
		this.parkName = parkName;
	}
	public String getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}
	public Long getEnterTime() {
		return enterTime;
	}
	public void setEnterTime(Long enterTime) {
		this.enterTime = enterTime;
	}
	public Long getLeaveTime() {
		return leaveTime;
	}
	public void setLeaveTime(Long leaveTime) {
		this.leaveTime = leaveTime;
	}
	public Long getPayTime() {
		return payTime;
	}
	public void setPayTime(Long payTime) {
		this.payTime = payTime;
	}
	public Long getParkTime() {
		return parkTime;
	}
	public void setParkTime(Long parkTime) {
		this.parkTime = parkTime;
	}
	

}
