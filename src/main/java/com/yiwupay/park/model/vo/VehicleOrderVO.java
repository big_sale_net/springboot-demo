package com.yiwupay.park.model.vo;

/**
 * 查询缴费列表实体类
 * @author he
 *
 */
public class VehicleOrderVO {
	private	String	orderStatus;	//支付状态
	private	String	plateNo;	//车牌号
	private	String	parkName;	//停车场名称
	private	Long	totalFee;	//支付费用
	private	Long	enterTime;	//入场时间
	private	Integer	parkTime;	//停车时间
	
	public String getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}
	public String getPlateNo() {
		return plateNo;
	}
	public void setPlateNo(String plateNo) {
		this.plateNo = plateNo;
	}
	public String getParkName() {
		return parkName;
	}
	public void setParkName(String parkName) {
		this.parkName = parkName;
	}
	public Long getTotalFee() {
		return totalFee;
	}
	public void setTotalFee(Long totalFee) {
		this.totalFee = totalFee;
	}
	public Long getEnterTime() {
		return enterTime;
	}
	public void setEnterTime(Long enterTime) {
		this.enterTime = enterTime;
	}
	public Integer getParkTime() {
		return parkTime;
	}
	public void setParkTime(Integer parkTime) {
		this.parkTime = parkTime;
	}
	
}
