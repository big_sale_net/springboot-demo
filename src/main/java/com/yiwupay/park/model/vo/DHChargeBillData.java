/**
 * 
 */
package com.yiwupay.park.model.vo;

import java.util.List;

/**
 * @author wangp
 * 大华查询收费信息实体类
 */
public class DHChargeBillData {

private Long currentPage;
	
	private List<DHChargeBillPageData> pageData;
	
	private Long pageSize;
	
	private Long totalPage;
	
	private Long totalRows;

	public Long getCurrentPage() {
		return currentPage;
	}

	public List<DHChargeBillPageData> getPageData() {
		return pageData;
	}

	public Long getPageSize() {
		return pageSize;
	}

	public Long getTotalPage() {
		return totalPage;
	}

	public Long getTotalRows() {
		return totalRows;
	}

	public void setCurrentPage(Long currentPage) {
		this.currentPage = currentPage;
	}

	public void setPageData(List<DHChargeBillPageData> pageData) {
		this.pageData = pageData;
	}

	public void setPageSize(Long pageSize) {
		this.pageSize = pageSize;
	}

	public void setTotalPage(Long totalPage) {
		this.totalPage = totalPage;
	}

	public void setTotalRows(Long totalRows) {
		this.totalRows = totalRows;
	}
}
