package com.yiwupay.park.model.vo;

import java.util.Date;

import org.apache.commons.lang.StringUtils;

import com.yiwupay.park.annotation.RelationField;
import com.yiwupay.park.utils.DateUtil;

public class JHTChargeBillVO {
    
    /**
     * 车牌号码
     */
	@RelationField("plateNo")
    private String carNo;
    
    /**
     * 停车场UUID
     */
	@RelationField("outParkId")
    private String parkCode;
	
	/**
	 * 返回码
	 */
	private String retcode;
	
	/**
	 * 商户号
	 */
	private String businesserCode;
    
	/**
	 * 商户名称
	 */
	private String businesserName;
	
	
    /**
     * 停车场名称
     */
	@RelationField("parkName")
    private String parkName;
    
	/**
	 * 订单编号
	 */
    @RelationField("outPreBillId")
	private String orderNo;
	
	/**
	 * 商品名称
	 */
	private String goodName;
	
	/**
	 * 计费时间
	 */
	private String createTime;
	/**
	 * 离场时间
	 */
	private String endTime;
	
	/**
	 * 减扣金额
	 */
	private Double deductFee;
	
	/**
	 * 优惠金额
	 */
	private Double discountFee;
	
	/**
	 * 物流费用
	 */
	private Double transportFee;
	
	/**
	 * 其他费用
	 */
	private Double otherFee;
	
	/**
	 * 支付状态
	 */
	private String tradeStatus;
	
	/**
	 * 有效支付时长秒数
	 */
	
	private Integer validTimeLen;
	
	/**
	 * 剩余免费时间分钟数
	 */
	private Integer surplusMinute;
	
	/**
	 * 前端停车场返回的订单信息
	 */
	private String retmsg;
	
	/**
	 * 支付url
	 */
	private String payUrl;
	
    /**
     * 入场时间
     */
	@RelationField("enterTime")
    private String startTime;
    
    /**
     * 停车时长(单位秒)
     */
	@RelationField("parkingTime")
    private Integer serviceTime;
    
    
    
    /**
     * 账单缴费类型
     */
	@RelationField("type")
    private String type;
    
    /**
     * 应缴金额（元）
     */
	@RelationField("totalCost")
    private String serviceFee;
    
   
    
    /**
     * 应付金额（元）
     */
	@RelationField("realCost")
    private String totalFee;
    
    
    
    /**
     * 缴费后允许延时出场时间(分钟)
     */
	@RelationField("delayTime")
    private Integer freeMinute;
    
    /**
     * 卡号
     */
    private String cardNo;
    
  
  

	public String getCarNo() {
		return carNo;
	}

	public void setCarNo(String carNo) {
		this.carNo = carNo;
	}

	public String getParkCode() {
		return parkCode;
	}

	public void setParkCode(String parkCode) {
		this.parkCode = parkCode;
	}

	public String getRetcode() {
		return retcode;
	}

	public void setRetcode(String retcode) {
		this.retcode = retcode;
	}

	public String getBusinesserCode() {
		return businesserCode;
	}

	public void setBusinesserCode(String businesserCode) {
		this.businesserCode = businesserCode;
	}

	public String getBusinesserName() {
		return businesserName;
	}

	public void setBusinesserName(String businesserName) {
		this.businesserName = businesserName;
	}

	public String getParkName() {
		return parkName;
	}

	public void setParkName(String parkName) {
		this.parkName = parkName;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getGoodName() {
		return goodName;
	}

	public void setGoodName(String goodName) {
		this.goodName = goodName;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public Double getDeductFee() {
		return deductFee;
	}

	public void setDeductFee(Double deductFee) {
		if(deductFee != null){
			deductFee = deductFee * 100;
		}
		this.deductFee = deductFee;
	}

	public Double getDiscountFee() {
		return discountFee;
	}

	public void setDiscountFee(Double discountFee) {
		if(discountFee != null){
			this.discountFee = discountFee *100;
		}else{
			this.discountFee = 0.0;
		}
		
	}

	public Double getTransportFee() {
		return transportFee;
	}

	public void setTransportFee(Double transportFee) {
		if(transportFee != null){
			transportFee = transportFee * 100;
		}
		this.transportFee = transportFee;
	}

	public Double getOtherFee() {
		return otherFee;
	}

	public void setOtherFee(Double otherFee) {
		if(otherFee != null){
			otherFee = otherFee * 100;
		}
		this.otherFee = otherFee;
	}

	public String getServiceFee() {
		return serviceFee;
	}

	public void setServiceFee(String serviceFee) {
		if(serviceFee != null){
			Double costD = new Double(serviceFee)*100;
			this.serviceFee = String.valueOf(costD.longValue());
		}else{
			this.serviceFee = "0";
		}
		
	}

	public String getTotalFee() {
		return totalFee;
	}

	public void setTotalFee(String totalFee) {
		if(totalFee != null){
			Double costD = new Double(totalFee) * 100;
			this.totalFee = String.valueOf(costD.longValue());
			
		}else{
			this.totalFee = "0";
		}
		
	}

	public String getTradeStatus() {
		return tradeStatus;
	}

	public void setTradeStatus(String tradeStatus) {
		this.tradeStatus = tradeStatus;
	}

	public Integer getValidTimeLen() {
		return validTimeLen;
	}

	public void setValidTimeLen(Integer validTimeLen) {
		this.validTimeLen = validTimeLen;
	}

	public Integer getSurplusMinute() {
		return surplusMinute;
	}

	public void setSurplusMinute(Integer surplusMinute) {
		this.surplusMinute = surplusMinute;
	}

	public String getRetmsg() {
		return retmsg;
	}

	public void setRetmsg(String retmsg) {
		this.retmsg = retmsg;
	}

	public String getPayUrl() {
		return payUrl;
	}

	public void setPayUrl(String payUrl) {
		this.payUrl = payUrl;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		if(StringUtils.isNotBlank(startTime)){
			Date stringToDateByFormat = DateUtil.stringToDateByFormat(startTime,DateUtil.DB_YMDHMS_FORMAT);
			Long dateToLong = DateUtil.getDateToLong(stringToDateByFormat,DateUtil.YMDHMS_FORMAT);
			this.startTime = dateToLong.toString();
		}else{
			this.startTime = startTime;
		}
		
	}

	public Integer getServiceTime() {
		return serviceTime;
	}

	public void setServiceTime(Integer serviceTime) {
		if(StringUtils.isNotBlank(serviceTime+"")){
			serviceTime = serviceTime%60==0? serviceTime/60:serviceTime/60 + 1 ;
		}
		this.serviceTime = serviceTime;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	

	public Integer getFreeMinute() {
		return freeMinute;
	}

	public void setFreeMinute(Integer freeMinute) {
		this.freeMinute = freeMinute;
	}

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}



	@Override
	public String toString() {
		return "JHTChargeBillVO [carNo=" + carNo + ", parkCode=" + parkCode + ", retcode=" + retcode
				+ ", businesserCode=" + businesserCode + ", businesserName=" + businesserName + ", parkName=" + parkName
				+ ", orderNo=" + orderNo + ", goodName=" + goodName + ", createTime=" + createTime + ", endTime="
				+ endTime + ", deductFee=" + deductFee + ", discountFee=" + discountFee + ", transportFee="
				+ transportFee + ", otherFee=" + otherFee + ", tradeStatus=" + tradeStatus + ", validTimeLen="
				+ validTimeLen + ", surplusMinute=" + surplusMinute + ", retmsg=" + retmsg + ", payUrl=" + payUrl
				+ ", startTime=" + startTime + ", serviceTime=" + serviceTime + ", type=" + type + ", serviceFee="
				+ serviceFee + ", totalFee=" + totalFee + ", freeMinute=" + freeMinute + ", cardNo=" + cardNo
				+  "]";
	}
    
    
}
