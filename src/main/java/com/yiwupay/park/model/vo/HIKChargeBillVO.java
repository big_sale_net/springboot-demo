package com.yiwupay.park.model.vo;

import com.yiwupay.park.annotation.RelationField;
import com.yiwupay.park.utils.MapCacheUtil;

/**
 * 海康预付订单实体类
 * @author he
 *
 */
public class HIKChargeBillVO {
	/**
	 * 预账单UUID
	 */
	@RelationField("outPreBillId")
    private String preBillUuid;
    
    /**
     * 车牌号码
     */
	@RelationField("plateNo")
    private String plateNo;
    
    /**
     * 停车场UUID
     */
	@RelationField("outParkId")
    private String parkUuid;
    
    /**
     * 停车场名称
     */
	@RelationField("parkName")
    private String parkName;
    
    /**
     * 入场时间
     */
	@RelationField("enterTime")
    private String enterTime;
    
    /**
     * 停车时长(分钟)
     */
	@RelationField("parkingTime")
    private Integer parkingTime;
    
    /**
     * 账单缴费类型
     */
	@RelationField("type")
    private String type;
    
    /**
     * 应付金额（分）
     */
	@RelationField("realCost")
    private String cost;
    
    /**
     * 总收费金额（分）
     */
	@RelationField("totalCost")
    private String totalCost;
    
    /**
     * 已支付金额（分）
     */
	@RelationField("cost")
    private String paidCost;
    
    /**
     * 过车记录UUID
     */
	@RelationField("outRecordId")
    private String recordUuid;
    
    /**
     * 缴费后允许延时出场时间(分钟)
     */
	@RelationField("delayTime")
    private Integer delayTime;
    
    /**
     * 卡号
     */
    private String cardNo;
    
    /**
     * 车辆类型
     */
	@RelationField("vehicleType")
    private String vehicleType;

	public String getPreBillUuid() {
		return preBillUuid;
	}

	public void setPreBillUuid(String preBillUuid) {
		this.preBillUuid = preBillUuid;
	}

	public String getPlateNo() {
		return plateNo;
	}

	public void setPlateNo(String plateNo) {
		this.plateNo = plateNo;
	}

	public String getParkUuid() {
		return parkUuid;
	}

	public void setParkUuid(String parkUuid) {
		this.parkUuid = parkUuid;
	}

	public String getParkName() {
		return parkName;
	}

	public void setParkName(String parkName) {
		this.parkName = parkName;
	}

	public String getEnterTime() {
		return enterTime;
	}

	public void setEnterTime(String enterTime) {
		this.enterTime = enterTime;
	}

	public Integer getParkingTime() {
		return parkingTime;
	}

	public void setParkingTime(Integer parkingTime) {
		this.parkingTime = parkingTime;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = MapCacheUtil.HIK_ORDER_TYPE.get(type);
	}

	public String getCost() {
		return cost;
	}

	public void setCost(String cost) {
		if(cost!=null){
			Double costD = Double.parseDouble(cost)*100;
			this.cost = String.valueOf(costD.longValue());
		}else{
			this.cost = "0";
		}
	}

	public String getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(String totalCost) {
		if(totalCost!=null){
			Double totalCostD = Double.parseDouble(totalCost)*100;
			this.totalCost = String.valueOf(totalCostD.longValue());
		}else{
			this.totalCost = "0";
		}
	}

	public String getPaidCost() {
		return paidCost;
	}

	public void setPaidCost(String paidCost) {
		if(paidCost!=null){
			Double paidCostD = Double.parseDouble(paidCost)*100;
			this.paidCost = String.valueOf(paidCostD.longValue());
		}else{
			this.paidCost = "0";
		}
	}

	public String getRecordUuid() {
		return recordUuid;
	}

	public void setRecordUuid(String recordUuid) {
		this.recordUuid = recordUuid;
	}

	public Integer getDelayTime() {
		return delayTime;
	}

	public void setDelayTime(Integer delayTime) {
		this.delayTime = delayTime;
	}

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	public String getVehicleType() {
		return vehicleType;
	}

	public void setVehicleType(String vehicleType) {
		try {
			this.vehicleType = MapCacheUtil.HIK_CAR_TYPE.get(Integer.parseInt(vehicleType));
		} catch (Exception e) {
			this.vehicleType = vehicleType;
		}
	}
}
