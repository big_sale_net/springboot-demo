/**
 * 
 */
package com.yiwupay.park.model.vo;

import com.yiwupay.park.model.ParkVehicleRecordTemp;
import com.yiwupay.park.utils.MapCacheUtil;

/**
 * @author wangp
 * 大华过车记录实体类 List pageData中的类
 */
public class DHVehicleRecordPageData {
	
	private String capTime; //抓拍时间
	
	private String capTimeStr; //抓拍时间 YYYY-MM-DD HH24:MI:SS
	
	private String carColor; //车辆颜色（不支持）
	
	private String carDirect; //行车方向，8-进场，9-出场
	
	private String carDirectStr; //行车方向：驶入|驶出
	
	private String carInnerCategory; //车辆进出类型
	
	private String carNum; //车牌号码
	
	private String carWayCode; //车道号
	
	private String devChnId; //卡口相机通道编码
	
	private String devChnName; //卡口相机通道名称
	
	private String devChnNum; //卡口相机通道号
	
	private String devId; //设备编码
	
	private String devName; //设备名称
	
	private String id; //DB id
	
	private String originalPicPath; //抓拍图片，大图，前面加http://IP:端口 可打开
	
	private String parkingLot; //停车场名称
	
	private String parkingLotCode; //停车场编码
	
	private String realCapturePicPath; //抓拍图片，小图，前面加http://IP:端口 可打开
	
	private String carType;//车辆类型
	
	private String carBrand;
	
	private String carImgUrl;
	
	private String carNumPic;
	
	private String carNumcolor;
	
	private String carNumcolorStr;
	
	private String draw;
	
	private String pageNum;
	
	private String pageSize;
	
	private String strobeState;

	public String getCapTime() {
		return capTime;
	}

	public String getCapTimeStr() {
		return capTimeStr;
	}

	public String getCarColor() {
		return carColor;
	}

	public String getCarDirect() {
		return carDirect;
	}

	public String getCarDirectStr() {
		return carDirectStr;
	}

	public String getCarInnerCategory() {
		return carInnerCategory;
	}

	public String getCarNum() {
		return carNum;
	}

	public String getCarWayCode() {
		return carWayCode;
	}

	public String getDevChnId() {
		return devChnId;
	}

	public String getDevChnName() {
		return devChnName;
	}

	public String getDevChnNum() {
		return devChnNum;
	}

	public String getDevId() {
		return devId;
	}

	public String getDevName() {
		return devName;
	}

	public String getId() {
		return id;
	}

	public String getOriginalPicPath() {
		return originalPicPath;
	}

	public String getParkingLot() {
		return parkingLot;
	}

	public String getParkingLotCode() {
		return parkingLotCode;
	}

	public String getRealCapturePicPath() {
		return realCapturePicPath;
	}

	public void setCapTime(String capTime) {
		this.capTime = capTime;
	}

	public void setCapTimeStr(String capTimeStr) {
		this.capTimeStr = capTimeStr;
	}

	public void setCarColor(String carColor) {
		this.carColor = carColor;
	}

	public void setCarDirect(String carDirect) {
		this.carDirect = carDirect;
	}

	public void setCarDirectStr(String carDirectStr) {
		this.carDirectStr = carDirectStr;
	}

	public void setCarInnerCategory(String carInnerCategory) {
		this.carInnerCategory = carInnerCategory;
	}

	public void setCarNum(String carNum) {
		this.carNum = carNum;
	}

	public void setCarWayCode(String carWayCode) {
		this.carWayCode = carWayCode;
	}

	public void setDevChnId(String devChnId) {
		this.devChnId = devChnId;
	}

	public void setDevChnName(String devChnName) {
		this.devChnName = devChnName;
	}

	public void setDevChnNum(String devChnNum) {
		this.devChnNum = devChnNum;
	}

	public void setDevId(String devId) {
		this.devId = devId;
	}

	public void setDevName(String devName) {
		this.devName = devName;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setOriginalPicPath(String originalPicPath) {
		this.originalPicPath = originalPicPath;
	}

	public void setParkingLot(String parkingLot) {
		this.parkingLot = parkingLot;
	}

	public void setParkingLotCode(String parkingLotCode) {
		this.parkingLotCode = parkingLotCode;
	}

	public void setRealCapturePicPath(String realCapturePicPath) {
		this.realCapturePicPath = realCapturePicPath;
	}

	public String getCarType() {
		return carType;
	}

	public void setCarType(String carType) {
		this.carType = carType;
	}

	public String getCarBrand() {
		return carBrand;
	}

	public void setCarBrand(String carBrand) {
		this.carBrand = carBrand;
	}

	public String getCarImgUrl() {
		return carImgUrl;
	}

	public void setCarImgUrl(String carImgUrl) {
		this.carImgUrl = carImgUrl;
	}

	public String getCarNumPic() {
		return carNumPic;
	}

	public void setCarNumPic(String carNumPic) {
		this.carNumPic = carNumPic;
	}

	public String getCarNumcolor() {
		return carNumcolor;
	}

	public void setCarNumcolor(String carNumcolor) {
		this.carNumcolor = carNumcolor;
	}

	public String getCarNumcolorStr() {
		return carNumcolorStr;
	}

	public void setCarNumcolorStr(String carNumcolorStr) {
		this.carNumcolorStr = carNumcolorStr;
	}

	public String getDraw() {
		return draw;
	}

	public void setDraw(String draw) {
		this.draw = draw;
	}

	public String getPageNum() {
		return pageNum;
	}

	public void setPageNum(String pageNum) {
		this.pageNum = pageNum;
	}

	public String getPageSize() {
		return pageSize;
	}

	public void setPageSize(String pageSize) {
		this.pageSize = pageSize;
	}

	public String getStrobeState() {
		return strobeState;
	}

	public void setStrobeState(String strobeState) {
		this.strobeState = strobeState;
	}

	@Override
	public String toString() {
		return "DHVehicleRecordPageData [capTime=" + capTime + ", capTimeStr=" + capTimeStr + ", carColor=" + carColor
				+ ", carDirect=" + carDirect + ", carDirectStr=" + carDirectStr + ", carInnerCategory="
				+ carInnerCategory + ", carNum=" + carNum + ", carWayCode=" + carWayCode + ", devChnId=" + devChnId
				+ ", devChnName=" + devChnName + ", devChnNum=" + devChnNum + ", devId=" + devId + ", devName="
				+ devName + ", id=" + id + ", originalPicPath=" + originalPicPath + ", parkingLot=" + parkingLot
				+ ", parkingLotCode=" + parkingLotCode + ", realCapturePicPath=" + realCapturePicPath + "]";
	}
	
	/**过车记录转化*/
	public static ParkVehicleRecordTemp toParkVehicleRecordStatis(DHVehicleRecordPageData data){
		ParkVehicleRecordTemp re = new ParkVehicleRecordTemp();
		re.setOutRecordId(data.getId());
		re.setPlateNo(data.getCarNum());
		re.setCrosstime(new Long(data.getCapTimeStr().replaceAll("-", "").replaceAll(" ", "").replaceAll(":", "")));
		//re.setCrossType((data.getCarDirect() == "8") ? "0" : "1");   //放行方式
		re.setIsOut("8".equals(data.getCarDirect()) ? "0" : "1");
		re.setOutParkingId(data.getParkingLotCode());
		re.setOutEntranceId(data.getDevId());
		re.setOutRoadwayId(data.getCarWayCode());
		re.setCarType(MapCacheUtil.DH_CAR_TYPE.get(data.getCarType()));
		re.setCarColor(MapCacheUtil.DH_CAR_COLOR.get(data.getCarColor()));
		//re.setPlateImgUrl(data.platePicUrl);
		re.setCarImgUrl(data.getOriginalPicPath());
		//re.setFaceImgUrl(data.facePicUrl);
		return re;
	}
	
}
