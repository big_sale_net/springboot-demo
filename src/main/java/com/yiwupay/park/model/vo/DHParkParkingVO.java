/**
 * 
 */
package com.yiwupay.park.model.vo;

import com.yiwupay.park.annotation.RelationField;

/**
 * 
 * 大华停车场信息查询接口返回结果接收实体
 * @author wangpeng
 *
 */
public class DHParkParkingVO {
	
	/**
	 * 车牌个数
	 */
	private Integer carNum;
	
	/**
	 * 入口个数
	 */
	private Integer entranceNum;
	
	/**
	 * 出口个数
	 */
	private Integer exitusNum;
	
	/**
	 * 车场id
	 */
	@RelationField("outParkId")
	private String id;
	
	/**
	 * 余位数
	 */
	@RelationField("leftPlace")
	private Integer idleLot;
	
	/**
	 * 余位屏数
	 */
	private Integer ledNum;
	
	/**
	 * 停车场名称
	 */
	@RelationField("parkName")
	private String parkingLot;
	
	/**
	 * 停车场编码
	 */
	@RelationField("parkCode")
	private String parkingLotCode;
	
	/**
	 * 车位总数
	 */
	@RelationField("totalPlace")
	private Integer totalLot;
	
	/**
	 * 已用车位数
	 */
	private Integer usedLot;

	public Integer getCarNum() {
		return carNum;
	}

	public void setCarNum(Integer carNum) {
		this.carNum = carNum;
	}

	public Integer getEntranceNum() {
		return entranceNum;
	}

	public void setEntranceNum(Integer entranceNum) {
		this.entranceNum = entranceNum;
	}

	public Integer getExitusNum() {
		return exitusNum;
	}

	public void setExitusNum(Integer exitusNum) {
		this.exitusNum = exitusNum;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getIdleLot() {
		return idleLot;
	}

	public void setIdleLot(Integer idleLot) {
		this.idleLot = idleLot;
	}

	public Integer getLedNum() {
		return ledNum;
	}

	public void setLedNum(Integer ledNum) {
		this.ledNum = ledNum;
	}

	public String getParkingLot() {
		return parkingLot;
	}

	public void setParkingLot(String parkingLot) {
		this.parkingLot = parkingLot;
	}

	public String getParkingLotCode() {
		return parkingLotCode;
	}

	public void setParkingLotCode(String parkingLotCode) {
		this.parkingLotCode = parkingLotCode;
	}

	public Integer getTotalLot() {
		return totalLot;
	}

	public void setTotalLot(Integer totalLot) {
		this.totalLot = totalLot;
	}

	public Integer getUsedLot() {
		return usedLot;
	}

	public void setUsedLot(Integer usedLot) {
		this.usedLot = usedLot;
	}
}
