package com.yiwupay.park.model.vo;

import org.apache.commons.lang.StringUtils;

/**
 * 缴费记录
 * @author he
 *
 */
public class PaymentPage {
	private Long paymentId;
	private String plateNo;
	private Long payTime;
	private String parkingName;
	private Long cost;
	private String orderStatus;
	private Long enterTime;
	private Long parkTime;
	private String orderName = "临时停车费用";
	
	public Long getPaymentId() {
		return paymentId;
	}
	public void setPaymentId(Long paymentId) {
		this.paymentId = paymentId;
	}
	public String getPlateNo() {
		return plateNo;
	}
	public void setPlateNo(String plateNo) {
		this.plateNo = plateNo;
	}
	public Long getPayTime() {
		return payTime;
	}
	public void setPayTime(Long payTime) {
		this.payTime = payTime;
	}
	public String getParkingName() {
		return parkingName;
	}
	public void setParkingName(String parkingName) {
		this.parkingName = parkingName;
	}
	public Long getCost() {
		return cost;
	}
	public void setCost(Long cost) {
		this.cost = cost;
	}
	public String getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}
	public Long getEnterTime() {
		return enterTime;
	}
	public void setEnterTime(Long enterTime) {
		this.enterTime = enterTime;
	}
	public Long getParkTime() {
		return parkTime;
	}
	public void setParkTime(Long parkTime) {
		this.parkTime = parkTime;
	}
	public String getOrderName() {
		return orderName;
	}
	public void setOrderName(String orderName) {
		if(!StringUtils.isEmpty(orderName)){
			this.orderName = orderName;
		}
	}
	
}
