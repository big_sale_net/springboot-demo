package com.yiwupay.park.model.vo;

import com.yiwupay.park.enums.OrderStatusEnum;

/**
 *  捷惠通系统通知已支付返回实体类
 * @author zls
 *
 */
public class JHTPayBackVO {
	/**
	 * 处理结果0：成功， 1：失败
	 */
   private int retCode;
   /**
    * 订单编号
    */
   private String orderNo;
public int getRetCode() {
	return retCode;
}
public void setRetCode(int retCode) {
	this.retCode = retCode;
}
public String getOrderNo() {
	return orderNo;
}
public void setOrderNo(String orderNo) {
	this.orderNo = orderNo;
}
@Override
public String toString() {
	return "JHTPayBackVO [retCode=" + retCode + ", orderNo=" + orderNo + "]";
}
public static PayBackVO toPayBackVO(JHTPayBackVO jhtpb){
	PayBackVO pb = new PayBackVO();
	pb.setStatus(jhtpb.getRetCode()==0 ? OrderStatusEnum.pay_yes.name() : OrderStatusEnum.pay_not.name());
	return pb;
}

}
