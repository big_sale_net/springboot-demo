package com.yiwupay.park.model.vo;
/**
 * 捷惠通微信缴费记录实体类
 * @author zls
 *
 */
public class JHTVehiclePaymentVO {

    private Long systemId;

    private String outPaymentId;

    private Long recordId;

    //进场时间
    private Long enterTime;
    //应付金额
    private Long realCost;
    //总金额
    private Long totalCost;
    //实付金额
    private Long cost;
    //进场后允许出场时间（分）
    private Integer delayTime;
    //支付方式
    private String payType;
    //订单类型
    private String orderType;
    //订单状态
    private String orderStatus;
    //账单创建时间
    private Long createTime;
    //账单更新时间
    private Long updateTime;
    //支付时间
    private Long payTime;
    //停车时长
    private Integer parkTime;
    
    private Long parkingId;

    private String outParkingId;
    //车牌
    private String plateNo;
   
  
  

    public Long getSystemId() {
        return systemId;
    }

    public void setSystemId(Long systemId) {
        this.systemId = systemId;
    }

    public String getOutPaymentId() {
        return outPaymentId;
    }

    public void setOutPaymentId(String outPaymentId) {
        this.outPaymentId = outPaymentId == null ? null : outPaymentId.trim();
    }

    public Long getRecordId() {
        return recordId;
    }

    public void setRecordId(Long recordId) {
        this.recordId = recordId;
    }

    public Long getEnterTime() {
        return enterTime;
    }

	public Long getRealCost() {
		return realCost;
	}

	public void setRealCost(Long realCost) {
		this.realCost = realCost;
	}

	public Long getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(Long totalCost) {
		this.totalCost = totalCost;
	}

	public Long getCost() {
		return cost;
	}

	public void setCost(Long cost) {
		this.cost = cost;
	}

	public Integer getDelayTime() {
		return delayTime;
	}

	public void setDelayTime(Integer delayTime) {
		this.delayTime = delayTime;
	}

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public Long getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Long createTime) {
		this.createTime = createTime;
	}

	public Long getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Long updateTime) {
		this.updateTime = updateTime;
	}

	public Long getPayTime() {
		return payTime;
	}

	public void setPayTime(Long payTime) {
		this.payTime = payTime;
	}

	public Integer getParkTime() {
		return parkTime;
	}

	public void setParkTime(Integer parkTime) {
		this.parkTime = parkTime;
	}

	public Long getParkingId() {
		return parkingId;
	}

	public void setParkingId(Long parkingId) {
		this.parkingId = parkingId;
	}

	public String getOutParkingId() {
		return outParkingId;
	}

	public void setOutParkingId(String outParkingId) {
		this.outParkingId = outParkingId;
	}
	public String getPlateNo() {
		return plateNo;
	}

	public void setPlateNo(String plateNo) {
		this.plateNo = plateNo;
	}

	public void setEnterTime(Long enterTime) {
		this.enterTime = enterTime;
	}

}