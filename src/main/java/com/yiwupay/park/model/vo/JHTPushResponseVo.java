package com.yiwupay.park.model.vo;
/**
 * 捷惠通推送返回的参数
 * @author zls
 *
 */
public class JHTPushResponseVo {
    private String itemId;
    private String code;
    private String message;
    
    public JHTPushResponseVo(){}
    
	public JHTPushResponseVo(String itemId, String code, String message) {
		this.itemId = itemId;
		this.code = code;
		this.message = message;
	}

	public String getItemId() {
		return itemId;
	}
	
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
    
}
