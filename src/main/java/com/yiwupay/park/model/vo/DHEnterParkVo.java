/**
 * 
 */
package com.yiwupay.park.model.vo;

import com.yiwupay.park.model.dh.CommParam;

/**
 * 大华停车系统车辆进场推送实体类
 * @author wangp
 *
 */
public class DHEnterParkVo {

	/**
	 * 操作时间
	 * [20] （必填）（格式：yyyyMMddHHmmss）
	 */
	private String actTime;
	
	/**
	 * 操作类型
	 * [4] （必填）（0代表月租长包车辆， 1代表时租访客车辆， 2代表免费车辆， 3代表异常未知车辆）
	 */
	private String actType;
	
	/**
	 * 附加泊位
	 * [16]
	 */
	private String addBerth;
	
	/**
	 * 进场时间
	 * [20] （必填）（格式：yyyyMMddHHmmss）
	 */
	private String arriveTime;
	
	/**
	 * 泊位编号
	 * [16]
	 */
	private String berthId;

	/**
	 * 业务流水号
	 * [20] （进场数据ID）
	 */
	private String bizSn;

	/**
	 * 业务类型
	 * [4] （必填）（1-进场，2-出场）
	 */
	private String businessType;

	/**
	 * 车牌号
	 * [16] （必填）
	 */
	private String carNum;

	/**
	 * 车辆类型
	 * [4] （必填）（1-小型车，2-大型车）
	 */
	private String carType;

	/**
	 * 通用字段
	 * 包含通用字段中的所有字段
	 */
	private CommParam commParam;

	/**
	 * 访客剩余车位
	 * [8] （必填）
	 */
	private String guestRemainNum;

	/**
	 * 包月证号
	 * [32]
	 */
	private String monthlyCertNumber;

	/**
	 * 月租剩余车位	
	 * [8] （必填）
	 */
	private String monthlyRemainNum;

	/**
	 * 工号
	 * [12] （停车场端收费管理系统的登录工号）
	 */
	private String uid;

	/**
	 * 停车凭证号
	 * [20]
	 */
	private String voucherNo;

	/**
	 * 停车凭证类型
	 * [4] （1、交通卡 2、银联卡 3、第三方支付 51、VIP卡号 101、临时车卡号 102、临时车票号）
	 */
	private String voucherType;

	public String getActTime() {
		return actTime;
	}

	public String getActType() {
		return actType;
	}

	public String getAddBerth() {
		return addBerth;
	}

	public String getArriveTime() {
		return arriveTime;
	}

	public String getBerthId() {
		return berthId;
	}

	public String getBizSn() {
		return bizSn;
	}

	public String getBusinessType() {
		return businessType;
	}

	public String getCarNum() {
		return carNum;
	}

	public String getCarType() {
		return carType;
	}

	public CommParam getCommParam() {
		return commParam;
	}

	public String getGuestRemainNum() {
		return guestRemainNum;
	}

	public String getMonthlyCertNumber() {
		return monthlyCertNumber;
	}

	public String getMonthlyRemainNum() {
		return monthlyRemainNum;
	}

	public String getUid() {
		return uid;
	}

	public String getVoucherNo() {
		return voucherNo;
	}

	public String getVoucherType() {
		return voucherType;
	}

	public void setActTime(String actTime) {
		this.actTime = actTime;
	}

	public void setActType(String actType) {
		this.actType = actType;
	}

	public void setAddBerth(String addBerth) {
		this.addBerth = addBerth;
	}

	public void setArriveTime(String arriveTime) {
		this.arriveTime = arriveTime;
	}

	public void setBerthId(String berthId) {
		this.berthId = berthId;
	}

	public void setBizSn(String bizSn) {
		this.bizSn = bizSn;
	}

	public void setBusinessType(String businessType) {
		this.businessType = businessType;
	}

	public void setCarNum(String carNum) {
		this.carNum = carNum;
	}

	public void setCarType(String carType) {
		this.carType = carType;
	}

	public void setCommParam(CommParam commParam) {
		this.commParam = commParam;
	}

	public void setGuestRemainNum(String guestRemainNum) {
		this.guestRemainNum = guestRemainNum;
	}

	public void setMonthlyCertNumber(String monthlyCertNumber) {
		this.monthlyCertNumber = monthlyCertNumber;
	}

	public void setMonthlyRemainNum(String monthlyRemainNum) {
		this.monthlyRemainNum = monthlyRemainNum;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public void setVoucherNo(String voucherNo) {
		this.voucherNo = voucherNo;
	}

	public void setVoucherType(String voucherType) {
		this.voucherType = voucherType;
	}

	@Override
	public String toString() {
		return "DHEnterParkVo [actTime=" + actTime + ", actType=" + actType + ", addBerth=" + addBerth + ", arriveTime="
				+ arriveTime + ", berthId=" + berthId + ", bizSn=" + bizSn + ", businessType=" + businessType
				+ ", carNum=" + carNum + ", carType=" + carType + ", commParam=" + commParam + ", guestRemainNum="
				+ guestRemainNum + ", monthlyCertNumber=" + monthlyCertNumber + ", monthlyRemainNum=" + monthlyRemainNum
				+ ", uid=" + uid + ", voucherNo=" + voucherNo + ", voucherType=" + voucherType + "]";
	}

	
}
