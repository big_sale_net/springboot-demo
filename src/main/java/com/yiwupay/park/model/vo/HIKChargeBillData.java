package com.yiwupay.park.model.vo;

import com.yiwupay.park.model.ParkVehiclePaymentStatis;
import com.yiwupay.park.utils.DateUtil;
import com.yiwupay.park.utils.MapCacheUtil;

/**
 * 海康缴费记录实体类
 * @author he
 *
 */
public class HIKChargeBillData {
	
	private	String	billUuid; 	//	缴费记录UUID
	
	private	String	parkUuid;	//	停车场UUID 
	
	private	String	parkName; 	//	停车场名称
	
	private	String  plateNo;  // 车牌号
	
	private	String  cardNo;  // 卡号
	
	private	Long	totalCost;	//	总金额
	
	private	Long	realCost; 	//	应收金额
	
	private	Long	cost; 	//	实收金额
	
	private	String	chargeRuleName; 	//	收费规则名称
	
	private	String	exceptionRuleName; 	//	异常放行规则名称
	
	private	String	reductCode;	//	优惠码
	
	private	String	reductType;	//	减免类型：优惠券，减免规则
	
	private	String	chargeType; 	//	收费类型
	
	private	Long	enterTime; 	//	车辆进场时间
	
	private	Long	costTime; 	//	缴费时间
	
	private	Long	createTime; 	//	账单创建时间
	
	private	String	operator; 	//	操作员

	public String getBillUuid() {
		return billUuid;
	}

	public void setBillUuid(String billUuid) {
		this.billUuid = billUuid;
	}

	public String getParkUuid() {
		return parkUuid;
	}

	public void setParkUuid(String parkUuid) {
		this.parkUuid = parkUuid;
	}

	public String getParkName() {
		return parkName;
	}

	public void setParkName(String parkName) {
		this.parkName = parkName;
	}

	public String getPlateNo() {
		return plateNo;
	}

	public void setPlateNo(String plateNo) {
		this.plateNo = plateNo;
	}

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	public Long getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(String totalCost) {
		if(totalCost!=null){
			Double totalCostD = Double.parseDouble(totalCost)*100;
			this.totalCost = totalCostD.longValue();
		}else{
			this.totalCost = 0L;
		}
		
	}

	public Long getRealCost() {
		return realCost;
	}

	public void setRealCost(String realCost) {
		if(realCost!=null){
			Double realCostD = Double.parseDouble(realCost)*100;
			this.realCost = realCostD.longValue();
		}else{
			this.realCost = 0L;
		}
	}

	public Long getCost() {
		return cost;
	}

	public void setCost(String cost) {
		if(cost!=null){
			Double costD = Double.parseDouble(cost)*100;
			this.cost = costD.longValue();
		}else{
			this.cost = 0L;
		}
	}

	public String getChargeRuleName() {
		return chargeRuleName;
	}

	public void setChargeRuleName(String chargeRuleName) {
		this.chargeRuleName = chargeRuleName;
	}

	public String getExceptionRuleName() {
		return exceptionRuleName;
	}

	public void setExceptionRuleName(String exceptionRuleName) {
		this.exceptionRuleName = exceptionRuleName;
	}

	public String getReductCode() {
		return reductCode;
	}

	public void setReductCode(String reductCode) {
		this.reductCode = reductCode;
	}

	public String getReductType() {
		return reductType;
	}

	public void setReductType(String reductType) {
		this.reductType = reductType;
	}

	public String getChargeType() {
		return chargeType;
	}

	public void setChargeType(Integer chargeType) {
		this.chargeType = MapCacheUtil.HIK_PAY_TYPE.get(chargeType);
	}

	public Long getEnterTime() {
		return enterTime;
	}

	public void setEnterTime(Long enterTime) {
		this.enterTime = DateUtil.timeMillisToLong(enterTime);
	}

	public Long getCostTime() {
		return costTime;
	}

	public void setCostTime(Long costTime) {
		this.costTime = DateUtil.timeMillisToLong(costTime);
	}

	public Long getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Long createTime) {
		this.createTime = DateUtil.timeMillisToLong(createTime);
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}
	
	public static ParkVehiclePaymentStatis toParkVehiclePaymentStatis(HIKChargeBillData data){
		ParkVehiclePaymentStatis payStatis = new ParkVehiclePaymentStatis();
		payStatis.setOutPaymentId(data.billUuid);
		payStatis.setOutParkingId(data.parkUuid);
		payStatis.setPlateNo(data.plateNo);
		payStatis.setTotalCost(data.totalCost);
		payStatis.setRealCost(data.realCost);
		payStatis.setCost(data.cost);
		payStatis.setPaymentRuleName(data.chargeRuleName);
		payStatis.setExceptionRuleName(data.exceptionRuleName);		
		payStatis.setPayType(data.chargeType);
		payStatis.setPayTime(data.costTime);
		payStatis.setCreateTime(data.createTime);
		payStatis.setEnterTime(data.enterTime);
		return payStatis;
	}
	
}
