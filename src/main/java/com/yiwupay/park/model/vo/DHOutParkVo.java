/**
 * 
 */
package com.yiwupay.park.model.vo;

import java.util.ArrayList;

import com.yiwupay.park.model.dh.DiscountFree;
import com.yiwupay.park.model.dh.Money;
import com.yiwupay.park.model.dh.Rate;
import com.yiwupay.park.model.dh.Time;

/**
 * 大华停车系统车辆出场推送实体类
 * @author wangp
 *
 */
public class DHOutParkVo {

	/**
	 * 操作时间
	 * [20] （必填）（格式：yyyyMMddHHmmss）
	 */
	private String actTime;

	/**
	 * 操作类型
	 * [4] （必填）（0代表月租长包车辆， 1代表时租访客车辆， 2代表免费车辆， 3代表异常未知车辆）
	 */
	private String actType;

	/**
	 * 附加泊位
	 * [16]
	 */
	private String addBerth;

	/**
	 * 进场时间
	 * [20] （必填）（格式：yyyyMMddHHmmss）
	 */
	private String arriveTime;

	/**
	 * 泊位编号
	 * [16]
	 */
	private String berthId;

	/**
	 * 业务流水号
	 * [20] （出场数据ID）
	 */
	private String bizSn;

	/**
	 * 业务类型
	 * [4] （必填）（1-进场，2-出场）
	 */
	private String businessType;

	/**
	 * 车牌号
	 * [16] （必填）
	 */
	private String carNum;

	/**
	 * 车辆类型
	 * [4] （必填）（1-小型车，2-大型车）
	 */
	private String carType;

	/**
	 * 通用字段
	 * 包含通用字段中的所有字段
	 */
	private String commParam;

	/**
	 * 免费优惠
	 */
	private DiscountFree discountFree;

	/**
	 * 金额优惠集合
	 */
	private ArrayList<Money> discountMoney;

	/**
	 * 折扣优惠集合
	 */
	private ArrayList<Rate> discountRate;

	/**
	 * 时间优惠集合
	 */
	private ArrayList<Time> discountTime;

	/**
	 * 访客剩余车位
	 * [8] （必填）
	 */
	private String guestRemainNum;

	/**
	 * 是否停车场端线下收费
	 * [1] （1-是，0-否）
	 */
	private String isOfflinePay;

	/**
	 * 是否按线上返回的金额收费
	 * 1：是，2：否
	 */
	private String isPayByLineAmount;

	/**
	 * 离场时间
	 * [20] （必填）（格式：yyyyMMddHHmmss）
	 */
	private String leaveTime;

	/**
	 * 包月证号
	 * [32]
	 */
	private String monthlyCertNumber;

	/**
	 * 月租剩余车位
	 * [8] （必填）
	 */
	private String monthlyRemainNum;

	/**
	 * 预约订单号
	 * [12] （预约订单号，若有则需要上传）
	 */
	private String orderNo;

	/**
	 * 停车时长
	 * [8] （单位：秒）
	 */
	private String parkingTimeLength;

	/**
	 * 收费金额
	 * [8] （必填）
	 */
	private String payMoney;

	/**
	 * 支付类型
	 * [4] （必填）（0代表现金支付， 1代表交通卡支付， 2代表银行卡支付， 3代表手机支付）
	 */
	private String paymentType;

	/**
	 * 工号
	 * [12] （停车场端收费管理系统的登录工号）
	 */
	private String uid;

	/**
	 * 停车凭证号
	 * [20]
	 */
	private String voucherNo;

	/**
	 * 停车凭证类型
	 * [4]
	 */
	private String voucherType;

	public String getActTime() {
		return actTime;
	}

	public String getActType() {
		return actType;
	}

	public String getAddBerth() {
		return addBerth;
	}

	public String getArriveTime() {
		return arriveTime;
	}

	public String getBerthId() {
		return berthId;
	}

	public String getBizSn() {
		return bizSn;
	}

	public String getBusinessType() {
		return businessType;
	}

	public String getCarNum() {
		return carNum;
	}

	public String getCarType() {
		return carType;
	}

	public String getCommParam() {
		return commParam;
	}

	public DiscountFree getDiscountFree() {
		return discountFree;
	}

	public ArrayList<Money> getDiscountMoney() {
		return discountMoney;
	}

	public ArrayList<Rate> getDiscountRate() {
		return discountRate;
	}

	public ArrayList<Time> getDiscountTime() {
		return discountTime;
	}

	public String getGuestRemainNum() {
		return guestRemainNum;
	}

	public String getIsOfflinePay() {
		return isOfflinePay;
	}

	public String getIsPayByLineAmount() {
		return isPayByLineAmount;
	}

	public String getLeaveTime() {
		return leaveTime;
	}

	public String getMonthlyCertNumber() {
		return monthlyCertNumber;
	}

	public String getMonthlyRemainNum() {
		return monthlyRemainNum;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public String getParkingTimeLength() {
		return parkingTimeLength;
	}

	public String getPayMoney() {
		return payMoney;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public String getUid() {
		return uid;
	}

	public String getVoucherNo() {
		return voucherNo;
	}

	public String getVoucherType() {
		return voucherType;
	}

	public void setActTime(String actTime) {
		this.actTime = actTime;
	}

	public void setActType(String actType) {
		this.actType = actType;
	}

	public void setAddBerth(String addBerth) {
		this.addBerth = addBerth;
	}

	public void setArriveTime(String arriveTime) {
		this.arriveTime = arriveTime;
	}

	public void setBerthId(String berthId) {
		this.berthId = berthId;
	}

	public void setBizSn(String bizSn) {
		this.bizSn = bizSn;
	}

	public void setBusinessType(String businessType) {
		this.businessType = businessType;
	}

	public void setCarNum(String carNum) {
		this.carNum = carNum;
	}

	public void setCarType(String carType) {
		this.carType = carType;
	}

	public void setCommParam(String commParam) {
		this.commParam = commParam;
	}

	public void setDiscountFree(DiscountFree discountFree) {
		this.discountFree = discountFree;
	}

	public void setDiscountMoney(ArrayList<Money> discountMoney) {
		this.discountMoney = discountMoney;
	}

	public void setDiscountRate(ArrayList<Rate> discountRate) {
		this.discountRate = discountRate;
	}

	public void setDiscountTime(ArrayList<Time> discountTime) {
		this.discountTime = discountTime;
	}

	public void setGuestRemainNum(String guestRemainNum) {
		this.guestRemainNum = guestRemainNum;
	}

	public void setIsOfflinePay(String isOfflinePay) {
		this.isOfflinePay = isOfflinePay;
	}

	public void setIsPayByLineAmount(String isPayByLineAmount) {
		this.isPayByLineAmount = isPayByLineAmount;
	}

	public void setLeaveTime(String leaveTime) {
		this.leaveTime = leaveTime;
	}

	public void setMonthlyCertNumber(String monthlyCertNumber) {
		this.monthlyCertNumber = monthlyCertNumber;
	}

	public void setMonthlyRemainNum(String monthlyRemainNum) {
		this.monthlyRemainNum = monthlyRemainNum;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public void setParkingTimeLength(String parkingTimeLength) {
		this.parkingTimeLength = parkingTimeLength;
	}

	public void setPayMoney(String payMoney) {
		this.payMoney = payMoney;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public void setVoucherNo(String voucherNo) {
		this.voucherNo = voucherNo;
	}

	public void setVoucherType(String voucherType) {
		this.voucherType = voucherType;
	}

	@Override
	public String toString() {
		return "DHOutParkVo [actTime=" + actTime + ", actType=" + actType + ", addBerth=" + addBerth + ", arriveTime="
				+ arriveTime + ", berthId=" + berthId + ", bizSn=" + bizSn + ", businessType=" + businessType
				+ ", carNum=" + carNum + ", carType=" + carType + ", commParam=" + commParam + ", discountFree="
				+ discountFree + ", discountMoney=" + discountMoney + ", discountRate=" + discountRate
				+ ", discountTime=" + discountTime + ", guestRemainNum=" + guestRemainNum + ", isOfflinePay="
				+ isOfflinePay + ", isPayByLineAmount=" + isPayByLineAmount + ", leaveTime=" + leaveTime
				+ ", monthlyCertNumber=" + monthlyCertNumber + ", monthlyRemainNum=" + monthlyRemainNum + ", orderNo="
				+ orderNo + ", parkingTimeLength=" + parkingTimeLength + ", payMoney=" + payMoney + ", paymentType="
				+ paymentType + ", uid=" + uid + ", voucherNo=" + voucherNo + ", voucherType=" + voucherType + "]";
	}
	
	
}
