package com.yiwupay.park.model.vo;

import com.yiwupay.park.annotation.RelationField;

/**
 * 红门剩余停车位实体
 * @author zls
 *
 */
public class HMParkParkingVO {
    
	/**
	 * 停车场编号
	 */
	@RelationField("outParkId")
	private String parkingId;
	/**
	 * 车场名称
	 */
	@RelationField("parkName")
	private String parkingName;
	/**
	 * 记录更新时间
	 */
	private String updateTime;
	/**
	 * 车位总数
	 */
	@RelationField("totalPlace")
	private Integer total;
	/**
	 * 剩余车位数
	 */
	@RelationField("leftPlace")
	private Integer available;
	/**
	 * 剩余临时车位数
	 */
	private Integer tempValid;
	/**
	 * 固定车位总数
	 */
	private Integer fixedTotal;
	/**
	 * 剩余固定车位数
	 */
	private Integer fixedValid;
	/**
	 * 预定车位总数
	 */
	private Integer orderTotal;
	/**
	 * 已使用预定车位数
	 */
	private Integer orderOccupy;
	public String getParkingId() {
		return parkingId;
	}
	public void setParkingId(String parkingId) {
		this.parkingId = parkingId;
	}
	public String getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
	public Integer getTotal() {
		return total;
	}
	public void setTotal(Integer total) {
		this.total = total;
	}
	public Integer getAvailable() {
		return available;
	}
	public void setAvailable(Integer available) {
		this.available = available;
	}
	public Integer getTempValid() {
		return tempValid;
	}
	public void setTempValid(Integer tempValid) {
		this.tempValid = tempValid;
	}
	public Integer getFixedTotal() {
		return fixedTotal;
	}
	public void setFixedTotal(Integer fixedTotal) {
		this.fixedTotal = fixedTotal;
	}
	public Integer getFixedValid() {
		return fixedValid;
	}
	public void setFixedValid(Integer fixedValid) {
		this.fixedValid = fixedValid;
	}
	public Integer getOrderTotal() {
		return orderTotal;
	}
	public void setOrderTotal(Integer orderTotal) {
		this.orderTotal = orderTotal;
	}
	public Integer getOrderOccupy() {
		return orderOccupy;
	}
	public void setOrderOccupy(Integer orderOccupy) {
		this.orderOccupy = orderOccupy;
	}
	public String getParkingName() {
		return parkingName;
	}
	public void setParkingName(String parkingName) {
		this.parkingName = parkingName;
	}
	@Override
	public String toString() {
		return "HMParkParkingVO [parkingId=" + parkingId + ", parkingName=" + parkingName + ", updateTime=" + updateTime
				+ ", total=" + total + ", available=" + available + ", tempValid=" + tempValid + ", fixedTotal="
				+ fixedTotal + ", fixedValid=" + fixedValid + ", orderTotal=" + orderTotal + ", orderOccupy="
				+ orderOccupy + "]";
	}
	
	
	
	
}
