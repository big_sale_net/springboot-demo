package com.yiwupay.park.model.vo;
import com.yiwupay.park.annotation.RelationField;
/**
 * 红门系统预付账单实体类
 */
public class HMChargeBillVO {
    /**
     * 车牌号码
     */
	@RelationField("plateNo")
    private String carPlateNo;
    /**
     * 停车场UUID
     */
	@RelationField("outParkId")
    private String parkingId;
	/**
	 * 订单编号
	 */
    @RelationField("outPreBillId")
	private String parkingOrder;
    /**
     * 入场时间
     */
	@RelationField("enterTime")
    private String enterTime;
    /**
     * 停车时长，如0天3小时23分
     */
    private Integer parkingTime;
	/**
	 * 停车分钟
	 */
    @RelationField("parkingTime")
    private Integer parkingMinute;
    /**
     * 应付金额（分）
     */
	@RelationField("realCost")
    private String payValue;
    /**
     * 卡号
     */
    private String cardNo;
   /**
    * 车辆类型：1 小车 2 大车
    */
    private String carType;
    /**
     * 停车流水号
     */
    @RelationField("parkingSerial")
    private String parkingSerial;
    
	public String getCarPlateNo() {
		return carPlateNo;
	}
	
	public void setCarPlateNo(String carPlateNo) {
		this.carPlateNo = carPlateNo;
	}
	
	public String getParkingId() {
		return parkingId;
	}
	
	public void setParkingId(String parkingId) {
		this.parkingId = parkingId;
	}
	
	public String getParkingOrder() {
		return parkingOrder;
	}
	
	public void setParkingOrder(String parkingOrder) {
		this.parkingOrder = parkingOrder;
	}
	
	public String getEnterTime() {
		return enterTime;
	}
	
	public void setEnterTime(String enterTime) {
		this.enterTime = enterTime;
	}
	
	public Integer getParkingTime() {
		return parkingTime;
	}
	
	public void setParkingTime(Integer parkingTime) {
		this.parkingTime = parkingTime;
	}
	
	public Integer getParkingMinute() {
		return parkingMinute;
	}
	
	public void setParkingMinute(Integer parkingMinute) {
		this.parkingMinute = parkingMinute == null ? 0 : parkingMinute;
	}
	
	public String getPayValue() {
		return payValue;
	}
	
	public void setPayValue(String payValue) {
		this.payValue = payValue == null ? "0" : payValue;
	}
	
	public String getCardNo() {
		return cardNo;
	}
	
	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}
	
	public String getCarType() {
		return carType;
	}
	
	public void setCarType(String carType) {
		this.carType = carType;
	}
	
	public String getParkingSerial() {
		return parkingSerial;
	}
	
	public void setParkingSerial(String parkingSerial) {
		this.parkingSerial = parkingSerial;
	}
	
	@Override
	public String toString() {
		return "HMChargeBillVO [carPlateNo=" + carPlateNo + ", parkingId=" + parkingId + ", parkingOrder=" + parkingOrder
				+ ", enterTime=" + enterTime + ", parkingTime=" + parkingTime + ", parkingMinute=" + parkingMinute
				+ ", payValue=" + payValue + ", cardNo=" + cardNo + ", carType=" + carType + ", parkingSerial="
				+ parkingSerial + "]";
	}
}
