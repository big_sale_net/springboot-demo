package com.yiwupay.park.model;

/**
 * 过车记录表（历史）
 * @author he
 *
 */
public class ParkVehicleRecordStatis {
    private Long recordStId;

    private Long systemId;

    private String outRecordId;

    private String plateNo;

    private Long enterTime;

    private String enterType;

    private Long parkingId;

    private String outParkingId;

    private String parkingCode;

    private Long enterId;

    private String outEnterId;

    private String enterCode;

    private Long enterRoadwayId;

    private String outEnterRoadwayId;

    private String enterRoadwayCode;

    private Long exitTime;

    private String exitType;

    private Long exitId;

    private String outExitId;

    private String exitCode;

    private Long exitRoadwayId;

    private String outExitRoadwayId;

    private String exitRoadwayCode;

    private String isOut;

    private String plateImgUrl;

    private String carImgUrl;

    private String faceImgUrl;

    private String carColor;

    private String carType;

    private String carBrand;

    public Long getRecordStId() {
        return recordStId;
    }

    public void setRecordStId(Long recordStId) {
        this.recordStId = recordStId;
    }

    public Long getSystemId() {
        return systemId;
    }

    public void setSystemId(Long systemId) {
        this.systemId = systemId;
    }

    public String getOutRecordId() {
        return outRecordId;
    }

    public void setOutRecordId(String outRecordId) {
        this.outRecordId = outRecordId == null ? null : outRecordId.trim();
    }

    public String getPlateNo() {
        return plateNo;
    }

    public void setPlateNo(String plateNo) {
        this.plateNo = plateNo == null ? null : plateNo.trim();
    }

    public Long getEnterTime() {
        return enterTime;
    }

    public void setEnterTime(Long enterTime) {
        this.enterTime = enterTime;
    }

    public String getEnterType() {
        return enterType;
    }

    public void setEnterType(String enterType) {
        this.enterType = enterType == null ? null : enterType.trim();
    }

    public Long getParkingId() {
        return parkingId;
    }

    public void setParkingId(Long parkingId) {
        this.parkingId = parkingId;
    }

    public String getOutParkingId() {
        return outParkingId;
    }

    public void setOutParkingId(String outParkingId) {
        this.outParkingId = outParkingId == null ? null : outParkingId.trim();
    }

    public String getParkingCode() {
        return parkingCode;
    }

    public void setParkingCode(String parkingCode) {
        this.parkingCode = parkingCode == null ? null : parkingCode.trim();
    }

    public Long getEnterId() {
        return enterId;
    }

    public void setEnterId(Long enterId) {
        this.enterId = enterId;
    }

    public String getOutEnterId() {
        return outEnterId;
    }

    public void setOutEnterId(String outEnterId) {
        this.outEnterId = outEnterId == null ? null : outEnterId.trim();
    }

    public String getEnterCode() {
        return enterCode;
    }

    public void setEnterCode(String enterCode) {
        this.enterCode = enterCode == null ? null : enterCode.trim();
    }

    public Long getEnterRoadwayId() {
        return enterRoadwayId;
    }

    public void setEnterRoadwayId(Long enterRoadwayId) {
        this.enterRoadwayId = enterRoadwayId;
    }

    public String getOutEnterRoadwayId() {
        return outEnterRoadwayId;
    }

    public void setOutEnterRoadwayId(String outEnterRoadwayId) {
        this.outEnterRoadwayId = outEnterRoadwayId == null ? null : outEnterRoadwayId.trim();
    }

    public String getEnterRoadwayCode() {
        return enterRoadwayCode;
    }

    public void setEnterRoadwayCode(String enterRoadwayCode) {
        this.enterRoadwayCode = enterRoadwayCode == null ? null : enterRoadwayCode.trim();
    }

    public Long getExitTime() {
        return exitTime;
    }

    public void setExitTime(Long exitTime) {
        this.exitTime = exitTime;
    }

    public String getExitType() {
        return exitType;
    }

    public void setExitType(String exitType) {
        this.exitType = exitType == null ? null : exitType.trim();
    }

    public Long getExitId() {
        return exitId;
    }

    public void setExitId(Long exitId) {
        this.exitId = exitId;
    }

    public String getOutExitId() {
        return outExitId;
    }

    public void setOutExitId(String outExitId) {
        this.outExitId = outExitId == null ? null : outExitId.trim();
    }

    public String getExitCode() {
        return exitCode;
    }

    public void setExitCode(String exitCode) {
        this.exitCode = exitCode == null ? null : exitCode.trim();
    }

    public Long getExitRoadwayId() {
        return exitRoadwayId;
    }

    public void setExitRoadwayId(Long exitRoadwayId) {
        this.exitRoadwayId = exitRoadwayId;
    }

    public String getOutExitRoadwayId() {
        return outExitRoadwayId;
    }

    public void setOutExitRoadwayId(String outExitRoadwayId) {
        this.outExitRoadwayId = outExitRoadwayId == null ? null : outExitRoadwayId.trim();
    }

    public String getExitRoadwayCode() {
        return exitRoadwayCode;
    }

    public void setExitRoadwayCode(String exitRoadwayCode) {
        this.exitRoadwayCode = exitRoadwayCode == null ? null : exitRoadwayCode.trim();
    }

    public String getIsOut() {
        return isOut;
    }

    public void setIsOut(String isOut) {
        this.isOut = isOut == null ? null : isOut.trim();
    }

    public String getPlateImgUrl() {
        return plateImgUrl;
    }

    public void setPlateImgUrl(String plateImgUrl) {
        this.plateImgUrl = plateImgUrl == null ? null : plateImgUrl.trim();
    }

    public String getCarImgUrl() {
        return carImgUrl;
    }

    public void setCarImgUrl(String carImgUrl) {
        this.carImgUrl = carImgUrl == null ? null : carImgUrl.trim();
    }

    public String getFaceImgUrl() {
        return faceImgUrl;
    }

    public void setFaceImgUrl(String faceImgUrl) {
        this.faceImgUrl = faceImgUrl == null ? null : faceImgUrl.trim();
    }

    public String getCarColor() {
        return carColor;
    }

    public void setCarColor(String carColor) {
        this.carColor = carColor == null ? null : carColor.trim();
    }

    public String getCarType() {
        return carType;
    }

    public void setCarType(String carType) {
        this.carType = carType == null ? null : carType.trim();
    }

    public String getCarBrand() {
        return carBrand;
    }

    public void setCarBrand(String carBrand) {
        this.carBrand = carBrand == null ? null : carBrand.trim();
    }
}