package com.yiwupay.park.model;

public class ParkVehiclePaymentStatis {
	//id
    private Long paymentStId;

    private Long systemId;

    private String outPaymentId;

    private Long recordId;

    private String outRecordId;
    //进场时间
    private Long enterTime;
    //应付金额
    private Long realCost;
    //总金额
    private Long totalCost;
    //实付金额
    private Long cost;
    //进场后允许出场时间（分）
    private Integer delayTime;
    //支付方式
    private String payType;
    //订单类型
    private String orderType;
    //订单状态
    private String orderStatus;
    //账单创建时间
    private Long createTime;
    //账单更新时间
    private Long updateTime;
    //支付时间
    private Long payTime;
    //停车时长
    private Integer parkTime;
    
    private Long parkingId;

    private String outParkingId;

    private String parkingCode;
    //车牌
    private String plateNo;
    //收费规则
    private String paymentRuleName;
    //异常放行规则
    private String exceptionRuleName;
    
    private String s1;

    private String s2;

    public Long getPaymentStId() {
        return paymentStId;
    }

    public void setPaymentStId(Long paymentStId) {
        this.paymentStId = paymentStId;
    }

    public Long getSystemId() {
        return systemId;
    }

    public void setSystemId(Long systemId) {
        this.systemId = systemId;
    }

    public String getOutPaymentId() {
        return outPaymentId;
    }

    public void setOutPaymentId(String outPaymentId) {
        this.outPaymentId = outPaymentId == null ? null : outPaymentId.trim();
    }

    public Long getRecordId() {
        return recordId;
    }

    public void setRecordId(Long recordId) {
        this.recordId = recordId;
    }

    public String getOutRecordId() {
        return outRecordId;
    }

    public void setOutRecordId(String outRecordId) {
        this.outRecordId = outRecordId == null ? null : outRecordId.trim();
    }

    public Long getEnterTime() {
        return enterTime;
    }

	public Long getRealCost() {
		return realCost;
	}

	public void setRealCost(Long realCost) {
		this.realCost = realCost;
	}

	public Long getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(Long totalCost) {
		this.totalCost = totalCost;
	}

	public Long getCost() {
		return cost;
	}

	public void setCost(Long cost) {
		this.cost = cost;
	}

	public Integer getDelayTime() {
		return delayTime;
	}

	public void setDelayTime(Integer delayTime) {
		this.delayTime = delayTime;
	}

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public Long getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Long createTime) {
		this.createTime = createTime;
	}

	public Long getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Long updateTime) {
		this.updateTime = updateTime;
	}

	public Long getPayTime() {
		return payTime;
	}

	public void setPayTime(Long payTime) {
		this.payTime = payTime;
	}

	public Integer getParkTime() {
		return parkTime;
	}

	public void setParkTime(Integer parkTime) {
		this.parkTime = parkTime;
	}

	public Long getParkingId() {
		return parkingId;
	}

	public void setParkingId(Long parkingId) {
		this.parkingId = parkingId;
	}

	public String getOutParkingId() {
		return outParkingId;
	}

	public void setOutParkingId(String outParkingId) {
		this.outParkingId = outParkingId;
	}

	public String getParkingCode() {
		return parkingCode;
	}

	public void setParkingCode(String parkingCode) {
		this.parkingCode = parkingCode;
	}

	public String getPlateNo() {
		return plateNo;
	}

	public void setPlateNo(String plateNo) {
		this.plateNo = plateNo;
	}

	public String getPaymentRuleName() {
		return paymentRuleName;
	}

	public void setPaymentRuleName(String paymentRuleName) {
		this.paymentRuleName = paymentRuleName;
	}

	public String getExceptionRuleName() {
		return exceptionRuleName;
	}

	public void setExceptionRuleName(String exceptionRuleName) {
		this.exceptionRuleName = exceptionRuleName;
	}

	public void setEnterTime(Long enterTime) {
		this.enterTime = enterTime;
	}

	public String getS1() {
		return s1;
	}

	public void setS1(String s1) {
		this.s1 = s1;
	}

	public String getS2() {
		return s2;
	}

	public void setS2(String s2) {
		this.s2 = s2;
	}

  
}