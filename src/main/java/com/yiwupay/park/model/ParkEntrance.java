package com.yiwupay.park.model;
/**
 * 停车场出入口实体类(对应PS_PARK_ENTRANCE表)
 * @author zls
 *
 */
public class ParkEntrance {
	/**
	 * ID
	 */
    private Long entranceId;
    
    /**
     * 系统id
     */
    private Long systemId;
    
    /**
     * 市场id
     */
    private String marektId;
    
    /**
     * 停车场id
     */
    private Long parkingId;
    
    /**
     * 接入系统停车场id
     */
    private String outParkingId;
    
    /**
     * 出入口id
     */
    private String outEntranceId;
    
    /**
     * 出入口code
     */
    private String entranceCode;
    
    /**
     * 出入口名称
     */
    private String entranceName;

    public Long getEntranceId() {
        return entranceId;
    }

    public void setEntranceId(Long entranceId) {
        this.entranceId = entranceId;
    }

    public Long getSystemId() {
        return systemId;
    }

    public void setSystemId(Long systemId) {
        this.systemId = systemId;
    }

    public String getMarektId() {
        return marektId;
    }

    public void setMarektId(String marektId) {
        this.marektId = marektId == null ? null : marektId.trim();
    }

    public Long getParkingId() {
        return parkingId;
    }

    public void setParkingId(Long parkingId) {
        this.parkingId = parkingId;
    }

    public String getOutParkingId() {
        return outParkingId;
    }

    public void setOutParkingId(String outParkingId) {
        this.outParkingId = outParkingId == null ? null : outParkingId.trim();
    }

    public String getOutEntranceId() {
        return outEntranceId;
    }

    public void setOutEntranceId(String outEntranceId) {
        this.outEntranceId = outEntranceId == null ? null : outEntranceId.trim();
    }

    public String getEntranceCode() {
        return entranceCode;
    }

    public void setEntranceCode(String entranceCode) {
        this.entranceCode = entranceCode == null ? null : entranceCode.trim();
    }

    public String getEntranceName() {
        return entranceName;
    }

    public void setEntranceName(String entranceName) {
        this.entranceName = entranceName == null ? null : entranceName.trim();
    }
}