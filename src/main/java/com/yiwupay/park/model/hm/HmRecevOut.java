package com.yiwupay.park.model.hm;

import com.yiwupay.park.enums.VehicleStatusEnum;
import com.yiwupay.park.model.ParkVehicleRecord;
import com.yiwupay.park.utils.MapCacheUtil;

/**
 * 红门推送系统出场纪录
 * @author tongsonghao
 * */
public class HmRecevOut {
	/**
	 * 区域id
	 */
	private String areaId;
	/**
	 * 卡编号
	 */
	private String cardNo;
	/**
	 * 卡类型(1. 临时车辆，2. 月卡车辆，3. 贵宾车辆，4. 储值车辆，0. 其它未知)
	 */
	private String cardType;
	/**
	 * 入场设备序列号
	 */
//	private String enterDeviceNo;
	/**
	 * 入场前端流水号
	 */
	private String enterSerialNo;
	/**
	 * 入场岗亭
	 */
	private String enterStation;
	/**
	 * 入场岗亭Id
	 */
//	private String enterStationId;
	/**
	 * 入场时间
	 */
	private String enterTime;
	/**
	 * 入场值班人员Id
	 */
	private String enterUserId;
	
	/**
	 * 出场设备序列号
	 */
//	private String exitDeviceNo;
	/**
	 * 出场前端流水号
	 */
	private String exitSerialNo;
	/**
	 * 出场岗亭
	 */
	private String exitStation;
	/**
	 * 出场岗亭Id
	 */
//	private String exitStationId;
	/**
	 * 出场时间
	 */
	private String exitTime;
	/**
	 * 出场值班人员Id
	 */
	private String exitUserId;
	/**
	 * 免费金额
	 */
	private float freeAmount;
	/**
	 * 记录Id
	 */
	private String id;
	/**
	 * 停车场会员Id
	 */
	private String memberId;
	/**
	 * 支付金额
	 */
	private float paidAmount;
	/**
	 * parkingId
	 */
	private String parkingId;
	/**
	 * 停车时长
	 */
	private int parkingSeconds;
	/**
	 * 支付设备序列号
	 */
//	private String payDeviceSerialNo;
	/**
	 * 前端支付类型(0:现金,1:场中支付（中央缴费、微信）,2:第三方代收,3:ATM)
	 */
	private String payType;
	/**
	 * 总金额
	 */
	private float totalAmount;
	/**
	 * 车辆号牌
	 */
	private String vehiclePlate;
	/**
	 * 车辆类型(1:小车;2:大车;3:超大车)
	 */
	private String vehicleType;
	
	
	
	public String getAreaId() {
		return areaId;
	}
	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}
	public String getCardNo() {
		return cardNo;
	}
	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
//	public String getEnterDeviceNo() {
//		return enterDeviceNo;
//	}
//	public void setEnterDeviceNo(String enterDeviceNo) {
//		this.enterDeviceNo = enterDeviceNo;
//	}
	public String getEnterSerialNo() {
		return enterSerialNo;
	}
	public void setEnterSerialNo(String enterSerialNo) {
		this.enterSerialNo = enterSerialNo;
	}
	public String getEnterStation() {
		return enterStation;
	}
	public void setEnterStation(String enterStation) {
		this.enterStation = enterStation;
	}
//	public String getEnterStationId() {
//		return enterStationId;
//	}
//	public void setEnterStationId(String enterStationId) {
//		this.enterStationId = enterStationId;
//	}
	public String getEnterTime() {
		return enterTime;
	}
	public void setEnterTime(String enterTime) {
		this.enterTime = enterTime;
	}
	public String getEnterUserId() {
		return enterUserId;
	}
	public void setEnterUserId(String enterUserId) {
		this.enterUserId = enterUserId;
	}
//	public String getExitDeviceNo() {
//		return exitDeviceNo;
//	}
//	public void setExitDeviceNo(String exitDeviceNo) {
//		this.exitDeviceNo = exitDeviceNo;
//	}
	public String getExitSerialNo() {
		return exitSerialNo;
	}
	public void setExitSerialNo(String exitSerialNo) {
		this.exitSerialNo = exitSerialNo;
	}
	public String getExitStation() {
		return exitStation;
	}
	public void setExitStation(String exitStation) {
		this.exitStation = exitStation;
	}
//	public String getExitStationId() {
//		return exitStationId;
//	}
//	public void setExitStationId(String exitStationId) {
//		this.exitStationId = exitStationId;
//	}
	public String getExitTime() {
		return exitTime;
	}
	public void setExitTime(String exitTime) {
		this.exitTime = exitTime;
	}
	public String getExitUserId() {
		return exitUserId;
	}
	public void setExitUserId(String exitUserId) {
		this.exitUserId = exitUserId;
	}
	public float getFreeAmount() {
		return freeAmount;
	}
	public void setFreeAmount(float freeAmount) {
		this.freeAmount = freeAmount;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getMemberId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	public float getPaidAmount() {
		return paidAmount;
	}
	public void setPaidAmount(float paidAmount) {
		this.paidAmount = paidAmount;
	}
	public String getParkingId() {
		return parkingId;
	}
	public void setParkingId(String parkingId) {
		this.parkingId = parkingId;
	}
	public int getParkingSeconds() {
		return parkingSeconds;
	}
	public void setParkingSeconds(int parkingSeconds) {
		this.parkingSeconds = parkingSeconds;
	}
//	public String getPayDeviceSerialNo() {
//		return payDeviceSerialNo;
//	}
//	public void setPayDeviceSerialNo(String payDeviceSerialNo) {
//		this.payDeviceSerialNo = payDeviceSerialNo;
//	}
	public String getPayType() {
		return payType;
	}
	public void setPayType(String payType) {
		this.payType = payType;
	}
	public float getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(float totalAmount) {
		this.totalAmount = totalAmount;
	}
	public String getVehiclePlate() {
		return vehiclePlate;
	}
	public void setVehiclePlate(String vehiclePlate) {
		this.vehiclePlate = vehiclePlate;
	}
	public String getVehicleType() {
		return vehicleType;
	}
	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}
	
	public static ParkVehicleRecord toParkVehicleRecord(HmRecevOut data, Long systemId) {
		ParkVehicleRecord inputRecord=new ParkVehicleRecord();
		inputRecord.setSystemId(systemId);
		inputRecord.setPlateNo(data.getVehiclePlate());
		inputRecord.setIsOut(VehicleStatusEnum.OUT.getStatus());
		inputRecord.setOutParkingId(data.getAreaId());
		inputRecord.setEnterTime(data.getEnterTime()!=null?Long.parseLong(data.getEnterTime()):null);
		inputRecord.setOutTime(data.getExitTime()!=null?Long.parseLong(data.getExitTime()):null);
		inputRecord.setOutRoadwayIdExit(MapCacheUtil.HM_ROADWAY_ID.get(data.getExitStation()));
		return inputRecord;
		
	} 
	
	@Override
	public String toString() {
		return "HmRecevOut [areaId=" + areaId + ", cardNo=" + cardNo + ", cardType=" + cardType + ", enterSerialNo="
				+ enterSerialNo + ", enterStation=" + enterStation + ", enterTime=" + enterTime + ", enterUserId="
				+ enterUserId + ", exitSerialNo=" + exitSerialNo + ", exitStation=" + exitStation + ", exitTime="
				+ exitTime + ", exitUserId=" + exitUserId + ", freeAmount=" + freeAmount + ", id=" + id + ", memberId="
				+ memberId + ", paidAmount=" + paidAmount + ", parkingId=" + parkingId + ", parkingSeconds="
				+ parkingSeconds + ", payType=" + payType + ", totalAmount=" + totalAmount + ", vehiclePlate="
				+ vehiclePlate + ", vehicleType=" + vehicleType + "]";
	}
	
	
	
	
}
