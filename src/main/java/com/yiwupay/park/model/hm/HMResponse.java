package com.yiwupay.park.model.hm;

/**
 * 红门进场场推送返回内容
 * @author zls
 *
 */
public class HMResponse {
	/**
	 * 返回码
	 */
    private String code;
    /**
     * 返回requestId
     */
    private String requestId;
    
    
    
    public HMResponse() {
		super();
	}



	public HMResponse(String code, String requestId) {
		super();
		this.code = code;
		this.requestId = requestId;
	}



	public String getCode() {
		return code;
	}



	public void setCode(String code) {
		this.code = code;
	}



	public String getRequestId() {
		return requestId;
	}



	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	

	
}
