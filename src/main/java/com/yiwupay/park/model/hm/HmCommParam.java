package com.yiwupay.park.model.hm;
/**
 * 红门系统推送接收通用类
 * @author zls
 *
 */
public class HmCommParam {
   private String type;
   private String appId;
   private String requestId;
   private String timestamp;
   private String sign;
   
   private Object body;

public String getType() {
	return type;
}

public void setType(String type) {
	this.type = type;
}

public String getAppId() {
	return appId;
}

public void setAppId(String appId) {
	this.appId = appId;
}

public String getRequestId() {
	return requestId;
}

public void setRequestId(String requestId) {
	this.requestId = requestId;
}

public String getTimestamp() {
	return timestamp;
}

public void setTimestamp(String timestamp) {
	this.timestamp = timestamp;
}

public String getSign() {
	return sign;
}

public void setSign(String sign) {
	this.sign = sign;
}

public Object getBody() {
	return body;
}

public void setBody(Object body) {
	this.body = body;
}
   
   
}
