package com.yiwupay.park.model.hm;

import com.yiwupay.park.enums.VehicleStatusEnum;
import com.yiwupay.park.model.ParkVehicleRecord;
import com.yiwupay.park.service.ParkParkingCacheService;
import com.yiwupay.park.utils.MapCacheUtil;

/**
 * 红门系统入场记录
 * @author zls
 *
 */
public class HmRecevEnter {
	/**
	 * 接入系统停车场ID
	 */
   private String areaId;                
   /**
    * 卡编号
    */
   private String cardNo;
   /**
    * 卡类型 1. 临时车辆，2. 月卡车辆，3. 贵宾车辆，4. 储值车辆，0. 其它未知
    */
   private String cardType;
   /**
    * 入场设备序列号
    */
//   private String enterDeviceNo;
   /**
    * 入场前端流水号
    */
   private String enterSerialNo;
   /**
    * 入场岗亭
    */
   private String enterStation;
   /**
    * 入场岗亭id
    */
//   private String enterStationId;
   /**
    * 入场时间
    */
   private String enterTime;
   /**
    * 入场值班人员ID
    */
   private String enterUserId;
   /**
    * 接入系统过车记录ID
    */
   private String id;
   /**
    * 停车场会员id
    */
   private String parkingId;
   /**
    * 整车图片
    */
   private String enterBigImg;
   /**
    * 车牌图片
    */
   private String enterSmallImg;
   /**
    * 车辆号牌
    */
   private String vehiclePlate;
   /**
    * 车辆类型1：小车，2：大车，3：超大车
    */
   private String vehicleType;
	   
	public String getAreaId() {
		return areaId;
	}
	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}
	public String getCardNo() {
		return cardNo;
	}
	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
//	public String getEnterDeviceNo() {
//		return enterDeviceNo;
//	}
//	public void setEnterDeviceNo(String enterDeviceNo) {
//		this.enterDeviceNo = enterDeviceNo;
//	}
	public String getEnterSerialNo() {
		return enterSerialNo;
	}
	public void setEnterSerialNo(String enterSerialNo) {
		this.enterSerialNo = enterSerialNo;
	}
	public String getEnterStation() {
		return enterStation;
	}
	public void setEnterStation(String enterStation) {
		this.enterStation = enterStation;
	}
//	public String getEnterStationId() {
//		return enterStationId;
//	}
//	public void setEnterStationId(String enterStationId) {
//		this.enterStationId = enterStationId;
//	}
	public String getEnterTime() {
		return enterTime;
	}
	public void setEnterTime(String enterTime) {
		this.enterTime = enterTime;
	}
	public String getEnterUserId() {
		return enterUserId;
	}
	public void setEnterUserId(String enterUserId) {
		this.enterUserId = enterUserId;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getParkingId() {
		return parkingId;
	}
	public void setParkingId(String parkingId) {
		this.parkingId = parkingId;
	}
	public String getVehiclePlate() {
		return vehiclePlate;
	}
	public void setVehiclePlate(String vehiclePlate) {
		this.vehiclePlate = vehiclePlate;
	}
	public String getVehicleType() {
		return vehicleType;
	}
	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}
	
	public String getEnterBigImg() {
		return enterBigImg;
	}
	public void setEnterBigImg(String enterBigImg) {
		this.enterBigImg = enterBigImg;
	}
	public String getEnterSmallImg() {
		return enterSmallImg;
	}
	public void setEnterSmallImg(String enterSmallImg) {
		this.enterSmallImg = enterSmallImg;
	}
	public static ParkVehicleRecord toParkVehicleRecord(HmRecevEnter data, Long systemId) {
		ParkVehicleRecord inputRecord = new ParkVehicleRecord();
		inputRecord.setSystemId(systemId);
		inputRecord.setPlateNo(data.getVehiclePlate());
		inputRecord.setIsOut(VehicleStatusEnum.ENTER.getStatus());
		inputRecord.setOutParkingId(data.getAreaId());
		inputRecord.setEnterTime(data.getEnterTime()!=null?Long.parseLong(data.getEnterTime()):null);
		String key_parkingIdMap = ParkParkingCacheService.creatKeyOfParkIdMap(systemId,
				data.getAreaId());
		inputRecord.setParkingId(ParkParkingCacheService.getParkIdMap().get(key_parkingIdMap));
		inputRecord.setOutRecordId(data.getId());//接入停车系统过车记录ID
		inputRecord.setOutEntranceId(MapCacheUtil.HM_ROADWAY_ID.get(data.getEnterStation()));
		inputRecord.setPlateImgUrl(data.getEnterSmallImg());
		inputRecord.setCarImgUrl(data.getEnterBigImg());
		inputRecord.setOutRoadwayId(MapCacheUtil.HM_ROADWAY_ID.get(data.getEnterStation()));
		return inputRecord;
	}
	
	
	
	
	
	@Override
	public String toString() {
		return "HmRecevEnter [areaId=" + areaId + ", cardNo=" + cardNo + ", cardType=" + cardType + ", enterSerialNo="
				+ enterSerialNo + ", enterStation=" + enterStation + ", enterTime=" + enterTime + ", enterUserId="
				+ enterUserId + ", id=" + id + ", parkingId=" + parkingId + ", enterBigImg=" + enterBigImg
				+ ", enterSmallImg=" + enterSmallImg + ", vehiclePlate=" + vehiclePlate + ", vehicleType=" + vehicleType
				+ "]";
	}
	   
   
}
