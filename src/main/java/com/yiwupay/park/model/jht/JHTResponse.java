package com.yiwupay.park.model.jht;

import java.util.List;

public class JHTResponse<T> {
	/**
	 * 返回码
	 */
    private Integer resultCode;
    /**
     * 返回信息
     */
    private String message;
    
    
    
    public JHTResponse() {
		super();
	}

	public JHTResponse(Integer resultCode, String message) {
		this.resultCode = resultCode;
		this.message = message;
	}

	private List<T> dataItems;

	public Integer getResultCode() {
		return resultCode;
	}

	public void setResultCode(Integer resultCode) {
		this.resultCode = resultCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<T> getDataItems() {
		return dataItems;
	}

	public void setDataItems(List<T> dataItems) {
		this.dataItems = dataItems;
	}

	@Override
	public String toString() {
		return "JhtResultParam [resultCode=" + resultCode + ", message=" + message + ", dataItems=" + dataItems + "]";
	}
    
}
