package com.yiwupay.park.model.jht;

import java.util.Date;

import com.yiwupay.park.enums.VehicleStatusEnum;
import com.yiwupay.park.model.ParkVehicleRecord;
import com.yiwupay.park.model.dh.RecevEnterParam;
import com.yiwupay.park.service.ParkParkingCacheService;
import com.yiwupay.park.utils.DateUtil;
import com.yiwupay.park.utils.park.JHTParkCodeMap;

/**
 * 捷惠通推送系统入场记录
 * @author zls
 *
 */
public class JhtRecevEnter {
	/**
	 * 记录id
	 */
	private String itemId;
	/**
	 * 停车场名称
	 */
	private String parkName;
	/**
	 * 停车场编号
	 */
	private String parkCode;
	
	/**
	 * 入场时间
	 */
	private String inTime;
	
	/**
	 * 操作员
	 */
	private String inOperator;
	
	/**
	 * 车牌
	 */
	private String carNumber;
	/**
	 * 未发行的卡号，如纸票号等
	 */
	private String idno;
	
	/**
	 * 设备名称
	 */
	private String equipName;
	
	/**
	 * 设备编号
	 */
	private String equipCode;
	
	/**
	 * 备用字段
	 */
	private String attach;
	
	private String isReal;
	
	private String inTotal;

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getParkName() {
		return parkName;
	}

	public void setParkName(String parkName) {
		this.parkName = parkName;
	}

	public String getParkCode() {
		return parkCode;
	}

	public void setParkCode(String parkCode) {
		this.parkCode = parkCode;
	}

	public String getInTime() {
		return inTime;
	}

	public void setInTime(String inTime) {
		this.inTime = inTime;
	}

	public String getInOperator() {
		return inOperator;
	}

	public void setInOperator(String inOperator) {
		this.inOperator = inOperator;
	}

	public String getCarNumber() {
		return carNumber;
	}

	public void setCarNumber(String carNumber) {
		this.carNumber = carNumber;
	}

	public String getIdno() {
		return idno;
	}

	public void setIdno(String idno) {
		this.idno = idno;
	}

	public String getEquipName() {
		return equipName;
	}

	public void setEquipName(String equipName) {
		this.equipName = equipName;
	}

	public String getEquipCode() {
		return equipCode;
	}

	public void setEquipCode(String equipCode) {
		this.equipCode = equipCode;
	}

	public String getAttach() {
		return attach;
	}

	public void setAttach(String attach) {
		this.attach = attach;
	}
    
	public String getIsReal() {
		return isReal;
	}

	public void setIsReal(String isReal) {
		this.isReal = isReal;
	}

	public String getInTotal() {
		return inTotal;
	}

	public void setInTotal(String inTotal) {
		this.inTotal = inTotal;
	}

	public static ParkVehicleRecord toParkVehicleRecord(JhtRecevEnter data, Long systemId) {
		ParkVehicleRecord inputRecord = new ParkVehicleRecord();
		inputRecord.setSystemId(systemId);
		inputRecord.setPlateNo(data.getCarNumber().replace("-", ""));
		inputRecord.setIsOut(VehicleStatusEnum.ENTER.getStatus());
		inputRecord.setOutParkingId(JHTParkCodeMap.map.get(data.getEquipName()));
		Date date = DateUtil.stringToDateByFormat(data.getInTime(), DateUtil.DB_YMDHMS_FORMAT);
		inputRecord.setEnterTime(DateUtil.getDateToLong(date, DateUtil.YMDHMS_FORMAT));
		String key_parkingIdMap = ParkParkingCacheService.creatKeyOfParkIdMap(systemId,
				JHTParkCodeMap.map.get(data.getEquipName()));
		inputRecord.setParkingId(ParkParkingCacheService.getParkIdMap().get(key_parkingIdMap));
		inputRecord.setOutRecordId(data.getItemId());//接入停车系统过车记录ID
		inputRecord.setOutEntranceId(data.getEquipCode());
		inputRecord.setOutEntranceId(data.getEquipCode());
		inputRecord.setOutRoadwayId(data.getEquipCode());
		return inputRecord;
		
	}

	@Override
	public String toString() {
		return "JhtRecevEnter [itemId=" + itemId + ", parkName=" + parkName + ", parkCode=" + parkCode + ", inTime="
				+ inTime + ", inOperator=" + inOperator + ", carNumber=" + carNumber + ", idno=" + idno + ", equipName="
				+ equipName + ", equipCode=" + equipCode + ", attach=" + attach + ", isReal=" + isReal + ", inTotal="
				+ inTotal + "]";
	}
	

	
}
