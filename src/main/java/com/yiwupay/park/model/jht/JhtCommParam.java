package com.yiwupay.park.model.jht;

import java.util.List;

import com.yiwupay.park.utils.JsonUtils;
/**
 * 通用请求参数
 * @author zls
 *
 * @param <T>
 */
public class JhtCommParam {
	/**
	 * 平台编号
	 */
     private String pno;
     /**
      * 令牌
      */
     private String tn;
     /**
      * 请求时间
      */
     private String ts;
     
     /**
      * 签名
      */
     private String sn;
     
     /**
      * 版本号
      */
     private String ve;
     
     /**
      * 业务参数
      */
     private String dataItems;

	public String getPno() {
		return pno;
	}

	public void setPno(String pno) {
		this.pno = pno;
	}

	public String getTn() {
		return tn;
	}

	public void setTn(String tn) {
		this.tn = tn;
	}

	public String getTs() {
		return ts;
	}

	public void setTs(String ts) {
		this.ts = ts;
	}

	public String getSn() {
		return sn;
	}

	public void setSn(String sn) {
		this.sn = sn;
	}

	public String getVe() {
		return ve;
	}

	public void setVe(String ve) {
		this.ve = ve;
	}

	public String getDataItems() {
		return dataItems;
	}

	public void setDataItems(String dataItems) {
		this.dataItems = dataItems;
	}

	@Override
	public String toString() {
		return "JhtCommParam [pno=" + pno + ", tn=" + tn + ", ts=" + ts + ", sn=" + sn + ", ve=" + ve + ", dataItems="
				+ dataItems + "]";
	}

	
}
