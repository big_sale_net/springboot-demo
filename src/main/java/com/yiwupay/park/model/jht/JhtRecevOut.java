package com.yiwupay.park.model.jht;

import java.util.Date;

import com.yiwupay.park.enums.JHTOutTypeEnum;
import com.yiwupay.park.enums.VehicleStatusEnum;
import com.yiwupay.park.model.ParkVehicleRecord;
import com.yiwupay.park.utils.DateUtil;

/**
 * 捷惠通推送系统出场记录
 * @author zls
 *
 */
public class JhtRecevOut {
	/**
	 * 记录id
	 */
   private String itemId;
   /**
    * 停车场名称
    */
   private String parkName;
   /**
    * 停车场编号
    */
   private String parkCode;
   /**
    * 车牌
    */
   private String carNumber;
   /**
    * 出场时间
    */
   private String outTime;
   /**
    * 应收金额(元)
    */
   private Double ysMoney;
   /**
    * 优惠金额
    */
   private Double yhMoney;
   /**
    * 实收金额
    */
   private Double ssMoney;
   /**
    * 免费金额
    */
   private Double freeMoney;
   /**
    * 回滚减免金额
    */
   private Double hgMoney;
   /**
    * 入场时间
    */
   private String inTime;
   /**
    * 入场设备编号
    */
   private String inEquipCode;
   /**
    * 入场设备名称
    */
   private String inEquipName;
   /**
    * 入场记录id
    */
   private String inRecordId;
   /**
    * 出场方式
    */
   private String outMode;
   /**
    * 操作员
    */
   private String outOperator;
   /**
    * 出场设备编号
    */
   private String outEquipCode;
   /**
    * 车场设备名称
    */
   private String outEquipName;
   /**
    * 付款方式
    */
   private String payTypeName;
   /**
    * 未发行的卡号，如纸票号等
    */
   private String idno;
   /**
    * 是否是收费记录0：出场记录，1：收费记录(非出场记录)
    */
   private String isReal;
   /**
    * 备用字段
    */
   private String attach;
   
   private String inTotal;
public String getItemId() {
	return itemId;
}
public void setItemId(String itemId) {
	this.itemId = itemId;
}
public String getParkName() {
	return parkName;
}
public void setParkName(String parkName) {
	this.parkName = parkName;
}
public String getParkCode() {
	return parkCode;
}
public void setParkCode(String parkCode) {
	this.parkCode = parkCode;
}
public String getCarNumber() {
	return carNumber;
}
public void setCarNumber(String carNumber) {
	this.carNumber = carNumber;
}
public String getOutTime() {
	return outTime;
}
public void setOutTime(String outTime) {
	this.outTime = outTime;
}
public Double getYsMoney() {
	return ysMoney;
}
public void setYsMoney(Double ysMoney) {
	this.ysMoney = ysMoney;
}
public Double getYhMoney() {
	return yhMoney;
}
public void setYhMoney(Double yhMoney) {
	this.yhMoney = yhMoney;
}
public Double getSsMoney() {
	return ssMoney;
}
public void setSsMoney(Double ssMoney) {
		this.ssMoney = ssMoney;
}
public Double getFreeMoney() {
	return freeMoney;
}
public void setFreeMoney(Double freeMoney) {
	this.freeMoney = freeMoney;
}
public Double getHgMoney() {
	return hgMoney;
}
public void setHgMoney(Double hgMoney) {
	this.hgMoney = hgMoney;
}
public String getInTime() {
	return inTime;
}
public void setInTime(String inTime) {
	this.inTime = inTime;
}
public String getInEquipCode() {
	return inEquipCode;
}
public void setInEquipCode(String inEquipCode) {
	this.inEquipCode = inEquipCode;
}
public String getInEquipName() {
	return inEquipName;
}
public void setInEquipName(String inEquipName) {
	this.inEquipName = inEquipName;
}
public String getInRecordId() {
	return inRecordId;
}
public void setInRecordId(String inRecordId) {
	this.inRecordId = inRecordId;
}
public String getOutMode() {
	return outMode;
}
public void setOutMode(String outMode) {
	this.outMode = outMode;
}
public String getOutOperator() {
	return outOperator;
}
public void setOutOperator(String outOperator) {
	this.outOperator = outOperator;
}
public String getOutEquipCode() {
	return outEquipCode;
}
public void setOutEquipCode(String outEquipCode) {
	this.outEquipCode = outEquipCode;
}
public String getOutEquipName() {
	return outEquipName;
}
public void setOutEquipName(String outEquipName) {
	this.outEquipName = outEquipName;
}
public String getPayTypeName() {
	return payTypeName;
}
public void setPayTypeName(String payTypeName) {
	this.payTypeName = payTypeName;
}
public String getIdno() {
	return idno;
}
public void setIdno(String idno) {
	this.idno = idno;
}
public String getIsReal() {
	return isReal;
}
public void setIsReal(String isReal) {
	this.isReal = isReal;
}
public String getAttach() {
	return attach;
}
public void setAttach(String attach) {
	this.attach = attach;
}

public static ParkVehicleRecord toParkVehicleRecord(JhtRecevOut data, Long systemId) {
	ParkVehicleRecord inputRecord = new ParkVehicleRecord();
	inputRecord.setSystemId(systemId);
	inputRecord.setPlateNo(data.getCarNumber().replace("-", ""));
	inputRecord.setIsOut(VehicleStatusEnum.OUT.getStatus());
	Date date = DateUtil.stringToDateByFormat(data.getInTime(), DateUtil.DB_YMDHMS_FORMAT);
	inputRecord.setEnterTime(DateUtil.getDateToLong(date, DateUtil.YMDHMS_FORMAT));
	Date date2 = DateUtil.stringToDateByFormat(data.getOutTime(), DateUtil.DB_YMDHMS_FORMAT);
	inputRecord.setOutTime(DateUtil.getDateToLong(date2, DateUtil.YMDHMS_FORMAT));
	if(data.getOutMode() != null && "FREETIME".equals(data.getOutMode())){
		inputRecord.setOutType("正常收费");
	}else{
		inputRecord.setOutType(JHTOutTypeEnum.getEnum(data.getOutMode()).getText());
	}
	inputRecord.setOutRoadwayIdExit(data.getOutEquipCode());
	return inputRecord;
	
}

public String getInTotal() {
	return inTotal;
}
public void setInTotal(String inTotal) {
	this.inTotal = inTotal;
}
@Override
public String toString() {
	return "JhtRecevOut [itemId=" + itemId + ", parkName=" + parkName + ", parkCode=" + parkCode + ", carNumber="
			+ carNumber + ", outTime=" + outTime + ", ysMoney=" + ysMoney + ", yhMoney=" + yhMoney + ", ssMoney="
			+ ssMoney + ", freeMoney=" + freeMoney + ", hgMoney=" + hgMoney + ", inTime=" + inTime + ", inEquipCode="
			+ inEquipCode + ", inEquipName=" + inEquipName + ", inRecordId=" + inRecordId + ", outMode=" + outMode
			+ ", outOperator=" + outOperator + ", outEquipCode=" + outEquipCode + ", outEquipName=" + outEquipName
			+ ", payTypeName=" + payTypeName + ", idno=" + idno + ", isReal=" + isReal + ", attach=" + attach
			+ ", inTotal=" + inTotal + "]";
}

   
   
}
