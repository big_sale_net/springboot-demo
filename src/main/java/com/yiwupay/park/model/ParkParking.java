package com.yiwupay.park.model;

/**
 * 停车场实体类(对应PS_PARK_PARKING表)
 * @author zls
 *
 */
public class ParkParking {
	/**
	 * ID自增
	 */
    private Long parkingId;
    
    /**
     * 系统id
     */
    private Long systemId;
    
    /**
     * 市场id
     */
    private String marektId;
    
    /**
     * 接入系统停车场ID
     */
    private String outParkingId;
    
    /**
     * 接入系统停车场CODE
     */
    private String parkingCode;
    
    /**
     * 停车场名称
     */
    private String parkingName;
    
    /**
     * 总车位数
     */
    private Integer totalPlace;
    
    /**
     * 剩余车位
     */
    private Integer leftPlace;
    
    /**
     * 备注
     */
    private String remark;
    
    /**
     * 创建时间
     */
    private Long createTime;
    
    /**
     * 更新时间
     */
    private Long updateTime;
    
    //收费规则
    private String feeRule;

    public Long getParkingId() {
        return parkingId;
    }

    public void setParkingId(Long parkingId) {
        this.parkingId = parkingId;
    }

    public Long getSystemId() {
        return systemId;
    }

    public void setSystemId(Long systemId) {
        this.systemId = systemId;
    }

    public String getMarektId() {
        return marektId;
    }

    public void setMarektId(String marektId) {
        this.marektId = marektId == null ? null : marektId.trim();
    }

    
    public String getOutParkingId() {
		return outParkingId;
	}

	public void setOutParkingId(String outParkingId) {
		this.outParkingId = outParkingId;
	}

	public String getParkingCode() {
        return parkingCode;
    }

    public void setParkingCode(String parkingCode) {
        this.parkingCode = parkingCode == null ? null : parkingCode.trim();
    }

    public String getParkingName() {
        return parkingName;
    }

    public void setParkingName(String parkingName) {
        this.parkingName = parkingName == null ? null : parkingName.trim();
    }

   

    public Integer getTotalPlace() {
		return totalPlace;
	}

	public void setTotalPlace(Integer totalPlace) {
		this.totalPlace = totalPlace;
	}

	public Integer getLeftPlace() {
		return leftPlace;
	}

	public void setLeftPlace(Integer leftPlace) {
		this.leftPlace = leftPlace;
	}

	public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Long updateTime) {
        this.updateTime = updateTime;
    }

	public String getFeeRule() {
		return feeRule;
	}

	public void setFeeRule(String feeRule) {
		this.feeRule = feeRule;
	}

	@Override
	public String toString() {
		return "ParkParking [parkingId=" + parkingId + ", systemId=" + systemId + ", marektId=" + marektId
				+ ", outParkingId=" + outParkingId + ", parkingCode=" + parkingCode + ", parkingName=" + parkingName
				+ ", totalPlace=" + totalPlace + ", leftPlace=" + leftPlace + ", remark=" + remark + ", createTime="
				+ createTime + ", updateTime=" + updateTime + ", feeRule=" + feeRule + "]";
	}
    
}