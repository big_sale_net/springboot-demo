package com.yiwupay.park.model;

/**
 * 过车记录表（临时，只记录出场记录）
 * @author he
 *
 */
public class ParkVehicleRecordTemp {
    private Long recordTempId;

    private Long systemId;

    private String outRecordId;

    private String plateNo;

    private Long crosstime;

    private String plateImgUrl;

    private String carImgUrl;

    private String faceImgUrl;

    private String carColor;

    private String carType;

    private String carBrand;

    private Long parkingId;

    private String outParkingId;

    private String parkingCode;

    private Long entranceId;

    private String outEntranceId;

    private String entranceCode;

    private Long roadwayId;

    private String outRoadwayId;

    private String roadwayCode;

    private String isOut;

    private String crossType;

    private Long s1;

    private String s2;

    public Long getRecordTempId() {
		return recordTempId;
	}

	public void setRecordTempId(Long recordTempId) {
		this.recordTempId = recordTempId;
	}

	public Long getSystemId() {
        return systemId;
    }

    public void setSystemId(Long systemId) {
        this.systemId = systemId;
    }

    public String getOutRecordId() {
        return outRecordId;
    }

    public void setOutRecordId(String outRecordId) {
        this.outRecordId = outRecordId == null ? null : outRecordId.trim();
    }

    public String getPlateNo() {
        return plateNo;
    }

    public void setPlateNo(String plateNo) {
        this.plateNo = plateNo == null ? null : plateNo.trim();
    }

    public Long getCrosstime() {
        return crosstime;
    }

    public void setCrosstime(Long crosstime) {
        this.crosstime = crosstime;
    }

    public String getPlateImgUrl() {
        return plateImgUrl;
    }

    public void setPlateImgUrl(String plateImgUrl) {
        this.plateImgUrl = plateImgUrl == null ? null : plateImgUrl.trim();
    }

    public String getCarImgUrl() {
        return carImgUrl;
    }

    public void setCarImgUrl(String carImgUrl) {
        this.carImgUrl = carImgUrl == null ? null : carImgUrl.trim();
    }

    public String getFaceImgUrl() {
        return faceImgUrl;
    }

    public void setFaceImgUrl(String faceImgUrl) {
        this.faceImgUrl = faceImgUrl == null ? null : faceImgUrl.trim();
    }

    public String getCarColor() {
        return carColor;
    }

    public void setCarColor(String carColor) {
        this.carColor = carColor == null ? null : carColor.trim();
    }

    public String getCarType() {
        return carType;
    }

    public void setCarType(String carType) {
        this.carType = carType == null ? null : carType.trim();
    }

    public String getCarBrand() {
        return carBrand;
    }

    public void setCarBrand(String carBrand) {
        this.carBrand = carBrand == null ? null : carBrand.trim();
    }

    public Long getParkingId() {
        return parkingId;
    }

    public void setParkingId(Long parkingId) {
        this.parkingId = parkingId;
    }

    public String getOutParkingId() {
        return outParkingId;
    }

    public void setOutParkingId(String outParkingId) {
        this.outParkingId = outParkingId == null ? null : outParkingId.trim();
    }

    public String getParkingCode() {
        return parkingCode;
    }

    public void setParkingCode(String parkingCode) {
        this.parkingCode = parkingCode == null ? null : parkingCode.trim();
    }

    public Long getEntranceId() {
        return entranceId;
    }

    public void setEntranceId(Long entranceId) {
        this.entranceId = entranceId;
    }

    public String getOutEntranceId() {
        return outEntranceId;
    }

    public void setOutEntranceId(String outEntranceId) {
        this.outEntranceId = outEntranceId == null ? null : outEntranceId.trim();
    }

    public String getEntranceCode() {
        return entranceCode;
    }

    public void setEntranceCode(String entranceCode) {
        this.entranceCode = entranceCode == null ? null : entranceCode.trim();
    }

    public Long getRoadwayId() {
        return roadwayId;
    }

    public void setRoadwayId(Long roadwayId) {
        this.roadwayId = roadwayId;
    }

    public String getOutRoadwayId() {
        return outRoadwayId;
    }

    public void setOutRoadwayId(String outRoadwayId) {
        this.outRoadwayId = outRoadwayId == null ? null : outRoadwayId.trim();
    }

    public String getRoadwayCode() {
        return roadwayCode;
    }

    public void setRoadwayCode(String roadwayCode) {
        this.roadwayCode = roadwayCode == null ? null : roadwayCode.trim();
    }

    public String getIsOut() {
        return isOut;
    }

    public void setIsOut(String isOut) {
        this.isOut = isOut == null ? null : isOut.trim();
    }

    public String getCrossType() {
        return crossType;
    }

    public void setCrossType(String crossType) {
        this.crossType = crossType == null ? null : crossType.trim();
    }

    public Long getS1() {
        return s1;
    }

    public void setS1(Long s1) {
        this.s1 = s1;
    }

    public String getS2() {
        return s2;
    }

    public void setS2(String s2) {
        this.s2 = s2 == null ? null : s2.trim();
    }
}