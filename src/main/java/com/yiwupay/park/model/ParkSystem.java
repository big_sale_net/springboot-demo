package com.yiwupay.park.model;
/**
 * 接入系统实体类（对应ps_park_system表）
 * @author zls
 *
 */
public class ParkSystem {
	/**
	 * 系统id
	 */
    private Long systemId;
    
    /**
     * 系统ip
     */
    private String systemIp;
    
    /**
     * 接入系统品牌
     */
    private String systemName;
    
    /**
     * 接入系统品牌code
     */
    private String systemCode;

    public Long getSystemId() {
        return systemId;
    }

    public void setSystemId(Long systemId) {
        this.systemId = systemId;
    }

    public String getSystemIp() {
		return systemIp;
	}

	public void setSystemIp(String systemIp) {
		this.systemIp = systemIp;
	}

	public String getSystemName() {
        return systemName;
    }

    public void setSystemName(String systemName) {
        this.systemName = systemName == null ? null : systemName.trim();
    }

	public String getSystemCode() {
		return systemCode;
	}

	public void setSystemCode(String systemCode) {
		this.systemCode = systemCode;
	}
    
}