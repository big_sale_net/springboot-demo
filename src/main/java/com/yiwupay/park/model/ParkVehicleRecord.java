package com.yiwupay.park.model;
/**
 * 过车记录实体类(对应PS_PARK_VEHICLE_RECORD表)
 * @author zls
 *
 */
public class ParkVehicleRecord {
	/**
	 * ID自增
	 */
    private Long recordId;
    
    /**
     * 接入停车系统ID
     */
    private Long systemId;
    
    /**
     * 接入停车系统过车记录ID
     */
    private String outRecordId;
    
    /**
     * 车牌号
     */
    private String plateNo;
    
    /**
     * 车辆进入时间
     */
    private Long enterTime;
    
    /**
     * 车辆进入方式
     */
    private String enterType;
    
    /**
     * 车牌图片url
     */
    private String plateImgUrl;
    
    /**
     * 车辆图片url
     */
    private String carImgUrl;    
    
    /**
     * 人脸图片url
     */
    private String faceImgUrl;
    
    /**
     * 车辆颜色类型
     */
    private String carColor;
    
    /**
     * 车辆类型
     */
    private String carType;
    
    /**
     * 车辆品牌
     */
    private String carBrand;
    
    /**
     * 停车场ID
     */
    private Long parkingId;
    
    /**
     * 接入系统停车场ID
     */
    private String outParkingId;
    
    /**
     * 停车场编码
     */
    private String parkingCode;
    
    /**
     * 出入口ID
     */
    private Long entranceId;
    
    /**
     * 接入系统出入口ID
     */
    private String outEntranceId;
    
    /**
     * 出入口编码
     */
    private String entranceCode;
    
    /**
     * 车道ID
     */
    private Long roadwayId;
    
    /**
     * 接入系统车道ID
     */
    private String outRoadwayId;
    
    /**
     * 车道编码
     */
    private String roadwayCode;
    
    /**
     * 是否出场
     */
    private String isOut;
    
    /**
     * 放行方式
     */
    private String outType;
    
    /**
     * 出场时间
     */
    private Long outTime;
    
    private Long createTime;
    
    private Long updateTime;
    
    /**
     * 车道ID
     */
    private Long roadwayIdExit;
    
    /**
     * 接入系统车道ID
     */
    private String outRoadwayIdExit;
    
    /**
     * 车道编码
     */
    private String roadwayCodeExit;

    public Long getRecordId() {
        return recordId;
    }

    public void setRecordId(Long recordId) {
        this.recordId = recordId;
    }

    public Long getSystemId() {
        return systemId;
    }

    public void setSystemId(Long systemId) {
        this.systemId = systemId;
    }

    public String getOutRecordId() {
        return outRecordId;
    }

    public void setOutRecordId(String outRecordId) {
        this.outRecordId = outRecordId == null ? null : outRecordId.trim();
    }

    public String getPlateNo() {
        return plateNo;
    }

    public void setPlateNo(String plateNo) {
        this.plateNo = plateNo == null ? null : plateNo.trim();
    }

    public Long getEnterTime() {
        return enterTime;
    }

    public void setEnterTime(Long enterTime) {
        this.enterTime = enterTime;
    }

    public String getEnterType() {
		return enterType;
	}

	public void setEnterType(String enterType) {
		this.enterType = enterType;
	}

	public String getPlateImgUrl() {
        return plateImgUrl;
    }

    public void setPlateImgUrl(String plateImgUrl) {
        this.plateImgUrl = plateImgUrl == null ? null : plateImgUrl.trim();
    }

    public String getCarImgUrl() {
        return carImgUrl;
    }

    public void setCarImgUrl(String carImgUrl) {
        this.carImgUrl = carImgUrl == null ? null : carImgUrl.trim();
    }

    public String getFaceImgUrl() {
        return faceImgUrl;
    }

    public void setFaceImgUrl(String faceImgUrl) {
        this.faceImgUrl = faceImgUrl == null ? null : faceImgUrl.trim();
    }

    public String getCarColor() {
        return carColor;
    }

    public void setCarColor(String carColor) {
        this.carColor = carColor == null ? null : carColor.trim();
    }

    public String getCarType() {
        return carType;
    }

    public void setCarType(String carType) {
        this.carType = carType == null ? null : carType.trim();
    }

    public String getCarBrand() {
        return carBrand;
    }

    public void setCarBrand(String carBrand) {
        this.carBrand = carBrand == null ? null : carBrand.trim();
    }

    public Long getParkingId() {
        return parkingId;
    }

    public void setParkingId(Long parkingId) {
        this.parkingId = parkingId;
    }

    public String getOutParkingId() {
        return outParkingId;
    }

    public void setOutParkingId(String outParkingId) {
        this.outParkingId = outParkingId == null ? null : outParkingId.trim();
    }

    public String getParkingCode() {
        return parkingCode;
    }

    public void setParkingCode(String parkingCode) {
        this.parkingCode = parkingCode == null ? null : parkingCode.trim();
    }

    public Long getEntranceId() {
        return entranceId;
    }

    public void setEntranceId(Long entranceId) {
        this.entranceId = entranceId;
    }

    public String getOutEntranceId() {
        return outEntranceId;
    }

    public void setOutEntranceId(String outEntranceId) {
        this.outEntranceId = outEntranceId == null ? null : outEntranceId.trim();
    }

    public String getEntranceCode() {
        return entranceCode;
    }

    public void setEntranceCode(String entranceCode) {
        this.entranceCode = entranceCode == null ? null : entranceCode.trim();
    }

    public Long getRoadwayId() {
        return roadwayId;
    }

    public void setRoadwayId(Long roadwayId) {
        this.roadwayId = roadwayId;
    }

    public String getOutRoadwayId() {
        return outRoadwayId;
    }

    public void setOutRoadwayId(String outRoadwayId) {
        this.outRoadwayId = outRoadwayId == null ? null : outRoadwayId.trim();
    }

    public String getRoadwayCode() {
        return roadwayCode;
    }

    public void setRoadwayCode(String roadwayCode) {
        this.roadwayCode = roadwayCode == null ? null : roadwayCode.trim();
    }

    public String getIsOut() {
        return isOut;
    }

    public void setIsOut(String isOut) {
        this.isOut = isOut == null ? null : isOut.trim();
    }

    public String getOutType() {
        return outType;
    }

    public void setOutType(String outType) {
        this.outType = outType == null ? null : outType.trim();
    }

    public Long getOutTime() {
        return outTime;
    }

    public void setOutTime(Long outTime) {
        this.outTime = outTime;
    }

	public Long getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Long createTime) {
		this.createTime = createTime;
	}

	public Long getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Long updateTime) {
		this.updateTime = updateTime;
	}

	public Long getRoadwayIdExit() {
		return roadwayIdExit;
	}

	public void setRoadwayIdExit(Long roadwayIdExit) {
		this.roadwayIdExit = roadwayIdExit;
	}

	public String getOutRoadwayIdExit() {
		return outRoadwayIdExit;
	}

	public void setOutRoadwayIdExit(String outRoadwayIdExit) {
		this.outRoadwayIdExit = outRoadwayIdExit;
	}

	public String getRoadwayCodeExit() {
		return roadwayCodeExit;
	}

	public void setRoadwayCodeExit(String roadwayCodeExit) {
		this.roadwayCodeExit = roadwayCodeExit;
	}

	@Override
	public String toString() {
		return "ParkVehicleRecord [recordId=" + recordId + ", systemId=" + systemId + ", outRecordId=" + outRecordId
				+ ", plateNo=" + plateNo + ", enterTime=" + enterTime + ", enterType=" + enterType + ", plateImgUrl="
				+ plateImgUrl + ", carImgUrl=" + carImgUrl + ", faceImgUrl=" + faceImgUrl + ", carColor=" + carColor
				+ ", carType=" + carType + ", carBrand=" + carBrand + ", parkingId=" + parkingId + ", outParkingId="
				+ outParkingId + ", parkingCode=" + parkingCode + ", entranceId=" + entranceId + ", outEntranceId="
				+ outEntranceId + ", entranceCode=" + entranceCode + ", roadwayId=" + roadwayId + ", outRoadwayId="
				+ outRoadwayId + ", roadwayCode=" + roadwayCode + ", isOut=" + isOut + ", outType=" + outType
				+ ", outTime=" + outTime + ", createTime=" + createTime + ", updateTime=" + updateTime
				+ ", roadwayIdExit=" + roadwayIdExit + ", outRoadwayIdExit=" + outRoadwayIdExit + ", roadwayCodeExit="
				+ roadwayCodeExit + "]";
	}
    
}