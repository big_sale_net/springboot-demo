/**
 * 
 */
package com.yiwupay.park.model.dh;

/**
 * @author admin
 *
 */
public class Money {
	/**
	 * 名称
	 * 优惠名称
	 */
	private String name;
	
	/**
	 * 备注
	 * 备注说明
	 */
	private String remark;

	/**
	 * 金额值
	 * （单位：元）
	 */
	private String value;

	public String getName() {
		return name;
	}

	public String getRemark() {
		return remark;
	}

	public String getValue() {
		return value;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "Money [name=" + name + ", remark=" + remark + ", value=" + value + "]";
	}
	
	
}
