/**
 * 
 */
package com.yiwupay.park.model.dh;

/**
 * @author admin
 *
 */
public class DiscountFree {

	/**
	 * 是否免费
	 * 1：是，2：否
	 */
	private String isFree;

	/**
	 * 备注
	 * 备注说明
	 */
	private String remark;

	public String getIsFree() {
		return isFree;
	}

	public String getRemark() {
		return remark;
	}

	public void setIsFree(String isFree) {
		this.isFree = isFree;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Override
	public String toString() {
		return "DiscountFree [isFree=" + isFree + ", remark=" + remark + "]";
	}
	
	
}
