/**
 * 
 */
package com.yiwupay.park.model.dh;

/**
 * @author wangp
 *
 */
public class CommParam {

	/**
	 * 应用版本号
	 * [20] （收费系统应用软件版本号）
	 */
	private String appVer;

	/**
	 * 是否实时数据
	 * [1] （必填）（1-是，0-否）
	 */
	private String flag;

	/**
	 * 车场账号
	 * [20] （必填）（系统为每个停车场分配的编号） 该编号对应的是，场区配置中，扩展配置下的“扩展编号”
	 */
	private String parkNum;

	/**
	 * PSAM卡号
	 * [12] 可不填
	 */
	private String psam;

	/**
	 * SIM卡号
	 * [20]
	 */
	private String sim;

	/**
	 * 系统版本号
	 * [20] （操作系统版本号）
	 */
	private String sysVer;

	/**
	 * 终端序列号
	 * [16] （数据采集器的硬件编号）
	 */
	private String tsn;

	public String getAppVer() {
		return appVer;
	}

	public String getFlag() {
		return flag;
	}

	public String getParkNum() {
		return parkNum;
	}

	public String getPsam() {
		return psam;
	}

	public String getSim() {
		return sim;
	}

	public String getSysVer() {
		return sysVer;
	}

	public String getTsn() {
		return tsn;
	}

	public void setAppVer(String appVer) {
		this.appVer = appVer;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public void setParkNum(String parkNum) {
		this.parkNum = parkNum;
	}

	public void setPsam(String psam) {
		this.psam = psam;
	}

	public void setSim(String sim) {
		this.sim = sim;
	}

	public void setSysVer(String sysVer) {
		this.sysVer = sysVer;
	}

	public void setTsn(String tsn) {
		this.tsn = tsn;
	}

	@Override
	public String toString() {
		return "CommParam [appVer=" + appVer + ", flag=" + flag + ", parkNum=" + parkNum + ", psam=" + psam + ", sim="
				+ sim + ", sysVer=" + sysVer + ", tsn=" + tsn + "]";
	}
	
	
}
