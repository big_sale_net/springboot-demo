package com.yiwupay.park.model.dh;

import java.util.Map;

import com.yiwupay.park.model.ParkVehicleRecord;
import com.yiwupay.park.service.ParkParkingCacheService;

public class RecevEnterParam {
	private String actTime; // 操作时间
	private String actType; // 操作类型
	private String addBerth; // 附加泊位
	private String arriveTime; // 进场时间
	private String berthId; // 泊位编号
	private String bizSn; // 业务流水号
	private String businessType; // 业务类型
	private String carNum; // 车牌号
	private String carNumColor; // 车辆颜色
	private String carImgUrl; // 车辆图片
	private String carType; // 车辆类型
	private CommParam commParam; // 通用字段
	private String guestRemainNum; // 访客剩余车位
	private String monthlyCertNumber; // 包月证号
	private String monthlyRemainNum; // 月租剩余车位
	private String parkingLot; // 车场编号
	private String sluiceDevChnId; // 设备编码
	private String sluiceDevChnName; // 
	private String platFormCode; // 停车场编号
	private String voucherNo; // 停车凭证号
	private String voucherType; // 停车凭证类型
	private String totRemainNum; // 总剩余车位
	private String uid; // 工号
	

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getTotRemainNum() {
		return totRemainNum;
	}

	public void setTotRemainNum(String totRemainNum) {
		this.totRemainNum = totRemainNum;
	}

	public String getActTime() {
		return actTime;
	}

	public void setActTime(String actTime) {
		this.actTime = actTime;
	}

	public String getActType() {
		return actType;
	}

	public void setActType(String actType) {
		this.actType = actType;
	}

	public String getAddBerth() {
		return addBerth;
	}

	public void setAddBerth(String addBerth) {
		this.addBerth = addBerth;
	}

	public String getArriveTime() {
		return arriveTime;
	}

	public void setArriveTime(String arriveTime) {
		this.arriveTime = arriveTime;
	}

	public String getBerthId() {
		return berthId;
	}

	public void setBerthId(String berthId) {
		this.berthId = berthId;
	}

	public String getBizSn() {
		return bizSn;
	}

	public void setBizSn(String bizSn) {
		this.bizSn = bizSn;
	}

	public String getBusinessType() {
		return businessType;
	}

	public void setBusinessType(String businessType) {
		this.businessType = businessType;
	}

	public String getCarNum() {
		return carNum;
	}

	public void setCarNum(String carNum) {
		this.carNum = carNum;
	}

	public String getCarType() {
		return carType;
	}

	public void setCarType(String carType) {
		this.carType = carType;
	}

	public CommParam getCommParam() {
		return commParam;
	}

	public void setCommParam(CommParam commParam) {
		this.commParam = commParam;
	}

	public String getGuestRemainNum() {
		return guestRemainNum;
	}

	public void setGuestRemainNum(String guestRemainNum) {
		this.guestRemainNum = guestRemainNum;
	}

	public String getMonthlyCertNumber() {
		return monthlyCertNumber;
	}

	public void setMonthlyCertNumber(String monthlyCertNumber) {
		this.monthlyCertNumber = monthlyCertNumber;
	}

	public String getMonthlyRemainNum() {
		return monthlyRemainNum;
	}

	public void setMonthlyRemainNum(String monthlyRemainNum) {
		this.monthlyRemainNum = monthlyRemainNum;
	}

	public String getVoucherNo() {
		return voucherNo;
	}

	public void setVoucherNo(String voucherNo) {
		this.voucherNo = voucherNo;
	}

	public String getVoucherType() {
		return voucherType;
	}

	public void setVoucherType(String voucherType) {
		this.voucherType = voucherType;
	}

	public String getCarNumColor() {
		return carNumColor;
	}

	public String getParkingLot() {
		return parkingLot;
	}

	public String getSluiceDevChnId() {
		return sluiceDevChnId;
	}

	public String getSluiceDevChnName() {
		return sluiceDevChnName;
	}

	public String getPlatFormCode() {
		return platFormCode;
	}

	public void setCarNumColor(String carNumColor) {
		this.carNumColor = carNumColor;
	}

	public void setParkingLot(String parkingLot) {
		this.parkingLot = parkingLot;
	}

	public void setSluiceDevChnId(String sluiceDevChnId) {
		this.sluiceDevChnId = sluiceDevChnId;
	}

	public void setSluiceDevChnName(String sluiceDevChnName) {
		this.sluiceDevChnName = sluiceDevChnName;
	}

	public void setPlatFormCode(String platFormCode) {
		this.platFormCode = platFormCode;
	}
	
	public String getCarImgUrl() {
		return carImgUrl;
	}

	public void setCarImgUrl(String carImgUrl) {
		this.carImgUrl = carImgUrl;
	}

	
	
	@Override
	public String toString() {
		return "RecevEnterParam [actTime=" + actTime + ", actType=" + actType + ", addBerth=" + addBerth
				+ ", arriveTime=" + arriveTime + ", berthId=" + berthId + ", bizSn=" + bizSn + ", businessType="
				+ businessType + ", carNum=" + carNum + ", carNumColor=" + carNumColor + ", carImgUrl=" + carImgUrl
				+ ", carType=" + carType + ", commParam=" + commParam + ", guestRemainNum=" + guestRemainNum
				+ ", monthlyCertNumber=" + monthlyCertNumber + ", monthlyRemainNum=" + monthlyRemainNum
				+ ", parkingLot=" + parkingLot + ", sluiceDevChnId=" + sluiceDevChnId + ", sluiceDevChnName="
				+ sluiceDevChnName + ", platFormCode=" + platFormCode + ", voucherNo=" + voucherNo + ", voucherType="
				+ voucherType + ", totRemainNum=" + totRemainNum + ", uid=" + uid + "]";
	}

	/** 推送出场转化 */
	public static ParkVehicleRecord toParkVehicleRecord(RecevEnterParam data, Long systemId) {
		ParkVehicleRecord pvr = new ParkVehicleRecord();
		pvr.setSystemId(systemId);
		pvr.setPlateNo(data.getCarNum());
		pvr.setEnterTime(Long.parseLong(data.getArriveTime()));
		pvr.setCarImgUrl(data.getCarImgUrl());
		pvr.setCarColor(data.getCarNumColor());
		String carType = data.getCarType();
		if (carType.equals("0")) {
			pvr.setCarType("租长包车辆");
		} else if (carType.equals("1")) {
			pvr.setCarType("时租访客车辆");
		} else if (carType.equals("2")) {
			pvr.setCarType("代表免费车辆");
		} else if (carType.equals("3")) {
			pvr.setCarType("异常未知车辆");
		} else {
			pvr.setCarType("其他车辆");
		}
		Map<String, Long> parkIdMap = ParkParkingCacheService.getParkIdMap();
		String creatKeyOfParkIdMap = ParkParkingCacheService.creatKeyOfParkIdMap(systemId, data.getParkingLot());
		pvr.setParkingId(parkIdMap.get(creatKeyOfParkIdMap));
		pvr.setOutParkingId(data.getParkingLot());
		pvr.setIsOut("0");
		//存储闸道编码
		String devChnId = data.getSluiceDevChnId();
		String[] split = devChnId.split("\\$");
		String outRoadwayId = split[0];
		pvr.setOutRoadwayId(outRoadwayId);
		pvr.setOutEntranceId(outRoadwayId);
		return pvr;
	} 

}
