/**
 * 
 */
package com.yiwupay.park.model.dh;

/**
 * @author wangp
 *
 */
public class Rate {

	/**
	 * 名称
	 * 优惠名称
	 */
	private String name;
	
	/**
	 * 备注
	 * 备注说明
	 */
	private String remark;

	/**
	 * 折扣率
	 * 折扣百分比
	 */
	private String value;

	public String getName() {
		return name;
	}

	public String getRemark() {
		return remark;
	}

	public String getValue() {
		return value;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "Rate [name=" + name + ", remark=" + remark + ", value=" + value + "]";
	}
	
	
}
