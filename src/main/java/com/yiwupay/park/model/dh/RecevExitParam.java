package com.yiwupay.park.model.dh;

public class RecevExitParam {

	private String actTime; // 操作时间
	private String actType; // 操作类型
	private String addBerth; // 附加泊位
	private String arriveTime; // 进场时间
	private String berthId; // 泊位编号
	private String bizSn; // 业务流水号
	private String businessType; // 业务类型
	private String carImgUrl; // 车辆图片
	private String carNum; // 车牌号
	private String carNumColor; // 车辆颜色
	private String carType; // 车辆类型
	private CommParam commParam; // 通用字段
	private String guestRemainNum; // 访客剩余车位
	private String isOfflinePay; // 是否停车场端线下收费（1-是，0-否）
	private String leaveTime; // 离场时间（格式：yyyyMMddHHmmss）
	private String monthlyRemainNum; // 月租剩余车位
	private String orderNo; // 预约订单号
	private String parkingLot; // 停车场编码
	private String parkingTimeLength; // 停车时长（单位：秒）
	private String payMoney; // 收费金额
	private String paymentType; // 支付类型（0代表现金支付， 1代表交通卡支付， 2代表银行卡支付， 3代表手机支付）
	private String platFormCode; // 平台编码
	private String sluiceDevChnId; // 设备编码
	private String sluiceDevChnName; //设备名称
	private String totRemainNum; // 总剩余车位
	private String voucherNo; // 停车凭证号
	private String voucherType; // 停车凭证类型
	private Object discountFree; // 免费优惠
	private Object discountMoney; // 金额优惠集合
	private Object discountRate; // 折扣优惠集合
	private Object discountTime; // 时间优惠集合
	private String RecevExitParam;// 
	private String monthlyCertNumber;// 包月证号 
	private String uid;// 工号
	private String isPayByLineAmount;// 是否按线上返回的金额收费
	
	
	public String getIsPayByLineAmount() {
		return isPayByLineAmount;
	}

	public void setIsPayByLineAmount(String isPayByLineAmount) {
		this.isPayByLineAmount = isPayByLineAmount;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getMonthlyCertNumber() {
		return monthlyCertNumber;
	}

	public void setMonthlyCertNumber(String monthlyCertNumber) {
		this.monthlyCertNumber = monthlyCertNumber;
	}

	public String getRecevExitParam() {
		return RecevExitParam;
	}

	public void setRecevExitParam(String recevExitParam) {
		RecevExitParam = recevExitParam;
	}

	public Object getDiscountMoney() {
		return discountMoney;
	}

	public Object getDiscountRate() {
		return discountRate;
	}

	public Object getDiscountTime() {
		return discountTime;
	}

	public void setDiscountMoney(Object discountMoney) {
		this.discountMoney = discountMoney;
	}

	public void setDiscountRate(Object discountRate) {
		this.discountRate = discountRate;
	}

	public void setDiscountTime(Object discountTime) {
		this.discountTime = discountTime;
	}

	public Object getDiscountFree() {
		return discountFree;
	}

	public void setDiscountFree(Object discountFree) {
		this.discountFree = discountFree;
	}

	public String getActTime() {
		return actTime;
	}

	public void setActTime(String actTime) {
		this.actTime = actTime;
	}

	public String getActType() {
		return actType;
	}

	public void setActType(String actType) {
		this.actType = actType;
	}

	public String getAddBerth() {
		return addBerth;
	}

	public void setAddBerth(String addBerth) {
		this.addBerth = addBerth;
	}

	public String getArriveTime() {
		return arriveTime;
	}

	public void setArriveTime(String arriveTime) {
		this.arriveTime = arriveTime;
	}

	public String getBerthId() {
		return berthId;
	}

	public void setBerthId(String berthId) {
		this.berthId = berthId;
	}

	public String getBizSn() {
		return bizSn;
	}

	public void setBizSn(String bizSn) {
		this.bizSn = bizSn;
	}

	public String getBusinessType() {
		return businessType;
	}

	public void setBusinessType(String businessType) {
		this.businessType = businessType;
	}

	public String getCarNum() {
		return carNum;
	}

	public void setCarNum(String carNum) {
		this.carNum = carNum;
	}

	public String getCarType() {
		return carType;
	}

	public void setCarType(String carType) {
		this.carType = carType;
	}

	public CommParam getCommParam() {
		return commParam;
	}

	public void setCommParam(CommParam commParam) {
		this.commParam = commParam;
	}

	public String getGuestRemainNum() {
		return guestRemainNum;
	}

	public void setGuestRemainNum(String guestRemainNum) {
		this.guestRemainNum = guestRemainNum;
	}

	public String getMonthlyRemainNum() {
		return monthlyRemainNum;
	}

	public void setMonthlyRemainNum(String monthlyRemainNum) {
		this.monthlyRemainNum = monthlyRemainNum;
	}

	public String getVoucherNo() {
		return voucherNo;
	}

	public void setVoucherNo(String voucherNo) {
		this.voucherNo = voucherNo;
	}

	public String getVoucherType() {
		return voucherType;
	}

	public void setVoucherType(String voucherType) {
		this.voucherType = voucherType;
	}

	public String getIsOfflinePay() {
		return isOfflinePay;
	}

	public void setIsOfflinePay(String isOfflinePay) {
		this.isOfflinePay = isOfflinePay;
	}

	public String getLeaveTime() {
		return leaveTime;
	}

	public void setLeaveTime(String leaveTime) {
		this.leaveTime = leaveTime;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getParkingTimeLength() {
		return parkingTimeLength;
	}

	public void setParkingTimeLength(String parkingTimeLength) {
		this.parkingTimeLength = parkingTimeLength;
	}

	public String getPayMoney() {
		return payMoney;
	}

	public void setPayMoney(String payMoney) {
		this.payMoney = payMoney;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getCarImgUrl() {
		return carImgUrl;
	}

	public String getCarNumColor() {
		return carNumColor;
	}

	public String getParkingLot() {
		return parkingLot;
	}

	public String getPlatFormCode() {
		return platFormCode;
	}

	public String getSluiceDevChnId() {
		return sluiceDevChnId;
	}

	public String getSluiceDevChnName() {
		return sluiceDevChnName;
	}

	public String getTotRemainNum() {
		return totRemainNum;
	}

	public void setCarImgUrl(String carImgUrl) {
		this.carImgUrl = carImgUrl;
	}

	public void setCarNumColor(String carNumColor) {
		this.carNumColor = carNumColor;
	}

	public void setParkingLot(String parkingLot) {
		this.parkingLot = parkingLot;
	}

	public void setPlatFormCode(String platFormCode) {
		this.platFormCode = platFormCode;
	}

	public void setSluiceDevChnId(String sluiceDevChnId) {
		this.sluiceDevChnId = sluiceDevChnId;
	}

	public void setSluiceDevChnName(String sluiceDevChnName) {
		this.sluiceDevChnName = sluiceDevChnName;
	}

	public void setTotRemainNum(String totRemainNum) {
		this.totRemainNum = totRemainNum;
	}

	@Override
	public String toString() {
		return "RecevExitParam [actTime=" + actTime + ", actType=" + actType + ", addBerth=" + addBerth
				+ ", arriveTime=" + arriveTime + ", berthId=" + berthId + ", bizSn=" + bizSn + ", businessType="
				+ businessType + ", carImgUrl=" + carImgUrl + ", carNum=" + carNum + ", carNumColor=" + carNumColor
				+ ", carType=" + carType + ", commParam=" + commParam + ", guestRemainNum=" + guestRemainNum
				+ ", isOfflinePay=" + isOfflinePay + ", leaveTime=" + leaveTime + ", monthlyRemainNum="
				+ monthlyRemainNum + ", orderNo=" + orderNo + ", parkingLot=" + parkingLot + ", parkingTimeLength="
				+ parkingTimeLength + ", payMoney=" + payMoney + ", paymentType=" + paymentType + ", platFormCode="
				+ platFormCode + ", sluiceDevChnId=" + sluiceDevChnId + ", sluiceDevChnName=" + sluiceDevChnName
				+ ", totRemainNum=" + totRemainNum + ", voucherNo=" + voucherNo + ", voucherType=" + voucherType
				+ ", discountFree=" + discountFree + ", discountMoney=" + discountMoney + ", discountRate="
				+ discountRate + ", discountTime=" + discountTime + ", RecevExitParam=" + RecevExitParam
				+ ", monthlyCertNumber=" + monthlyCertNumber + ", uid=" + uid + ", isPayByLineAmount="
				+ isPayByLineAmount + "]";
	}


}
