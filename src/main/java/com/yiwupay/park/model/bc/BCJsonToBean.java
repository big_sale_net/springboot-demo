package com.yiwupay.park.model.bc;

import org.apache.commons.lang.StringUtils;

import com.yiwupay.park.enums.BCFieldEnum;
import com.yiwupay.park.enums.OrderTypeEnum;
import com.yiwupay.park.enums.VehicleStatusEnum;
import com.yiwupay.park.model.ParkParking;
import com.yiwupay.park.model.ParkVehicleRecord;
import com.yiwupay.park.model.vo.ChargeBillVO;
import com.yiwupay.park.model.vo.ParkParkingVO;
import com.yiwupay.park.service.ParkParkingCacheService;
import com.yiwupay.park.utils.MapCacheUtil;

import net.sf.json.JSONObject;

public class BCJsonToBean {
	/**
	 * SYSTEMId=6
	 */
	public final static Long SYSTEMId = 6L;
	
	/**
	 * json转化订单展示类
	 * @param json
	 * @return
	 */
	public static ChargeBillVO toChargeBillVO(JSONObject backJson) {
		ChargeBillVO bill = new ChargeBillVO();
		bill.setOutPreBillId(backJson.optString(BCFieldEnum.recordId.name()));
		bill.setOutRecordId(backJson.optString(BCFieldEnum.recordId.name()));
		bill.setPlateNo(backJson.optString(BCFieldEnum.plateId.name()));
		bill.setCost("0");
		bill.setTotalCost(backJson.optString(BCFieldEnum.charge.name(), "0"));
		bill.setRealCost(backJson.optString(BCFieldEnum.payCharge.name(), "0"));
		String inTime = backJson.optString(BCFieldEnum.inTime.name()).replaceAll("[^\\d]+", "").trim();
		bill.setEnterTime(inTime);
		bill.setDelayTime(backJson.optInt(BCFieldEnum.paidfreeTime.name()));
		bill.setParkingTime(backJson.optInt(BCFieldEnum.stopTimeTotal.name()));
		Long cost = backJson.optLong(BCFieldEnum.paidTotal.name());
		if(cost.longValue()== 0) {
			bill.setType(OrderTypeEnum.NORMAL_ORDER.name());
		}else {
			bill.setType(OrderTypeEnum.OVERTIME_ORDER.name());
		}
		return bill;
	}
	
	/**
	 * json转化停车信息展示类
	 * @param json
	 * @return
	 */
	public static ParkParkingVO toParkParkingVO(JSONObject json) {
		ParkParkingVO p = new ParkParkingVO();
		p.setTotalPlace(json.optInt(""));
		p.setLeftPlace(json.optInt(""));
		return p;
	}
	
	/**
	 * json 转化入场过车记录实体类
	 * @param json
	 * @param systemId
	 */
	public static ParkVehicleRecord toEnterRecord(JSONObject json) {
		ParkVehicleRecord r = new ParkVehicleRecord();
		r.setSystemId(SYSTEMId);
		r.setPlateNo(json.optString(BCFieldEnum.plateId.name()));
		r.setIsOut(VehicleStatusEnum.ENTER.getStatus());
		
		String inTime = json.optString(BCFieldEnum.inTime.name()).replaceAll("[^\\d]+", "").trim();
		if(StringUtils.isNotBlank(inTime)) {
			r.setEnterTime(Long.parseLong(inTime));
		}

		r.setOutRecordId(json.optString(BCFieldEnum.recordId.name()));
		r.setCarImgUrl(json.optString(BCFieldEnum.inImage.name()));
		String inChannel = json.optString(BCFieldEnum.inChannel.name()).trim();
		r.setOutEntranceId(MapCacheUtil.BC_ROADWAY_ID.get(inChannel));
		r.setOutRoadwayId(MapCacheUtil.BC_ROADWAY_ID.get(inChannel));
		String outParkId = MapCacheUtil.BC_PARKID_ID.get(inChannel);
		r.setOutParkingId(outParkId);
		String creatKeyOfParkIdMap = ParkParkingCacheService.creatKeyOfParkIdMap(SYSTEMId, outParkId);
		r.setParkingId(ParkParkingCacheService.getParkIdMap().get(creatKeyOfParkIdMap));
		return r;
		
	}

	/**
	 * json 转化出场过车记录实体类
	 * @param json
	 * @param systemId
	 * @return
	 */
	public static ParkVehicleRecord toExitRecord(JSONObject json) {
		ParkVehicleRecord r = new ParkVehicleRecord();
		r.setPlateNo(json.optString(BCFieldEnum.plateId.name()));
		String inTime = json.optString(BCFieldEnum.inTime.name()).replaceAll("[^\\d]+", "").trim();
		if(StringUtils.isNotBlank(inTime)) {
			r.setEnterTime(Long.parseLong(inTime));
		}
		r.setOutRecordId(json.optString(BCFieldEnum.recordId.name()));
		r.setIsOut(VehicleStatusEnum.OUT.getStatus());
		r.setSystemId(SYSTEMId);
		String outTime = json.optString(BCFieldEnum.outTime.name()).replaceAll("[^\\d]+", "").trim();
		if(StringUtils.isNotBlank(outTime)) {
			r.setOutTime(Long.parseLong(outTime));
		}
		String outChannel = json.optString(BCFieldEnum.outChannel.name()).trim();
		r.setOutRoadwayIdExit(MapCacheUtil.BC_ROADWAY_ID.get(outChannel));
		return r;
	}
	
	/**
	 * json转化停车信息实体类
	 * @param json
	 * @param systemId
	 * @return
	 */
	public static ParkParking toParkParking(JSONObject json) {
		ParkParking parkParking = new ParkParking();
		parkParking.setParkingId(json.optLong(BCFieldEnum.parkId.name()));
		parkParking.setSystemId(SYSTEMId);
		parkParking.setLeftPlace(json.optInt(BCFieldEnum.tmpSpaceCount.name()));
		parkParking.setTotalPlace(json.optInt(BCFieldEnum.spaceCount.name()));
		parkParking.setOutParkingId(json.optString(BCFieldEnum.lanKaParkNo.name()));
		parkParking.setRemark("0");
		return parkParking;
	}
	
}
