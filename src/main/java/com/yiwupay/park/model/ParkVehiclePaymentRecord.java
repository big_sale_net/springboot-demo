package com.yiwupay.park.model;

/**
 * 过车缴费记录(对应PS_PARK_VEHICLE_PAYMENT_RECORD表)
 * @author zls
 *
 */
public class ParkVehiclePaymentRecord {
	/**
	 * 订单id
	 */
    private Long paymentId;
    
    /**
     * 接入停车系统ID
     */
    private Long systemId;
    
    /**
     * 接入系统订单id
     */
    private String outPaymentId;
    
    /**
     * 过车记录id
     */
    private Long recordId;
    
    /**
     * 接入系统过车记录id
     */
    private String outRecordId;
    
    /**
     * 停车时长
     */
    private Integer parkTime;
    
    /**
     * 应付金额
     */
    private Long realCost;
    
    /**
     * 总收费金额
     */
    private Long totalCost;
    
    /**
     * 已支付金额
     */
    private Long cost;
    
    /**
     * 缴费后允许出场延长时间
     */
    private Integer delayTime;
    
    /**
     * 中央收费，自助缴费，支付宝支付，微信支付，手持设备缴 费，第三方缴费
     */
    private String payType;
    
    /**
     * 账单缴费类型
     */
    private String orderType;
    
    /**
     * 订单状态
     */
    private String orderStatus;
    
    /**
     * 订单创建时间
     */
    private Long createTime;
    
    /**
     * 订单更新时间
     */
    private Long updateTime;
    
    /**
     * 订单付款时间
     */
    private Long payTime;

    /**
     * 停车流水号
     */
    private String parkingSerial;
    
    public Long getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(Long paymentId) {
        this.paymentId = paymentId;
    }

    public Long getSystemId() {
        return systemId;
    }

    public void setSystemId(Long systemId) {
        this.systemId = systemId;
    }

    public String getOutPaymentId() {
        return outPaymentId;
    }

    public void setOutPaymentId(String outPaymentId) {
        this.outPaymentId = outPaymentId == null ? null : outPaymentId.trim();
    }

    public Long getRecordId() {
        return recordId;
    }

    public void setRecordId(Long recordId) {
        this.recordId = recordId;
    }

    public String getOutRecordId() {
        return outRecordId;
    }

    public void setOutRecordId(String outRecordId) {
        this.outRecordId = outRecordId == null ? null : outRecordId.trim();
    }

    public Integer getParkTime() {
        return parkTime;
    }

    public void setParkTime(Integer parkTime) {
        this.parkTime = parkTime;
    }

    public Long getRealCost() {
        return realCost;
    }

    public void setRealCost(Long realCost) {
        this.realCost = realCost;
    }

    public Long getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(Long totalCost) {
        this.totalCost = totalCost;
    }

    public Long getCost() {
        return cost;
    }

    public void setCost(Long cost) {
        this.cost = cost;
    }

    public Integer getDelayTime() {
        return delayTime;
    }

    public void setDelayTime(Integer delayTime) {
        this.delayTime = delayTime;
    }

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType == null ? null : payType.trim();
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType == null ? null : orderType.trim();
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus == null ? null : orderStatus.trim();
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Long updateTime) {
        this.updateTime = updateTime;
    }

    public Long getPayTime() {
        return payTime;
    }

    public void setPayTime(Long payTime) {
        this.payTime = payTime;
    }

	public String getParkingSerial() {
		return parkingSerial;
	}

	public void setParkingSerial(String parkingSerial) {
		this.parkingSerial = parkingSerial;
	}

	@Override
	public String toString() {
		return "ParkVehiclePaymentRecord [paymentId=" + paymentId + ", systemId=" + systemId + ", outPaymentId="
				+ outPaymentId + ", recordId=" + recordId + ", outRecordId=" + outRecordId + ", parkTime=" + parkTime
				+ ", realCost=" + realCost + ", totalCost=" + totalCost + ", cost=" + cost + ", delayTime=" + delayTime
				+ ", payType=" + payType + ", orderType=" + orderType + ", orderStatus=" + orderStatus + ", createTime="
				+ createTime + ", updateTime=" + updateTime + ", payTime=" + payTime + ", parkingSerial="
				+ parkingSerial + "]";
	}
}