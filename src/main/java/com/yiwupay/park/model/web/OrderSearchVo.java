package com.yiwupay.park.model.web;

/**
 * 订单查询返回实体对象
 * Title: OrderSearchVo
 * Description: 
 * Company:  
 * @author cailuxi
 * @date 2018年1月18日
 */
public class OrderSearchVo {

	/**
	 * 订单号
	 */
	private Long orderId;
	
	/**
	 * 车牌号
	 */
	private String plateNo;
	
	/**
	 * 所属市场
	 */
	private String marketName;
	
	/**
	 * 停车场
	 */
	private String parkingName;
	
	/**
	 * 停车时长
	 */
	private Long parkTime;
	
	/**
	 * 应付金额
	 */
	private Long totalCost;
	
	/**
	 * 实付金额
	 */
	private Long totalFee;
	
	/**
	 * 支付时间
	 */
	private Long payTime;
	
	/**
	 * 缴费单号
	 */
	private Long outTradeNo;
	
	/**
	 * 离场状态
	 */
	private String isOut;
	
	/**
	 * 入场时间
	 */
	private Long enterTime;
	
	/**
	 * 离场时间
	 */
	private Long outTime;

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public String getPlateNo() {
		return plateNo;
	}

	public void setPlateNo(String plateNo) {
		this.plateNo = plateNo;
	}

	public String getMarketName() {
		return marketName;
	}

	public void setMarketName(String marketName) {
		this.marketName = marketName;
	}

	public String getParkingName() {
		return parkingName;
	}

	public void setParkingName(String parkingName) {
		this.parkingName = parkingName;
	}

	public Long getParkTime() {
		return parkTime;
	}

	public void setParkTime(Long parkTime) {
		this.parkTime = parkTime;
	}

	public Long getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(Long totalCost) {
		this.totalCost = totalCost;
	}

	public Long getTotalFee() {
		return totalFee;
	}

	public void setTotalFee(Long totalFee) {
		this.totalFee = totalFee;
	}

	public Long getPayTime() {
		return payTime;
	}

	public void setPayTime(Long payTime) {
		this.payTime = payTime;
	}

	public Long getOutTradeNo() {
		return outTradeNo;
	}

	public void setOutTradeNo(Long outTradeNo) {
		this.outTradeNo = outTradeNo;
	}

	public String getIsOut() {
		return isOut;
	}

	public void setIsOut(String isOut) {
		this.isOut = isOut;
	}

	public Long getEnterTime() {
		return enterTime;
	}

	public void setEnterTime(Long enterTime) {
		this.enterTime = enterTime;
	}

	public Long getOutTime() {
		return outTime;
	}

	public void setOutTime(Long outTime) {
		this.outTime = outTime;
	}

	
}
