package com.yiwupay.park.model.web;

public class CaiwuStatisWeb {
	private Long parkingId;
	/**
     * 市场id
     */
    private String marketId;
    /**
     * 停车场名称
     */
    private String parkingName;
    
    /**
     * 市场id
     */
    private String marketName;
    
    //岗亭支付笔数
    private Integer countPaySentry;
    //微信支付笔数
    private Integer countPayWx;
    //岗亭支付金额
    private Long sumPaySentry;
    //微信支付金额
    private Long sumPayWx;
    //车辆进出数
    private Integer countVehicle;
    //总支付笔数
    private Integer countPay;
    
    
    
	public Long getParkingId() {
		return parkingId;
	}
	public void setParkingId(Long parkingId) {
		this.parkingId = parkingId;
	}
	public String getMarketId() {
		return marketId;
	}
	public void setMarketId(String marketId) {
		this.marketId = marketId;
	}
	public String getParkingName() {
		return parkingName;
	}
	public void setParkingName(String parkingName) {
		this.parkingName = parkingName;
	}
	public String getMarketName() {
		return marketName;
	}
	public void setMarketName(String marketName) {
		this.marketName = marketName;
	}
	public Integer getCountPaySentry() {
		return countPaySentry;
	}
	public void setCountPaySentry(Integer countPaySentry) {
		this.countPaySentry = countPaySentry;
	}
	public Integer getCountPayWx() {
		return countPayWx;
	}
	public void setCountPayWx(Integer countPayWx) {
		this.countPayWx = countPayWx;
	}
	public Long getSumPaySentry() {
		return sumPaySentry;
	}
	public void setSumPaySentry(Long sumPaySentry) {
		this.sumPaySentry = sumPaySentry;
	}
	public Long getSumPayWx() {
		return sumPayWx;
	}
	public void setSumPayWx(Long sumPayWx) {
		this.sumPayWx = sumPayWx;
	}
	public Integer getCountVehicle() {
		return countVehicle;
	}
	public void setCountVehicle(Integer countVehicle) {
		this.countVehicle = countVehicle;
	}
	public Integer getCountPay() {
		return countPay;
	}
	public void setCountPay(Integer countPay) {
		this.countPay = countPay;
	}
	
    
}
