package com.yiwupay.park.model.web;

/**
 * 流动车辆查询实体对象
 * Title: VehicleSearchVo
 * Description: 
 * Company:  
 * @author cailuxi
 * @date 2018年1月18日
 */
public class VehicleSearchVo {

	private Long recordStId;
	
	/**
	 * 市场ID
	 */
	private Long marketId;
	
	/**
	 * 停车场ID
	 */
	private String parkingId;
	
	/**
	 * 所属市场
	 */
	private String marketName;
	
	/**
	 * 停车场
	 */
	private String parkingName;
	
	/**
	 * 车牌号
	 */
	private String plateNo;
	
	/**
	 * 入场时间
	 */
	private Long enterTime;
	
	/**
	 * 出场时间
	 */
	private Long exitTime;
	
	/**
	 * 应付金额
	 */
	private Long cost;
	
	/**
	 * 收费类型
	 */
	private String payType;
	
	/**
	 * 数据库获取时间
	 */
	private Long dataGetTime;
	
	/**
	 * 离场状态
	 */
	private String isOut;

	public Long getRecordStId() {
		return recordStId;
	}

	public Long getMarketId() {
		return marketId;
	}

	public String getParkingId() {
		return parkingId;
	}

	public String getMarketName() {
		return marketName;
	}

	public String getParkingName() {
		return parkingName;
	}

	public String getPlateNo() {
		return plateNo;
	}

	public Long getEnterTime() {
		return enterTime;
	}

	public Long getExitTime() {
		return exitTime;
	}

	public Long getCost() {
		return cost;
	}

	public String getPayType() {
		return payType;
	}

	public Long getDataGetTime() {
		return dataGetTime;
	}

	public String getIsOut() {
		return isOut;
	}

	public void setRecordStId(Long recordStId) {
		this.recordStId = recordStId;
	}

	public void setMarketId(Long marketId) {
		this.marketId = marketId;
	}

	public void setParkingId(String parkingId) {
		this.parkingId = parkingId;
	}

	public void setMarketName(String marketName) {
		this.marketName = marketName;
	}

	public void setParkingName(String parkingName) {
		this.parkingName = parkingName;
	}

	public void setPlateNo(String plateNo) {
		this.plateNo = plateNo;
	}

	public void setEnterTime(Long enterTime) {
		this.enterTime = enterTime;
	}

	public void setExitTime(Long exitTime) {
		this.exitTime = exitTime;
	}

	public void setCost(Long cost) {
		this.cost = cost;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	public void setDataGetTime(Long dataGetTime) {
		this.dataGetTime = dataGetTime;
	}

	public void setIsOut(String isOut) {
		this.isOut = isOut;
	}

}
