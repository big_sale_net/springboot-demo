package com.yiwupay.park.model.web;
/**
 * 车辆统计
 * @author zls
 *
 */
public class VehicleStaticSearchVo {
	/**
	 * 车牌号
	 */
     private String plateNo;
     
     /**
      * 车辆类型
      */
     private String carType;
     
     /**
      * 车辆颜色
      */
     private String carColor;
     
     /**
      * 最好进入停车场
      */
     private String lastParkName;
     
     /**
      * 最后进入停车场时间
      */
     private Long lastEnterTime;
     
     /**
      * 累计进入次数
      */
     private Integer staticCount;

	public String getPlateNo() {
		return plateNo;
	}

	public void setPlateNo(String plateNo) {
		this.plateNo = plateNo;
	}

	public String getCarType() {
		return carType;
	}

	public void setCarType(String carType) {
		this.carType = carType;
	}

	public String getCarColor() {
		return carColor;
	}

	public void setCarColor(String carColor) {
		this.carColor = carColor;
	}

	public String getLastParkName() {
		return lastParkName;
	}

	public void setLastParkName(String lastParkName) {
		this.lastParkName = lastParkName;
	}

	public Long getLastEnterTime() {
		return lastEnterTime;
	}

	public void setLastEnterTime(Long lastEnterTime) {
		this.lastEnterTime = lastEnterTime;
	}

	public Integer getStaticCount() {
		return staticCount;
	}

	public void setStaticCount(Integer staticCount) {
		this.staticCount = staticCount;
	}

	@Override
	public String toString() {
		return "VehicleStaticSearchVo [plateNo=" + plateNo + ", carType=" + carType + ", carColor=" + carColor
				+ ", lastParkName=" + lastParkName + ", lastEnterTime=" + lastEnterTime + ", staticCount=" + staticCount
				+ "]";
	}
     
}
