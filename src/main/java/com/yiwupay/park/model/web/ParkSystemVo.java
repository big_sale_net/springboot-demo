package com.yiwupay.park.model.web;


import java.util.List;

/**
 * 停车场列表查询VO
 * @author wangpeng
 */

public class ParkSystemVo {
	//所属市场ID
	private String marketId;
	//所属市场
	private String marketName;
	//停车场ID
	private Long parkingId;
	//停车场名称
	private String parkingName;
	//停车场设备名称
	private String systemName;
	//车道名称
	private List<String> roadWayName;
	//车道数量
	private Integer drivewayCount;
	//总车位
	private String totalPlace;
	//剩余车位
	private String leftPlace;
	//数据更新
	private String updateTime;
	//标记接口是否畅通 0畅通 1异常
	private String remark;
	public String getMarketId() {
		return marketId;
	}
	public String getMarketName() {
		return marketName;
	}
	public Long getParkingId() {
		return parkingId;
	}
	public String getParkingName() {
		return parkingName;
	}
	public String getSystemName() {
		return systemName;
	}
	public List<String> getRoadWayName() {
		return roadWayName;
	}
	public Integer getDrivewayCount() {
		return drivewayCount;
	}
	public String getTotalPlace() {
		return totalPlace;
	}
	public String getLeftPlace() {
		return leftPlace;
	}
	public String getUpdateTime() {
		return updateTime;
	}
	public String getRemark() {
		return remark;
	}
	public void setMarketId(String marketId) {
		this.marketId = marketId;
	}
	public void setMarketName(String marketName) {
		this.marketName = marketName;
	}
	public void setParkingId(Long parkingId) {
		this.parkingId = parkingId;
	}
	public void setParkingName(String parkingName) {
		this.parkingName = parkingName;
	}
	public void setSystemName(String systemName) {
		this.systemName = systemName;
	}
	public void setRoadWayName(List<String> roadWayName) {
		this.roadWayName = roadWayName;
	}
	public void setDrivewayCount(Integer drivewayCount) {
		this.drivewayCount = drivewayCount;
	}
	public void setTotalPlace(String totalPlace) {
		this.totalPlace = totalPlace;
	}
	public void setLeftPlace(String leftPlace) {
		this.leftPlace = leftPlace;
	}
	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	@Override
	public String toString() {
		return "ParkSystemVo [marketId=" + marketId + ", marketName=" + marketName + ", parkingId=" + parkingId
				+ ", parkingName=" + parkingName + ", systemName=" + systemName + ", roadWayName=" + roadWayName
				+ ", drivewayCount=" + drivewayCount + ", totalPlace=" + totalPlace + ", leftPlace=" + leftPlace
				+ ", updateTime=" + updateTime + ", remark=" + remark + "]";
	}
	
}
