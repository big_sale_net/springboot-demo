package com.yiwupay.park.model.web;

/**
 * 应付金额和支付类型
 * Title: VehicleCostVo
 * Description: 
 * Company:  
 * @author cailuxi
 * @date 2018年1月25日
 */
public class VehicleCostVo {

	/**
	 * 应付金额
	 */
	private Long cost;
	
	/**
	 * 支付类型
	 */
	private String payType;

	public Long getCost() {
		return cost;
	}

	public void setCost(Long cost) {
		this.cost = cost;
	}

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}
	
}
