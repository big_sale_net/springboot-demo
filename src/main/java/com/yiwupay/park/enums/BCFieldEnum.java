package com.yiwupay.park.enums;

public enum BCFieldEnum {
	/** 响应状态码 */
	status,
	/**  订单(过车)id*/
	recordId, 
	/**  车牌号*/
	plateId,
	/** 信息公司停车场id */
	parkId, 
	/** 进场时间 */
	inTime, 
	/** 停车时长 */
	stopTimeTotal, 
	/** 本次应收金额 */
	payCharge, 
	/** 总金额 */
	charge, 
	/** 实收金额 */
	realCharge,
	/** 已支付金额 */
	paidTotal, 
	/** 支付时间 */
	payTime,
	/** 支付方式*/
	payType,
	/** 支付后免费停车时长 */
	paidfreeTime, 
	/** 本次算费时间 */
	getTimes, 
	/** 总优惠金额 */
	profitChargeTotal, 
	/** 本次优惠停车时长 */
	profitTime,
	/** 本次优惠金额 */
	profitCharge,
	/** 免费停车总时长 */
	profitTimeTotal, 
	/** 备注 */
	memo, 
	/** 请求时发送的,返回时带回*/
	timeStamp, 
	/** 线上分配交易流水号*/
	orderNo, 
	/** [JsonArray（int）] 优惠代号*/
	profitCode, 
	/** 出场时间 */
	outTime,
	/** 车牌颜色 */
	plateColor,
	/** 蓝卡停车场编号 */
	lanKaParkNo,
	/** 入场图片名称 */
	inImage,
	/** 入口通道名称*/
	inChannel,
	/** 出口通道名称*/
	outChannel,
	/** 实时剩余车位 */
	parkSpaceNum,
	/** 签名 */
	signature,
	/** 临时车位数 */
	tmpSpaceCount,
	/** 总车位数*/
	spaceCount,
	
}
