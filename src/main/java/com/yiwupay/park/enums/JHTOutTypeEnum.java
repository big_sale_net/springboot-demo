package com.yiwupay.park.enums;

/**
 * 捷惠通车辆出场方式
 * @author zls
 *
 */
public enum JHTOutTypeEnum {
	NORMAL("正常收费"),
	FREETIME("免费时间内"),
	FREE("免费"),
	HIGHFEE("最高收费记录"),
	DISCNTFREE("优惠后免费"),
	MANPUT("人工放行"),
	HANDWORK("手动开闸"),
	UNTEMPFREE("非临时车辆"),
	FIXCAR("固定车辆"),
	OTHER("其他");
	
	private String text;
	
	public static JHTOutTypeEnum getEnum(String status) {
		for (JHTOutTypeEnum rse : JHTOutTypeEnum.values()) {
			if (rse.name().equals(status)) {
				return rse;
			}
		}
		return null;
	}
	private JHTOutTypeEnum(String text) {
		this.text = text;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	
	
}
