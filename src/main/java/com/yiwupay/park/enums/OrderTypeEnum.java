package com.yiwupay.park.enums;

public enum OrderTypeEnum {
	
	NORMAL_ORDER("正常收费"),
	OVERTIME_ORDER("超时收费");
	
	private String text;
	
	private OrderTypeEnum(String text) {
		this.text = text;
	}
	
	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	
}
