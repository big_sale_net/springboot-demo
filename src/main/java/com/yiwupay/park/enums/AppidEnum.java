package com.yiwupay.park.enums;

public enum AppidEnum {
	
	YCB_WX(null, "义采宝公众号"),
	ONCCC_MINIP("wx3bc30ce2b0df1e7a", "信息公司小程序");
	
	private String appid;
	private String remark;
	
	private AppidEnum(String appid, String remark) {
		this.appid = appid;
		this.remark = remark;
	}

	public String getAppid() {
		return appid;
	}

	public void setAppid(String appid) {
		this.appid = appid;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

}
