package com.yiwupay.park.enums;

public enum OrderStatusEnum {
	pay_yes("已支付"),
	pay_not("未支付");
	
	private String text;

	private OrderStatusEnum(String text) {
		this.text = text;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
	
}
