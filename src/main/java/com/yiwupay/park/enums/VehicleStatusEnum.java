package com.yiwupay.park.enums;

public enum VehicleStatusEnum {
	OUT("1", "出场"),
	ENTER("0", "入场");
	private String status;
	private String statusName;
	private VehicleStatusEnum(String status, String statusName) {
		this.status = status;
		this.statusName = statusName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getStatusName() {
		return statusName;
	}
	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}
	
	
}
