package com.yiwupay.park.enums;
import java.util.EnumMap;
import java.util.Iterator;
import java.util.Map.Entry;
/**
 * 红门系统返回code
 * @author zls
 *
 */
public enum HMErrorCodeEnum {
	SUCCESS(0, "成功"),
	ARGS_MISSING(1001, "缺少参数"),ARGS_ERROR(1002, "参数错误"),AUTH_ERROR(1003, "认证失败"),SIGN_ERROR(1004, "签名错误"),
	TYPE_UNAUTHORIZED(1005, "type未授权"),TYPE_NOT_FOUND(1006, "不存在的请求类型"),JSON_PARSE_ERROR(1007, "json格式错误"),INTERNAL_ERROR(1008, "内部错误"),
	CALL_DENIED(1009, "调用被拒绝"),APP_DISABLED(1010, "APP被禁用"),LOCK_FAILED(1011, "获取锁失败"),EXCEPTION(1012, "运行异常"),RECORD_NOT_FOUND(1013, "记录未找到"),
	NON_TEMP_VEHICLE(1014, "非临时车"),PARKING_INTERNAL_ERROR(1015, "停车场内部处理错误"),ORDER_EXCEPTION(1016, "订单异常"),ORDER_REPEATED(1017, "订单重复"),
	ORDER_NOT_FOUND(1018, "未找到订单"),VEHICLE_HAS_EXITED(1019, "车辆已出场"),PARKING_OFFLINE(1020, "停车场不在线"),TIMEOUT(1021, "超时");
	
	 private int value;
	 private String msg;
	 
	 public static Integer getEnumValue(String code) {
			for (HMErrorCodeEnum err : HMErrorCodeEnum.values()) {
				if (err.name().equals(code)) {
					return err.getValue();
				}
			}
			return 9999;//未知异常
		}
	 
	 public static String getEnumMsg(String code) {
			for (HMErrorCodeEnum err : HMErrorCodeEnum.values()) {
				if (err.name().equals(code)) {
					return err.getMsg();
				}
			}
			return "未知异常";//未知异常
		}
	 
	 private HMErrorCodeEnum(int value, String msg){
		 this.value = value;
		 this.msg = msg;
	 }
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	
	
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	

}
