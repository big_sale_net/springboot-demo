package com.yiwupay.park.enums;

public enum JHTResultCodeEnum {
	ERROR_13(301,"IP未绑定"),
	
	ERROR_12(205,"验签失败"),
	ERROR_11(204,"格式错误"),
	ERROR_10(203,"令牌过期"),
	ERROR_9(202,"密码错误"),
	ERROR_8(201,"账号不存在"),
	
	ERROR_7_DATA(105,":dataItem为空"),
	ERROR_6_TN(104,"令牌为空"),
	ERROR_5_SIGN(103,"签名为空"),
	ERROR_4_PASS(102,"密码为空"),
	ERROR_3_CID(101,"账号为空"),
	ERROR_2_PARAM(100,"参数为空"),
	ERROR_1(1,"失败"),
	SUCCESS(0, "成功");
	
	private Integer code;
	private String text;
	
	private JHTResultCodeEnum(Integer code, String text) {
		this.code = code;
		this.text = text;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
}