package com.yiwupay.park.enums;

/**
 * 用户状态
 * 
 * @author Administrator
 *
 */
public enum VehicleOrderStatusEnum {
	NOT_IN("未进场"), NO_FEE("已进场，金额为0"), WAIT_PAY("已进场，金额大于0");

	private final String value;

	public String getValue() {
		return value;
	}

	VehicleOrderStatusEnum(String value) {
		this.value = value;
	}
}
