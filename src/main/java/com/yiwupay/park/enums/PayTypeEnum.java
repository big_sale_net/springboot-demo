package com.yiwupay.park.enums;

public enum PayTypeEnum {
	SENTRY("岗亭收费"),
	CENTRAL("中央收费"),   
	SELF("自助缴费"),
	ZFB("支付宝支付"),
	WX("微信支付"),
	MOBILE("手持设备缴费"), 
	ACCOUNT("账户缴费"),
	OTHER("平台微信支付"),
	FREE("免费"),
	AUTO("自动扣费"),
	APP("app支付"),
	FATHER("子场区费用由父场区代收");
	
	private String text;

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	private PayTypeEnum(String text) {
		this.text = text;
	}
	
}
