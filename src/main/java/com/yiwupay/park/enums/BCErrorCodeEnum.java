package com.yiwupay.park.enums;

public enum BCErrorCodeEnum {
	 
	ERR_100001("100001","获取云时钟错误"),
	ERR_100003("100003","云算费错误"),
	ERR_100004("100004","签名错误"),
	ERR_100006("100006","入场错误"),
	ERR_100007("100007","出场错误"),
	ERR_100008("100008","出场支付异常"),
	ERR_100009("100009","车辆用户类型调整异常"),
	ERR_100016("100016","断网入场上传错误"),
	ERR_100017("100017","断网出场上传错误"),
	ERR_SYS("900001","异常"),
	UNKNOW("0","未知");
	
	private String errorCode;
	private String msg;
	
	private BCErrorCodeEnum(String errorCode, String msg) {
		this.errorCode = errorCode;
		this.msg = msg;
	}
	
	public static BCErrorCodeEnum getEnumByErrorCode(String errorCode) {
		for(BCErrorCodeEnum b :BCErrorCodeEnum.values()) {
			if(b.getErrorCode().equals(errorCode)) {
				return b;
			}
		}
		return UNKNOW;
	}
	
	public static String getMsgByErrorCode(String errorCode) {
		return getEnumByErrorCode(errorCode).getMsg();
	}
	
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	

}
