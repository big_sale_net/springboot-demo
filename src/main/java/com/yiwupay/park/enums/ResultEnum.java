package com.yiwupay.park.enums;

public enum ResultEnum {
	
	ERROR_9(1009,"版本不兼容"),
	ERROR_8(1008,"内部接口调用超时"),
	
	ERROR_7_COST(1007,"支付金额错误"),
	ERROR_6_PAY(1006,"支付失败"),
	ERROR_5_ORDER(1005,"订单过期"),
	ERROR_4_TOKEN(1004,"token无效"),
	ERROR_3_TIMESTAMP(1003,"time时间戳无效"),
	ERROR_2_APPKEY(1002,"appkey无效"),
	ERROR_1(1001,"API参数无效"),
	ERROR_SYS(1000,"系统异常"),
	SUCCESS(0, "接口调用成功，并正常返回");
	
	private Integer code;
	private String text;
	
	private ResultEnum(Integer code, String text) {
		this.code = code;
		this.text = text;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
}