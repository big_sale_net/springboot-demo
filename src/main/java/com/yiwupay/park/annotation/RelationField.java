package com.yiwupay.park.annotation;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;
/**
 * 属性关系注解，只用在成员变量上
 * @author he
 */
@Target({FIELD})
@Retention(RUNTIME)
public @interface RelationField {
	/**
	 * value值对应一个属性名
	 * 即被注解的属性与另一个属性名为value值关联
	 */
	String value();
}
