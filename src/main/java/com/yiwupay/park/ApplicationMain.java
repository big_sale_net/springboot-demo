package com.yiwupay.park;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@SpringBootApplication/*(exclude = {HibernateJpaAutoConfiguration.class, JpaRepositoriesAutoConfiguration.class })*/
@ComponentScan("com.yiwupay.park")
@Configuration
public class ApplicationMain extends SpringBootServletInitializer {
	
	 	@Override
	    protected SpringApplicationBuilder configure(
	            SpringApplicationBuilder application) {
	        return application.sources(ApplicationMain.class);
	    }
	 
	     
	    public static void main(String[] args) {
	        SpringApplication.run(ApplicationMain.class, args);
	    }
}
